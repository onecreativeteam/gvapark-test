<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class GvaparkAdminMailer extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        $header = trans('messages.admin_mail_header');
        
        $data = ['reservationData'=>session('createdReservation'), 'priceListData'=>session('priceList'), 'clientData'=>session('client'), 'header'=>$header];

        return $this->view('mails.reservationAdminMail')->with('data', $data);
    }
}
