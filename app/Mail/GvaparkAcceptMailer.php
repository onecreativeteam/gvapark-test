<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class GvaparkAcceptMailer extends Mailable
{
    use Queueable, SerializesModels;

    private $id;
    private $reservation;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id = null, $reservation = null)
    {
        $this->id = $id;
        $this->reservation = $reservation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        //$header = '';
        $data = ['reservationData'=>$this->reservation];

        return $this->from('info@gvapark.ch', 'GVA Park')
            ->subject('Confirmation de votre réservation ID : ' . $this->id . 'auprès de GVA Park')
            ->view('mails.reservationAcceptMail')
            ->with('data', $data);
    }
}
