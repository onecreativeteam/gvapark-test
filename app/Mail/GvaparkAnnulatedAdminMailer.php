<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class GvaparkAnnulatedAdminMailer extends Mailable
{
    use Queueable, SerializesModels;

    private $reservation;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($reservation = null)
    {
        $this->reservation = $reservation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = ['reservationData'=>$this->reservation];

        return $this->view('mails.reservationAnnulatedAdminMail')->with('data', $data);
    }
}
