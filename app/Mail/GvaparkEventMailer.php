<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class GvaparkEventMailer extends Mailable
{
    use Queueable, SerializesModels;

    private $pdf;
    private $pdfData;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($pdf = null, $pdfData = null)
    {
        $this->pdf = $pdf;
        $this->pdfData = $pdfData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        $data = ['reservation' => $this->pdfData, 'header' => trans('messages.reservation_mail_header')];

        return $this->from('info@gvapark.ch', 'GVA Park')
                ->subject(trans('messages.reservation_mail_subject_1') . $this->pdfData['reservation_id'] . trans('messages.reservation_mail_subject_2'))
                ->view('mails.reservationEventMail')->with('data', $data)
                ->attachData($this->pdf, 'invoice.pdf', [
                    'mime' => 'application/pdf',
                ]);
    }
}
