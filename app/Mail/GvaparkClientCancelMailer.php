<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class GvaparkClientCancelMailer extends Mailable
{
    use Queueable, SerializesModels;

    private $reservation;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($reservation = null)
    {
        $this->reservation = $reservation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = ['reservationData'=>$this->reservation];

        return $this->from('info@gvapark.ch', 'GVA Park')
            ->subject(trans('messages.cancel_mail_subject_1') . $this->reservation->id . trans('messages.cancel_mail_subject_2'))
            ->view('mails.client-cancel-mail')
            ->with('data', $data);
    }
}
