<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class GvaparkMailer extends Mailable
{
    use Queueable, SerializesModels;

    private $id;
    private $pdf;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($pdf = null, $id = null)
    {
        $this->pdf = $pdf;
        $this->id = $id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        $header = trans('messages.reservation_mail_header');
        $data = ['reservationData'=>session('createdReservation'), 'priceListData'=>session('priceList'), 'clientData'=>session('clientData'), 'header'=>$header];

        return $this->from('info@gvapark.ch', 'GVA Park')
                ->subject(trans('messages.reservation_mail_subject_1') . $this->id . trans('messages.reservation_mail_subject_2'))
                ->view('mails.reservationMail')->with('data', $data)
                ->attachData($this->pdf, 'invoice.pdf', [
                    'mime' => 'application/pdf',
                ]);
    }
}
