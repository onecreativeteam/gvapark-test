<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class GvaparkEventAdminMailer extends Mailable
{
    use Queueable, SerializesModels;

    private $pdfData;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($pdfData = null)
    {
        $this->pdfData = $pdfData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        $data = ['reservation'=>$this->pdfData, 'header'=>trans('messages.admin_mail_header')];

        return $this->view('mails.reservationEventAdminMail')->with('data', $data);
    }
}
