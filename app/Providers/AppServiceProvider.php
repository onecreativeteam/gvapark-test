<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Config;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (is_null(session('disable_valet'))) {
            session()->put('disable_valet', 0);
        }

        $admin_options = Config::get('admin_nav');
        
        view()->composer('layouts.parts.nav_admin', function($view) use ($admin_options){
            $view->with('admin_options',$admin_options);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
