<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

use Request;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapHomeRoute();

        $this->mapWebRoutes();

        $this->mapAdminRoutes();

        $this->mapApiRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        if (!array_key_exists(Request::segment(1), config('app.locales'))) {
            $locale = config('app.default_locale');
        } else {
            $locale = Request::segment(1);
        }

        app()->setLocale($locale);

        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace,
            'prefix' => $locale,
        ], function ($router) {
            require base_path('routes/web.php');
        });

        if (!array_key_exists(Request::segment(1), config('app.locales'))) {

            $segments = Request::segments();

            $segments = array_prepend($segments, config('app.locale'));

            redirect(\App::make('url')->to('/').'/'.implode('/', $segments))->send();

        }
    }

    protected function mapHomeRoute()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/home.php');
        });
    }

    /**
     * Define the "admin" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapAdminRoutes()
    {
        if (!array_key_exists(Request::segment(1), config('app.locales'))) {
            $locale = config('app.default_locale');
        } else {
            $locale = app()->getLocale();
        }

        app()->setLocale($locale);
        
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace,
            'prefix' => $locale,
        ], function ($router) {
            require base_path('routes/admin.php');
        });
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::group([
            'middleware' => 'api',
            'namespace' => $this->namespace,
            'prefix' => 'api',
        ], function ($router) {
            require base_path('routes/api.php');
        });
    }
}
