<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        logout as performLogout;
    }

    protected function redirectTo()
    {
        return redirect()->route('home');
    }

    /**
     * Where to redirect users after login / registration.
     */
    protected function authenticated(Request $request, $user)
    {
        if (is_null($user->role)) {
            $this->performLogout($request);
            return redirect()->route('home');
        }

        if ($user->role != 'basic') {
            return redirect()->route('admin.home');
        }

        return redirect()->route('home');
    }

    /**
     * Where to redirect users after login / registration.
     */
    protected function unauthenticated(Request $request, $user)
    {
        return redirect()->route('login');
    }

    /**
     * Perform custom logout.
     */
    public function logout(Request $request)
    {
        $this->performLogout($request);
        
        return redirect()->route('home');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }
}
