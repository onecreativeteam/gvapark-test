<?php
namespace App\Http\Controllers\Auth;

use App\Models\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

use Illuminate\Http\Request;

use Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        Auth::login($user);

        return redirect()->route('home');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'company_name' => 'sometimes|max:100|min:1',
            'phone' => 'sometimes|max:20|min:1',
            'adress' => 'sometimes|max:45|min:1',
            'adress2' => 'sometimes|max:45|min:1',
            'city' => 'sometimes|max:45|min:1',
            'postcode' => 'sometimes|max:20|min:1',
            'description' => '',
            'car_brand' => 'sometimes|max:60|min:1',
            'car_model' => 'sometimes|max:60|min:1',
            'car_registration_number' => 'sometimes|max:60|min:1',
            'flight_number' => 'sometimes|max:60|min:1',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'first_name' => $data['first_name'] ? $data['first_name'] : '',
            'last_name' => $data['last_name'] ? $data['last_name'] : '',
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'company_name' => isset($data['company_name']) ? $data['company_name'] : '',
            'phone' => isset($data['phone']) ? $data['phone'] : '',
            'adress' => isset($data['adress']) ? $data['adress'] : '',
            'adress2' => isset($data['adress2']) ? $data['adress2'] : '',
            'city' => isset($data['city']) ? $data['city'] : '',
            'postcode' => isset($data['postcode']) ? $data['postcode'] : '',
            'description' => isset($data['description']) ? $data['description'] : '',
            'car_brand' => isset($data['car_brand']) ? $data['car_brand'] : '',
            'car_model' => isset($data['car_model']) ? $data['car_model'] : '',
            'car_registration_number' => isset($data['car_registration_number']) ? $data['car_registration_number'] : '',
            'flight_number' => isset($data['flight_number']) ? $data['flight_number'] : '',
        ]);
    }
}
