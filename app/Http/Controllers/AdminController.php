<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use App\Repositories\ReservationRepository;

use App\Http\Requests\UpdateAdminPasswordRequest;
use App\Http\Requests\CreateWorkerRequest;
use App\Http\Requests\UpdateAdminUserRequest;

use Auth;
use Hash;
use Carbon\Carbon;

use Illuminate\Http\Request;

class AdminController extends Controller
{   
    public $userRepo;
    public $reservationRepo;

    public function __construct(
        UserRepository $userRepo,
        ReservationRepository $reservationRepo
    ) {
        $this->userRepo = $userRepo;
        $this->reservationRepo = $reservationRepo;
    }

    public function index()
    {
        if (Auth::user()->role !== 'admin') {
            return redirect()->route('admin.calendar.show', ['type' => 'valet']);
        }

        $today = Carbon::now();

        $data = [];
        $data['startYear']          = $today->year;
        $data['startMonth']         = $today->month;
        $data['startYearMonths']    = $this->reservationRepo->getMonthsByYear($today->year);
        $data['yearsArray']         = $this->reservationRepo->getYears();
        $data['currentMonthProfit'] = $this->reservationRepo->getTotalReservationsProfitByDate($today->year, $today->month);
        $data['profitChartArray']   = $this->reservationRepo->getDayProfitByDate($today->year, $today->month);

        foreach ($data['yearsArray'] as $k => $array) {
            foreach ($array as $k => $year) {
                if ($k != 'allowAnnulation') {
                    $yearsSelect[$year] = $year;
                }
            }
        }

        $simpleMonths = self::getMonthsNames();
        foreach ($data['startYearMonths'] as $k => $array) {
            foreach ($array as $month) {
                if (array_key_exists($month, $simpleMonths)) {
                    $monthsSelect[$month] = $simpleMonths[$month];
                }
            }
        }
        
        return view('admin.dashboard.index')->with('data', $data)->with('yearsSelect', @$yearsSelect)->with('monthsSelect', @$monthsSelect);
    }

    public function users()
    {
        $rolesSelect = [];
        $rolesSelect['worker'] = 'Voiturier';
        $rolesSelect['receptionist'] = 'Receptionist';
        $rolesSelect['admin'] = 'Administrator';

        return view('admin.users')
                ->with('user', Auth::user())
                ->with('rolesSelect', $rolesSelect)
                ->with('users', $this->userRepo->getModel()->where('role', '!=', null)->where('role', '!=', 'basic')->get());
    }

    public function updatePassword(UpdateAdminPasswordRequest $request)
    {
        $data['password'] = bcrypt($request->password);

        $this->userRepo->update(Auth::user()->id, $data);

        return redirect()->back();
    }

    public function createUser(CreateWorkerRequest $request)
    {
        $data['role'] = $request->role;
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['password'] = bcrypt($request->password);

        $this->userRepo->create($data);
        
        return redirect()->back();
    }

    public function editUser($id)
    {
        $user = $this->userRepo->find($id);

        $rolesSelect = [];
        $rolesSelect['worker'] = 'Voiturier';
        $rolesSelect['receptionist'] = 'Receptionist';
        $rolesSelect['admin'] = 'Administrator';

        return view('admin.users-edit')->with('user', $user)->with('rolesSelect', $rolesSelect);
    }

    public function updateUser(UpdateAdminUserRequest $request, $id)
    {
        $data = $request->all();

        if ($data['password'] != '') {
            $data['password'] = Hash::make($data['password']);
        }

        $this->userRepo->update($id, $data);

        return redirect()->route('admin.users');
    }

    public function deleteWorkerUser($id)
    {
        $this->userRepo->delete($id);
        
        return redirect()->back();
    }

    public static function getMonthsNames()
    {
        return array(
            '1' => 'January',
            '2' => 'February',
            '3' => 'March',
            '4' => 'April',
            '5' => 'May',
            '6' => 'June',
            '7' => 'July',
            '8' => 'August',
            '9' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December'
        );
    }
}
