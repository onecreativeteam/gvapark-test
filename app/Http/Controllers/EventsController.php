<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\EventReservationRequest;

use App\Repositories\EventRepository;
use App\Repositories\EventReservationRepository;

use App\Services\MailVerificationService;

use Response;
use Config;
use PDF;

class EventsController extends Controller
{
    public $eventsRepo;
    public $eventReservationRepo;

    public function __construct(
        EventRepository $eventsRepo,
        EventReservationRepository $eventReservationRepo
    ) {
        $this->eventsRepo = $eventsRepo;
        $this->eventReservationRepo = $eventReservationRepo;
    }

    public function index()
    {
        return view('events-index')->with('events', $this->eventsRepo->getTranslatedWhereWith(['is_active', '=', '1']));
    }

    public function inner($slug)
    {
        $event = $this->eventsRepo->getTranslatedWhereWith(['slug', '=', $slug])->first();

        return view('events-inner')
                ->with('event', $event)
                ->with('params', self::paymentParams($event))
                ->with('createdEventReservation', @session('createdEventReservation'));
    }

    public function postAjax($event_id, EventReservationRequest $request)
    {
        $event = $this->eventsRepo->find($event_id);

        if (count($event->completed) >= $event->max_reservation_count) {

            return response()->json([
                'status'        => 'error',
                'responseJSON'  => ['error' => 'Not Allowed to make reservation.']
            ]);

        }

        $data = $this->eventReservationRepo->prepareReservationData($request, $event);

        if (is_null(session('createdEventReservation'))) {
            $createdEventReservation    = $this->eventReservationRepo->create($data);

            session()->put('createdEventReservation', $createdEventReservation);
        } else {
            $createdEventReservation    = $this->eventReservationRepo->update(session('createdEventReservation')->id, $data);

            session()->put('createdEventReservation', $createdEventReservation);
        }

        session()->put('started_reservation_for_event', $event_id);
        session()->put('started_event_reservation', $createdEventReservation->id);

        return response()->json([
            'status' => 'success',
            'object' => $createdEventReservation
        ]);
    }

    public function thankYou()
    {
        $reservation    = session('createdEventReservation');

        $pdfData        = $this->eventReservationRepo->preparePdfData($reservation);
        $pdfStream      = PDF::loadView('facture-events', $pdfData)->stream();

        $sendMail       = MailVerificationService::sendReservationEventMail($reservation->email, $pdfStream, $pdfData);
        $sendMailAdmin  = MailVerificationService::sendReservationEventAdminMail($pdfData);

        session()->flush();
        session()->regenerate();

        return view('events-thank')->with('reservation', $reservation);
    }

    public function generateInvoice($id = null)
    {
        $reservation = $this->eventReservationRepo->find($id);
        $data        = $this->eventReservationRepo->preparePdfData($reservation);
        $pdf         = PDF::loadView('facture-events', $data);
        
        return $pdf->download('facture.pdf');
    }

    public function paymentParams($event)
    {
        $data               = [];
        $data['MERCHANT']   = '3000012256';
        $data['AMOUNT']     = str_replace('.', '', number_format($event->price, 2));
        $data['CURRENCY']   = 'CHF';
        $data['SIGN']       = '1502110835215';
        $data['REFNO']      = uniqid();
        $data['LANG']       = app()->getLocale();

        session()->put('event_payment_id', $data['REFNO']);

        return $data;
    }
}
