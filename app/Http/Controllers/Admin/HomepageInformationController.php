<?php
namespace App\Http\Controllers\Admin;

use App\Repositories\HomepageInformationRepository as Homepage;
use App\Repositories\ReservationRepository as Reservation;
use App\Repositories\SystemOptionsRepository as SystemOptions;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ReservationController;
use Auth;
use DB;
use DateTime;
use App\Services\FileService;
use Mail;
use Response;
use View;
use Carbon\Carbon;

class HomepageInformationController extends Controller
{

    public $homepageModel;
    public $reservationModel;
    public $systemOptionsModel;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->homepageModel = app(Homepage::class);
        $this->reservationModel = app(Reservation::class);
        $this->systemOptionsModel = app(SystemOptions::class);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->homepageModel->first();

        return view('admin.homepage.index')->with('data', $data);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $homepageInfo = $this->homepageModel;

        if (is_null($homepageInfo->first())) $homepageInfo->create($data);
        else $homepageInfo->updateFirst($data);

        return redirect()->route('admin.homepage.show');
    }

    public function indexCalendar()
    {
        $data = $this->reservationModel->getAllCalendar();
        $array = [];
        $array2 = [];
        $x = 1;

        foreach ($data as $k => $value) {

            $arrivalAt = Carbon::parse($value['date_of_arrival'])->format('Y-m-d');
            $departureAt = Carbon::parse($value['date_of_departure'])->format('Y-m-d');

            $item = $value;
            $item['start_date'] = $value['date_of_arrival'];
            $array[] = $item;

            // if ($arrivalAt == $departureAt) {

            //     continue;

            // } else {

                $item['start_date'] = $value['date_of_departure'];
                $item['is_cloned'] = true;
                $array2[] = $item;

            // }
            
        }

        $data = array_merge($array2, $array);

        $countParking = self::getParkingOccupationCount();
        $systemOptionsParkingCount = $this->systemOptionsModel->first()->parking_spots_count;

        if (!empty($systemOptionsParkingCount)) $nonOccupiedParking = $systemOptionsParkingCount - $countParking;
        else $nonOccupiedParking = 500;

        //return response(view('admin.calendar.index', array('data'=>$data, 'countParking'=>$countParking, 'nonOccupiedParking'=>$nonOccupiedParking)),200);
        return View::make('admin.calendar.index')->with(array('data'=>$data, 'countParking'=>$countParking, 'nonOccupiedParking'=>$nonOccupiedParking));
    }

    public function getParkingOccupationCount($now = null)
    {
        $now = new DateTime('now');
        $now = $now->format('Y-m-d H:i:s');

        $parkingOccupationCount = DB::table('reservations')
            ->whereDate('date_of_departure', '>', $now)
            ->whereDate('date_of_arrival', '<', $now)
            ->where('is_arrived', '=', 1)
            ->count();

        return $parkingOccupationCount;
    }

}
