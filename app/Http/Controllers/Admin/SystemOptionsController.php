<?php
namespace App\Http\Controllers\Admin;

use App\Http\Requests\SystemOptionsRequest;
use App\Repositories\ParkPlaceRepository;
use App\Repositories\SystemOptionRepository;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

class SystemOptionsController extends Controller
{
    public $systemOptionRepo;
    /**
     * @var ParkPlaceRepository
     */
    private $parkPlaceRepository;

    /**
     * @param SystemOptionRepository $systemOptionRepo
     * @param ParkPlaceRepository    $parkPlaceRepository
     */
    public function __construct(
        SystemOptionRepository $systemOptionRepo,
        ParkPlaceRepository $parkPlaceRepository
    ) {
        $this->systemOptionRepo = $systemOptionRepo;
        $this->parkPlaceRepository = $parkPlaceRepository;
    }

    public function create()
    {
        $data = $this->systemOptionRepo->first();

        return view('admin.system.index')->with('data', $data);
    }

	/**
     * @param SystemOptionsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SystemOptionsRequest $request)
    {
        if (!isset($request->reservation_have_options)) {
            $request->request->add(['reservation_have_options' => 0]);
        } else {
            $request->request->add(['reservation_have_options' => 1]);
        }
        
        $data = $request->except(['_token']);
        if (is_null($options = $this->systemOptionRepo->first())) {
            $this->systemOptionRepo->create($data);
            $this->parkPlaceRepository->createMultiple($data['parking_spots_count']);
        } else {
            if (count($this->parkPlaceRepository->all()) < 1) {
                $this->parkPlaceRepository->createMultiple($data['parking_spots_count']);
            } else if ($options->parking_spots_count < $data['parking_spots_count']) {
                $this->parkPlaceRepository->createMultiple($data['parking_spots_count'] - $options->parking_spots_count);
            } elseif ($options->parking_spots_count > $data['parking_spots_count']) {
                $this->parkPlaceRepository->deleteMultiple(range($data['parking_spots_count'] + 1, $options->parking_spots_count));
            }

            $this->systemOptionRepo->updateRecord($options, $data);
        }

        return redirect()->route('admin.system.show');
    }
}
