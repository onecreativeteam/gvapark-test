<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Repositories\ReservationRepository;
use App\Repositories\EventReservationRepository;
use App\Repositories\EventRepository;

use Illuminate\Http\Request;

use Datatables;

class DataTablesController extends Controller
{
    private $reservationRepo;
    private $eventReservationRepo;
    private $eventsRepo;

    public function __construct(
        ReservationRepository $reservationRepo,
        EventReservationRepository $eventReservationRepo,
        EventRepository $eventsRepo
    ) {
        $this->reservationRepo = $reservationRepo;
        $this->eventReservationRepo = $eventReservationRepo;
        $this->eventsRepo = $eventsRepo;
    }

    public function reservationDataTable()
    {
        $reservations = $this->reservationRepo->getAllOrderedDT();
        $cancelTxt = "'Are you sure ?'";

        return Datatables::of($reservations)
        ->add_column('image',
            '<a href="/{{app()->getLocale()}}/admin/reservation/gallery/view/{{$id}}" target="_blank" class="btn {{ $has_gallery == "1" ? "btn-success" : "btn-info" }} btn-xs">{{ $has_gallery == "1" ? "View" : "No" }}</a>'
        )
        ->addColumn('cancel', function($reservation) use ($cancelTxt) {
            if (!is_null($reservation->payment)) {
                if ($reservation->is_canceled == '0') {
                    return '<a href="/'.app()->getLocale().'/admin/transaction-cancel/'.$reservation->id.'" target="_blank" onclick="return confirm('.$cancelTxt.');" class="btn cancelBtn btn-danger btn-xs">Cancel</a>';
                }
            }
        })
        ->add_column('operations',
            '<a class="btn btn-primary btn-block btn-xs" href="/{{app()->getLocale()}}/admin/reservations/{{$id}}/edit">Edit</a>
             <a class="btn btn-warning btn-block btn-xs" href="/{{app()->getLocale()}}/generate-invoice/{{$id}}">PDF</a>'
        )->make(true);
    }

    public function eventReservationDataTable()
    {
        $cancelTxt = "'Are you sure ?'";

        return Datatables::of($this->eventReservationRepo->getAllWithTranslation())
        ->addColumn('cancel', function($reservation) use ($cancelTxt) {
            if ($reservation->payment_status == 'completed') {
                    return '<a href="/'.app()->getLocale().'/admin/event-transaction-cancel/'.$reservation->id.'" target="_blank" onclick="return confirm('.$cancelTxt.');"  class="btn cancelBtn btn-danger btn-xs">Cancel</a>';
            }
        })
        ->addColumn('event', function($reservation) {
            return $reservation->event->name;
        })->make(true);
    }

    public function eventsDataTable()
    {
        return Datatables::of($this->eventsRepo->getAllWithTranslation())->add_column('operations',
            '<a class="btn btn-sm blue pull-left" href="events/{{$id}}/edit"><i class="fa fa-edit"></i> Edit</a>
            <form class="pull-left" method="POST" action="events/{{$id}}" accept-charset="UTF-8">
                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                <input name="_method" type="hidden" value="DELETE">
                <button class="btn btn-sm red deleteRowBttn" type="submit"><i class="fa fa-trash-o"></i> Delete</button>
            </form>')->make(true);
    }
}
