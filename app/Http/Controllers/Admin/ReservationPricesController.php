<?php
namespace App\Http\Controllers\Admin;

use App\Repositories\ReservationPriceRepository;
use App\Repositories\SystemOptionRepository;

use App\Http\Requests\ReservationPriceRequest;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class ReservationPricesController extends Controller
{
    private $reservationPriceRepo;
    private $systemOptionRepo;

    public function __construct(
        ReservationPriceRepository $reservationPriceRepo,
        SystemOptionRepository $systemOptionRepo
    ) {
        $this->reservationPriceRepo = $reservationPriceRepo;
        $this->systemOptionRepo = $systemOptionRepo;
    }

    public function index($type)
    {
        return view('admin.prices.index')
                ->with('type', $type)
                ->with('prices', $this->reservationPriceRepo->getPricesByType($type))
                ->with('pricesCount', $this->systemOptionRepo->first()->reservation_prices_count);
    }

    public function store(ReservationPriceRequest $request)
    {
        $existingPrices = $this->reservationPriceRepo->getPricesByType($request->type);

        if (count($existingPrices) > 0) {
            $this->reservationPriceRepo->getModel()->whereIn('id', $existingPrices->pluck('id'))->delete();
        }

        $this->reservationPriceRepo->savePrices($request);

        return redirect()->route('admin.prices.show', [$request->type]);
    }
}
