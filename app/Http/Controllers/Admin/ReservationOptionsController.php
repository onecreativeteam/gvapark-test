<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Repositories\ReservationOptionRepository;

use Config;

class ReservationOptionsController extends Controller
{
    public $optionsRepo;

    public function __construct(
        ReservationOptionRepository $optionsRepo
    ) {
        $this->optionsRepo = $optionsRepo;
    }

    public function index()
    {
        return view('admin.reservation_options.index')
                ->with('options', $this->optionsRepo->getAllWithTranslation());
    }

    public function create()
    {
        return view('admin.reservation_options.create')
                ->with('optionTypes', Config::get('option_types'))
                ->with('supportedLocales', Config::get('app.locales'));
    }

    public function store(Request $request)
    {
        $this->optionsRepo->createWithTranslations($request, 'name');

        return redirect()->route('admin.list.options');
    }

    public function edit($id)
    {
        return view('admin.reservation_options.edit')
                ->with('optionTypes', Config::get('option_types'))
                ->with('supportedLocales', Config::get('app.locales'))
                ->with('option', $this->optionsRepo->find($id));
    }

    public function update($id, Request $request)
    {
        $this->optionsRepo->updateWithTranslations($request, $id, 'name');

        return redirect()->route('admin.list.options');
    }

    public function destroy($id)
    {
        $this->optionsRepo->delete($id);

        return redirect()->route('admin.list.options');
    }
}
