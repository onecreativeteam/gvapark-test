<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\UpdateReservationRequest;
use App\Repositories\ParkPlaceRepository;
use App\Repositories\ReservationRepository;
use App\Repositories\UserRepository;
use App\Repositories\ReservationGalleryRepository;
use App\Repositories\PromoCodeRepository;
use App\Repositories\SystemOptionRepository;

use Illuminate\Http\Request;

use App\Http\Requests\ReservationRequest;
use App\Http\Requests\AdminReservationRequest;
use App\Http\Requests\PostReservationRequest;
use App\Http\Requests\PromoCodeRequest;
use App\Http\Requests\ClientLoginRequest;
use App\Http\Requests\UpdateClientRequest;

use App\Services\MailVerificationService;
use App\Services\PricelistService;
use App\Services\PaymentService;

use App\Mail\GvaparkAdminMailer;

use Auth;
use Hash;
use Illuminate\Http\Response;
use Validator;
use Mail;
use DateTime;
use Config;
use Session;
use Redirect;
use Carbon\Carbon;
use PDF;

class ReservationController extends Controller
{
    public $galleryPath;
    
    public $reservationRepo;
    public $userRepo;
    public $reservationGalleryRepo;
    public $promoCodeRepo;
    public $systemOptionRepo;
    public $priceListService;
    /**
     * @var ParkPlaceRepository
     */
    private $parkPlaceRepository;

    /**
     * @param ReservationRepository        $reservationRepo
     * @param UserRepository               $userRepo
     * @param ReservationGalleryRepository $reservationGalleryRepo
     * @param PromoCodeRepository          $promoCodeRepo
     * @param SystemOptionRepository       $systemOptionRepo
     * @param PricelistService             $priceListService
     * @param ParkPlaceRepository          $parkPlaceRepository
     */
    public function __construct(
        ReservationRepository $reservationRepo,
        UserRepository $userRepo,
        ReservationGalleryRepository $reservationGalleryRepo,
        PromoCodeRepository $promoCodeRepo,
        SystemOptionRepository $systemOptionRepo,
        PricelistService $priceListService,
        ParkPlaceRepository $parkPlaceRepository
    ) {
        $this->galleryPath = 'uploads'. DIRECTORY_SEPARATOR . 'reservations' . DIRECTORY_SEPARATOR .'gallery' .DIRECTORY_SEPARATOR;

        $this->reservationRepo = $reservationRepo;
        $this->userRepo = $userRepo;
        $this->reservationGalleryRepo = $reservationGalleryRepo;
        $this->promoCodeRepo = $promoCodeRepo;
        $this->systemOptionRepo = $systemOptionRepo;
        $this->priceListService = $priceListService;
        $this->parkPlaceRepository = $parkPlaceRepository;
    }

    public function setArrived(Request $request)
    {
        $id     = $request->obj_id;

        $obj    = $this->reservationRepo->find($id)->toArray();
        $place  = $this->parkPlaceRepository->findByName($request->get('park_place'));
        $errors = [];

        if ($request->park_place != '') {

            if ($request->park_place == '0') {

                $obj['park_place_id'] = null;
                
            } else {

                if (!is_null($place)) {
                    $obj['park_place_id'] = $place->name;
                } else {
                    $errors = ['Park place is not existing.'];
                }

            }

        }

        $now = Carbon::now()->format('Y-m-d H:i:s');

        if ($obj['is_arrived'] == 0) {

            $obj['is_arrived'] = 1;
            $obj['arrived_by_user'] = Auth::user()->id;
            $obj['arrived_at'] = $now;

            if ($this->reservationRepo->checkMailConnection() === true) {
                $sendMail = MailVerificationService::sendReservationAcceptMail($obj['email'], $id, $obj);
            }
        }

        foreach ($request->all() as $k => $picture) {

            if (is_file($picture))
            {
                if ($picture->isValid()) {

                    $imageName = str_replace(' ', '', $id.'_'.time().'_'.$picture->getClientOriginalName());
                    $picture->move(public_path($this->galleryPath), $imageName);
                    $picture_path = $this->galleryPath . $imageName;
                    
                    $data['reservations_id'] = $id;
                    $data['picture'] = $picture_path;

                    $this->reservationGalleryRepo->create($data);

                    $obj['has_gallery'] = 1;
                }

            }
            
        }

        $reservation = $this->reservationRepo->update($id, $obj);

        if (count($errors)) {
            return response(['errors'=>$errors], 422);
        } else {
            return response(['response'=>$reservation , 'errors'=>$errors], 200);
        }
    }

    public function removeParkPlacesByTime()
    {
        $this->reservationRepo->clearParkPlaces();
    }

    public function setReturned($id)
    {
        $obj = $this->reservationRepo->find($id);
        
        $reservation = $obj->toArray();

        $now = Carbon::now()->format('Y-m-d H:i:s');

        $reservation['is_returned']         = 1;
        $reservation['returned_by_user']    = Auth::user()->id;
        $reservation['returned_at']         = $now;

        if (!is_null($obj->parkPlace)) {
            $reservation['park_place_id']   = null;
        }

        $updated = $this->reservationRepo->update($id, $reservation);

        try {
            $this->reservationRepo->sendReviewMail($updated);
        } catch (\Exception $e) {
            // ...
        }

        return redirect()->back();
    }

    public function viewGallery($id)
    {
        $data = $this->reservationGalleryRepo->getPictures($id);

        return view('admin.reservations.gallery')->with('data', $data);
    }

    public function changeReservationDates(Request $request)
    {
        $data = $request->all();

        $id = $data['cd_obj_id'];

        $obj = $this->reservationRepo->find($id)->toArray();

        $dateArrival = Carbon::parse($data['obj_start_date'])->format('Y-m-d H:i:s');
        $dateDepartured = Carbon::parse($data['obj_end_date'])->format('Y-m-d H:i:s');

        $obj['date_of_arrival'] = $dateArrival;
        $obj['date_of_departure'] = $dateDepartured;

        $this->reservationRepo->update($id, $obj);

        return redirect()->route('admin.calendar.show');
    }

    public function reservationsList()
    {
        $reservations = $this->reservationRepo->getAllOrdered();

        return view('admin.reservations.index')->with('reservations', $reservations);
    }

    public function editReservation($id)
    {
        $reservation = $this->reservationRepo->find($id);

        $optionsSelect = Config::get('reservation_options');
        $statusesSelect = Config::get('reservation_statuses');

        return view('admin.reservations.edit')->with('data', $reservation)->with('client', @$reservation->user)->with('optionsSelect', $optionsSelect)->with('statusesSelect', $statusesSelect);
    }

    public function updateReservation($id, Request $request)
    {
        $data = $request->all();

        $data['is_updated_from_admin'] = 1;

        $priceList = $this->priceListService->getPrices($data);

        $reservation = $this->reservationRepo->find($id)->toArray();

        $data['changed_price_to_charge']        = $priceList['absolute_total'];

        if(isset($data['use_admin_price'])) {
            $data['changed_price_to_charge']    = $data['price_to_charge_admin'];
        }

        $data['reservation_option']             = $data['options'];

        $data['days_to_stay']                   = $priceList['daysToCharge'];
        $data['vat']                            = $priceList['totalVAT'];
        
        $this->reservationRepo->update($id, $data);

        $this->userRepo->update($reservation['user_id'], $data);

        return redirect()->route('admin.reservation.list');
    }

    public function newAdminReservation()
    {
        return view('admin.reservations.new');
    }

    public function postNewAdminReservation(AdminReservationRequest $request)
    {
        $validator = Validator::make($request->all(), $request->rules());

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withInput($request)
                ->withErrors($validator);
        }

        $data = $request->all();

        $client['first_name']               = $data['first_name'];
        $client['last_name']                = $data['last_name'];
        $client['company_name']             = $data['company_name'];
        $client['email']                    = $data['email'];
        $client['password']                 = Hash::make('123456');
        $client['phone']                    = $data['phone'];
        $client['adress']                   = $data['adress'];
        $client['adress2']                  = $data['adress2'];
        $client['city']                     = $data['city'];
        $client['postcode']                 = $data['postcode'];
        $client['description']              = $data['description'];
        $client['car_brand']                = $data['car_brand'];
        $client['car_model']                = $data['car_model'];
        $client['car_registration_number']  = $data['car_registration_number'];
        $client['flight_number']            = $data['flight_number'];
        $client['role']                     = 'basic';

        $createdClient = $this->userRepo->create($client);

        $reservation['date_of_arrival']     = Carbon::parse($request->date_of_arrival.' '.$request->hour_of_arrival.':'.$request->minute_of_arrival.':00')
                                                    ->format('Y-m-d H:i:s');
        $reservation['date_of_departure']   = Carbon::parse($request->date_of_departure .' '.$request->hour_of_departure.':'.$request->minute_of_departure.':00')
                                                    ->format('Y-m-d H:i:s');

        $reservation['full_date_of_arrival']    = Carbon::parse($request->date_of_arrival.' '.$request->hour_of_arrival.':'.$request->minute_of_arrival.':00')
                                                    ->format('Y-m-d H:i:s');
        $reservation['full_date_of_departure']  = Carbon::parse($request->date_of_departure .' '.$request->hour_of_departure.':'.$request->minute_of_departure.':00')
                                                    ->format('Y-m-d H:i:s');

        $reservation['date_of_reservation']     = Carbon::now()
                                                    ->format('Y-m-d H:i:s');

        $reservation['reservation_type']        = $data['reservation_type'];

        $priceList = $this->priceListService->getPrices($reservation);
        
        $reservation['user_id']              = $createdClient->id;
        $reservation['client_first_name']    = $data['first_name'];
        $reservation['client_last_name']     = $data['last_name'];
        $reservation['email']                = $data['email'];
        $reservation['days_to_stay']         = $priceList['daysToCharge'];
        $reservation['price_to_charge']      = $priceList['absolute_total'];
        $reservation['vat']                  = $priceList['totalVAT'];
        $reservation['payment_method']       = 'admin-reservation';
        $reservation['system_payment_id']    = uniqid();
        $reservation['applied_promo']        = 0;                                    
        $reservation['is_arrived']           = 0;
        $reservation['payment_status']       = 'completed';
        
        $createdReservation = $this->reservationRepo->create($reservation);

        return redirect()->route('admin.reservation.list');
    }

    public function deleteReservation(Request $request, $id)
    {
        if (Auth::user()->role !== 'admin') {
            return redirect()->back()->withErrors(['Erreur', 'Vous n\'avez pas l\'autorisation pour cette opération.']);
        }

        $this->reservationRepo->delete($id);
        
        return redirect()->route('admin.reservation.list');
    }

    public function deleteCalendarReservation(Request $request)
    {
        if (Auth::user()->role !== 'admin') {
            return redirect()->back()->withErrors(['Erreur', 'Vous n\'avez pas l\'autorisation pour cette opération.']);
        }

        $this->reservationRepo->delete($request->id);
        
        return redirect()->route('admin.reservation.list');
    }

}
