<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\Repositories\HomepageRepository;
use App\Repositories\ReservationRepository;
use App\Repositories\SystemOptionRepository;

use Auth;
use DB;
use DateTime;
use App\Services\FileService;
use Mail;
use Response;
use View;
use Carbon\Carbon;
use Config;

class HomepageController extends Controller
{
    public $homepageRepo;
    public $reservationRepo;
    public $systemOptionRepo;

    public function __construct(
        HomepageRepository $homepageRepo,
        ReservationRepository $reservationRepo,
        SystemOptionRepository $systemOptionRepo
    ) {
        $this->homepageRepo = $homepageRepo;
        $this->reservationRepo = $reservationRepo;
        $this->systemOptionRepo = $systemOptionRepo;
    }

    public function create()
    {
        $data = $this->homepageRepo->first();

        return view('admin.homepage.index')
                ->with('data', @$data)
                ->with('supportedLocales', Config::get('app.locales'));
    }

    public function store(Request $request)
    {
        if (is_null($this->homepageRepo->first())) {
            $this->homepageRepo->createWithTranslations($request, 'title');
        } else {
            $this->homepageRepo->updateWithTranslations($request, $this->homepageRepo->first()->id, 'title');
        }
        
        session()->flash('success', 'Success.');

        return redirect()->route('admin.homepage.show');
    }

    public function indexCalendar($type = 'valet')
    {
        $data   = $this->reservationRepo->getAllCalendar($type);
        $array  = [];
        $array2 = [];
        $x      = 1;

        $now    = new DateTime('now');
        $now    = $now->format('Y-m-d');

        foreach ($data as $k => $value) {
            $arrivalAt      = Carbon::parse($value['date_of_arrival'])->format('Y-m-d');
            $departureAt    = Carbon::parse($value['date_of_departure'])->format('Y-m-d');

            $item               = $value;
            $item['start_date'] = $value['date_of_arrival'];
            $array[]            = $item;

            if ( ($value['is_arrived'] != '1') && ($value['date_of_arrival'] < $now) ) {
                continue;
            } else {
                $item['start_date'] = $value['date_of_departure'];
                $item['is_cloned']  = true;
                $array2[]           = $item;
            }
        }

        $data = array_merge($array2, $array);

        if ($type == 'navet') {
            // broi vsichki - broi valet - broi zaeti za navet
            $systemOptionsParkingCount = $this->systemOptionRepo->first()->parking_spots_count; //broi za vsichki
            $valetParkingCount = $this->systemOptionRepo->first()->valet_max_count; //broi za valet
            $countParking = self::getParkingOccupationCount(null, $type); // broi zaeti za navet

            $nonOccupiedParking = $systemOptionsParkingCount - $valetParkingCount - $countParking;

        } elseif ($type == 'valet') {
            // broi za valet - zaeti za valet
            $valetParkingCount = $this->systemOptionRepo->first()->valet_max_count; // broi za valet
            $countParking = self::getParkingOccupationCount(null, $type); // zaeti za valet

            $nonOccupiedParking = $valetParkingCount - $countParking;
        }

        return View::make('admin.calendar.index')
                    ->with(array('data'=>$data, 'countParking'=>$countParking, 'type'=>$type, 'nonOccupiedParking'=>$nonOccupiedParking));
    }

    public function getParkingOccupationCount($now = null, $type)
    {
        $now = new DateTime('now');
        $now = $now->format('Y-m-d');

        $parkingOccupationCount = DB::table('reservations')
            ->whereDate('date_of_departure', '>=', $now)
            ->whereDate('date_of_arrival', '<=', $now)
            ->where('reservation_type', '=', $type)
            ->where('payment_status', '=', 'completed')
            ->where('is_returned', '!=', '1')
            ->count();

        return $parkingOccupationCount;
    }

}
