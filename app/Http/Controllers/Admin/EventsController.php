<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Repositories\EventRepository;

use Response;

use Config;

class EventsController extends Controller
{
    public $eventsRepo;

    public function __construct(
        EventRepository $eventsRepo
    ) {
        $this->eventsRepo = $eventsRepo;
    }

    public function index()
    {
        return view('admin.events.index')->with('events', $this->eventsRepo->getAllWithTranslation());
    }

    public function create()
    {
        return view('admin.events.create')
                    ->with('supportedLocales', Config::get('app.locales'));
    }

    public function store(Request $request)
    {
        if (!isset($request->is_active)) $request->request->add(['is_active' => 0]);

        $this->eventsRepo->createWithTranslations($request, 'name');

        return redirect()->route('admin.list.events');
    }

    public function edit($id)
    {
        return view('admin.events.edit')
                ->with('event', $this->eventsRepo->find($id))
                ->with('supportedLocales', Config::get('app.locales'));
    }

    public function update(Request $request, $id)
    {
        if (!isset($request->is_active)) $request->request->add(['is_active' => 0]);
        
        $this->eventsRepo->updateWithTranslations($request, $id, 'name');

        return redirect()->route('admin.list.events');
    }

    public function destroy($id)
    {
        $this->eventsRepo->delete($id);

        return redirect()->route('admin.list.events');
    }

    public function listReservations()
    {
        return view('admin.events.reservations');
    }
}
