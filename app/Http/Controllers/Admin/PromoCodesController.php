<?php
namespace App\Http\Controllers\Admin;

use App\Repositories\PromoCodeRepository;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use Response;
use Carbon\Carbon;

class PromoCodesController extends Controller
{
    public $promoCodeRepo;

    public function __construct(
        PromoCodeRepository $promoCodeRepo
    ) {
        $this->promoCodeRepo = $promoCodeRepo;
    }

    public function index()
    {
        return view('admin.promo_codes.index')->with('promoCodes', $this->promoCodeRepo->getAll());
    }

    public function create()
    {
        return view('admin.promo_codes.create');
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $this->promoCodeRepo->create($data);

        return redirect()->route('admin.list.promo');
    }

    public function edit($id)
    {
        return view('admin.promo_codes.edit')->with('data', $this->promoCodeRepo->find($id));
    }

    public function update($id, Request $request)
    {
        $this->promoCodeRepo->update($id, $request->all());

        return redirect()->route('admin.list.promo');
    }

    public function destroy($id)
    {
        $this->promoCodeRepo->delete($id);

        return redirect()->route('admin.list.promo');
    }
}
