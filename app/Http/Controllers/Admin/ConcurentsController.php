<?php
namespace App\Http\Controllers\Admin;

use App\Repositories\ConcurentRepository;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use Config;

class ConcurentsController extends Controller
{
    public $concurentRepo;

    public function __construct(
        ConcurentRepository $concurentRepo
    ) {
        $this->concurentRepo = $concurentRepo;
    }

    public function index()
    {
        return view('admin.concurents.index')
                ->with('concurents', $this->concurentRepo->getAllWithTranslation('is_main','desc'));
    }

    public function create()
    {
        return view('admin.concurents.create')
                ->with('supportedLocales', Config::get('app.locales'));
    }

    public function store(Request $request)
    {
        $this->concurentRepo->createWithTranslations($request, 'name');

        return redirect()->route('admin.list.concurents');
    }

    public function edit($id)
    {
        return view('admin.concurents.edit')
                ->with('concurent', $this->concurentRepo->find($id))
                ->with('supportedLocales', Config::get('app.locales'));
    }

    public function update($id, Request $request)
    {
        $this->concurentRepo->updateWithTranslations($request, $id, 'name');

        return redirect()->route('admin.list.concurents');
    }

    public function destroy($id)
    {
        $this->concurentRepo->delete($id);

        return redirect()->route('admin.list.concurents');
    }
}
