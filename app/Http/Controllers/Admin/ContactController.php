<?php
namespace App\Http\Controllers\Admin;

use App\Repositories\ContactRepository;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Mail;
use Response;
use Carbon\Carbon;

class ContactController extends Controller
{
    public $contactRepo;

    public function __construct(
        ContactRepository $contactRepo
    ) {
        $this->contactRepo = $contactRepo;
    }

    public function create()
    {
        $data = $this->contactRepo->first();

        return view('admin.contact.index')->with('data', $data);
    }

    public function store(Request $request)
    {
        $data = $request->except(['_token']);

        if (is_null($this->contactRepo->first())) $this->contactRepo->create($data);
        else $this->contactRepo->updateRecord($this->contactRepo->first(), $data);

        return redirect()->route('admin.contact.show');
    }
}
