<?php
namespace App\Http\Controllers\Admin;

use App\Repositories\SystemOptionRepository as SystemOptions;
use App\Repositories\ReservationRepository as Reservation;

use App\Http\Controllers\Controller;
use Response;
use Validator;
use DB;
use Carbon\Carbon;
use DateTime;
use Session;
use Auth;
use Datatables;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public $systemOptionsModel;
    public $reservationModel;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->systemOptionsModel = app(SystemOptions::class);
        $this->reservationModel = app(Reservation::class);
    }

    public function getParkingOccupationCount(Request $request)
    {
        $date = $request->date;
        $date = Carbon::parse($date)->format('Y-m-d');
        $type = $request->type;

        $parkingOccupationCount = DB::table('reservations')
            ->whereDate('date_of_departure', '>=', $date)
            ->whereDate('date_of_arrival', '<=', $date)
            ->where('reservation_type', '=', $type)
            ->where('payment_status', '=', 'completed')
            ->where('is_returned', '!=', '1')
            ->count();

        if ($type == 'navet') {

            $systemOptionsParkingCount = $this->systemOptionsModel->first()->parking_spots_count;
            $valetParkingCount = $this->systemOptionsModel->first()->valet_max_count;
            $countParking = $parkingOccupationCount;

            $nonOccupiedParking = $systemOptionsParkingCount - $valetParkingCount - $countParking;

        } elseif ($type == 'valet') {

            $valetParkingCount = $this->systemOptionsModel->first()->valet_max_count;
            $countParking = $parkingOccupationCount;

            $nonOccupiedParking = $valetParkingCount - $countParking;
        }

        $systemOptionsParkingCount = !empty($this->systemOptionsModel->first()) ? $this->systemOptionsModel->first()->parking_spots_count : 500;

        return ['free_spots' => $nonOccupiedParking, 'reservations' => $parkingOccupationCount];
    }

    public function getProfitByDate(Request $request)
    {
        $year = $request->year;
        $month = $request->month;

        if ($month > 0) {
            $data = $this->reservationModel->getDayProfitByDate($year, $month);
        } else {
            $data = $this->reservationModel->getMonthProfitByYear($year);
        }

        return ['data'=>$data, 'total'=>$this->reservationModel->getTotalReservationsProfitByDate($year, $month)];
    }

    public function getMonthsByYear(Request $request)
    {
        $year = $request->year;

        $data = $this->reservationModel->getMonthsByYear($year);

        $simpleMonths = self::getMonthsNames();

        $monthsSelect[0] = 'All';
        foreach ($data as $k => $array) {
            foreach ($array as $month) {
                if (array_key_exists($month, $simpleMonths)) {
                    $monthsSelect[$month] = $simpleMonths[$month];
                }
            }
        }

        return $monthsSelect;
    }

    public static function getMonthsNames()
    {
        return array(
            '0' => 'All',
            '1' => 'January',
            '2' => 'February',
            '3' => 'March',
            '4' => 'April',
            '5' => 'May',
            '6' => 'June',
            '7' => 'July',
            '8' => 'August',
            '9' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December'
        );
    }
}
