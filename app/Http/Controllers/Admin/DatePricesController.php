<?php
namespace App\Http\Controllers\Admin;

use App\Repositories\DatePriceRepository;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

class DatePricesController extends Controller
{
    public $datePriceRepo;

    public function __construct(
        DatePriceRepository $datePriceRepo
    ) {
        $this->datePriceRepo = $datePriceRepo;
    }

    public function index()
    {
        return view('admin.date_prices.index');
    }

    public function create()
    {
        return view('admin.date_prices.create');
    }

    public function store(Request $request)
    {
        // VALIDATE DATES RANGE - cannot be overlaping with existing records
        if ($this->datePriceRepo->validateDateRange($request) === false) {
            return redirect()->back()->withErrors(['This date range is not correct. You can not overlap dates for custom prices.']);
        }

        $this->datePriceRepo->create($request->all());

        return redirect()->route('admin.list.date.prices');
    }

    public function edit($id)
    {
        return view('admin.date_prices.edit')
                ->with('datePrice', $this->datePriceRepo->find($id));
    }

    public function update(Request $request, $id)
    {
        // VALIDATE DATES RANGE - cannot be overlaping with existing records
        if ($this->datePriceRepo->validateDateRange($request, $id) === false) {
            return redirect()->back()->withErrors(['This date range is not correct. You can not overlap dates for custom prices.']);
        }

        $this->datePriceRepo->update($id, $request->all());

        return redirect()->route('admin.list.date.prices');
    }

    public function destroy($id)
    {
        $this->datePriceRepo->delete($id);

        return redirect()->route('admin.list.date.prices');
    }
}
