<?php
namespace App\Http\Controllers\Admin;

use App\Repositories\FaqRepository;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use Response;
use Config;

class FaqController extends Controller
{
    public $faqRepo;

    public function __construct(
        FaqRepository $faqRepo
    ) {
        $this->faqRepo = $faqRepo;
    }

    public function index()
    {
        $faqData = $this->faqRepo->getAllWithTranslation();

        return view('admin.faq.index')->with('faqData', $faqData);
    }

    public function create()
    {
        return view('admin.faq.create')
                    ->with('groups', Config::get('faq_groups'))
                    ->with('supportedLocales', Config::get('app.locales'));
    }

    public function store(Request $request)
    {
        $this->faqRepo->createWithTranslations($request, 'question');

        return redirect()->route('admin.list.faq');
    }

    public function edit($id)
    {
        return view('admin.faq.edit')
                ->with('faq', $this->faqRepo->find($id))
                ->with('groups', Config::get('faq_groups'))
                ->with('supportedLocales', Config::get('app.locales'));
    }

    public function update(Request $request, $id)
    {
        $this->faqRepo->updateWithTranslations($request, $id, 'question');

        return redirect()->route('admin.list.faq');
    }

    public function destroy($id)
    {
        $this->faqRepo->delete($id);

        return redirect()->route('admin.list.faq');
    }
}
