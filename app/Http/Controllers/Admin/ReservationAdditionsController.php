<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Repositories\ReservationAdditionRepository;

use Config;

class ReservationAdditionsController extends Controller
{
    public $additionsRepo;

    public function __construct(
        ReservationAdditionRepository $additionsRepo
    ) {
        $this->additionsRepo = $additionsRepo;
    }

    public function index()
    {
        return view('admin.reservation_additions.index')
                ->with('additions', $this->additionsRepo->getAllWithTranslation());
    }

    public function create()
    {
        return view('admin.reservation_additions.create')
                ->with('supportedLocales', Config::get('app.locales'));
    }

    public function store(Request $request)
    {
        if ($request->price == '') {
            $request->request->add(['price' => 0]);
        }

        $this->additionsRepo->createWithTranslations($request, 'name');

        return redirect()->route('admin.list.additions');
    }

    public function edit($id)
    {
        return view('admin.reservation_additions.edit')
                ->with('supportedLocales', Config::get('app.locales'))
                ->with('addition', $this->additionsRepo->find($id));
    }

    public function update($id, Request $request)
    {
        $this->additionsRepo->updateWithTranslations($request, $id, 'name');

        return redirect()->route('admin.list.additions');
    }

    public function destroy($id)
    {
        $this->additionsRepo->delete($id);

        return redirect()->route('admin.list.additions');
    }
}
