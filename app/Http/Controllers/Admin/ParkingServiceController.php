<?php
namespace App\Http\Controllers\Admin;

use App\Repositories\OurServiceRepository;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use Auth;
use Mail;
use Response;
use Carbon\Carbon;
use Config;

class ParkingServiceController extends Controller
{
    public $ourServiceRepo;

    public function __construct(
        OurServiceRepository $ourServiceRepo
    ) {
        $this->ourServiceRepo = $ourServiceRepo;
    }

    public function create()
    {
        $data = $this->ourServiceRepo->first();

        return view('admin.parking_service.index')->with('data', @$data)->with('supportedLocales', Config::get('app.locales'));
    }

    public function store(Request $request)
    {
        if (is_null($this->ourServiceRepo->first())) {
            $this->ourServiceRepo->createWithTranslations($request, 'main_info_title');
        } else {
            $this->ourServiceRepo->updateWithTranslations($request, $this->ourServiceRepo->first()->id, 'main_info_title');
        }
        
        session()->flash('success', 'Success.');

        return redirect()->route('admin.parking.service.show');
    }
}
