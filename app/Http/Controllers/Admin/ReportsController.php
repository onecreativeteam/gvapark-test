<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use App\Repositories\ReservationRepository;

use Auth;
use Mail;
use DB;
use Response;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class ReportsController extends Controller
{
    public $reservationRepo;

    public function __construct(
        ReservationRepository $reservationRepo
    ) {
        $this->reservationRepo = $reservationRepo;
    }

    public function index()
    {
        // $now = Carbon::now()->format('Y-m-d H:i:s');
        // dd($this->reservationRepo->getModel()->where('date_of_arrival', '>=', $now)->whereNotNull('reservation_option')->where('reservation_option','!=','null')->where('reservation_option','!=','')->get()->toArray());
        return view('admin.reports.index');
    }

    public function reservationsExcelExport()
    {
        $reservations = DB::table('reservations')
                            ->leftJoin('users', 'users.id','=','reservations.user_id')
                            ->select('reservations.*', 'users.car_registration_number', 'users.flight_number')
                            ->get();
        $reservations = json_decode(json_encode($reservations), true);

        Excel::create('Reservations list', function ( $excel ) use ( $reservations ) {

            $excel->sheet('Reservations', function($sheet) use ($reservations) {

                //$pArr = array();
                // foreach($reservations as $reservation) {

                //     $pData = array(
                //         $product->id,
                //         $product->name,
                //         $product->text,
                //         (!is_null($product->category)) ? $product->category->name : '',
                //         (!is_null($product->subCategory)) ? $product->subCategory->name : '',
                //         (!is_null($product->manufacturer)) ? $product->manufacturer->name : '',
                //         (!is_null($product->model)) ? $product->model->name : '',
                //         $product->keywords,
                //         $product->description,
                //         $product->image,
                //         $product->created_at,
                //         $product->updated_at,
                //     );
                //     array_push($pArr, $pData);

                // }

                $sheet->fromArray($reservations,null,'A1',false);
                // ->prependRow(
                //     [
                //     'Уникален ключ',
                //     'Ключ на опция',
                //     'Име на опция',
                //     'Категория',
                //     'Промо код',
                //     'Име клиент',
                //     'Фамилия клиент',
                //     'Е-меил',
                //     'Дата на резервация',
                //     'Дата на пристигане',
                //     'Дата на заминаване',
                //     'Дни за престой',
                //     'Начислена цена',
                //     'Променена цена',
                //     'ДДС',
                //     'Метод плащане',
                //     'ID - плащане',
                //     'Приложен промо-код',
                //     'Пристигнал (да/не)',
                //     'Пристигнал на',
                //     'Дата на създаване',
                //     'Дата на последна промяна',
                //     ]
                // );
            });

        })->export( 'xls' );

        return redirect()->route('admin.reports');
    }

    public function reservationsProfitExcelExport(Request $request)
    {
        $reservations = $this->reservationRepo->whereBetween('date_of_reservation', $request->start_date, $request->end_date);
        $data['reservations'] = $reservations;

        $total = 0;
        foreach ($reservations as $reservation) {
            if (!is_null($reservation->changed_price_to_charge)) {
                $total += $reservation->changed_price_to_charge;
            } else {
                $total += $reservation->price_to_charge;
            }
        }

        $data['total'] = $total;

        Excel::create('Reservation Profit', function ( $excel ) use ( $data ) {
            
            $excel->sheet('Profit', function($sheet) use ($data) {

                $sheet->fromArray($data['reservations'],null,'A1',false,false)
                ->prependRow(
                    [
                    'First name',
                    'Last name',
                    'Email',
                    'Date of reservation',
                    'Days to stay',
                    'Calculated price',
                    'Changed price',
                    ]
                );

                $sheet->rows([array('',''),array('Total:',$data['total'].' CHF')]);

            });

        })->export('xls');
    }

    public function clientsExcell()
    {
        $clientsModel = $this->clientsModel;
        $clients = $clientsModel->getAll();

        Excel::create('Clients list', function ( $excel ) use ( $clients ) {

            $excel->sheet('Clients', function($sheet) use ($clients) {

                $sheet->fromArray($clients,null,'A1',false);
            });

        })->export( 'xls' );

        return redirect()->route('admin.reports');
    }
}
