<?php
namespace App\Http\Controllers;

use App\Repositories\ClientsRepository as Client;
use Auth;
use Validator;
use App\Http\Requests\ReviewsRequest;

class ReviewsController extends Controller
{
    public $clientsModel;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->clientsModel = app(Client::class);
    }

    public function store(ReviewsRequest $request)
    {
        $validator = Validator::make($request->all(), $request->rules());

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withInput($request)
                ->withErrors($validator);
        }
        
        $data = $request->all();
        $data['is_approved'] = 0;
        $review = $this->clientsModel;

        $review->create($data);

        return redirect()->route('avis-clients');
    }

    public function indexReviews()
    {
        $reviews = $this->clientsModel;
        $data = $reviews->getAll();

        return view('admin.reviews.index')->with('data', $data);
    }

    public function approveReviews($id)
    {
        $reviews = $this->clientsModel;

        $obj = $reviews->find($id)->first()->toArray();
        $obj['is_approved'] = 1;

        $reviews->update($id, $obj);

        return redirect()->route('admin.reviews.show');
    }

    public function deleteReviews($id)
    {
        $reviews = $this->clientsModel;

        $obj = $reviews->find($id)->first();
        $obj->delete();

        return redirect()->route('admin.reviews.show');
    }
}
