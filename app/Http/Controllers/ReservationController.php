<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Repositories\ReservationRepository;
use App\Repositories\UserRepository;
use App\Repositories\ReservationGalleryRepository;
use App\Repositories\PromoCodeRepository;
use App\Repositories\SystemOptionRepository;

use Illuminate\Http\Request;

use App\Http\Requests\ReservationRequest;
use App\Http\Requests\ReservationCalculationRequest;
use App\Http\Requests\AdminReservationRequest;
use App\Http\Requests\PostReservationRequest;
use App\Http\Requests\PromoCodeRequest;
use App\Http\Requests\ClientLoginRequest;
use App\Http\Requests\UpdateClientRequest;

use App\Services\MailVerificationService;
use App\Services\PricelistService;
use App\Services\PaymentService;

use App\Mail\GvaparkAdminMailer;

use Auth;
use Hash;
use Validator;
use Mail;
use DateTime;
use Config;
use Session;
use Redirect;
use Carbon\Carbon;
use PDF;

class ReservationController extends Controller
{
    public $reservationRepo;
    public $userRepo;
    public $reservationGalleryRepo;
    public $promoCodeRepo;
    public $systemOptionRepo;
    public $priceListService;

    public function __construct(
        ReservationRepository $reservationRepo,
        UserRepository $userRepo,
        ReservationGalleryRepository $reservationGalleryRepo,
        PromoCodeRepository $promoCodeRepo,
        SystemOptionRepository $systemOptionRepo,
        PricelistService $priceListService
    ) {
        $this->reservationRepo = $reservationRepo;
        $this->userRepo = $userRepo;
        $this->reservationGalleryRepo = $reservationGalleryRepo;
        $this->promoCodeRepo = $promoCodeRepo;
        $this->systemOptionRepo = $systemOptionRepo;
        $this->priceListService = $priceListService;
    }

    public function postReservationCart(ReservationRequest $request)
    {
        $maxDays = $this->priceListService->validateMaxDays($request);
        if ($maxDays === false) {
            return redirect()->back()->withErrors(['Cannot make reservation above maximum days.']);
        }

        $checkFreeSpots = $this->reservationRepo->validateFreeSpots($request);
        if (count($checkFreeSpots['errors']) > 0) {
            return redirect()->back()->withErrors([$checkFreeSpots['errors']]);
        }
        session()->put('disable_valet', $checkFreeSpots['disable_valet']);

        if ($request->reservation_type == 'valet' && session('disable_valet') == '1') {
            return redirect()->back()->withErrors([trans('messages.validation_reservation_type_must')]);
        }

        $selectedDates = $this->reservationRepo->setSessionDates($request);

        $allowedReservationTime = $this->priceListService->validateReservationTime(
                $selectedDates['full_date_of_arrival'],
                $selectedDates['full_date_of_departure']
        );

        if ($allowedReservationTime === false) {
            return redirect()->route('reservation.one.get')->withErrors([trans('messages.validation_reservation_not_allowed_time')]);
        } else if ($allowedReservationTime === 'errorFutureDate') {
            return redirect()->route('reservation.one.get')->withErrors([trans('messages.validation_reservation_future_date')]);
        }

        $priceList = $this->priceListService->getPrices($selectedDates);

        if (isset($request->request_homepage) && $request->request_homepage == '1') {
            return redirect()->route('reservation.one.get');
        }

        return redirect()->route('reservation.two.get');
    }

    public function postReservationOptions(ReservationRequest $request)
    {
        $selectedDates  = $this->reservationRepo->setSessionDates($request);
        $priceList      = $this->priceListService->getPrices($selectedDates);

        return redirect()->route('reservation.three.get');
    }

    public function postReservationUser(PostReservationRequest $request)
    {
        $clientData     = $request->all();

        if (!isset($clientData['date_of_birth']) || $clientData['date_of_birth'] == '') {
            unset($clientData['date_of_birth']);
        }

        if (session('success_auth_client') == '1') {

            $updated = $this->userRepo->update(Auth::user()->id, $clientData);

            session()->put('clientData', $updated);

        } else {

            if (isset($request->password) && !empty($request->password)) {

                $clientData['password']     = Hash::make($request->password);
                $clientData['old_password'] = $request->password;

            }

            $created = $this->userRepo->create($clientData);

            session()->put('clientData', $created);

            $auth = Auth::attempt(['email' => $request->email, 'password' => $request->password]);

            session()->put('success_auth_client', 1);
        }

        return redirect()->route('reservation.five.get');
    }

    public function getReservationOne()
    {
        return view('reservation_one');
    }

    public function getReservationTwo()
    {
        if (is_null(session('selectedDates'))) {
            return redirect()->route('reservation.one.get');
        }

        $options                            = $this->systemOptionRepo->first();

        $data['show_reservation_options']   = (!empty($options) && $options->reservation_have_options == '1') ? 1 : 0;
        $data['extereur_price']             = $options->extereur_price ? $options->extereur_price : 0;
        $data['intereur_price']             = $options->intereur_price ? $options->intereur_price : 0;
        $data['extra_price']                = $options->extra_price ? $options->extra_price : 0;
        $data['electric_charge_price']      = $options->electric_charge_price ? $options->electric_charge_price : 0;

        return view('reservation_two')->with('data', $data);
    }

    public function getReservationThree()
    {
        if (is_null(session('selectedDates'))) {
            return redirect()->route('reservation.one.get');
        }

        return view('reservation_three');
    }

    public function getReservationFour()
    {
        return view('reservation_four');
    }

    public function getReservationFive()
    {
        if (session()->has('createdReservation')) {

            if (!is_null(session('paymentData'))) {

                $reservationData['payment_status'] = session('paymentData')['payment_status'];

                $reservation = $this->reservationRepo->update(session('createdReservation')->id, $reservationData);

                session()->put('createdReservation', $reservation);

            }

        }

        return view('reservation_five')->with('params', $this->priceListService->dataTransParams());
    }

    public function initReservation()
    {
        $this->userRepo->handleNewReservationData();

        $reservation = $this->reservationRepo->createOrUpdate(session('reservationData'));

        return response()->json([
            'status' => !is_null(session('reservationData')) ? 'success' : 'error',
            'reservation' => !is_null(session('createdReservation')) ? session('createdReservation') : [],
            'reservationData' => !is_null(session('reservationData')) ? session('reservationData') : [],
        ]);
    }

    public function getReservationConfirmationPage()
    {
        if (is_null(session('clientData')) || is_null(session('reservationData'))) {
            return redirect()->route('reservation.one.get');
        }
        
        $clientData         = session()->get('clientData');
        $paymentData        = session()->has('paymentData') ? session()->get('paymentData') : [];
        $reservationData    = session()->get('reservationData');

        $reservationData['payment_method']      = isset($paymentData['payment_method']) ? $paymentData['payment_method'] : 'datatrans';
        $reservationData['payment_id']          = isset($paymentData['payment_id']) ? $paymentData['payment_id'] : session('system_payment_id');
        $reservationData['payment_status']      = isset($paymentData['payment_status']) ? $paymentData['payment_status'] : 'completed';
        $reservationData['system_payment_id']   = session('system_payment_id') ? session('system_payment_id') : null;

        session()->put('reservationData', $reservationData);

        $idData['id'] = session('createdReservation')->id;

        $createdReservation = $this->reservationRepo->update($idData['id'], $reservationData);

        $createdReservation->payment()->create(session('paymentResponse'));

        $pdfData = $this->priceListService->preparePdfData($createdReservation);

        $pdfStream = PDF::loadView('facture', $pdfData)->stream();

        if ($this->reservationRepo->checkMailConnection() === true) {
            $sendMail = MailVerificationService::sendReservationMail($clientData['email'], $pdfStream, $idData['id']);
            $sendMailAdmin = MailVerificationService::sendReservationAdminMail();

            if ($createdReservation->reservation_option != 'null' && $createdReservation->reservation_option != '') {
                $sendMailAdmin = MailVerificationService::sendReservationAdminMail('carspa@gvapark.ch');
                $sendMailAdmin = MailVerificationService::sendReservationAdminMail('borislav.onecreative@gmail.com');
            }
        }

        session()->flush();
        session()->regenerate();
        return view('reservation_confirm')->with('paymentData', $paymentData)->with('reservationData', $reservationData)->with('idData', $idData);
    }

    public function viewClientGallery($id)
    {
        if (!session()->has('clientData')) return redirect()->back();

        $data = $this->reservationGalleryRepo->getPictures($id);

        return view('gallery')->with('data', $data);
    }

    public function applyPromoCode(PromoCodeRequest $request)
    {
        if (isset(session('priceList')['is_aplied_promo']) && session('priceList')['is_aplied_promo'] === 1)
        {
            return redirect()->back()->withErrors([trans('messages.validation_promo_code_applied')]);
        }

        $promoCode = $this->promoCodeRepo->getModel()->where('code', '=', $request->promo_code)->first();

        if (is_null($promoCode)) {
            return redirect()->back()->withErrors([trans('messages.validation_promo_code_exists')]);
        }

        $now = Carbon::now()->format('Y-m-d');
        $startDate = Carbon::parse($promoCode->start_date)->format('Y-m-d');
        $endDate = Carbon::parse($promoCode->end_date)->format('Y-m-d');

        if (!empty($promoCode) && !empty(session('priceList'))) {
            if (($now >= $startDate) && ($now <= $endDate))
            {

                $sessionPriceList = session('priceList');

                $totalPrice = $sessionPriceList['totalPriceWithVAT'];

                $sessionPriceList['is_aplied_promo'] = 1;
                $sessionPriceList['promo_value_percent'] = intval($promoCode->promotion_value);
                $sessionPriceList['promo_code_id'] = $promoCode->id;
                $sessionPriceList['promo_discount'] = ((intval($promoCode->promotion_value) / 100) * $totalPrice);
                $sessionPriceList['totalPromoPriceWithVAT'] = $totalPrice - ((intval($promoCode->promotion_value) / 100) * $totalPrice);
                $sessionPriceList['absolute_total'] = $sessionPriceList['totalPromoPriceWithVAT'];

                session()->put('priceList', $sessionPriceList);
            }
            else
            {
                return redirect()->back()->withErrors([trans('messages.validation_promo_code_old')]);
            }
        }

        return redirect()->back();
    }

    public function generateInvoice($id = null)
    {
        $reservation = $this->reservationRepo->find($id);

        $data = $this->priceListService->preparePdfData($reservation);

        $pdf = PDF::loadView('facture', $data);
        
        return $pdf->download('facture.pdf');
    }

    public function authClient(ClientLoginRequest $request)
    {
        Auth::attempt(['email' => $request->login_email, 'password' => $request->login_password]);

        if (!is_null(Auth::user())) {

            session()->put('clientData', Auth::user());
            session()->put('success_auth_client', 1);

            if (!is_null(session('priceList'))) {
                return redirect()->route('reservation.four.get');
            }

        } else {

            return redirect()->back()->withErrors(['Erreur', trans('messages.validation_auth_client_incorect_pass')]);

        }

        return redirect()->back();
    }

    public function logoutClient()
    {
        session()->flush();

        return redirect('/');
    }

    public function showClientProfile($id)
    {
        if (!session()->has('success_auth_client')) {
            return redirect()->route('home');
        }

        if (Auth::user()->id != $id) {
            session()->flush();
            return redirect()->route('home');
        }

        $client             = $this->userRepo->find($id);

        return view('profile')
            ->with('clientData', $client)
            ->with('reservations', $client->reservations);
    }

    public function updateClientProfile(UpdateClientRequest $request, $id)
    {
        if (Auth::user()->id != $id) {
            session()->flush();
            return redirect()->route('home');
        }
        
        if ($request->has('change_password')) {
            $data               = $request->except(['password','email_confirm']);
            $data['password']   = Hash::make($request->password);
        } else {
            $data               = $request->all();
        }
        
        $updated                = $this->userRepo->update($id, $data);

        session()->put('clientData', $updated);

        return redirect()->back();
    }

    public function anulateClientReservation($id)
    {
        $reservation        = $this->reservationRepo->find($id)->toArray();

        $date_of_arrival    = $reservation['date_of_arrival'];
        $dateRestrict       = strtotime(Carbon::parse($date_of_arrival)->subHours(24)->format('Y/m/d H:i'));
        $now                = strtotime(Carbon::now()->format('Y/m/d H:i'));

        if ($dateRestrict < $now) {
            return redirect()->back()->withErrors(['Erreur', trans('messages.validation_anulate_reservation')]);
        }

        $reservation['payment_status'] = 'annulated';

        $this->reservationRepo->update($id, $reservation);

        if ($this->reservationRepo->checkMailConnection() === true) {
            $sendMailAdmin = MailVerificationService::sendAnnulatedReservationAdminMail($reservation);
        }

        return redirect()->back();
    }

    public function getPreCalculatedPrices(ReservationCalculationRequest $request)
    {
        $selectedDates = $this->reservationRepo->setSessionDates($request);

        $checkFreeSpots = $this->reservationRepo->validateFreeSpots($request);

        session()->put('disable_valet', $checkFreeSpots['disable_valet']);

        if ( $checkFreeSpots['disable_valet'] =='0') {
            $allowedReservationTime = $this->priceListService->validateReservationTime($selectedDates['full_date_of_arrival'],$selectedDates['full_date_of_departure']);
            if ($allowedReservationTime === 'errordateValet') {
                session()->put('disable_valet', 1);
                $checkFreeSpots['disable_valet'] = 1;
                session()->put('date_propose',trans('messages.validation_reservation_not_allowed_time'));
            }
        }

        if (is_array($selectedDates)) {
            $prices = $this->priceListService->getPrices($selectedDates);
        }

        if (isset($prices)) {
            return response()->json(['prices'=>$prices, 'disable_valet'=>$checkFreeSpots['disable_valet'], 'date_propose'=> session('date_propose')]);
        }
    }

}
