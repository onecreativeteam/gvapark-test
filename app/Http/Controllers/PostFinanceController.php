<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\ReservationRepository;
use App\Repositories\EventRepository;
use App\Repositories\EventReservationRepository;

use App\Services\MailVerificationService;

use Ixudra\Curl\Facades\Curl;
use Config;
use URL;
use Session;
use Redirect;

class PostFinanceController extends Controller
{
    public $reservationRepo;
    public $eventRepo;
    public $eventReservationRepo;
    public $mailService;

    public function __construct(
        ReservationRepository $reservationRepo,
        EventRepository $eventRepo,
        EventReservationRepository $eventReservationRepo,
        MailVerificationService $mailService
    ) {
        $this->reservationRepo = $reservationRepo;
        $this->eventRepo = $eventRepo;
        $this->eventReservationRepo = $eventReservationRepo;
        $this->mailService = $mailService;
    }

    public function statusPayment(Request $request)
    {
        if (session('event_payment_id') && session('event_payment_id') == $request->refno) {
            return self::eventStatusPayment($request);
        }

        $status = $request->status;

        session()->put('paymentResponse', $request->all());

        $paymentData = [];

        if (isset($status) && !empty($status))
        {
            switch ($status) {
                
                case 'error':
                    //payment is declined by system
                    $paymentData['payment_status'] = 'declined';

                    session()->put('paymentData', $paymentData);

                    return Redirect::route('reservation.five.get', ['nooverride'=>1])->withErrors([trans('messages.validation_payment_declined')]);

                case 'cancel':
                    //payment meets exception
                    $paymentData['payment_status'] = 'canceled';

                    session()->put('paymentData', $paymentData);

                    return Redirect::route('reservation.five.get', ['nooverride'=>1])->withErrors([trans('messages.validation_payment_canceled')]);

                case 'success':
                    //payment is successfull
                    $paymentData['payment_method'] = 'datatrans - '.$request->pmethod;

                    $paymentData['payment_id'] = $request->uppTransactionId;

                    $paymentData['payment_status'] = 'completed';
                    
                    session()->put('paymentData', $paymentData);

                    return Redirect::route('reservation.complete', ['nooverride'=>1]);
            }
        }

        return Redirect::route('reservation.five.get', ['nooverride'=>1])->with('error', trans('validation_payment_failed'));
    }

    public function eventStatusPayment($request)
    {
        $status             = $request->status;

        $eventPaymentData   = [];
        $reservation        = $this->eventReservationRepo->find(session('started_event_reservation'));

        // General Update for Reservation
        $eventPaymentData['system_payment_id'] = $request->refno;

        if (isset($status) && !empty($status))
        {
            switch ($status) {
                
                case 'error':
                    $eventPaymentData['payment_status'] = 'declined';

                    $createdEventReservation = $this->eventReservationRepo->update(session('started_event_reservation'), $eventPaymentData);

                    session()->put('createdEventReservation', $createdEventReservation);

                    // return redirect to event-inner after changing to multiple events
                    return Redirect::route('events.inner', $reservation->event->slug)->withErrors([trans('messages.validation_payment_declined')]);

                case 'cancel':
                    $eventPaymentData['payment_status'] = 'canceled';

                    $createdEventReservation = $this->eventReservationRepo->update(session('started_event_reservation'), $eventPaymentData);

                    session()->put('createdEventReservation', $createdEventReservation);

                    // return redirect to event-inner after changing to multiple events
                    return Redirect::route('events.inner', $reservation->event->slug)->withErrors([trans('messages.validation_payment_canceled')]);

                case 'success':
                    $eventPaymentData['payment_method'] = 'datatrans - '.$request->pmethod;

                    $eventPaymentData['payment_id'] = $request->uppTransactionId;

                    $eventPaymentData['payment_status'] = 'completed';

                    $createdEventReservation = $this->eventReservationRepo->update(session('started_event_reservation'), $eventPaymentData);

                    session()->put('createdEventReservation', $createdEventReservation);

                    return Redirect::route('events.confirm');
            }
        }

        // return redirect to event-inner after changing to multiple events
        return Redirect::route('events.inner', $reservation->event->slug)->with('error', trans('validation_payment_failed'));
    }

    public function cancelTransaction($id)
    {
        $reservation = $this->reservationRepo->find($id);

        if (is_null($reservation->payment)) {
            return redirect()->back()->withErrors(['Cannot cancel your reservation. Missing data.']);
        }

        $post = '<?xml version="1.0" encoding="UTF-8" ?>
        <paymentService version="1">
          <body merchantId="3000012256">
            <transaction refno="'.$reservation->payment->refno.'">
              <request>
                <amount>'.$reservation->payment->amount.'</amount>
                <currency>'.$reservation->payment->currency.'</currency>
                <uppTransactionId>'.$reservation->payment->uppTransactionId.'</uppTransactionId>
                <transtype>06</transtype>
              </request>
            </transaction>
          </body>
        </paymentService>';

        // $response = Curl::to('https://api.sandbox.datatrans.com/upp/jsp/XML_processor.jsp')
        $response = Curl::to('https://api.datatrans.com/upp/jsp/XML_processor.jsp')
            ->withContentType('application/x-www-form-urlencoded')
            ->withData( array( 'xmlRequest' => $post ) )
            ->returnResponseObject()
            ->post();

        $status = self::namespacedXMLToArray($response->content);

        if ( ($status['body']['@attributes']['status'] == 'accepted') && !isset($status['body']['transaction']['error']) ) {
            $reservation->is_canceled = 1;
            $reservation->payment_status = 'canceled';
            $reservation->save();

            $this->mailService->sendReservationCancelMail($reservation);
            $this->mailService->sendClientReservationCancelMail($reservation);
        }

        return redirect()->back();
    }

    public function cancelEventTransaction($id)
    {
        $reservation = $this->eventReservationRepo->find($id);

        if ($reservation->payment_status != 'completed') {
            return redirect()->back()->withErrors(['Cannot cancel your reservation. Missing data.']);
        }

        $post = '<?xml version="1.0" encoding="UTF-8" ?>
        <paymentService version="1">
          <body merchantId="3000012256">
            <transaction refno="'.$reservation->system_payment_id.'">
              <request>
                <amount>'.str_replace('.', '', number_format($reservation->price, 2)).'</amount>
                <currency>CHF</currency>
                <uppTransactionId>'.$reservation->payment_id.'</uppTransactionId>
                <transtype>06</transtype>
              </request>
            </transaction>
          </body>
        </paymentService>';

        // $response = Curl::to('https://api.sandbox.datatrans.com/upp/jsp/XML_processor.jsp')
        $response = Curl::to('https://api.datatrans.com/upp/jsp/XML_processor.jsp')
            ->withContentType('application/x-www-form-urlencoded')
            ->withData( array( 'xmlRequest' => $post ) )
            ->returnResponseObject()
            ->post();

        $status = self::namespacedXMLToArray($response->content);

        if ( ($status['body']['@attributes']['status'] == 'accepted') && !isset($status['body']['transaction']['error']) ) {
            $reservation->payment_status = 'canceled';
            $reservation->save();
        }

        return redirect()->back();
    }

    public function namespacedXMLToArray($xml)
    {
        return json_decode(json_encode(simplexml_load_string($xml)), true);
    }
}
