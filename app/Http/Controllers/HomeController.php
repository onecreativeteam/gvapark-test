<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\HomepageRepository;
use App\Repositories\UserRepository;
use App\Repositories\OurServiceRepository;
use App\Repositories\ContactRepository;
use App\Repositories\FaqRepository;
use App\Repositories\ConcurentRepository;
use App\Repositories\SystemOptionRepository;
use App\Repositories\ReservationPriceRepository;

use Config;
use Response;

class HomeController extends Controller
{
    public $homepageRepo;
    public $userRepo;
    public $ourServiceRepo;
    public $contactRepo;
    public $faqRepo;
    public $concurentRepo;
    public $systemOptionRepo;
    public $pricesRepo;

    public function __construct(
        HomepageRepository $homepageRepo,
        UserRepository $userRepo,
        OurServiceRepository $ourServiceRepo,
        ContactRepository $contactRepo,
        FaqRepository $faqRepo,
        ConcurentRepository $concurentRepo,
        SystemOptionRepository $systemOptionRepo,
        ReservationPriceRepository $pricesRepo
    ) {
        $this->homepageRepo = $homepageRepo;
        $this->userRepo = $userRepo;
        $this->ourServiceRepo = $ourServiceRepo;
        $this->contactRepo = $contactRepo;
        $this->faqRepo = $faqRepo;
        $this->concurentRepo = $concurentRepo;
        $this->systemOptionRepo = $systemOptionRepo;
        $this->pricesRepo = $pricesRepo;
    }

    // public function index(Request $request)
    // {
    //     return view('index')
    //             ->with('data', $this->homepageRepo->getAllWithTranslation()->first())
    //             ->with('faqData', $this->faqRepo->getAllWithTranslation()->groupBy('group_id')->toArray())
    //             ->with('groups', Config::get('faq_groups'))
    //             ->with('system', $this->systemOptionRepo->first())
    //             ->with('concurents', $this->concurentRepo->getAllWithTranslation('is_main', 'desc'));
    // }

    public function index(Request $request)
    {
        return view('index-eli')
                ->with('data', $this->homepageRepo->getAllWithTranslation()->first())
                ->with('faqData', $this->faqRepo->getAllWithTranslation()->groupBy('group_id')->toArray())
                ->with('groups', Config::get('faq_groups'))
                ->with('system', $this->systemOptionRepo->first())
                ->with('pricesArray', $this->pricesRepo->getTablePricesArray())
                ->with('concurents', $this->concurentRepo->getAllWithTranslation('is_main', 'desc'));
    }

    public function sitemap()
    {
        return Response::view('sitemap')->header('Content-Type', 'application/xml');
    }

    public function showFacture()
    {
        $data['test'] = 'TEST';
        return view('facture', $data);
    }

    public function parkingServiceIndex()
    {
        $data = $this->ourServiceRepo->getAllWithTranslation()->first();

        return view('parking_service')->with('data', $data);
    }

    public function avisClientsIndex()
    {
        $data = [];

        return view('avis_clients')->with('clients', $data);
    }

    public function videoIndex()
    {
        return view('video');
    }

    public function contactIndex()
    {
        $data = $this->contactRepo->first();

        return view('contact')->with('data', $data);
    }

}
