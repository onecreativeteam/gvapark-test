<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Config;
use URL;
use Session;
use Redirect;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

class PaypalController extends Controller
{
    private $_api_context;
 
    public function __construct()
    {
        $paypal_conf = Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    public function postPayment()
    {
        $priceList = session('priceList');

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item1 = new Item();
        $item1->setName('Initial tax')
            ->setCurrency('CHF')
            ->setQuantity(1)
            ->setSku(rand(1, 9999))
            ->setPrice($priceList['initialTax']);

        $item2 = new Item();
        $item2->setName('Per day tax')
            ->setCurrency('CHF')
            ->setQuantity($priceList['daysToCharge'])
            ->setSku(rand(1, 9999))
            ->setPrice(10);

        $item3 = new Item();
        $item3->setName('TVA tax')
            ->setCurrency('CHF')
            ->setQuantity(1)
            ->setSku(rand(1, 9999))
            ->setPrice($priceList['totalVAT']);

        // $itemPaypalTax = new Item();
        // $itemPaypalTax->setName('Paypal payment tax')
        //     ->setCurrency('CHF')
        //     ->setQuantity(1)
        //     ->setSku(rand(1, 9999))
        //     ->setPrice(number_format((float)$priceList['paymentTax'], 2, '.', ''));

        if (isset($priceList['is_aplied_promo'])) {

            $discount = $priceList['totalPriceWithVAT'] - $priceList['totalPromoPriceWithVAT'];
            $item4 = new Item();

            $item4->setName('Discount')
                ->setCurrency('CHF')
                ->setQuantity(1)
                ->setSku(rand(1, 9999))
                ->setPrice(-$discount);
        }

        $itemList = new ItemList();
        if (isset($priceList['is_aplied_promo'])) {

            $itemList->setItems(array($item1, $item2, $item3, $item4));

        } else {

            $itemList->setItems(array($item1, $item2, $item3));

        }

        $amount = new Amount();
        // if (isset($priceList['is_aplied_promo'])) {

        //     $amount->setCurrency("CHF")->setTotal($priceList['totalPromoPriceWithVAT'] + $priceList['paypalTax']);
        // } else {

            $amount->setCurrency("CHF")->setTotal(number_format((float)$priceList['price_to_charge'], 2, '.', ''));
        // }

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("GvaParkReservation")
            ->setInvoiceNumber(uniqid());
     
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('paypal.status'))->setCancelUrl(URL::route('paypal.status'));
     
        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));

        try {
            $response = $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            if (config('app.debug')) {
                echo "Exception: " . $ex->getMessage() . PHP_EOL;
                $err_data = json_decode($ex->getData(), true);
                exit;
            } else {
                die('Error.');
            }
        }

        Session::put('paypal_payment_id', $response->getId());

        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirectUrl = $link->getHref();
                break;
            }
        }

        if (!empty($redirectUrl) && isset($redirectUrl)) return Redirect::to( $redirectUrl );

        return Redirect::route('reservation.checkout.get')->with('error', 'Paiement échoué');
    }

    public function getPaymentStatus(Request $request)
    {
        $session_payment_id = Session::get('paypal_payment_id');

        $payment_id = $request->paymentId;
        $payment_token = $request->token;
        $payment_payer = $request->PayerID; 

        Session::forget('paypal_payment_id');
     
        if (empty($payment_payer) || empty($payment_token)) {
            return Redirect::route('reservation.checkout.get')
                ->with('error', 'Paiement échoué');
        }
     
        $payment = Payment::get($payment_id, $this->_api_context);

        $execution = new PaymentExecution();
        $execution->setPayerId($payment_payer);
     
        $result = $payment->execute($execution, $this->_api_context);

        if ($result->getState() == 'approved') {

            $paymentData = [];
            $paymentData['payment_method'] = 'paypal';
            $paymentData['payment_id'] = $result->id;

            session()->put('paymentData', $paymentData);

            return Redirect::route('reservation.complete')->with('success', 'Paiement réussi');
        }

        return Redirect::route('reservation.checkout.get')->with('error', 'Paiement échoué');
    }

}
