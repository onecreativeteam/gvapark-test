<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

use App\Repositories\ReservationRepository;

class AdminMiddleware
{
    public $reservationRepo;

    public function __construct(
        ReservationRepository $reservationRepo
    ) {
        $this->reservationRepo = $reservationRepo;
    }

    public function handle($request, Closure $next, $guard = null)
    {
        $this->reservationRepo->clearParkPlaces();

        if ( Auth::check() && Auth::user()->role != 'basic' )
        {
            return $next($request);
        }

        return redirect()->route('login');
    }
}
