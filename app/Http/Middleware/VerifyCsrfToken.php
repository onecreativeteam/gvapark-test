<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '*/payment',
        '*/payment-status',
        '*/client_auth',
        '*/logout-client',
        '*/client/profile/*',
        '*/reservations/*/anulate',
        '*/apply-promo',
        '*/generate-invoice/*',
        '*/facture',
        '*/reservation-cart',
        '*/reservation-checkout',
        '*/reservation-confirm',
        '*/events/status-payment',
    ];
}
