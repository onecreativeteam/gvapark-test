<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

use URL;

class ParametersMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $parts = parse_url(URL::previous());

        if ($request->fullUrl() == URL::previous()) {
            return $next($request);
        } else {
            if (isset($parts['query'])) {
                return redirect()->route('home', [$parts['query']]);
            } else {
                return $next($request);
            }
        }

        return $next($request);
    }
}
