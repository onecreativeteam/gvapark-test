<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class EventReservationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:200|min:1',
            'last_name' => 'required|max:200|min:1',
            'email' => 'required|max:40|email',
            'car_number' => 'required|min:1|max:200',
            'date_of_arrival' => 'required|date|after_or_equal:today|after_or_equal:event_start_date|before_or_equal:event_end_date',
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => trans('messages.validation_first_name_required'),
            'first_name.max' => trans('messages.validation_first_name_max'),
            'last_name.required' => trans('messages.validation_last_name_required'),
            'last_name.max' => trans('messages.validation_last_name_max'),
            'email.required' => trans('messages.validation_email_required'),
            'email.email' => trans('messages.validation_email_email'),
            'car_number.required' => trans('messages.validation_car_registration_number_required'),
            'date_of_arrival.after_or_equal' => trans('messages.validation_reservation_future_date1'),
            'date_of_arrival.before_or_equal' => trans('messages.validation_reservation_future_date2'),
        ];
    }
}
