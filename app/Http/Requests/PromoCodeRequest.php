<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class PromoCodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'promo_code' => 'required|exists:promo_codes,code',
        ];
    }

    public function messages()
    {
        return [
            'promo_code.required' => trans('messages.validation_promo_code_required'),
            'promo_code.exists' => trans('messages.validation_promo_code_exists'),
        ];
    }
}
