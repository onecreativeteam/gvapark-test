<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class ReservationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // return Auth::guest();
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date_of_arrival' => 'required',
            'hour_of_arrival' => 'required|date_format:H:i',
            'date_of_departure' => 'required',
            'hour_of_departure' => 'required|date_format:H:i',
            // 'reservation_type' => 'required_if:check_type,1|in:valet,navet',
            'park_place_id' => 'sometimes|exists:park_places,id'
        ];
    }

    public function messages()
    {
        return [
            'date_of_arrival.required' => trans('messages.validation_date_of_arrival_required'),
            'date_of_departure.required' => trans('messages.validation_date_of_departure_required'),
            'hour_of_arrival.required' => trans('messages.validation_hour_of_arrival_required'),
            'minute_of_arrival.required' => trans('messages.validation_minute_of_arrival_required'),
            'hour_of_departure.required' => trans('messages.validation_hour_of_departure_required'),
            'minute_of_departure.required' => trans('messages.validation_minute_of_departure_required'),
            'date_of_arrival.date_format' => trans('messages.validation_date_of_arrival_format'),
            'date_of_departure.date_format' => trans('messages.validation_date_of_departure_format'),
            'hour_of_arrival.date_format' => trans('messages.validation_hour_of_arrival_format'),
            'minute_of_arrival.date_format' => trans('messages.validation_minute_of_arrival_format'),
            'hour_of_departure.date_format' => trans('messages.validation_hour_of_departure_format'),
            'minute_of_departure.date_format' => trans('messages.validation_minute_of_departure_format'),
            'reservation_type.required_if' => trans('messages.validation_reservation_type_must'),
        ];
    }
}
