<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class ClientLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'login_email' => 'required|exists:users,email',
            'login_password' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'login_email.required' => trans('messages.validation_login_email_required'),
            'login_email.exists' => trans('messages.validation_login_email_exist'),
            'login_password.required' => trans('messages.validation_login_password_required'),
        ];
    }
}
