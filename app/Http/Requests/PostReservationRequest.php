<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class PostReservationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return Auth::guest() && session()->has('priceList');
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:40|min:1',
            'last_name' => 'required|max:40|min:1',
            'company_name' => 'max:40|min:1',
            'email' => 'sometimes|required|email|unique:users,email',
            'phone' => 'required|min:1',
            'adress' => 'required|min:1',
            'date_of_birth' => 'sometimes|min:1|date_format:Y-m-d',

            'email_confirm' => 'sometimes|same:email|required',
            'password' => 'sometimes|required',

            'city' => 'required|min:1',
            'postcode' => 'required|min:1',
            'car_brand' => 'required|min:1',
            'car_model' => 'required|min:1',
            'car_registration_number' => 'required|min:1',
            // 'flight_number' => 'required|min:1',
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => trans('messages.validation_first_name_required'),
            'first_name.max' => trans('messages.validation_first_name_max'),
            'last_name.required' => trans('messages.validation_last_name_required'),

            'email_confirm.required' => trans('messages.validation_email_confirm_required'),
            'email_confirm.same' => trans('messages.validation_email_confirm_same'),
            'password.required' => trans('messages.validation_password_required'),

            'last_name.max' => trans('messages.validation_last_name_max'),
            'company_name.max' => trans('messages.validation_company_name_max'),
            'email.required' => trans('messages.validation_email_required'),
            'email.email' => trans('messages.validation_email_email'),
            //'email.unique' => 'We already have registration with this email. If you are the owner, please log into your profile.',
            'phone.required' => trans('messages.validation_phone_required'),
            'adress.required' => trans('messages.validation_adress_required'),
            'city.required' => trans('messages.validation_city_required'),
            'postcode.required' => trans('messages.validation_postcode_required'),
            'car_brand.required' => trans('messages.validation_car_brand_required'),
            'car_model.required' => trans('messages.validation_car_model_required'),
            'car_registration_number.required' => trans('messages.validation_car_registration_number_required'),
            'flight_number.required' => trans('messages.validation_flight_number_required'),
        ];
    }
}
