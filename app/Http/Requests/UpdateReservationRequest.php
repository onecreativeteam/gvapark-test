<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class UpdateReservationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // return Auth::guest();
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'park_place' => 'sometimes|exists:park_places,name|empty_park_place'
        ];
    }

    public function messages()
    {
        return [
            'park_place.empty_park_place' => trans('messages.empty_park_place'),
        ];
    }
}
