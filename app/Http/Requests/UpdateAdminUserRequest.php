<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Auth;

class UpdateAdminUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->role == 'admin';
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:6',
            'password' => 'sometimes|min:6|confirmed',
        ];
    }

    public function messages()
    {
        return [
            'password.required' => 'Password required.',
            'password.min' => 'Password must be atleast 6 characters.',
            'password.confirmed' => 'Password is not confirmed correctly.'
        ];
    }
}
