<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReviewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'message' => 'required|string',
            'rating' => 'numeric',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Votre nom est obligatoire.',
            'name.string' => 'Votre nom est faux.',
            'message.required' => 'Message obligatoire.',
            'name.string' => 'Votre message est erroné.',
            'rating.numeric' => 'L\'évaluation n\'est pas choisie.'
        ];
    }
}
