<?php
namespace App\Services;

use Mail;
use App\Mail\GvaparkMailer;
use App\Mail\GvaparkAdminMailer;
use App\Mail\GvaparkAnnulatedAdminMailer;
use App\Mail\GvaparkAcceptMailer;
use App\Mail\GvaparkCancelMailer;
use App\Mail\GvaparkClientCancelMailer;

use App\Mail\GvaparkEventMailer;
use App\Mail\GvaparkEventAdminMailer;

use Carbon\Carbon;
use Illuminate\Database\Connection;

class MailVerificationService
{
    // EVENT EMAIL
    public static function sendReservationEventMail($email, $pdf, $pdfData)
    {
        try {

            Mail::to($email)->send(new GvaparkEventMailer($pdf, $pdfData));

        } catch (Exception $ex) {

            return array('mailException', $ex);

        }

        return true;
    }

    // EVENT ADMIN EMAIL
    public static function sendReservationEventAdminMail($pdfData)
    {
        try {

            $adminEmail = config('mail.adminEmail');
            
            Mail::to($adminEmail)->send(new GvaparkEventAdminMailer($pdfData));

        } catch (Exception $ex) {

            return array('mailException', $ex);

        }

        return true;
    }

    /**
     *  send Email for new reservation
     */
    public static function sendReservationMail($email, $pdf, $id)
    {
        try {

            Mail::to($email)->send(new GvaparkMailer($pdf, $id));

        } catch (Exception $ex) {

            return array('mailException', $ex);

        }

        return true;
    }

    public static function sendReservationAdminMail($email = null)
    {
        try {

            if (is_null($email)) {
                $adminEmail = config('mail.adminEmail');
            } else {
                $adminEmail = $email;
            }
            
            Mail::to($adminEmail)->send(new GvaparkAdminMailer);

        } catch (Exception $ex) {

            return array('mailException', $ex);

        }

        return true;
    }

    public static function sendAnnulatedReservationAdminMail($reservation)
    {
        try {

            $adminEmail = config('mail.adminEmail');
            Mail::to($adminEmail)->send(new GvaparkAnnulatedAdminMailer($reservation));

        } catch (Exception $ex) {

            return array('mailException', $ex);

        }

        return true;
    }

    public static function sendReservationAcceptMail($email, $id, $obj)
    {
        try {

            Mail::to($email)->send(new GvaparkAcceptMailer($id, $obj));

        } catch (Exception $ex) {

            return array('mailException', $ex);

        }

        return true;
    }

    public static function sendReservationCancelMail($reservation)
    {
        try {
            
            Mail::to(config('mail.adminEmail'))->send(new GvaparkCancelMailer($reservation));

        } catch (Exception $ex) {

            return array('mailException', $ex);

        }

        return true;
    }

    public static function sendClientReservationCancelMail($reservation)
    {
        try {
            
            Mail::to($reservation->email)->send(new GvaparkClientCancelMailer($reservation));

        } catch (Exception $ex) {

            return array('mailException', $ex);

        }

        return true;
    }
}
