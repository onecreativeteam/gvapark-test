<?php
namespace App\Services;

use Mail;
use DateTime;
use App\Mail\GvaparkMailer;
use Carbon\Carbon;
use Illuminate\Database\Connection;

use App\Repositories\SystemOptionRepository;
use App\Repositories\ReservationRepository;
use App\Repositories\ReservationPriceRepository;
use App\Repositories\DatePriceRepository;

class PricelistService
{
    private $systemOptionRepo;
    private $reservationRepo;
    private $reservationPriceRepo;
    private $datePriceRepo;

    public function __construct(
        SystemOptionRepository $systemOptionRepo,
        ReservationRepository $reservationRepo,
        ReservationPriceRepository $reservationPriceRepo,
        DatePriceRepository $datePriceRepo
    ) {
        $this->systemOptionRepo = $systemOptionRepo;
        $this->reservationRepo = $reservationRepo;
        $this->reservationPriceRepo = $reservationPriceRepo;
        $this->datePriceRepo = $datePriceRepo;
    }

    public function getPrices($data)
    {
        if (isset($data['is_updated_from_admin'])) {
            $full_date_of_arrival = $data['date_of_arrival'];
            $full_date_of_departure = $data['date_of_departure'];
        } else {        
            $full_date_of_arrival   = session('selectedDates') ? session('selectedDates')['full_date_of_arrival']  : $data['full_date_of_arrival'];
            $full_date_of_departure = session('selectedDates') ? session('selectedDates')['full_date_of_departure']  : $data['full_date_of_departure'];
        }

        $dateX = strtotime($full_date_of_arrival);
        $dateY = strtotime($dateX);
        // $dateY = strtotime('+1 hour', $dateX);

        $date_of_arival = date('Y/m/d H:i:s', $dateY);

        $dateArrival    = new DateTime($full_date_of_arrival);
        $dateDeparture  = new DateTime($full_date_of_departure);

        $diffDate   = $dateDeparture->diff($dateArrival);
        $days       = $diffDate->days;
        $dateNumber = $diffDate->days .'.'.$diffDate->h.$diffDate->i;

        if (self::is_round($dateNumber) ) {
            $fullDaysToCharge = $days;
        } else {
            $fullDaysToCharge = $days + 1;
        }

        $priceList                  = array();
        $priceList['daysToCharge']  = $fullDaysToCharge;

        $navetCustomPrice = self::getCustomPrices($full_date_of_arrival, $full_date_of_departure, 'navet');
        $valetCustomPrice = self::getCustomPrices($full_date_of_arrival, $full_date_of_departure, 'valet');

        session()->put('navetCustomPrice', $navetCustomPrice);
        session()->put('valetCustomPrice', $valetCustomPrice);

        if (isset($data['reservation_type'])) {

            switch ($data['reservation_type']) {

                case 'navet':

                    $priceList['dayTax']        = $this->reservationPriceRepo->getPrice($fullDaysToCharge - $navetCustomPrice['countCustomDays'], $data['reservation_type']) + $navetCustomPrice['totalCustomPrice'];
                    $priceList['initialTax']    = 0;//$this->systemOptionRepo->getInitialTax();
                    break;

                case 'valet':

                    $priceList['dayTax']        = $this->reservationPriceRepo->getPrice($fullDaysToCharge - $valetCustomPrice['countCustomDays'], $data['reservation_type']) + $valetCustomPrice['totalCustomPrice'];
                    $priceList['initialTax']    = 0;//$this->systemOptionRepo->getVipInitialTax();
                    break;

                default:

                    $priceList['dayTax']        = $this->reservationPriceRepo->getPrice($fullDaysToCharge - $valetCustomPrice['countCustomDays'], $data['reservation_type']) + $valetCustomPrice['totalCustomPrice'];
                    $priceList['initialTax']    = 0;//$this->systemOptionRepo->getVipInitialTax();

            }

        }

        if (isset($data['options']) && !is_null($data['options']) && ($data['options'] != '')) {

            $priceList['selectedOption'] = $data['options'];

            switch ($data['options']) {
                case 'intereur':
                    $priceList['optionsTax'] = $this->systemOptionRepo->getIntereurTax();
                    break;
                case 'extereur':
                    $priceList['optionsTax'] = $this->systemOptionRepo->getExtereurTax();
                    break;
                case 'extra':
                    $priceList['optionsTax'] = $this->systemOptionRepo->getExtraTax();
                    break;
            }

        }

        if (isset($data['has_electric']) && ($data['has_electric'] != 'null')) {

            $priceList['has_electric']      = true;
            $priceList['electricTax']       = $this->systemOptionRepo->getElectricChargeTax();

        }

        $vat = 7.7; //in percentage %
        
        $priceList['totalDayTax']       = $priceList['dayTax'];
        $priceList['total']             = $priceList['totalDayTax'] + (isset($priceList['optionsTax']) ? $priceList['optionsTax'] : 0) + (isset($priceList['electricTax']) ? $priceList['electricTax'] : 0);
        $priceList['totalVAT']          = ($vat / 100) * $priceList['total'];
        $priceList['totalPriceWithVAT'] = number_format((float)$priceList['total'] + $priceList['totalVAT'], 2, '.', '');
        $priceList['absolute_total']    = $priceList['totalPriceWithVAT'];

        $priceList['navet_total'] = number_format($this->reservationPriceRepo->getPrice($priceList['daysToCharge'] - $navetCustomPrice['countCustomDays'], 'navet') + $navetCustomPrice['totalCustomPrice'], 2);
        $priceList['valet_total'] = number_format($this->reservationPriceRepo->getPrice($priceList['daysToCharge'] - $valetCustomPrice['countCustomDays'], 'valet') + $valetCustomPrice['totalCustomPrice'], 2);

        session()->put('priceList', $priceList);

        return $priceList;
    }

    // CUSTOM PRICE FOR SPECIFIC DATES
    public function getCustomPrices($full_date_of_arrival, $full_date_of_departure, $type)
    {
        $dateRange          = $this->reservationRepo->dateRange($full_date_of_arrival, $full_date_of_departure);
        $customDayPrices    = [];
        $totalCustomPrice   = 0;
        foreach ($dateRange as $date) {

            $customPrice = $this->datePriceRepo
                                ->getModel()
                                ->where('type', '=', $type)
                                ->where('date_from', '<=', $date)
                                ->where('date_to', '>=', $date)
                                ->first();

            if (!is_null($customPrice)) {
                $customDayPrices[$date] = $customPrice->price;
                $totalCustomPrice += $customPrice->price;
            }

        }
        $countCustomDays = count($customDayPrices);
        // dd($countCustomDays, $customDayPrices, $totalCustomPrice);
        return [
            'customDayPrices' => $customDayPrices,
            'totalCustomPrice' => $totalCustomPrice,
            'countCustomDays' => $countCustomDays,
        ];
    }

    static function is_round($value) {
        return is_numeric($value) && intval($value) == $value;
    }

    public  function preparePdfData($reservation)
    {
        $first_name = $reservation->user->first_name ? $reservation->user->first_name : '';
        $last_name = $reservation->user->last_name ? $reservation->user->last_name : '';

        $data['reservation_id'] = $reservation->id ? $reservation->id : '';
        $data['clientName'] = $first_name.' '.$last_name;
        $data['adress1'] = $reservation->user->adress ? $reservation->user->adress : '';
        $data['adress2'] = $reservation->user->adress2 ? $reservation->user->adress2 : '';
        $data['city'] = $reservation->user->city ? $reservation->user->city : '';
        $data['postcode'] = $reservation->user->postcode ? $reservation->user->postcode : '';
        $data['email'] = $reservation->user->email ? $reservation->user->email : '';
        $data['phone'] = $reservation->user->phone ? $reservation->user->phone : '';
        $data['car_brand'] = $reservation->user->car_brand ? $reservation->user->car_brand : '';
        $data['car_model'] = $reservation->user->car_model ? $reservation->user->car_model : '';
        $data['car_registration_number'] = $reservation->user->car_registration_number ? $reservation->user->car_registration_number : '';
        $data['flight_number'] = $reservation->user->flight_number ? $reservation->user->flight_number : '';
        $data['date_of_reservation'] = Carbon::parse($reservation->date_of_reservation)->format('Y/m/d');
        $data['tvaValue'] = (Carbon::parse($reservation->date_of_reservation)->year == '2017') ? '8' : '7.7';
        $data['date_of_arrival'] = Carbon::parse($reservation->date_of_arrival)->format('Y/m/d');
        $data['hour_of_arrival'] = Carbon::parse($reservation->date_of_arrival)->format('H:i');
        $data['date_of_departure'] = Carbon::parse($reservation->date_of_departure)->format('Y/m/d');
        $data['hour_of_departure'] = Carbon::parse($reservation->date_of_departure)->format('H:i');
        $data['days_to_stay'] = $reservation->days_to_stay ? $reservation->days_to_stay : '';
        $data['reservation_type'] = $reservation->reservation_type ? $reservation->reservation_type : 'valet';

        $navetCustomPrice = self::getCustomPrices($data['date_of_arrival'], $data['date_of_departure'], 'navet');
        $valetCustomPrice = self::getCustomPrices($data['date_of_arrival'], $data['date_of_departure'], 'valet');

        if (isset($data['reservation_type'])) {

            switch ($data['reservation_type']) {

                case 'navet':
                    $data['total_days_price']   = $this->reservationPriceRepo->getPrice($data['days_to_stay'] - $navetCustomPrice['countCustomDays'], $data['reservation_type']) + $navetCustomPrice['totalCustomPrice'];
                    $data['initial_tax']        = 0;
                    $data['daily_tax']          = 0;
                    $data['total']              = $data['total_days_price'];
                    break;

                case 'valet':
                    $data['total_days_price']   = $this->reservationPriceRepo->getPrice($data['days_to_stay'] - $valetCustomPrice['countCustomDays'], $data['reservation_type']) + $valetCustomPrice['totalCustomPrice'];
                    $data['initial_tax']        = 0;
                    $data['daily_tax']          = 0;
                    $data['total']              = $data['total_days_price'];
                    break;

                default:
                    $data['total_days_price']   = $this->reservationPriceRepo->getPrice($data['days_to_stay'] - $valetCustomPrice['countCustomDays'], $data['reservation_type']) + $valetCustomPrice['totalCustomPrice'];
                    $data['initial_tax']        = 0;
                    $data['daily_tax']          = 0;
                    $data['total']              = $data['total_days_price'];

            }

        }

        if ($reservation->has_electric == 1) {

            $data['has_electric']      = 1;
            $data['electricTax']       = $this->systemOptionRepo->getElectricChargeTax();

            $data['total'] += $data['electricTax'];

        }

        $data['vat'] = $reservation->vat;

        if (isset($reservation->changed_price_to_charge)) {
            $data['price_to_charge'] = $reservation->changed_price_to_charge ? $reservation->changed_price_to_charge : '';
        } else {
            $data['price_to_charge'] = $reservation->price_to_charge ? $reservation->price_to_charge : '';
        }


        if (isset($reservation->applied_promo) && $reservation->applied_promo == 1)
        {
            $data['promo_value_percent'] = number_format((float)$reservation->promocode->promotion_value, 2, '.', '');
            $data['promo_discount'] = ((intval($reservation->promocode->promotion_value) / 100) * $reservation->price_to_charge);
        }

        if (!is_null($reservation->reservation_option)) {
            if (($reservation->reservation_option != 'null') && ($reservation->reservation_option != '')) {
                $data['selectedOption'] = $reservation->reservation_option;

                switch ($data['selectedOption']) {
                    case 'intereur':
                        $data['optionTax'] = $this->systemOptionRepo->getIntereurTax();
                        break;
                    case 'extereur':
                        $data['optionTax'] = $this->systemOptionRepo->getExtereurTax();
                        break;
                    case 'extra':
                        $data['optionTax'] = $this->systemOptionRepo->getExtraTax();
                        break;
                }

                $data['total'] += $data['optionTax'];
            }
        }

        $data['payment_method'] = $reservation->payment_method ? $reservation->payment_method : 'Not paid';
        $data['payment_id'] = $reservation->payment_id ? $reservation->payment_id : 'NONE';
        $data['system_payment_id'] = $reservation->system_payment_id ? $reservation->system_payment_id : 'NONE';
        $data['applied_promo'] = $reservation->applied_promo ? $reservation->applied_promo : '';

        return $data;
    }

    public function validateReservationTime($date_of_arrival, $date_of_departure)
    {
        $dateRestrict = strtotime(Carbon::parse($date_of_arrival)->subHours(12)->format('Y/m/d H:i'));
        $now = strtotime(Carbon::now()->format('Y/m/d H:i'));
        $arrival = strtotime(Carbon::parse($date_of_arrival)->format('Y/m/d H:i'));
        $departure = strtotime(Carbon::parse($date_of_departure)->format('Y/m/d H:i'));

        // 12 hourse before reservation limitation commented
        
        if ($dateRestrict < $now) {
            return false;
        }

        if ($arrival > $departure) {
            return 'errorFutureDate';
        }

        return true;
    }

    public function validateMaxDays($request)
    {
        $date_arrival = $request->date_of_arrival.' '.$request->hour_of_arrival.':00';
        $date_departure = $request->date_of_departure .' '.$request->hour_of_departure.':00';

        $dateX = strtotime($date_arrival);
        $dateY = strtotime($dateX);

        $date_of_arival = date('Y/m/d H:i:s', $dateY);

        $dateArrival    = new DateTime($date_arrival);
        $dateDeparture  = new DateTime($date_departure);

        $diffDate   = $dateDeparture->diff($dateArrival);
        $days       = $diffDate->days;
        $dateNumber = $diffDate->days .'.'.$diffDate->h.$diffDate->i;

        if (self::is_round($dateNumber) ) {
            $fullDaysToCharge = $days;
        } else {
            $fullDaysToCharge = $days + 1;
        }

        if ($fullDaysToCharge > $this->systemOptionRepo->getReservationMaxDays()) {
            return false;
        }

        if ($fullDaysToCharge == 0) {
            return false;
        }

        return true;
    }

    public function dataTransParams()
    {
        $priceList  = session('priceList');
        $client     = session('clientData');
        $data       = [];
        
        $data['MERCHANT']   = '1100006700'; //test-account merchant
        $data['AMOUNT']     = str_replace('.', '', number_format($priceList['absolute_total'], 2));
        $data['VAT']        = str_replace('.', '', number_format($priceList['totalVAT'], 2));
        $data['CURRENCY']   = 'CHF';
        $data['SIGN']       = '1493286496862'; //test-account sign
        $data['REFNO']      = uniqid();
        $data['LANG']       = app()->getLocale();

        $data['customer-id']            = $client ? $client->id : '';
        $data['customer-name']          = $client ? $client->name : '';
        $data['customer-first-name']    = $client ? $client->first_name : '';
        $data['customer-last-name']     = $client ? $client->last_name : '';
        $data['customer-street']        = $client ? $client->adress : '';
        $data['customer-street2']       = $client ? $client->adress2 : '';
        $data['customer-city']          = $client ? $client->city : '';
        $data['customer-zip-code']      = $client ? $client->postcode : '';
        $data['customer-birth-date']    = !is_null($client->date_of_birth) ? Carbon::parse($client->date_of_birth)->format('Y-m-d') : '';
        $data['customer-language']      = app()->getLocale();
        $data['customer-phone']         = $client ? $client->phone : '';
        $data['customer-email']         = $client ? $client->email : '';

        $data['shipping-first-name']    = $client ? $client->first_name : '';
        $data['shipping-last-name']     = $client ? $client->last_name : '';
        $data['shipping-street']        = $client ? $client->adress : '';
        $data['shipping-street2']       = $client ? $client->adress2 : '';
        $data['shipping-zip-code']      = $client ? $client->postcode : '';
        $data['shipping-city']          = $client ? $client->city : '';

        session()->put('system_payment_id', $data['REFNO']);

        return $data;
    }
}
