<?php 

namespace App\Services;

use Intervention\Image\ImageManager as Image;
use Carbon\Carbon;

class FileService
{
    /**
     * 
     */
    public static function uploadFile($request, $name, $record, $record_value, $path = 'uploads')
    {
        $imageName = $name.'-'.Carbon::now()->format('Y-m-d-h-m-s').'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path($path), $imageName);
        $imagePath = sprintf('/%s/%s', $path ,$imageName);
        $record->$record_value = $imagePath;
        $record->save();
    }
}