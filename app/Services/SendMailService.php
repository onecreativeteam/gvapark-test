<?php
namespace App\Services;

use Carbon\Carbon;
use File;
use Image;
use Mail;
use Auth;

use App\Mail\RegistrationMail;

class SendMailService
{
    public static function sendRegistrationMail($user)
    {
        Mail::to($user->email)->send(new RegistrationMail($user));
    }
}