<?php

namespace App\Services;

use Laravel\Socialite\Contracts\Provider;
use App\Models\SocialAccount;
use App\Repositories\UserRoleRepository as UserRole;
use App\Repositories\StarUserInformationRepository as StarInfo;
use App\Models\User;

class SocialAccountService
{
    /**
     * @var App\Repositories\StarUserInformationRepository
     */
    protected $starUser;

    /**
    * @return void
    */
    public function __construct(StarInfo $starUser)
    {
        $this->starUser = $starUser;
    }
    
    public function createOrGetUser(Provider $provider)
    {
        $providerUser = $provider->user();
        $providerName = class_basename($provider);

        $account = SocialAccount::whereProvider($providerName)
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {
            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'facebook'
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {
                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'role_id' => 2,
                    'user_status_id' => 2,
                    'password' => bcrypt(str_slug($providerUser->getName())),
                    'accepted_tc' => 1,
                    'accepted_receive_newsletter' => 1,
                    'username' => $providerUser->getName(),
                    'slug' => str_slug($providerUser->getName())
                ]);
                $user->slug = $user->slug.'-'.$user->id;
                $user->star()->create([]);
                $user->starWallet()->create([]);
                $user->save();
            }

            $account->user()->associate($user);
            $account->save();

            return $user;
        }
    }
}
