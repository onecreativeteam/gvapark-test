<?php

namespace App\Models;

use App\Notifications\PasswordReset;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'first_name',
        'last_name',
        'date_of_birth',
        'email',
        'password',
        'company_name',
        'phone',
        'adress',
        'adress2',
        'city',
        'postcode',
        'description',
        'car_brand',
        'car_model',
        'car_registration_number',
        'flight_number',
        'role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordReset($token));
    }
    
    public function reservations()
    {
        return $this->hasMany(Reservation::class, 'user_id');
    }
}
