<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class SystemOptions extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'system_options';

    /**
     * Fillables
     */
    protected $fillable = [
        'parking_spots_count',
        'initial_price',
        'daily_price',
        'reservation_have_options',
        'extereur_price',
        'intereur_price',
        'extra_price',
        'created_at',
        'updated_at'
    ];

}
