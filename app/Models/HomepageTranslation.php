<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomepageTranslation extends Model
{
    public $timestamps = true;

    protected $table = 'homepage_translations';

    protected $fillable = [
        'homepage_id',
        'title',
        'description',
        'security_info',
        'simplicity_info',
        'speed_info',
        'service_info',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    public function parent()
    {
        return $this->belongsTo(Homepage::class, 'homepage_id');
    }
}
