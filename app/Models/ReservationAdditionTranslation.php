<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReservationAdditionTranslation extends Model
{
    public $timestamps = true;

    protected $table = 'reservation_additions_translations';

    protected $fillable = [
        'reservation_addition_id',
        'name',
        'visual_price',
        'description',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    public function parent()
    {
        return $this->belongsTo(ReservationAddition::class, 'reservation_addition_id');
    }
}
