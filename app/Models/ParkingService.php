<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParkingService extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'our_service_information';

    /**
     * Fillables
     */
    protected $fillable = [
        'main_info_title',
        'online_booking_info_title',
        'post_your_vehicle_info_title',
        'travel_info_title',
        'pickup_info_title',
        'main_info_description',
        'online_booking_info_description',
        'post_your_vehicle_info_description',
        'travel_info_description',
        'pickup_info_description',
        'created_at',
        'updated_at'
    ];

    protected $dates = ['created_at', 'updated_at'];
}
