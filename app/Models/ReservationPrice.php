<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReservationPrice extends Model
{
    protected $table = 'reservation_prices';

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'day',
        'price',
        'type',
        'created_at',
        'updated_at'
    ];
}
