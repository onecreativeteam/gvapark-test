<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'description',
        'short_description',
        'date_from',
        'date_to',
        'price',
        'max_reservation_count',
        'is_active', 
        'big_picture',
        'small_picture',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    // This is configuration for translated resources FK
    public $translatableKey = 'event_id';

    // This is configuration for translated resources FK
    public $fileAttribute = ['big_picture', 'small_picture'];

    // This is configuration for model files path
    public $filePathAttribute = 'uploads' . DIRECTORY_SEPARATOR . 'events' . DIRECTORY_SEPARATOR;

    // This is configuration for translatable fields for model
    public $translatableFields = [
        'name',
        'description',
        'short_description',
        'slug',
        'locale',
    ];

    // This is querying the translated instance depend on locale
    public function getTranslationsAttribute() {
        return $this->getRelationValue('translations')->keyBy('locale');
    }

    public function translated()
    {
        return $this->hasOne(EventTranslation::class, 'event_id')->where('locale', app()->getLocale());
    }

    public function translations()
    {
        return $this->hasMany(EventTranslation::class, 'event_id');
    }

    public function reservations()
    {
        return $this->hasMany(EventReservation::class, 'event_id');
    }

    public function completed()
    {
        return $this->hasMany(EventReservation::class, 'event_id')->where('payment_status', '=', 'completed');
    }
}
