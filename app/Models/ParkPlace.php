<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParkPlace extends Model
{
    protected $fillable = [
        'name'
    ];

    public function reservations()
    {
        return $this->hasMany(Reservation::class, 'park_place_id');
    }
}
