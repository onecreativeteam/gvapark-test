<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ReservationClients extends Authenticatable
{
    use Notifiable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'reservation_clients';

    /**
     * Fillables
     */
    protected $fillable = [
        'name',
        'first_name',
        'last_name',
        'company_name',
        'email',
        'password',
        'phone',
        'adress',
        'adress2',
        'city',
        'postcode',
        'description',
        'car_brand',
        'car_model',
        'car_registration_number',
        'flight_number',
        'role',
        'created_at',
        'updated_at'
    ];

    public function reservations()
    {
        return $this->hasMany('\App\Models\Reservation')->orderBy('id', 'DESC')->get();
    }

}
