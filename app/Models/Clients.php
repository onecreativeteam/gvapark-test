<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clients';

    /**
     * Fillables
     */
    protected $fillable = [
        'name',
        'message',
        'rating',
        'is_approved',
        'created_at',
        'updated_at'
    ];

    protected $dates = ['created_at', 'updated_at'];
}
