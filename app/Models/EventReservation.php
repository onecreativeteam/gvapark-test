<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventReservation extends Model
{
	protected $table = 'event_reservations';

    protected $fillable = [
        'event_id',
        'first_name',
        'last_name',
        'car_number',
        'email',
        'date_of_reservation',
        'date_of_arrival',
        'price',
        'vat',
        'payment_status',
        'payment_method',
        'payment_id',
        'system_payment_id',
        'created_at',
        'updated_at'
    ];

    public function event()
    {
        return $this->belongsTo(Event::class, 'event_id');
    }
}
