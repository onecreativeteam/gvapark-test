<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReservationAddition extends Model
{
    protected $table = 'reservation_additions';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'visual_price',
        'description',
        'price',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    // This is configuration for translated resources FK
    // public $fileAttribute = 'logo';

    // This is configuration for model files path
    // public $filePathAttribute = 'uploads' . DIRECTORY_SEPARATOR . 'option_logos' . DIRECTORY_SEPARATOR;

    // This is configuration for translated resources FK
    public $translatableKey = 'reservation_addition_id';

    // This is configuration for translatable fields for model
    public $translatableFields = [
        'name',
        'visual_price',
        'description',
        'slug',
        'locale'
    ];

    // This is querying the translated instance depend on locale
    public function getTranslationsAttribute() {
        return $this->getRelationValue('translations')->keyBy('locale');
    }

    public function translated()
    {
        return $this->hasOne(ReservationAdditionTranslation::class, 'reservation_addition_id')->where('locale', app()->getLocale());
    }

    public function translations()
    {
        return $this->hasMany(ReservationAdditionTranslation::class, 'reservation_addition_id');
    }
}
