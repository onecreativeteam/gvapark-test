<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $table = 'faq';

    public $timestamps = true;

    protected $fillable = [
        'group_id',
        'question',
        'answer',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    // This is configuration for translated resources FK
    // public $fileAttribute = 'picture';

    // This is configuration for model files path
    // public $filePathAttribute = 'uploads' . DIRECTORY_SEPARATOR . 'faq' . DIRECTORY_SEPARATOR;

    // This is configuration for translated resources FK
    public $translatableKey = 'faq_id';

    // This is configuration for translatable fields for model
    public $translatableFields = [
        'question',
        'answer',
        'slug',
        'locale',
    ];

    // This is querying the translated instance depend on locale
    public function getTranslationsAttribute() {
        return $this->getRelationValue('translations')->keyBy('locale');
    }

    public function translated()
    {
        return $this->hasOne(FaqTranslation::class, 'faq_id')->where('locale', app()->getLocale());
    }

    public function translations()
    {
        return $this->hasMany(FaqTranslation::class, 'faq_id');
    }
}
