<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OurService extends Model
{
    protected $table = 'our_service';

    public $timestamps = true;

    protected $fillable = [
        'main_info_title',
        'main_info_description',
        'online_booking_info_title',
        'online_booking_info_description',
        'post_your_vehicle_info_title',
        'post_your_vehicle_info_description',
        'travel_info_title',
        'travel_info_description',
        'pickup_info_title',
        'pickup_info_description',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    // This is configuration for translated resources FK
    // public $fileAttribute = 'picture';

    // This is configuration for model files path
    // public $filePathAttribute = 'uploads' . DIRECTORY_SEPARATOR . 'homepage' . DIRECTORY_SEPARATOR;

    // This is configuration for translated resources FK
    public $translatableKey = 'our_service_id';

    // This is configuration for translatable fields for model
    public $translatableFields = [
        'main_info_title',
        'main_info_description',
        'online_booking_info_title',
        'online_booking_info_description',
        'post_your_vehicle_info_title',
        'post_your_vehicle_info_description',
        'travel_info_title',
        'travel_info_description',
        'pickup_info_title',
        'pickup_info_description',
        'slug',
        'locale',
    ];

    // This is querying the translated instance depend on locale
    public function getTranslationsAttribute() {
        return $this->getRelationValue('translations')->keyBy('locale');
    }

    public function translated()
    {
        return $this->hasOne(OurServiceTranslation::class, 'our_service_id')->where('locale', app()->getLocale());
    }

    public function translations()
    {
        return $this->hasMany(OurServiceTranslation::class, 'our_service_id');
    }
}
