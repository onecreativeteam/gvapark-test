<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseCategory extends Model
{
    protected $table = 'base_categories';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'picture',
        'pictogram_picture',
        'show_in_menu',
        'show_in_homepage',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    // This is configuration for translated resources FK
    public $fileAttribute = 'picture';

    // This is configuration for model files path
    public $filePathAttribute = 'uploads' . DIRECTORY_SEPARATOR . 'base_category' . DIRECTORY_SEPARATOR;
    public $pictogramPathAttribute = 'uploads' . DIRECTORY_SEPARATOR . 'base_category' . DIRECTORY_SEPARATOR . 'pictogram' . DIRECTORY_SEPARATOR;

    // This is configuration for translated resources FK
    public $translatableKey = 'base_categories_id';

    // This is configuration for translatable fields for model
    public $translatableFields = [
        'name',
        'slug',
        'locale',
    ];

    // This is querying the translated instance depend on locale
    public function getTranslationsAttribute() {
        return $this->getRelationValue('translations')->keyBy('locale');
    }

    public function translated()
    {
        return $this->hasOne(BaseCategoryTranslation::class, 'base_categories_id')->where('locale', app()->getLocale());
    }

    public function translations()
    {
        return $this->hasMany(BaseCategoryTranslation::class, 'base_categories_id');
    }

    public function category()
    {
        return $this->hasMany(ProductCategory::class, 'base_categories_id');
    }
}
