<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OurServiceTranslation extends Model
{
    public $timestamps = true;

    protected $table = 'our_service_translations';

    protected $fillable = [
        'our_service_id',
        'main_info_title',
        'main_info_description',
        'online_booking_info_title',
        'online_booking_info_description',
        'post_your_vehicle_info_title',
        'post_your_vehicle_info_description',
        'travel_info_title',
        'travel_info_description',
        'pickup_info_title',
        'pickup_info_description',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    public function parent()
    {
        return $this->belongsTo(OurService::class, 'our_service_id');
    }
}
