<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReservationOption extends Model
{
    protected $table = 'reservation_options';

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'price',
        'created_at',
        'updated_at'
    ];
}
