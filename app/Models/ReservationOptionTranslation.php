<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReservationOptionTranslation extends Model
{
    public $timestamps = true;

    protected $table = 'reservation_options_translations';

    protected $fillable = [
        'reservation_option_id',
        'name',
        'description',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    public function parent()
    {
        return $this->belongsTo(ReservationOption::class, 'reservation_option_id');
    }
}
