<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Concurent extends Model
{
    protected $table = 'concurents';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'initial_price',
        'daily_price',
        'daily_price_sup',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    // This is configuration for translated resources FK
    // public $fileAttribute = 'picture';

    // This is configuration for model files path
    // public $filePathAttribute = 'uploads' . DIRECTORY_SEPARATOR . 'concurent' . DIRECTORY_SEPARATOR;

    // This is configuration for translated resources FK
    public $translatableKey = 'concurents_id';

    // This is configuration for translatable fields for model
    public $translatableFields = [
        'name',
        'initial_price',
        'daily_price',
        'daily_price_sup',
        'slug',
        'locale',
    ];

    // This is querying the translated instance depend on locale
    public function getTranslationsAttribute() {
        return $this->getRelationValue('translations')->keyBy('locale');
    }

    public function translated()
    {
        return $this->hasOne(ConcurentTranslation::class, 'concurents_id')->where('locale', app()->getLocale());
    }

    public function translations()
    {
        return $this->hasMany(ConcurentTranslation::class, 'concurents_id');
    }
}
