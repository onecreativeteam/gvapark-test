<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReservationGallery extends Model
{
    protected $table = 'reservations_gallery';

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reservations_id',
        'picture',
        'created_at',
        'updated_at'
    ];
}
