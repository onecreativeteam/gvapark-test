<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomepageInformation extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'homepage_information';

    /**
     * Fillables
     */
    protected $fillable = [
        'title',
        'description',
        'security_info',
        'simplicity_info',
        'speed_info',
        'service_info',
        'created_at',
        'updated_at'
    ];
}
