<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Homepage extends Model
{
    protected $table = 'homepage';

    public $timestamps = true;

    protected $fillable = [
        'title',
        'description',
        'security_info',
        'simplicity_info',
        'speed_info',
        'service_info',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    // This is configuration for translated resources FK
    // public $fileAttribute = 'picture';

    // This is configuration for model files path
    // public $filePathAttribute = 'uploads' . DIRECTORY_SEPARATOR . 'homepage' . DIRECTORY_SEPARATOR;

    // This is configuration for translated resources FK
    public $translatableKey = 'homepage_id';

    // This is configuration for translatable fields for model
    public $translatableFields = [
        'title',
        'description',
        'security_info',
        'simplicity_info',
        'speed_info',
        'service_info',
        'slug',
        'locale',
    ];

    // This is querying the translated instance depend on locale
    public function getTranslationsAttribute() {
        return $this->getRelationValue('translations')->keyBy('locale');
    }

    public function translated()
    {
        return $this->hasOne(HomepageTranslation::class, 'homepage_id')->where('locale', app()->getLocale());
    }

    public function translations()
    {
        return $this->hasMany(HomepageTranslation::class, 'homepage_id');
    }
}
