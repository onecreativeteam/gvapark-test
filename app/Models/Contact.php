<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contact';

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'address1',
        'address2',
        'telephone',
        'email',
        'navet_address1',
        'navet_address2',
        'navet_telephone',
        'navet_email',
        'created_at',
        'updated_at'
    ];
}
