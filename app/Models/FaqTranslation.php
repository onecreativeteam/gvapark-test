<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FaqTranslation extends Model
{
    public $timestamps = true;

    protected $table = 'faq_translations';

    protected $fillable = [
        'faq_id',
        'question',
        'answer',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    public function parent()
    {
        return $this->belongsTo(Faq::class, 'faq_id');
    }
}
