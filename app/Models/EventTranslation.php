<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventTranslation extends Model
{
    public $timestamps = true;

    protected $table = 'events_translations';

    protected $fillable = [
        'event_id',
        'name',
        'description',
        'short_description',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    public function parent()
    {
        return $this->belongsTo(Event::class, 'event_id');
    }
}
