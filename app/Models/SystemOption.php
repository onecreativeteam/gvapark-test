<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SystemOption extends Model
{
    protected $table = 'system_options';

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parking_spots_count',
        'valet_max_count',
        'initial_price',
        'daily_price',
        'vip_initial_price',
        'vip_daily_price',
        'reservation_have_options',
        'extereur_price',
        'intereur_price',
        'extra_price',
        'electric_charge_price',
        'created_at',
        'updated_at'
    ];
}
