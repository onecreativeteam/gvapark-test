<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DatePrice extends Model
{
    protected $table = 'date_prices';

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date_from',
        'date_to',
        'price',
        'type',
        'created_at',
        'updated_at'
    ];
}
