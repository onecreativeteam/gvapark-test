<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Reservation extends Model
{
    protected $table = 'reservations';

    public $timestamps = true;

    protected $appends = ['allowAnnulation', 'arrivedByUserName', 'returnedByUserName'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'park_place_id',
        'reservation_options_id',
        'reservation_option',
        'has_electric',
        'user_id',
        'email',
        'client_first_name',
        'client_last_name',
        'promo_code_id',
        'date_of_reservation',
        'date_of_arrival',
        'date_of_departure',
        'days_to_stay',
        'price_to_charge',
        'vat',
        'reservation_type',
        'payment_status',
        'payment_method',
        'payment_id',
        'system_payment_id',
        'changed_price_to_charge',
        'is_arrived',
        'arrived_by_user',
        'is_returned',
        'returned_by_user',
        'returned_at',
        'is_canceled',
        'has_gallery',
        'applied_promo',
        'arrived_at',
        'picture',
        'created_at',
        'updated_at'
    ];

    /**
     * Convert date format without time
     *
     * @return string $date
     */
    public function getDateOfReservationAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:s');
    }

    /**
     * Convert date format without time
     *
     * @return string $date
     */
    public function getDateOfArrivalAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:s');
    }

    /**
     * Convert date format without time
     *
     * @return string $date
     */
    public function getDateOfDepartureAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d H:i:s');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function promocode()
    {
        return $this->belongsTo(PromoCode::class, 'promo_code_id');
    }

    public function parkPlace()
    {
        return $this->belongsTo(ParkPlace::class, 'park_place_id', 'name'); // coment name before update parkPlace
    }

    public function payment()
    {
        return $this->hasOne(ReservationPayment::class, 'reservation_id');
    }

    public function getAllowAnnulationAttribute()
    {
        $now = strtotime(Carbon::now()->format('Y/m/d H:i'));
        $arrival = strtotime(Carbon::parse($this->date_of_arrival)->subHours(24)->format('Y/m/d H:i'));

        if ($now > $arrival) {
            return 0;
        }

        return 1;
    }

    public function getArrivedByUserNameAttribute()
    {
        $user = $this->belongsTo(User::class, 'arrived_by_user')->first();

        return $user['name'];
    }

    public function getReturnedByUserNameAttribute()
    {
        $user = $this->belongsTo(User::class, 'returned_by_user')->first();

        return $user['name'];
    }
}
