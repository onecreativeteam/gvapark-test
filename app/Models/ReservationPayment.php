<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReservationPayment extends Model
{
    protected $table = 'reservation_payments';

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reservation_id',
        'amount',
        'pmethod',
        'mode',
        'sign',
        'refno',
        'returnCustomerCountry',
        'reqtype',
        'acqAuthorizationCode',
        'theme',
        'uppReturnTarget',
        'responseMessage',
        'uppTransactionId',
        'responseCode',
        'expy',
        'merchantId',
        'currency',
        'expm',
        'version',
        'authorizationCode',
        'status',
        'uppMsgType',
        'created_at',
        'updated_at'
    ];

    public function reservation()
    {
        return $this->belongsTo(Reservation::class, 'reservation_id');
    }
}
