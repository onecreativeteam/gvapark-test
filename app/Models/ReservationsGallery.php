<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReservationsGallery extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'reservations_gallery';

    /**
     * Fillables
     */
    protected $fillable = [
        'reservations_id',
        'picture'
    ];
}
