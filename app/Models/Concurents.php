<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Concurents extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'concurents';

    /**
     * Fillables
     */
    protected $fillable = [
        'initial_price',
        'daily_price',
        'daily_price_sup',
        'name',
        'created_at',
        'updated_at'
    ];

}
