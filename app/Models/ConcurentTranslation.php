<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConcurentTranslation extends Model
{
    public $timestamps = true;

    protected $table = 'concurents_translations';

    protected $fillable = [
        'concurents_id',
        'name',
        'initial_price',
        'daily_price',
        'daily_price_sup',
        'slug',
        'locale',
        'created_at',
        'updated_at'
    ];

    public function parent()
    {
        return $this->belongsTo(Concurent::class, 'concurents_id');
    }
}
