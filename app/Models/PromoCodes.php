<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class PromoCodes extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'promo_codes';

    /**
     * Fillables
     */
    protected $fillable = [
        'code',
        'start_date',
        'end_date',
        'promotion_value',
        'created_at',
        'updated_at'
    ];

    /**
     * Convert date format without time
     *
     * @return string $date
     */
    public function getStartDateAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d');
    }

    /**
     * Convert date format without time
     *
     * @return string $date
     */
    public function getEndDateAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d');
    }
}
