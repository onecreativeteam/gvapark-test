<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\HomepageInformation;

class HomepageInformationRepository extends BaseRepository
{
    /**
    * @var App\Models\HomepageInformation $modelClass;
    */
    protected $modelClass = HomepageInformation::class;

    public function getHomepageInformation()
    {
        return $this->getModel()->first();
    }
}
