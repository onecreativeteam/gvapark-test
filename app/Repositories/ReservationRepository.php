<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\Reservation;

use DB;
use DateTime;
use Carbon\Carbon;

class ReservationRepository extends BaseRepository
{
    /**
    * @var App\Models\Reservation $modelClass;
    */
    protected $modelClass = Reservation::class;

    public function createOrUpdate($reservationData)
    {
        if (is_null(session('createdReservation'))) {

            $reservation = $this->getModel()->create($reservationData);
            session()->put('createdReservation', $reservation);

        } else {

            $reservation = $this->updateRecord(session('createdReservation'), $reservationData);
            session()->put('createdReservation', $reservation);

        }

        return $reservation;
    }

    public function getAll()
    {
        return $this->getModel()->all();
    }

    public function clearParkPlaces()
    {
        $reservations = $this->getModel()->whereHas('parkPlace')->get();

        $now = strtotime(Carbon::now()->format('Y/m/d H:i'));
        
        if (count($reservations) > 0) {
            foreach ($reservations as $reservation)
            {
                if (!is_null($reservation->parkPlace)) {

                    $checkData = strtotime(Carbon::parse($reservation->date_of_departure)->addHours(6)->format('Y/m/d H:i'));

                    if ($checkData < $now) {
                        $reservation->park_place_id = null;
                        $reservation->save();
                    }

                }
            }
        }
    }

    public function getAllOrdered()
    {
        return $this->getModel()->orderBy('id', 'desc')->paginate(10);
    }

    public function getAllOrderedDT()
    {
        return $this->getModel()
                ->select('client_first_name', 'client_last_name', 'id', 'date_of_reservation', 'date_of_arrival', 'date_of_departure', 'days_to_stay', 'payment_status', 'reservation_type', 'price_to_charge', 'changed_price_to_charge', 'has_gallery', 'is_canceled')
                ->with('payment')
                ->orderBy('id', 'desc');
                // ->get();
    }

    public function getAllCalendar($type)
    {
        $now = Carbon::now()->format('Y-m-d');
        $start = date('Y-m-d', strtotime("-2 months", strtotime($now)));
        $end = date('Y-m-d', strtotime("+2 months", strtotime($now)));
        
        return $this->getModel()
                ->where('payment_status', '=', 'completed')
                ->where('reservation_type', '=', $type)
                ->whereDate('date_of_arrival', '>=', $start)
                ->whereDate('date_of_arrival', '<=', $end)
                // This is commented because of request for hidden reservations
                // ->where(function ($query) {
                //     $query->whereDate('date_of_arrival', '>=', Carbon::now()->format('Y-m-d'))->orWhere('is_arrived', '=', '1');
                // })
                ->orderBy('date_of_arrival', 'ASC')->with('user', 'parkPlace')->get()->toArray();
    }

    public function getJson()
    {
        return $this->getModel()->all()->toJson();
    }

    public function whereBetween($field, $start, $end)
    {
        return $this->getModel()->whereBetween($field, array($start, $end))
        ->select('client_first_name', 'client_last_name', 'email', 'date_of_reservation', 'days_to_stay', 'price_to_charge', 'changed_price_to_charge')
        ->get();
    }

    public function getYears()
    {
        return $this->getModel()->select([ DB::raw('YEAR(date_of_reservation) as year') ])
                ->distinct()
                ->orderBy('year', 'asc')
                ->get()
                ->toArray();
    }

    public function getMonths()
    {
        return $this->getModel()->select([ DB::raw('MONTH(date_of_reservation) as month') ])
                ->distinct()
                ->orderBy('month', 'asc')
                ->get()
                ->toArray();
    }

    public function getMonthsByYear($year)
    {
        return $this->getModel()->select([ DB::raw('MONTH(date_of_reservation) as month') ])
                ->whereYear('date_of_reservation', '=', $year)
                ->distinct()
                ->orderBy('month', 'asc')
                ->get()
                ->toArray();
    }

    public function getDayProfitByDate($year, $month)
    {
        $data = $this->getModel()->select([
                DB::raw('DAY(date_of_reservation) AS day'),
                DB::raw('SUM(price_to_charge) AS profit'),
            ])
            ->whereYear('date_of_reservation', '=', $year)
            ->whereMonth('date_of_reservation', '=', $month)
            ->where('payment_status', '=', 'completed')
            ->groupBy('day')
            ->orderBy('day', 'ASC')
            ->get()
            ->toArray();

        $returnArray = [];
        foreach ($data as $k => $array) {
            $array['days_per_month'] = date('t');
            $returnArray[] = $array;
        }

        return $returnArray;
    }

    public function getMonthProfitByYear($year)
    {
        $data = $this->getModel()->select([
                DB::raw('MONTH(date_of_reservation) AS month'),
                DB::raw('SUM(price_to_charge) AS profit'),
            ])
            ->whereYear('date_of_reservation', '=', $year)
            ->groupBy('month')
            ->orderBy('month', 'ASC')
            ->get()
            ->toArray();

        $returnArray = [];
        foreach ($data as $k => $array) {
            $returnArray[] = $array;
        }

        return $returnArray;
    }

    public function getTotalReservationsProfitByDate($year, $month)
    {
        return $this->getModel()
                ->whereYear('date_of_reservation', '=', $year)
                ->whereMonth('date_of_reservation', '=', $month)
                ->where('payment_status', '=', 'completed')
                ->select(DB::raw('SUM(price_to_charge) as total_profit'))
                ->get()
                ->toArray();
    }

    public function getEachDayParkingOccupationCount($request)
    {
        $dates = self::dateRange($request->date_of_arrival, $request->date_of_departure);
        $systemOptionsParkingCount = DB::table('system_options')->first()->parking_spots_count;
        //$systemOptionsValetCount = DB::table('system_options')->first()->valet_max_count;

        $retour_validation = self::countplaceoccupatedvaletdays($request);
        $errors = [];
        $disable_valet = 0;
        foreach ($dates as $k => $date) {

            $parkingOccupationCount = DB::table('reservations')
                ->whereDate('date_of_departure', '>=', $date)
                ->whereDate('date_of_arrival', '<=', $date)
                ->where('payment_status', '=', 'completed')
                ->where('is_returned', '!=', '1')
                ->count();

            $count = $systemOptionsParkingCount - $parkingOccupationCount;

            if ($count <= 0) {
                $errors[$date] = 'Il n\'y a pas de places de stationnement disponibles à la date du - '.$date.'.';
            }

        }

        //&& !$request->has('request_homepage')
        if ($retour_validation==false) {
            $disable_valet = 1;
        }
        
        return array('errors'=>$errors, 'disable_valet'=>$disable_valet);
    }

    public function countplaceoccupatedvaletdays($request)
    {

        $systemOptionsValetCount = DB::table('system_options')->first()->valet_max_count;
        
        //For arrival
        $datetime_arrival=$request->date_of_arrival." ".$request->hour_of_arrival;
        $valetOccupationtimearrival = self::countplaceviphourdays($datetime_arrival);
        //DEPARTURE:
        $datetime_departure=$request->date_of_departure." ".$request->hour_of_departure;
        $valetOccupationtimedepartured = self::countplaceviphourdays($datetime_departure);

        if ($valetOccupationtimearrival < $systemOptionsValetCount AND $valetOccupationtimedepartured < $systemOptionsValetCount) {
            return true;
        }else{
            return false;
        }
    }

    public function countplaceviphourdays($datetime_valet)
    {
        $numMinutes = 59;
        $date_more = new DateTime($datetime_valet);
        $date_less = clone $date_more;
        $date_more->modify ("+{$numMinutes} minutes");
        $date_less->modify ("-{$numMinutes} minutes");

        $vipOccupationtime = DB::table('reservations')
            ->where(function($query) use ($date_less,$date_more){
                $query->whereBetween('date_of_departure', [$date_less, $date_more])
                ->orWhereBetween('date_of_arrival', [$date_less, $date_more]);
                    }) 
            ->where('reservation_type', '=', 'valet')
            ->where('payment_status', '=', 'completed')
            ->where('is_returned', '!=', '1')
            ->count();

        return $vipOccupationtime;
    }

    public function checkByDay($date, $hour = "00", $minute = "00")
    {
        return DB::table('reservations')
        ->where('date_of_departure', '>=', $date)
        ->where('date_of_arrival', '<=', $date)
        ->where('payment_status', '=', 'completed')
        // ->where('is_arrived', '!=', '0')
        ->where('is_returned', '!=', '1')
        ->count();
    }

    public function dateRange($first, $last, $step = '+1 day', $format = 'Y-m-d')
    {

        $dates = array();
        $current = strtotime( $first );
        $last = strtotime( $last );

        while ($current <= $last) {
            $dates[] = date( $format, $current );
            $current = strtotime( $step, $current );
        }

        return $dates;
    }

    public function validateFreeSpots($request)
    {
        $count = self::getEachDayParkingOccupationCount($request);
        
        if (count($count) > 0) {
            return $count;
        }

        return true;
    }

    public function setSessionDates($request)
    {
        $data = [];

        $data['date_of_arrival']        = $request->date_of_arrival ? $request->date_of_arrival : Carbon::now()->format('Y/m/d');
        $data['hour_of_arrival']        = $request->hour_of_arrival ? $request->hour_of_arrival : '00:00';

        $data['date_of_departure']      = $request->date_of_departure ? $request->date_of_departure : Carbon::now()->format('Y/m/d')->addDay();
        $data['hour_of_departure']      = $request->hour_of_departure ? $request->hour_of_departure : '00:00';

        $data['full_date_of_arrival']   = $request->date_of_arrival.' '.$request->hour_of_arrival.':00';
        $data['full_date_of_departure'] = $request->date_of_departure .' '.$request->hour_of_departure.':00';
        $data['options']                = $request->options ? $request->options : null;

        $data['has_electric']           = $request->has_electric ? $request->has_electric : null;

        $data['reservation_type']       = $request->reservation_type ? $request->reservation_type : 'valet';

        session()->put('selectedDates', $data);

        return $data;
    }

    public function checkMailConnection()
    {
        try {
            $transport = \Swift_SmtpTransport::newInstance('mail.infomaniak.ch', '587', 'tls');
            $transport->setUsername('service@gvapark.ch');
            $transport->setPassword('GVApark2017$');
            $mailer = \Swift_Mailer::newInstance($transport);
            $mailer->getTransport()->start();
            return true;
        } catch (\Swift_TransportException $e) {
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function sendReviewMail($reservation)
    {
        // Do not modify this part these keys are those of GVA park
        $ID_WEBSITE = "1ba5dcf9-461f-17c4-d903-78e3a32c285a"; // Please fill in the ID of your website "Verified Reviews"
        $SECURE_KEY = "1389fe5c-b95e-2f34-3982-fd129fe23ebf"; // Please fill in the Secret key of your website "Verified Reviews"

        $URL_AV = "http://www.avis-verifies.com/index.php";

        // The order reference
        $order_ref = $reservation->payment_id ? $reservation->payment_id : 'Reservation';
        $email = $reservation->user->email ? $reservation->user->email : 'borislav.onecreative@gmail.com';
        $firstname = $reservation->user->first_name ? $reservation->user->first_name : 'Undefined';
        $lastname = $reservation->user->last_name ? $reservation->user->last_name : 'Undefined';
        //The date of the order YYYY-MM-DD HH: MM: SS if you do not have the time, put 0
        $order_date = $reservation->date_of_reservation ? $reservation->date_of_reservation : Carbon::now()->format('Y-m-d H:i:s');

        /**
        *
        * Send a request
        *
        **/
        $descNotification =  array(
                'query'            => 'pushCommandeSHA1',   //Required
                'order_ref'        => $order_ref,           //Required - Reference order
                'email'            => $email,               //Required - Client email
                'lastname'         => $lastname,            //Required -  Client lastname
                'firstname'        => $firstname,           //Required -  Client firstname
                'order_date'       => $order_date,          //Required - Format YYYY-MM-JJ HH:MM:SS
                'delay'            => '0',                  //0=Immediately / days between 1 and 30 days
                'sign'             => '',
        );

        $descNotification['sign']=SHA1($descNotification['query'].$descNotification['order_ref'].$descNotification['email'].$descNotification['lastname'].$descNotification['firstname'].$descNotification['order_date'].$descNotification['delay'].$SECURE_KEY);

        $encryptedNotification=http_build_query(
            array(
                'idWebsite' => $ID_WEBSITE,
                'message' => json_encode($descNotification)
            )
        );

        $postNotification = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => $encryptedNotification
            )
        );

        $contextNotification = stream_context_create($postNotification);

        $resultNotification = file_get_contents($URL_AV.'?action=act_api_notification_sha1&type=json2', false, $contextNotification);

        $resultNotification = json_decode($resultNotification, true);

        // $fullstring = $resultNotification['debug'];
        // $parsed = self::get_string_between($fullstring, 'SignatureRec[', ']');
    }

    public function get_string_between($string, $start, $end)
    {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) {
            return '';
        }
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }
}
