<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\PromoCode;

class PromoCodeRepository extends BaseRepository
{
    /**
    * @var App\Models\PromoCode $modelClass;
    */
    protected $modelClass = PromoCode::class;

    public function getAll()
    {
        return $this->getModel()->all();
    }
}
