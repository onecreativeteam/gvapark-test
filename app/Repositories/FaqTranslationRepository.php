<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\FaqTranslation;

class FaqTranslationRepository extends BaseRepository
{
    /**
    * @var App\Models\FaqTranslation $modelClass;
    */
    protected $modelClass = FaqTranslation::class;
}
