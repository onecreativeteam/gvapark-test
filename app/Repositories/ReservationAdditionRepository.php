<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\ReservationAddition;

class ReservationAdditionRepository extends BaseRepository
{
    /**
    * @var App\Models\ReservationAddition $modelClass;
    */
    protected $modelClass = ReservationAddition::class;
}
