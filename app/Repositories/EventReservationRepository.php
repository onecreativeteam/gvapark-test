<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\EventReservation;

use Carbon\Carbon;

class EventReservationRepository extends BaseRepository
{
    /**
    * @var App\Models\EventReservation $modelClass;
    */
    protected $modelClass = EventReservation::class;

    public function prepareReservationData($request, $event)
    {
    	$data 						= [];
    	$data['event_id'] 			= $event->id;
    	$data['first_name'] 		= $request->first_name;
    	$data['last_name'] 			= $request->last_name;
    	$data['car_number'] 		= $request->car_number;
    	$data['email'] 				= $request->email;
    	$data['date_of_reservation'] = Carbon::now()->format('Y-m-d H:i:s');
    	$data['date_of_arrival'] 	= Carbon::parse($request->date_of_arrival)->format('Y-m-d H:i:s');
    	$data['price'] 				= $event->price;
    	$data['vat'] 				= (7.7 / 100) * $event->price;
    	$data['payment_status'] 	= 'pending';

    	return $data;
    }

    public function preparePdfData($reservation)
    {
        $pdfData                            = [];
        $pdfData['clientName']              = $reservation->first_name.' '.$reservation->last_name;
        $pdfData['email']                   = $reservation->email;
        $pdfData['reservation_id']          = $reservation->id;
        $pdfData['system_payment_id']       = $reservation->system_payment_id;
        $pdfData['date_of_reservation']     = Carbon::parse($reservation->date_of_reservation)->format('Y-m-d');
        $pdfData['payment_method']          = $reservation->payment_method ? $reservation->payment_method : 'datatrans';
        $pdfData['car_registration_number'] = $reservation->car_number;
        $pdfData['price']                   = number_format($reservation->price - $reservation->vat, 2);;
        $pdfData['vat']                     = $reservation->vat;
        $pdfData['totalWithVat']            = number_format($reservation->price, 2);
        $pdfData['tvaValue']                = '7.7';
        $pdfData['date_of_arrival']         = Carbon::parse($reservation->date_of_arrival)->format('Y-m-d');

        return $pdfData;
    }
}
