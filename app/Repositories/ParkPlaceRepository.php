<?php
namespace App\Repositories;

use App\Models\ParkPlace;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class ParkPlaceRepository extends BaseRepository
{
    /**
     * @var ParkPlace $modelClass ;
     */
    protected $modelClass = ParkPlace::class;

    public function getNonEmptyParkPlaces(array $parkPlaces)
    {
        return $this->getModel()
            ->join('reservations as r', 'r.park_place_id', '=', 'park_places.id')
            ->where('date_of_arrival', '<=', Carbon::now())
            ->where('date_of_departure', '>=', Carbon::now())
            ->where('is_arrived', true)
            ->whereIn('park_places.name', $parkPlaces)
            ->get(['park_places.*']);
    }

    public function last()
    {
        return $this->getModel()->orderByRaw('cast(name as unsigned) desc')->first();
    }

    public function createMultiple($count)
    {
        $last = $this->last();
        $name = $last === null ? 0 : $last->name;

        for ($i = 0; $i < $count; $i++) {
            $this->create(['name' => ++$name]);
        }
    }

    public function deleteMultiple($parkPlaces)
    {
        $this->getModel()->whereIn('name', $parkPlaces)->delete();
    }

    public function findByName($name)
    {
        return $this->getModel()->where('name', $name)->first();
    }

    public function checkPlaceIsEmpty($parkPlace)
    {
        /** @var Collection $parkReservations */
        $parkReservations = $this->getModel()
            ->join('reservations', 'park_place_id', '=', 'park_places.name')
            ->where('park_places.name', $parkPlace)
            ->where('is_arrived', true)
            ->where('date_of_departure', '>=', Carbon::now())
            ->get();

        return $parkReservations->isEmpty() ? true : false;
    }
}
