<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\ReservationGallery;

class ReservationGalleryRepository extends BaseRepository
{
    /**
    * @var App\Models\ReservationGallery $modelClass;
    */
    protected $modelClass = ReservationGallery::class;

    public function getAll()
    {
        return $this->getModel()->all();
    }

    public function groupBy($attr)
    {
        return $this->getModel()->get()->groupBy($attr);
    }

    public function getPictures($id)
    {
        return $this->getModel()->where('reservations_id', $id)->get()->toArray();
    }
}
