<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\ReservationsGallery;

class ReservationsGalleryRepository extends BaseRepository
{
    /**
    * @var App\Models\ReservationsGallery $modelClass;
    */
    protected $modelClass = ReservationsGallery::class;

    public function getAll()
    {
        return $this->getModel()->all();
    }

    public function groupBy($attr)
    {
        return $this->getModel()->get()->groupBy($attr);
    }

    public function getPictures($id)
    {
        return $this->getModel()->where('reservations_id', $id)->get()->toArray();
    }
}
