<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Query\Builder;
use Illuminate\Log\Writer;
use Illuminate\Events\Dispatcher;

/**
 * Class BaseRepository
 *
 * @package App\Repositories
 */
class BaseRepository extends Repository
{
    /**
    * where clause
    */
    public function where($params)
    {
        return $this->getModel()->where($params);
    }
    
    public function allWith($relations = [])
    {
        return $this->getModel()->with($relations)->get();
    }

    public function with($relations = [])
    {
        return $this->getModel()->with($relations);
    }

    public function findBy($attr, $param, $method = 'get', $relations = [])
    {
        return $this->getModel()->with($relations)->where($attr, $param)->$method();
    }
    
    /**
    *  fetch all data about user
    */
    public function getUserInfo($id)
    {
        return $this->getModel()
                    ->where('user_roles_id', $id)
                    ->firstOrFail()->toArray();
    }
    /**
     * get all users with given status
     */
    // public function getUser($relation, $attribute, $value, $method = 'get')
    // {
    //     return $this->getModel()->whereHas($relation, function ($query) use ($attribute, $value) {
    //           $query->where($attribute, '>=', $value);
    //     })->$method();
    // }

    public function firstOrCreate($params)
    {
        return $this->getModel()->firstOrCreate($params);
    }

    public function whereLike($collum, $param)
    {
        return $this->getModel()
                      ->where($collum, "LIKE", $param);
    }
}
