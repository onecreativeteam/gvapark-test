<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\OurService;

class OurServiceRepository extends BaseRepository
{
    /**
    * @var App\Models\OurService $modelClass;
    */
    protected $modelClass = OurService::class;
}
