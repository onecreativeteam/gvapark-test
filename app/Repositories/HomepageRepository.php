<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\Homepage;

class HomepageRepository extends BaseRepository
{
    /**
    * @var App\Models\Homepage $modelClass;
    */
    protected $modelClass = Homepage::class;

    public function getHomepageInformation()
    {
        return $this->getModel()->first();
    }
}
