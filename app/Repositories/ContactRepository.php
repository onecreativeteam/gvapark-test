<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\Contact;

class ContactRepository extends BaseRepository
{
    /**
    * @var App\Models\Reservation $modelClass;
    */
    protected $modelClass = Contact::class;
}
