<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\Concurents;

class ConcurentsRepository extends BaseRepository
{
    /**
    * @var App\Models\Concurents $modelClass;
    */
    protected $modelClass = Concurents::class;

    public function getAll()
    {
        return $this->getModel()->all();
    }
    
}
