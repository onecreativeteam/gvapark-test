<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\ReservationClients;

class ReservationClientsRepository extends BaseRepository
{
    /**
    * @var App\Models\Reservation $modelClass;
    */
    protected $modelClass = ReservationClients::class;

    public function getAll()
    {
        return $this->getModel()->all();
    }

    public function getByEmail($email)
    {
        return $this->getModel()->where('email', '=', $email)->first()->toArray();
    }
}
