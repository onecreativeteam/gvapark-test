<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\Event;

class EventRepository extends BaseRepository
{
    /**
    * @var App\Models\Event $modelClass;
    */
    protected $modelClass = Event::class;
}
