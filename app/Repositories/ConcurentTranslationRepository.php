<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\ConcurentTranslation;

class ConcurentTranslationRepository extends BaseRepository
{
    /**
    * @var App\Models\ConcurentTranslation $modelClass;
    */
    protected $modelClass = ConcurentTranslation::class;
}
