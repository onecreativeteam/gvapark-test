<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\DatePrice;

use DateTime;

class DatePriceRepository extends BaseRepository
{
    /**
    * @var App\Models\DatePrice $modelClass;
    */
    protected $modelClass = DatePrice::class;

    public function getPricesByType($type)
    {
        return $this->getModel()->where('type', '=', $type)->orderBy('price', 'asc')->get();
    }

    /*
        Accept 2 params:
            - day/s
            - type of reservation
    */ 
    public function getPrice($day, $type)
    {
        $price = $this->getModel()->where('type', '=', $type)->where('day', '=', $day)->first();

        if (!is_null($price)) {
            return number_format((float)$price->price, 2, '.', '');
        } else {
            return 0;
        }
    }

    // This Function check if the given date range from $request is not available(free)
    // and return the objects which are already in this date range.
    public function validateDateRange($request, $id = null)
    {
        if (!is_null($id)) {
            $collection     = $this->getModel()->where('type', '=', $request->type)->where('id', '!=', $id)->get();
        } else {
            $collection     = $this->getModel()->where('type', '=', $request->type)->get();
        }

        $validateFrom   = new DateTime($request->date_from);
        $validateTo     = new DateTime($request->date_to);

        $overLapDates   = $collection->filter(function ($datePriceObject, $key) use ($validateFrom, $validateTo) {

            $dateFrom   = new DateTime($datePriceObject->date_from);
            $dateTo     = new DateTime($datePriceObject->date_to);

            // dd(
            // $dateFrom,
            // $dateTo,
            // self::isDateBetweenDates($validateFrom, $dateFrom, $dateTo),
            // self::isDateBetweenDates($validateTo, $dateFrom, $dateTo)
            // );

            if (self::isDateBetweenDates($validateFrom, $dateFrom, $dateTo) || self::isDateBetweenDates($validateTo, $dateFrom, $dateTo))
            {
                return $datePriceObject;
            }

        });

        return count($overLapDates) == 0;
    }

    public function isDateBetweenDates(DateTime $validateDate, DateTime $startDate, DateTime $endDate) {
        return $validateDate >= $startDate && $validateDate <= $endDate;
    }
}
