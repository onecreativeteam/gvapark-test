<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\OurServiceTranslation;

class OurServiceTranslationRepository extends BaseRepository
{
    /**
    * @var App\Models\OurServiceTranslation $modelClass;
    */
    protected $modelClass = OurServiceTranslation::class;
}
