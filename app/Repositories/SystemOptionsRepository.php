<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\SystemOptions;

class SystemOptionsRepository extends BaseRepository
{
    /**
    * @var App\Models\Reservation $modelClass;
    */
    protected $modelClass = SystemOptions::class;

    public function getAll()
    {
        return $this->getModel()->all();
    }

    public function getDailyTax()
    {
        return $this->getModel()->first()->daily_price;
    }

    public function getInitialTax()
    {
        return $this->getModel()->first()->initial_price;
    }

    public function getExtereurTax()
    {
        return $this->getModel()->first()->extereur_price;
    }

    public function getIntereurTax()
    {
        return $this->getModel()->first()->intereur_price;
    }

    public function getExtraTax()
    {
        return $this->getModel()->first()->extra_price;
    }
}
