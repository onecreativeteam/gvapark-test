<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\HomepageTranslation;

class HomepageTranslationRepository extends BaseRepository
{
    /**
    * @var App\Models\HomepageTranslation $modelClass;
    */
    protected $modelClass = HomepageTranslation::class;
}
