<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\ParkingService;

class ParkingServiceRepository extends BaseRepository
{
    /**
    * @var App\Models\Reservation $modelClass;
    */
    protected $modelClass = ParkingService::class;
}
