<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\Faq;

class FaqRepository extends BaseRepository
{
    /**
    * @var App\Models\Faq $modelClass;
    */
    protected $modelClass = Faq::class;

    public function getAll()
    {
        return $this->getModel()->all();
    }

    public function groupBy($attr)
    {
        return $this->getModel()->get()->groupBy($attr);
    }
}
