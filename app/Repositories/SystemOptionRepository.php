<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\SystemOption;

class SystemOptionRepository extends BaseRepository
{
    /**
    * @var App\Models\SystemOption $modelClass;
    */
    protected $modelClass = SystemOption::class;

    public function getAll()
    {
        return $this->getModel()->all();
    }

    public function getDailyTax()
    {
        return number_format($this->getModel()->first()->daily_price, 2);
    }

    public function getInitialTax()
    {
        return number_format($this->getModel()->first()->initial_price, 2);
    }

    // VIP
    public function getVipDailyTax()
    {
        return number_format($this->getModel()->first()->vip_daily_price, 2);
    }

    // VIP
    public function getVipInitialTax()
    {
        return number_format($this->getModel()->first()->vip_initial_price, 2);
    }

    public function getExtereurTax()
    {
        return number_format($this->getModel()->first()->extereur_price, 2);
    }

    public function getIntereurTax()
    {
        return number_format($this->getModel()->first()->intereur_price, 2);
    }

    public function getExtraTax()
    {
        return number_format($this->getModel()->first()->extra_price, 2);
    }

    public function getElectricChargeTax()
    {
        return number_format($this->getModel()->first()->electric_charge_price, 2);
    }

    public function getReservationMaxDays()
    {
        return $this->getModel()->first()->reservation_prices_count;
    }
}
