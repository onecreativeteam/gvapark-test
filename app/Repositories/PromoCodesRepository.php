<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\PromoCodes;

class PromoCodesRepository extends BaseRepository
{
    /**
    * @var App\Models\Reservation $modelClass;
    */
    protected $modelClass = PromoCodes::class;

    public function getAll()
    {
        return $this->getModel()->all();
    }
}
