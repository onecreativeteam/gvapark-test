<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\ReservationPrice;

class ReservationPriceRepository extends BaseRepository
{
    /**
    * @var App\Models\ReservationPrice $modelClass;
    */
    protected $modelClass = ReservationPrice::class;

    public function getPricesByType($type)
    {
        return $this->getModel()->where('type', '=', $type)->orderBy('day', 'asc')->get();
    }

    public function getTablePricesArray()
    {
        $valetPrices = $this->getPricesByType('valet')->take(20)->toArray();
        $navetPrices = $this->getPricesByType('navet')->take(20)->toArray();
        $simpleArray = [];

        for ($i = 1; $i <= count($valetPrices); $i++) {
            $simpleArray[$i]['valet_price'] = $valetPrices[$i-1]['price'];
            $simpleArray[$i]['navet_price'] = $navetPrices[$i-1]['price'];
        }

        return $simpleArray;
    }

    public function savePrices($request)
    {
        $insertArray    = array();
        $type           = $request->type;

        foreach ($request->prices as $day => $price) {
            $current            = [];
            $current['day']     = $day;
            $current['price']   = $price['price'];
            $current['type']    = $type;

            array_push($insertArray, $current);
        }

        $this->getModel()->insert($insertArray);
    }

    /*
        Accept 2 params:
            - day/s
            - type of reservation
    */ 
    public function getPrice($day, $type)
    {
        $price = $this->getModel()->where('type', '=', $type)->where('day', '=', $day)->first();

        if (!is_null($price)) {
            return number_format((float)$price->price, 2, '.', '');
        } else {
            return 0;
        }
    }
}
