<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\Clients;

class ClientsRepository extends BaseRepository
{
    /**
    * @var App\Models\Reservation $modelClass;
    */
    protected $modelClass = Clients::class;

    public function getAll()
    {
        return $this->getModel()->all();
    }

    public function getApproved()
    {
        return $this->getModel()->where('is_approved', '=', '1')->orderBy('id', 'desc')->limit(4)->get();
    }
}
