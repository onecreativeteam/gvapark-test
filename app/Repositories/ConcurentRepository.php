<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\Concurent;

class ConcurentRepository extends BaseRepository
{
    /**
    * @var App\Models\Concurent $modelClass;
    */
    protected $modelClass = Concurent::class;

    public function getAll()
    {
        return $this->getModel()->all();
    }
}
