<?php
namespace App\Repositories;

use App\Models\ClientReview;

class ClientReviewRepository extends BaseRepository
{
    /**
    * @var App\Models\ClientReview $modelClass;
    */
    protected $modelClass = ClientReview::class;
}
