<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\ReservationOption;

class ReservationOptionRepository extends BaseRepository
{
    /**
    * @var App\Models\ReservationOption $modelClass;
    */
    protected $modelClass = ReservationOption::class;
}
