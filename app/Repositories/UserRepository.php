<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\User;

use Validator;
use Carbon\Carbon;

class UserRepository extends BaseRepository
{
    /**
    * @var App\Models\User $modelClass;
    */
    protected $modelClass = User::class;

    public function getApproved()
    {
        return $this->getModel()->where('is_approved', '=', '1')->orderBy('id', 'desc')->limit(4)->get();
    }

    public function handleReservationClientData($request)
    {
    	if (session('success_auth_client') == 0) {

            $rules = array(
                'email' => 'unique:users',
                'last_name' => 'required|max:20|min:1',
                'first_name' => 'required|max:20|min:1',
                'city' => 'required|max:30|min:1',
                'adress' => 'required|max:45|min:1',
                'postcode'=>'required|max:10|min:1',
                'company_name'=>'max:100',
                'car_brand'=>'required|max:60',
                'car_model'=>'required|max:60',
                'car_registration_number'=>'required|max:60',
                'flight_number'=>'required|max:60'
                );

            $messages = array('email.unique' => 'Nous avons déjà un compte avec ce courriel. Si vous êtes le propriétaire, veuillez vous connecter à votre profil.');

            $validatorEmail = Validator::make($request->all(), $rules, $messages);
            
            if ($validatorEmail->fails()) {

                return redirect()
                    ->back()
                    ->withInput($request->all())
                    ->withErrors($validatorEmail->errors()->all());
            }
            
        } else {

            $rules = array(
                'last_name' => 'required|max:20|min:1',
                'first_name' => 'required|max:20|min:1',
                'city' => 'required|max:30|min:1',
                'adress' => 'required|max:45|min:1',
                'postcode'=>'required|max:10|min:1',
                'company_name'=>'max:100',
                'car_brand'=>'required|max:60',
                'car_model'=>'required|max:60',
                'car_registration_number'=>'required|max:60',
                'flight_number'=>'required|max:60'
                );

            $messages = array();

            $validatorEmail = Validator::make($request->all(), $rules, $messages);
            
            if ($validatorEmail->fails()) {

                return redirect()
                    ->back()
                    ->withInput($request->all())
                    ->withErrors($validatorEmail->errors()->all());
            }

        }
    }

    public function handleNewReservationData()
    {
        $clientData                 = session('clientData');
        $priceListSession           = session('priceList');
        $reservationDatesSession    = session('selectedDates');

        $reservationData = array();

        if (!is_null($reservationDatesSession['options']))
        {
            $reservationData['reservation_option']  = $reservationDatesSession['options'];
        }
        $reservationData['user_id']                 = $clientData['id'];
        $reservationData['reservation_type']        = $reservationDatesSession['reservation_type'];
        $reservationData['client_first_name']       = $clientData['first_name'];
        $reservationData['client_last_name']        = $clientData['last_name'];
        $reservationData['email']                   = isset($clientData['email']) ? $clientData['email'] : session('clientData')->email;
        $reservationData['date_of_reservation']     = Carbon::now();
        $reservationData['date_of_arrival']         = Carbon::parse($reservationDatesSession['full_date_of_arrival'])->format('Y-m-d H:i:s');
        $reservationData['date_of_departure']       = Carbon::parse($reservationDatesSession['full_date_of_departure'])->format('Y-m-d H:i:s');
        $reservationData['days_to_stay']            = $priceListSession['daysToCharge'];
        $reservationData['price_to_charge']         = $priceListSession['totalPriceWithVAT'];
        $reservationData['vat']                     = $priceListSession['totalVAT'];
        $reservationData['payment_status']          = 'pending';
        $reservationData['payment_method']          = '';
        $reservationData['payment_id']              = '';
        $reservationData['is_arrived']              = 0;
        if (isset($priceListSession['is_aplied_promo']) && $priceListSession['is_aplied_promo'] == 1)
        {
            $reservationData['applied_promo']       = $priceListSession['is_aplied_promo'];
            $reservationData['promo_code_id']       = $priceListSession['promo_code_id'];
            $reservationData['price_to_charge']     = $priceListSession['totalPromoPriceWithVAT'];
        }
        if (isset($priceListSession['has_electric']) && $priceListSession['has_electric'] == 1) {
            $reservationData['has_electric']        = 1;
        }

        $priceListSession['price_to_charge']        = number_format((float)$priceListSession['absolute_total'], 2, '.', '');

        session()->put('reservationData', $reservationData);
        session()->put('priceList', $priceListSession);
    }
}
