<?php
namespace App\Repositories;

use App\Repositories\BaseRepository;

use App\Models\ReservationPayment;

class ReservationPaymentRepository extends BaseRepository
{
    /**
    * @var App\Models\ReservationPayment $modelClass;
    */
    protected $modelClass = ReservationPayment::class;
}
