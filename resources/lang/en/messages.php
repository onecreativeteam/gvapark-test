<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Website Localization Messages - EN
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    // Events //
    'events_button_payment' => 'PAYMENT',
    'events_list_h1' => 'UPCOMING EVENTS',
    'events_see_more' => 'see more',

    // Cancel Mail //
    'cancel_mail_subject_1' => 'Confirmation of cancellation booking ID : ',
    'cancel_mail_subject_2' => ' from GVA Park',
    'cancel_mail_hello' => 'Hello,',
    'cancel_mail_body' => 'We confirm the cancellation of your booking and its full refund.',
    'cancel_mail_footer1' => 'Kind Regards',
    'cancel_mail_footer2' => 'GVA Park team',
    // Cancel Mail //

    // TYPE OF RESERVATIONS //
    'navet_title' => 'Self Parking',
    'navet_description' => 'You drop your vehicle in our parking'."<br />".'Rue du Pré-de-la-Fontaine 19',
    'navet_tooltip' => 'You drop off your vehicle in our secure parking at the following address: rue du Pré-de-la-Fontaine 19, 1242 Satigny. A shuttle bus will take you to the airport at no additional cost.',
    
    'valet_title' => 'Privilege Parking',
    'valet_description' => 'You drop your vehicle at our address, WTC 1 Route de l\'Aéroport 10',
    'valet_tooltip' => 'You drop off your vehicle at our agency at the airport located at the World Trade Center. Your vehicle is then taken over by a valet',
        
    'valet_title_disabled' => 'The PRIVILEGE service is fully booked,',
    'valet_description_disabled' => 'please choose SELF option',
    'valet_tooltip_disabled' => 'Please choose SELF PARKING',

    'navet_video_header1' => 'From Lausanne',
    'navet_video_header2' => 'From Bardonnex',
    'validation_reservation_type_must' => 'The PRIVILEGE service is fully booked, please choose SELF option',
    // TYPE OF RESERVATIONS //
    'service_section_title' => 'Our valet parking or self-service solutions',
    'service_section_valet_head' => '- Drop off your vehicle in  the front of our office at the airport and travel with confidence',
    'service_section_valet_body' => 'With the Privilege option, you will drop off your vehicle at our agency at the airport located at the World Trade Center 1. Your vehicle is  taken by a valet. The day of your arrival ,your vehicle will be waiting in the same palce. Fast and convenient service.',
    'service_section_navet_head' => '- Drop off your vehicle at our covered parking and we will pick up you with our free shuttle to the airport',
    'service_section_navet_body' => 'With the option Self, you drop your vehicle in our covered and secure parking at the following address: rue de la Pre-de-la-Fontaine 19, 1242 Satigny. A shuttle bus will take you to the airport at no additional cost. The trip takes about 10 minutes and the shuttle runs every half hour',

    'table_head_days_name' => 'Days',
    'table_days_name' => 'Days',
    'table_day_name' => 'Day',
    'table_our_prices_header' => 'Our prices',
    'homepage_scroll_down' => 'Scroll Down',
    'homepage_table_reservation' => 'Book Now',
    'empty_park_place' => 'This park place is not empty',
    'check_park_places' => 'You are trying to remove non empty park places!',
    'open_from_to' => 'Open from 04:00 to 01:00',
    'welcome' => 'Welcome user!',
    'only_logout' => 'Logout',
    'only_login' => 'Login',
    'only_admin' => 'Admin',
    'only_log_in' => 'Log In',
    'order_description_instructions' => ' If you have ordered with us before, please enter your details below. If you are a new client, please complete the billing and delivery section',
    'form_email' => ' Identifier or email *',
    'form_password' => ' Password *',
    'form_password_reset' => 'Forgot your password? ',
    'form_button_connection' => ' Connection ',
    'menu_home' => 'Homepage',
    'menu_our_service' => 'Our service',
    'menu_reservation' => ' Booking ',
    'menu_avis_clients' => 'Customer reviews',
    'menu_videos' => 'Access',
    'menu_contact' => 'Contact',
    'reservation_date_arrival' => ' Departure date ',
    'reservation_hour_arrival' => ' Appointment time ',
    'reservation_date_departure' => ' Return date ',
    'reservation_hour_departure' => ' Return time ',
    'reservation_main_button_submit' => 'Reservation',
    'homepage_first_title' => 'Park & Fly',
    'homepage_follow_us' => 'Follow us',
    'homepage_copyright' => '© COPYRIGHT 2018 GVAPARK.CH. ALL RIGHTS RESERVED WEBDESIGN',

    'index_page_first_title' => 'Why to choose GVA Park',
    'index_page_security_title' => 'Security',
    'index_page_simplicity_title' => 'Simplicity',
    'index_page_speed_title' => 'Fast service',
    'index_page_service_title' => 'A recognised service',
    'index_page_second_title' => 'GVA Park compared to competitors',

    'index_page_table_header_one' => '1st day ',
    'index_page_table_header_two' => 'Additional day',
    'index_page_table_header_three' => 'Handling charges',

    'clients_page_title' => 'Customer reviews',
    'contact_page_telephone_label' => 'Phone number:',

    'profile_page_facture_details' => ' Invoice details ',
    'profile_page_last_name_label' => 'First name',
    'profile_page_first_name_label' => 'Last name',
    'profile_page_company_name_label' => ' Company name ',
    'profile_page_date_of_birth_label' => 'Date of Birth',
    'date_of_birth_tooltip' => 'Required when paying by invoice « swissbilling » for Swiss residents',
    'profile_page_change_password_label' => 'Change e-mail or password?',
    'profile_page_email_label' => 'E-mail address',
    'profile_page_email_confirm_label' => 'Confirm e-mail address',
    'profile_page_password_label' => 'Password',
    'profile_page_phone_label' => 'Phone number',
    'profile_page_adress_label' => 'Address',
    'profile_page_adress2_label' => 'Address 2',
    'profile_page_city_label' => 'City',
    'profile_page_postcode_label' => 'Postcode',
    'profile_page_car_information_label' => ' Additional information ',
    'profile_page_description_label' => ' Order notes ',
    'profile_page_car_brand_label' => 'Brand',
    'profile_page_car_model_label' => 'Model',
    'profile_page_car_registration_number_label' => ' Vehicle number plate',
    'profile_page_flight_number_label' => ' Return flight number',
    'profile_page_update_button' => 'Update',

    'profile_page_table_name' => 'Name',
    'profile_page_table_reservation_at' => 'Reservation at:',
    'profile_page_table_arrival_at' => 'Arrival at:',
    'profile_page_table_departure_at' => 'Departure at:',
    'profile_page_table_days_to_stay' => 'Days to stay:',
    'profile_page_table_status' => 'Status:',
    'profile_page_table_gallery' => 'Gallery:',
    'profile_page_table_tax' => 'Tax:',
    'profile_page_table_new_tax' => 'New Tax:',
    'profile_page_table_button_view_gallery' => 'view',
    'profile_page_table_button_annulation' => 'Cancel',
    'profile_page_table_button_facture' => 'Invoice',

    'reservation_page_first_title' => ' Booking ',
    'reservation_page_options_title' => 'Car wash',
    'reservation_page_options_intereur_title' => ' - Interior cleaning',
    'reservation_page_options_extereur_title' => ' - Exterior cleaning',
    'reservation_page_options_extra_title' => ' - Interior & Exterior cleaning',
    'reservation_page_options_none_title' => 'None',

    'reservation_page_options_intereur_selected' => 'Interior cleaning',
    'reservation_page_options_extereur_selected' => 'Exterior cleaning',
    'reservation_page_options_extra_selected' => 'Interior & Exterior cleaning',

    // Reservation Additions Re-design labels
    'reservation_step' => 'Step',
    'reservation_step_next' => 'Next',
    'reservation_page_options_h3' => 'Select additional services',
    'reservation_page_options_info' => 'Car wash',
    'reservation_page_options_electric_title' => 'Electric car charging',
    'reservation_page_options_intereur_descr' => 'Vacuum cleaner, glass, plastic, karcher (with wiping) and cleaning brush rims',
    'reservation_page_options_extereur_descr' => 'Vacuum cleaner, dust and kercher (without wiping)',

    'gallery_title' => 'Gallery',

    'facture_website_title' => 'GVA Automotive Switzerland SARL',
    'facture_adress1' => 'Case postale 193',
    'facture_adress2' => '1215 Genève 15 Aéroport',
    // 'facture_adress3' => '1215 Genève',
    'facture_navet_adress1' => 'rue du Pré-de-la-Fontaine 19',
    'facture_navet_adress2' => '1242 Satigny',
    'facture_telephone' => '+41 (0)22 510 14 40',
    'facture_email' => 'Mail: info@gvapark.ch',
    'facture_tva_number' => 'TVA No: CHE-301.929.928',
    'facture_label' => 'INVOICE',
    'facture_reservation_id_label' => 'Reservation ID :',
    'facture_transaction_id_label' => 'Datatrans ID : ',
    'facture_registration_number' => 'Order number:',
    'facture_date_of_reservation' => 'Order date:',
    'facture_payment_method' => 'Payment method: ',
    'facture_car_brand' => 'Brand:',
    'facture_car_model' => ' Model: ',
    'facture_registration_number' => ' Vehicle number plate: ',
    'facture_flight_number' => ' Return flight number:',

    'facture_products_label' => ' Products',
    'facture_quantity_label' => ' Quantity ',   
    'facture_price_label' => ' Price ',
    'facture_price_to_pay_label' => ' Take in charge ',
    'facture_days_label' => 'Days',
    'facture_option_label' => 'Option',
    'facture_promo_code_off_label' => 'PromoCode OFF',
    'facture_total_price_label' => 'Subtotal',
    'facture_only_total' => 'Total ',
    'facture_only_tva' => 'TVA',

    'reservation_cart_title' => 'My cart',
    'reservation_cart_price_label' => '« Take in charge », « Days » et «Exterior cleaning» were added to your cart.',
    'reservation_cart_panier_button' => 'Empty basket',
    'reservation_cart_panier_button2' => 'Update basket',
    'reservation_cart_apply_promo_button' => 'Use the voucher',
    'reservation_cart_table_total_panier_label' => 'Total basket',
    'reservation_cart_table_sous_total_label' => 'Subtotal',
    'reservation_cart_table_tva_label' => 'TVA (+7.7%)',
    'reservation_cart_table_total_label' => 'Total',
    'reservation_cart_table_promo_code_label' => 'Promo code',
    'reservation_cart_table_sous_total_with_promo_label' => 'Total with promo-code applied',
    'reservation_cart_table_submit_button_label' => 'Proceed to order',

    'reservation_checkout_first_label' => 'Do you have a voucher code?',
    'reservation_checkout_second_label' => 'Click here to enter your code',
    'reservation_checkout_apply_promo_label' => 'Apply the promo code',
    'reservation_checkout_info_label' => ' If you have ordered with us before, please enter your details below.',
    'reservation_checkout_info2_label' => 'If you are a new client, please click here.',
    'reservation_checkout_new_client_button_label' => 'New client',
    'reservation_checkout_facture_details_label' => ' Invoice details ',
    'reservation_checkout_table_head_label' => ' Your order ',
    'reservation_checkout_table_button_label' => 'Order',

    'reservation_confirm_label' => 'Booking Confirmed.',
    'reservation_confirm_label_strong' => 'Your booking is confirmed.',
    'reservation_confirm_label_second' => 'Thank you for choosing GVA Park',
    'reservation_confirm_label_last' => 'Important: in a few moments you will receive an acknowledgment of your booking in your email box. If you have not received anything within 10 minutes, please verify that GVA Park emails do not appear in your "spam mails".',
    'video_page_title' => 'How to find us',
    'video_page_header' => 'From Avanchets',
    'video_page_header2' => 'From Bardonnex',
    'video_page_header3' => 'From Blandonnet',
    'video_page_header4' => 'From Lausanne',
    'client_login_button' => 'Sign In',
    'reservation_main_button' => 'Reservation',
    'promo_code_placeholder' => 'Your coupon code',

    // Validation messages //

    'validation_date_of_arrival_required' => 'The arrival date is mandatory.',
    'validation_date_of_departure_required' => 'The departure date is mandatory.',
    'validation_hour_of_arrival_required' => 'The arrival time / hour is mandatory.',
    'validation_minute_of_arrival_required' => 'The arrival time / minute is mandatory.',
    'validation_hour_of_departure_required' => 'The departure time / hour is mandatory.',
    'validation_minute_of_departure_required' => 'The departure time / minute is mandatory.',
    'validation_date_of_arrival_format' => 'The arrival date is incorrect.',
    'validation_date_of_departure_format' => 'The departure date is incorrect.',
    'validation_hour_of_arrival_format' => 'The arrival time / hour is incorrect.',
    'validation_minute_of_arrival_format' => 'The arrival time / minute is incorrect.',
    'validation_hour_of_departure_format' => 'The departure time / hour is incorrect.',
    'validation_minute_of_departure_format' => 'The departure time / minute is incorrect.',

    'validation_login_email_required' => 'Your email is required.',
    'validation_login_email_exist' => 'It seems that we do not have registration for this email.',
    'validation_login_password_required' => 'Password required.',

    'validation_first_name_required' => 'First name required.',
    'validation_first_name_max' => 'The first name must not exceed 40 characters.',
    'validation_last_name_required' => 'The surname is mandatory.',
    'validation_email_confirm_required' => 'Please enter your confirmation e-mail.',
    'validation_email_confirm_same' => 'Email does not match.',
    'validation_password_required' => 'Password required',
    'validation_last_name_max' => 'The name cannot exceed 40 characters.',
    'validation_company_name_max' => 'The name of the company must not exceed 40 characters.',
    'validation_email_required' => 'E-mail address is required.',
    'validation_email_email' => 'Invalid email address.',
    'validation_phone_required' => 'Telephone required.',
    'validation_adress_required' => 'Address required.',
    'validation_city_required' => 'The city/town is mandatory.',
    'validation_postcode_required' => 'The postal code is mandatory.',
    'validation_car_brand_required' => 'The make is required.',
    'validation_car_model_required' => 'The model is mandatory.',
    'validation_car_registration_number_required' => 'Number plate is required.',
    'validation_flight_number_required' => 'Return flight number is mandatory.',

    'validation_promo_code_required' => 'Promotional code is mandatory.',
    'validation_promo_code_exists' => 'This promotional code does not exist.',

    'validation_payment_declined' => 'Payment refused. Please try again!',
    'validation_payment_canceled' => 'We have encountered a payment problem. Contact us for more information.',
    'validation_payment_failed' => 'Payment failed',

    'validation_reservation_not_allowed_time' => 'You cannot make a reservation 12 hours before the arrival date.',
    'validation_reservation_future_date' => 'Unable to make your reservation. Please check your dates.',

    'validation_promo_code_applied' => 'You\'ve already applied the promotional code.',
    'validation_promo_code_old' => 'The promotional code is not valid.',
    'validation_auth_client_incorect_pass' => 'Incorrect password. Please try again.',
    'validation_anulate_reservation' => 'You cannot cancel the reservation 24 hours before the date of arrival.',

    'reservation_mail_hello' => 'Hello',
    'reservation_mail_header' => 'This is an automatic message. Please do not reply to this email. We are pleased to confirm your booking, here are the details:',
    'reservation_mail_subject_1' => 'Confirmation of your reservation ID : ',
    'reservation_mail_subject_2' => ' about GVA Park',
    'reservation_mail_from' => 'From:',
    'reservation_mail_to' => 'To:',
    'reservation_mail_promo_code' => 'Code promotionnel OFF:',
    'reservation_mail_total' => 'Total:',

    'reservation_mail_row_1' => 'One of our valets will welcome you at our premises located at World Trade Center 1 - route de l\'Aéroport 10/1215 Geneva to take charge of your vehicle :',
    'reservation_mail_row_2' => 'To find us easily, we have posted some videos online:',
    'reservation_mail_row_3' => 'You can modify or cancel your booking at any time via your customer area:',

    'reservation_mail_row_1_navet' => 'One of our agents will welcome you in our parking located at Rue du Pré-de-la-Fontaine 19/1242 Satigny, GPS coordinates: 46 ° 13\'22.1 "N + 6 ° 03\'07.5" E. Then our shuttle will take you to our office at the airport. Departure is every 30 minutes.',
    'reservation_mail_row_2_navet' => 'To find us easily, we have posted some videos online:',
    'reservation_mail_row_3_navet' => 'You can modify or cancel your booking at any time via your customer area :',

    'admin_mail_header' => 'There is new reservation in GvaPark.',
    'admin_mail_h1' => 'Hello Admin.',

    'accept_mail_row1' => 'We will confirm the pick-up of your vehicle. The pictures can be found in your customer area on our website.',
    'accept_mail_row2' => 'We wish you a pleasant flight.',
    'accept_mail_row3' => 'Yours GVA Park team',

    'tc_title_main_1' => 'I accept the',
    'tc_title_main_2' => 'Terms and Conditions',
    'tc_title_1' => '1. Scope',
    'tc_description_1' => 'GVA Park is a brand managed by GVA Automotive Switzerland SARL, N° IDE : CHE-301.929.928. <br>These general conditions of sale are valid for the acceptance of clients vehicles at Geneva International Airport - World Trade Center after the reservation on www.gvapark.ch. The temporary transfer of the vehicle is made only for parking for the agreed rental period, until the return of the vehicle from the company to the client.',

    'tc_title_2' => '2. Contract activation',
    'tc_description_2' => 'The sending of a reservation via www.gvapark.ch constitutes a firm offer for the conclusion of a contract, which is concluded when sending a conformation email. The reservation must be made at least 12 hours before the desired time.  After this period, reservations are no longer possible.
                            If the reservation is made by a third party, the latter and the actual client will then be jointly and severally liable for all contractual obligations. Any claim concerning this contractual relationship can only be made by the client and can only be transferred to a third party with the effective consent of gvapark.ch',

    'tc_title_3' => '3. Services',
    'tc_description_3' => '3.1 Parking service
                            3.1.1 For each reservation, www.gvapark.ch puts a single parking space at the service of the client. The vehicle will be kept in a private car park, in a location chosen by GVA Park, throughout the duration of the contract. The service covers the transfer by GVA Park of the vehicle to its secured car park, as well as its return.
                            The client ensures that his/her vehicle is in good working conditions for the duration of the contract. In the event of a technical problem, it is the client’s responsibility to take appropriate measures at his/her own expense. If a Parking-Valet agent notices a fluid leak, then Parking-Valet reserves the right to make adequate repairs at the client\'s expense to resolve any problems. The client acknowledges the amount of any repair to Parking-Valet.
                            3.1.2 Opening times are set by www.gvapark.ch and advertised on our website. GVA Park reserves the right to modify these schedules unilaterally, provided that it is properly advertised. Upon booking, the client agrees to respect these opening hours and to deliver and retrieve his/her vehicle at the agreed times.',

    'tc_title_4' => '4. Prices and terms of payment',
    'tc_description_4' => '4.1 The rates in force are indicated on the homepage of www.gvapark.ch. They are also recalled and confirmed at the time of the reservation request. The rates in force are exclusive of tax.
                            4.2 The client is obliged to pay for the chosen service on www.gvapark.ch before the delivery of his/her vehicle. Payment is made at the time of booking, by means of a credit card. Any extension or breach of the contractual period of parking will be invoiced according to the rates in force indicated on our website. In this case, the client acknowledges the costs to www.gvapark.ch according to the new services.
                            4.3 In accordance with the laws in force, www.gvapark.ch has a right of retention and may, in the event of non-payment or problem with the credit card used at the reservation, refuse the return of the vehicle to the client. GVA Park may, if necessary, institute a lawsuit against the execution of the vehicle and the objects contained therein, in accordance with the Federal Law on Bankruptcy Proceedings.',

    'tc_title_5' => '5. Cancellations, changes to reservations and subsequent modifications to reserved services',
    'tc_description_5' => 'www.gvapark.ch offers the possibility to cancel free of charge any reservation up to 24 hours before its actual start. The operation can only be carried out through your on-line client space.
                            It is not possible to cancel your reservation less than 24 hours before the agreed date.
                            The client may also modify the time and date of the pick-up or return of his/her vehicle up to six hours before the agreed time, at no additional cost, by using a hypertext link sent to him/her in the confirmation e-mail.',

    'tc_title_6' => '6. Liability and waiver of liability',
    'tc_description_6' => '6.1 As a provider GVA Park undertakes to make every effort to execute its principal with due diligence to satisfy the client.
                            6.2 For the PRIVILEGE Parking service, an inventory is carried out at the time of the deposit of the vehicle in order to check the status. Any possible complaint relating to the state of the vehicle must be formulated by the customer during the restitution of the vehicle, after inventory of the places of exit carried by himself. For the SELF Parking service no inventory is made.
                            6.3 As a provider, GVA Park assumes responsibility only in case of serious and proven fault, whether it concerns material, physical or moral damage caused to the vehicle or the client, or in the case of restitution outside the deadline. Any claim must be notified to GVA Park before the vehicle leaves its point of return. Under the responsibility of GVA Park, the vehicle is insured against collision, theft, vandalism, floods, fire, but not against hailstones GVA Park is not responsible for the theft of objects inside the vehicles entrusted to it (such as cameras, mobile phones, car radios, jewellery and other valuables ...) any damage on rims, tires or any claws on the bodywork would not be covered - GVA Park cannot be held responsible for anything beyond its reasonable control.
                            6.4 GVA Park is entitled to return the vehicle to the person in possession of the e-mail for the pick-up of the vehicle. To guarantee the safety of the vehicle, GVA Park can only return it on the presentation of a suitable means of identification. GVA Park is not responsible for the loss or theft of the pick-up email, in particular if it is presented by a third party who uses it fraudulently to obtain the delivery of the vehicle.
                            6.6 In the case of loss of the vehicle keys from GVA Park, we undertake to provide the client with a vehicle so that he/she can return. We will take care of recovering the client’s spare key in time to be able to return the vehicle. GVA Park will bear the cost of preparing a replacement key.',

    'tc_title_7' => '7. Delivery of vehicle keys',
    'tc_description_7' => 'The client undertakes to hand over the keys of his vehicle to the employees of GVA Park',

    'tc_title_8' => '8. Miscellaneous',
    'tc_description_8' => '8.1. The personal data provided by the client on www.gvapark.ch , in particular at the time of booking, are used exclusively to ensure the implementation of the contractual services and can under no circumstances be disclosed to any third party.
                            8.2. Any changes to the Terms and Conditions must be notified in writing.  Any unjust or unequal change is prohibited.
                            8.3 The contractual relations between the client and www.gvapark.ch are subject exclusively to Swiss law. The exclusive judicial forum is Geneva.',

];

