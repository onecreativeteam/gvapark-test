<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Website Localization Messages - FR
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    // Events //
    'events_button_payment' => 'PAYER',
    'events_list_h1' => 'ÉVÈNEMENTS À VENIR',
    'events_see_more' => 'voir plus',

    // Cancel Mail //
    'cancel_mail_subject_1' => 'Confirmation d’annulation réservation ID : ',
    'cancel_mail_subject_2' => ' auprès de GVA Park',
    'cancel_mail_hello' => 'Bonjour,',
    'cancel_mail_body' => 'Nous vous confirmons l’annulation de votre réservation ainsi que son remboursement intégral.',
    'cancel_mail_footer1' => 'Cordialement',
    'cancel_mail_footer2' => 'L’équipe de GVA Park',
    // Cancel Mail //

    // TYPE OF RESERVATIONS //
    'navet_title' => 'Self Parking',
    'navet_description' => 'Vous déposez votre véhicule dans notre parking'."<br />".'Rue du Pré-de-la-Fontaine 19',
    'navet_tooltip' => 'Vous déposez votre véhicule dans notre parking sécurisé à l’adresse suivante: rue du Pré-de-la-Fontaine 19, 1242 Satigny. Une navette vous conduira à l’aéroport sans frais supplémentaires.',
    
    'valet_title' => 'Privilége Parking',
    'valet_description' => 'Vous déposez votre véhicule à nos locaux, WTC 1 Route de l\'Aéroport 10',
    'valet_tooltip' => 'Vous déposez votre véhicule à notre agence à l’aéroport située au World Trade Center. Votre véhicule est alors pris en charge par un voiturier.',
    
    'valet_title_disabled' => 'Les service PIVILÉGE est actuellement complet,',
    'valet_description_disabled' => 'veuillez choisir l\'option SELF',
    'valet_tooltip_disabled' => 'Veuillez choisir SELF PARKING',

    'navet_video_header1' => 'Depuis Lausanne',
    'navet_video_header2' => 'Depuis Bardonnex',
    'validation_reservation_type_must' => 'Les service PIVILÉGE est actuellement complet, veuillez choisir l\'option SELF',
    // TYPE OF RESERVATIONS //
    'service_section_title' => 'Nos solutions de parking avec voiturier ou en self-service',
    'service_section_valet_head' => '- Déposez votre véhicule devant nos locaux à l\'aéroport et voyagez en toute sérénité',
    'service_section_valet_body' => 'Avec l’option Privilège, vous déposez votre véhicule à notre agence à l’aéroport située au World Trade Center 1. Votre véhicule est alors pris en charge par un voiturier. A votre retour, votre véhicule vous attendra au même endroit. Un service rapide et pratique.',
    'service_section_navet_head' => '- Déposez votre véhicule à notre parking couvert et rendez-vous à l’aéroport avec notre navette gratuite',
    'service_section_navet_body' => 'Avec l’option Self, vous déposez votre véhicule dans notre parking couvert et sécurisé à l’adresse suivante: rue du Pré-de-la-Fontaine 19, 1242 Satigny. Une navette vous conduira à l’aéroport sans frais supplémentaires. Le trajet dure environ 10 minutes et la navette circule toutes les demi-heures',

    'table_head_days_name' => 'les journées',
    'table_days_name' => 'Jours',
    'table_day_name' => 'Jour',
    'table_our_prices_header' => 'Nos Prix',
    'homepage_scroll_down' => 'Faire Défiler',
    'homepage_table_reservation' => 'Réservation',
    'open_from_to' => 'Ouvert de 04:00 à 01:00',
    'welcome' => 'Welcome user!',
    'only_logout' => 'Logout',
    'only_login' => 'Login',
    'only_admin' => 'Admin',
    'only_log_in' => 'Log In',
    'order_description_instructions' => 'Si vous avez déjà commandé avec nous auparavant, veuillez saisir vos coordonnées ci-dessous.Si vous êtes un nouveau client,veuillez renseigner la section facturation et livraison.',
    'form_email' => 'Identifiant ou e-mail *',
    'form_password' => 'Mot de passe *',
    'form_password_reset' => 'Mot de passe oublie?',
    'form_button_connection' => 'Connexion',
    'menu_home' => 'Accueil',
    'menu_our_service' => 'Notre service',
    'menu_reservation' => 'Réservation',
    'menu_avis_clients' => 'Avis clients',
    'menu_videos' => 'Accès',
    'menu_contact' => 'Contact',
    'reservation_date_arrival' => 'Date de départ',
    'reservation_hour_arrival' => 'Heure de rendez-vous',
    'reservation_date_departure' => 'Date de retour',
    'reservation_hour_departure' => 'Heure de retour',
    'reservation_main_button_submit' => 'Reservation',
    'homepage_first_title' => 'Park & Fly',
    'homepage_follow_us' => 'Follow us',
    'homepage_copyright' => '© COPYRIGHT 2018 GVAPARK.CH. TOUS DROITS SONT RÉSERVÉ CRÉATION DU SITE INTERNET',

    'index_page_first_title' => 'Pourquoi choisir GVA Park',
    'index_page_security_title' => 'Sécurité',
    'index_page_simplicity_title' => 'Simplicité',
    'index_page_speed_title' => 'Rapidité',
    'index_page_service_title' => 'Un service reconnu',
    'index_page_second_title' => 'GVA Park VS autres',

    'index_page_table_header_one' => '1ER Jour',
    'index_page_table_header_two' => 'JOUR SUP.',
    'index_page_table_header_three' => 'PRISE EN CHARGE',

    'clients_page_title' => 'Avis clients',
    'contact_page_telephone_label' => 'Tel:',

    'profile_page_facture_details' => 'Détails de la facturation',
    'profile_page_last_name_label' => 'Prénom',
    'profile_page_first_name_label' => 'Nom',
    'profile_page_company_name_label' => 'Nom de l’entreprise',
    'profile_page_date_of_birth_label' => 'Date de naissance',
    'date_of_birth_tooltip' => 'Nécessaire en cas de paiement par facture « swissbilling » pour des résidents Suisse',
    'profile_page_change_password_label' => 'Changer l\'email et le mot de passe?',
    'profile_page_email_label' => 'Adresse e-mail',
    'profile_page_email_confirm_label' => 'Adresse e-mail confirm',
    'profile_page_password_label' => 'Password',
    'profile_page_phone_label' => 'Téléphone',
    'profile_page_adress_label' => 'Adresse',
    'profile_page_adress2_label' => 'Adresse 2',
    'profile_page_city_label' => 'Ville',
    'profile_page_postcode_label' => 'Code postal',
    'profile_page_car_information_label' => 'Information complémentaire',
    'profile_page_description_label' => 'Notes de la commande',
    'profile_page_car_brand_label' => 'Marque',
    'profile_page_car_model_label' => 'Modèle',
    'profile_page_car_registration_number_label' => 'Plaque d\'immatriculation',
    'profile_page_flight_number_label' => 'Numéro de vol retour',
    'profile_page_update_button' => 'Mettre à jour',

    'profile_page_table_name' => 'Name',
    'profile_page_table_reservation_at' => 'Reservation at:',
    'profile_page_table_arrival_at' => 'Arrival at:',
    'profile_page_table_departure_at' => 'Departure at:',
    'profile_page_table_days_to_stay' => 'Days to stay:',
    'profile_page_table_status' => 'Status:',
    'profile_page_table_gallery' => 'Gallery:',
    'profile_page_table_tax' => 'Tax:',
    'profile_page_table_new_tax' => 'New Tax:',
    'profile_page_table_button_view_gallery' => 'view',
    'profile_page_table_button_annulation' => 'Annulation',
    'profile_page_table_button_facture' => 'Facture',

    'reservation_page_first_title' => 'Réservation',
    'reservation_page_options_title' => 'Lavage voiture',
    'reservation_page_options_intereur_title' => ' - Nettoyage premium',
    'reservation_page_options_extereur_title' => ' - Nettoyage basique',
    'reservation_page_options_extra_title' => ' - Nettoyage premium & extérieur',
    'reservation_page_options_none_title' => 'Non',

    'reservation_page_options_intereur_selected' => 'Nettoyage premium',
    'reservation_page_options_extereur_selected' => 'Nettoyage basique',
    'reservation_page_options_extra_selected' => 'Nettoyage premium & extérieur',

    // Reservation Additions Re-design labels
    'reservation_step' => 'Étape',
    'reservation_step_next' => 'Suivant',
    'reservation_page_options_h3' => 'Sélectionner des services supplémentaires',
    'reservation_page_options_info' => 'Lavage voiture',
    'reservation_page_options_electric_title' => 'Recharge voiture électrique',
    'reservation_page_options_intereur_descr' => 'Aspirateur, vitre, plastique, karcher (avec essuyage) et nettoyage jantes à la brosse',
    'reservation_page_options_extereur_descr' => 'Aspirateur, poussière et karcher (sans essuyage)',


    'gallery_title' => 'Gallery',
    'facture_website_title' => 'GVA Automotive Switzerland SARL',
    'facture_adress1' => 'Case postale 193',
    'facture_adress2' => '1215 Genève 15 Aéroport',
    // 'facture_adress3' => '1215 Genève',
    'facture_navet_adress1' => 'rue du Pré-de-la-Fontaine 19',
    'facture_navet_adress2' => '1242 Satigny',
    'facture_telephone' => 'Tel: 022 510 14 40',
    'facture_email' => 'Mail: info@gvapark.ch',
    'facture_tva_number' => 'TVA No: CHE-301.929.928',
    'facture_label' => 'FACTURE',
    'facture_reservation_id_label' => 'Reservation ID :',
    'facture_transaction_id_label' => 'Datatrans ID : ',
    'facture_registration_number' => 'Numéro de commande:',
    'facture_date_of_reservation' => 'Date de commande:',
    'facture_payment_method' => 'Méthode de paiement: ',
    'facture_car_brand' => 'Marque:',
    'facture_car_model' => ' Modèle: ',
    'facture_registration_number' => 'Plaque d`immatriculation: ',
    'facture_flight_number' => 'Numéro de vol retour:',

    'facture_products_label' => 'Produits',
    'facture_quantity_label' => 'Quantité',
    'facture_price_label' => 'Prix',
    'facture_price_to_pay_label' => 'Prise en charge',
    'facture_days_label' => 'Jours',
    'facture_option_label' => 'Option',
    'facture_promo_code_off_label' => 'PromoCode OFF',
    'facture_total_price_label' => 'Sous-total',
    'facture_only_total' => 'Total ',
    'facture_only_tva' => 'TVA',

    'reservation_cart_title' => 'Panier',
    'reservation_cart_price_label' => '«Prise en charge», «Jours» et «Nettoyage basique» ont été ajoutés à votre panier.',
    'reservation_cart_panier_button' => 'vider le panier',
    'reservation_cart_panier_button2' => 'mettre a jour le panier',
    'reservation_cart_apply_promo_button' => 'APPLIQUER LE COUPON',
    'reservation_cart_table_total_panier_label' => 'Total panier',
    'reservation_cart_table_sous_total_label' => 'Sous-total',
    'reservation_cart_table_tva_label' => 'TVA (+7.7%)',
    'reservation_cart_table_total_label' => 'Total',
    'reservation_cart_table_promo_code_label' => 'Promo code',
    'reservation_cart_table_sous_total_with_promo_label' => 'Total with promo-code applied',
    'reservation_cart_table_submit_button_label' => 'Procéder à la commande',

    'reservation_checkout_first_label' => 'Avez-vous un code promo ?',
    'reservation_checkout_second_label' => 'Cliquez ici pour entrer votre code',
    'reservation_checkout_apply_promo_label' => 'Appliquer le code promo',
    'reservation_checkout_info_label' => 'Si vous avez déjà commandé avec nous auparavant, veuillez saisir vos coordonnées ci-dessous.',
    'reservation_checkout_info2_label' => 'Si vous-êtes un nouveau client veuillez cliquer ici.',
    'reservation_checkout_new_client_button_label' => 'Nouveau Client',
    'reservation_checkout_facture_details_label' => 'Détails de la facturation',
    'reservation_checkout_table_head_label' => 'Votre commande',
    'reservation_checkout_table_button_label' => 'commander',

    'reservation_confirm_label' => 'Réservation Confirmée.',
    'reservation_confirm_label_strong' => 'Votre réservation est bien confirmé.',
    'reservation_confirm_label_second' => 'Merci de nous avoir choisi',
    'reservation_confirm_label_last' => 'Important : vous recevrez d\'ici quelques instants un accusé de réception de votre réservation dans votre boîte e-mail.
                                            Si vous n\'avez rien reçu dans les 10 minutes qui suivent,
                                            veuillez vérifier que les e-mails de GVA Park n\'apparaissent pas dans vos « courriers indésirables » ou « spams ».',
    'video_page_title' => 'Comment nous trouver',
    'video_page_header' => 'Depuis Avanchets',
    'video_page_header2' => 'Depuis Bardonnex',
    'video_page_header3' => 'Depuis Blandonnet',
    'video_page_header4' => 'Depuis Lausanne',
    'client_login_button' => 'Connexion',
    'reservation_main_button' => 'Reservation',
    'promo_code_placeholder' => 'Votre code promo',

    // Validation messages //

    'validation_date_of_arrival_required' => 'La date d\'arrivée est obligatoire.',
    'validation_date_of_departure_required' => 'La date de départ est obligatoire.',
    'validation_hour_of_arrival_required' => 'La minute d\'arrivée est obligatoire.',
    'validation_minute_of_arrival_required' => 'La minute d\'arrivée est obligatoire.',
    'validation_hour_of_departure_required' => 'L\'heure de départ est obligatoire.',
    'validation_minute_of_departure_required' => 'La minute de départ est obligatoire.',
    'validation_date_of_arrival_format' => 'La date d\'arrivée est incorrecte.',
    'validation_date_of_departure_format' => 'La date de départ est incorrecte.',
    'validation_hour_of_arrival_format' => 'L\'heure d\'arrivée est incorrecte.',
    'validation_minute_of_arrival_format' => 'La minute d\'arrivée est incorrecte.',
    'validation_hour_of_departure_format' => 'L\'heure de départ est incorrecte.',
    'validation_minute_of_departure_format' => 'La minute de départ est incorrecte.',

    'validation_login_email_required' => 'Otre email est requis.',
    'validation_login_email_exist' => 'Il semble que nous n\'avons pas d\'enregistrement pour cet e-mail.',
    'validation_login_password_required' => 'Mot de passe requis.',

    'validation_first_name_required' => 'Prénom obligatoire.',
    'validation_first_name_max' => 'Le prénom ne doit pas dépasser 40 symboles.',
    'validation_last_name_required' => 'Le nom de famille est obligatoire.',
    'validation_email_confirm_required' => 'Veuillez saisir votre e-mail de confirmation.',
    'validation_email_confirm_same' => 'Email ne correspond pas.',
    'validation_password_required' => 'Mot de passe requis',
    'validation_last_name_max' => 'Le nom ne peut pas dépasser 40 symboles.',
    'validation_company_name_max' => 'Le nom de l\'entreprise ne doit pas dépasser 40 symboles.',
    'validation_email_required' => 'Adresse e-mail est nécessaire.',
    'validation_email_email' => 'Adresse email invalide.',
    'validation_phone_required' => 'Téléphone requis.',
    'validation_adress_required' => 'Adresse requise.',
    'validation_city_required' => 'La ville est obligatoire.',
    'validation_postcode_required' => 'Le code postal est obligatoire.',
    'validation_car_brand_required' => 'Marque est nécessaire.',
    'validation_car_model_required' => 'Le modèle est obligatoire.',
    'validation_car_registration_number_required' => 'Numéro de plaque est obligatoire.',
    'validation_flight_number_required' => 'Numéro de vol de retour est obligatoire.',

    'validation_promo_code_required' => 'Le code promotionnel est obligatoire.',
    'validation_promo_code_exists' => 'Ce code promotionnel n\'existe pas.',

    'validation_payment_declined' => 'Paiement refusé. Veuillez réessayer!',
    'validation_payment_canceled' => 'Nous-avons rencontré un problème de paiement. Contactez nous pour plus d\'informations.',
    'validation_payment_failed' => 'Paiement échoué',

    'validation_reservation_not_allowed_time' => 'Vous ne pouvez pas effectuer de réservation 12 heures avant la date d\'arrivée.',
    'validation_reservation_future_date' => 'Impossible d\'effectuer votre réservation. Veuillez vérifier vos dates.',
    'validation_promo_code_applied' => 'Vous avez déjà appliqué le code promotionnel.',
    'validation_promo_code_old' => 'Le code promotionnel n\'est pas valable.',
    'validation_auth_client_incorect_pass' => 'Mot de passe incorrect. Veuillez réessayer.',
    'validation_anulate_reservation' => 'Vous ne pouvez pas annuler la réservation 24 heures avant la date d\'arrivée.',

    'reservation_mail_hello' => 'Bonjour',
    'reservation_mail_header' => 'C’est avec le plaisir que nous vous confirmons votre réservation, voici les détails :',
    'reservation_mail_subject_1' => 'Confirmation de votre réservation ID : ',
    'reservation_mail_subject_2' => ' auprès de GVA Park',
    'reservation_mail_from' => 'De:',
    'reservation_mail_to' => 'À:',
    'reservation_mail_promo_code' => 'Code promotionnel OFF:',
    'reservation_mail_total' => 'Total:',
    'reservation_mail_row_1' => 'L’un de nos agents vous accueillera dans nos locaux situés à World Trade Center 1 – route de lAéroport 10 / 1215 Genève pour prendre en charge votre véhicule :',
    'reservation_mail_row_2' => 'Pour nous trouver facilement nous avons mis en ligne quelques vidéos:',
    'reservation_mail_row_3' => 'Vous pouvez à chaque instant modifier ou annuler votre réservation via votre espace client :',

    'reservation_mail_row_1_navet' => 'L’un de nos agents vous accueillera dans notre parking situé a Rue du Pré-de-la-Fontaine 19 / 1242 Satigny, coordonnées GPS: 46°13\'22.1"N+6°03\'07.5"E . Ensuite notre navette vous amènera jusqu\'à nos locaux a l\'aéroport. Le départ est toutes les 30 minutes.',
    'reservation_mail_row_2_navet' => 'Pour nous trouver facilement nous avons mis en ligne quelques vidéos:',
    'reservation_mail_row_3_navet' => 'Vous pouvez à chaque instant modifier ou annuler votre réservation via votre espace client :',

    'admin_mail_header' => 'Il y a une nouvelle réservation dans GvaPark.',
    'admin_mail_h1' => 'Bonjour Admin.',

    'accept_mail_row1' => 'nous vous confirmons la prise en charge de votre véhicule. Les photos se trouvent dans votre espace client sur notre site.',
    'accept_mail_row2' => 'Nous vous souhaitions un agréable vol.',
    'accept_mail_row3' => 'L\'équipe de GVA Park',

    'tc_title_main_1' => 'J’accepte les',
    'tc_title_main_2' => 'Conditions Générales',

    'tc_title_1' => '1. Champ d’application',
    'tc_description_1' => 'GVA Park est une marque gérée par la société GVA Automotive Switzerland SARL, N° IDE : CHE-301.929.928 <br>
                            Ces conditions générales de vente sont valables pour l’acceptation de véhicules des clients à l’aéroport international de Genève - World Trade Center après réservation sur www.gvapark.ch. La cession temporaire du véhicule se fait au seul titre de stationnement pour la durée de location convenue, jusqu\'à la restitution du véhicule de l\'entreprise au client.',
    'tc_title_2' => '2. Élaboration du contrat',
    'tc_description_2' => 'L\'envoi d\'une réservation via le site www.gvapark.ch constitue une offre ferme en vue de la conclusion d\'un contrat, qui est conclu effectivement lors de l\'envoi d\'un courrier électronique de confirmation. La réservation doit être effectuée au moins 12 heures avant l\'heure souhaitée. Passé ce délai, les réservations ne sont plus possibles.<br>
                            Si la réservation est effectuée par un tiers, alors il sera solidairement responsable, avec le client effectif, de toutes les obligations contractuelles. Toute réclamation concernant cette relation contractuelle ne peut être effectuée que par le client, et ne peut être transférée à un tiers qu\'avec le consentement effectif de www.gvapark.ch',
    'tc_title_3' => '3. Volume des prestations',
    'tc_description_3' => '3.1 Pour chaque réservation, www.gvapark.ch met au service du client un seul titre de stationnement. Le véhicule sera gardé dans un parking privé, dans un emplacement choisi par GVA Park, durant toute la durée du contrat. La prestation recouvre le transfert par GVA Park du véhicule vers son parking sécurisé, ainsi que sa restitution. Le client assure que son véhicule est en état de rouler pour toute la durée du contrat. En cas de problème technique, il appartient au client de prendre les mesures appropriées à ses frais propres. Si les agents de GVA Park notent une fuite de fluide, alors GVA Park se réserve le droit d\'effectuer les réparations adéquates aux frais du client pour résoudre tout problème. Le client reconnaît devoir le montant de toute réparation éventuelle à GVA Park.<br>
                            3.1 Les horaires d\'ouverture sont fixés par www.gvapark.ch et annoncées sur notre site internet. GVA Park se réserve le droit de modifier ces horaires de façon unilatérale à condition de l\'annoncer de façon adéquate. À la réservation, le client s\'engage à respecter ces horaires d\'ouverture et à remettre et récupérer son véhicule aux horaires convenus.',
    'tc_title_4' => '4. Prix et conditions de paiement',
    'tc_description_4' => '4.1 Les tarifs en vigueur sont indiqués sur la page d\'accueil de www.gvapark.ch, Ils sont en outre rappelés et confirmés lors de la demande de réservation. Les tarifs en vigueur sont HT.<br>
                    4.2 Le client est tenu de régler la prestation choisie sur www.gvapark.ch avant la livraison de son véhicule. Le paiement se fait au moment de la réservation, par le moyen d\'une carte de crédit. Toute extension ou violation de la période contractuelle de stationnement seront facturées conformément aux tarifs en vigueur indiqués sur notre site internet. Le client reconnaît dans ce cas devoir à www.gvapark.ch  le cout mis à jour selon les nouvelles prestations.<br>
                    4.3 En accord avec les lois en vigueur, www.gvapark.ch dispose d\'un droit de rétention et peut, en cas de défaut de paiement ou de problème avec la carte de crédit employée à la réservation, refuser la restitution du véhicule au Client. Le cas échéant, GVA Park peut engager une poursuite en réalisation du véhicule et des objets qu\'il contient, en conformité avec la Loi Fédérale sur la poursuite pour dettes et faillites.',
    'tc_title_5' => '5 Annulations, modifications de réservation et modifications ultérieures des prestations réservées',
    'tc_description_5' => 'www.gvapark.ch offre la possibilité d\'annuler gratuitement toute réservation jusqu\'à 24 heures avant son début effectif. L\'opération peut s\'effectuer uniquement via votre espace client en ligne.
                    Il est impossible d\'annuler sa réservation moins de 24 heures avant la date convenue.
                    Le Client a également la possibilité de modifier, sans frais supplémentaire, l\'heure et la date de la prise en charge ou de la restitution de son véhicule jusqu\'à six heures avant l\'heure convenue, en utilisant un lien hypertexte envoyé à cet effet dans le courrier électronique de confirmation.
                    ',
    'tc_title_6' => '6. Responsabilité et exonération de responsabilit',
    'tc_description_6' => '6.1 En tant que prestataire, GVA Park s\'engage à tout mettre en œuvre pour exécuter son mandant avec toute la diligence requise pour satisfaire le client.<br>
                    6.2 Pour le service PRIVILEGE Parking un état des lieux est effectué au moment du dépôt du véhicule a fin d’constater l’état. Toute réclamation éventuelle relative à \'état du véhicule doit être formulée par le client lors de la restitution du véhicule, après état des lieux de sortie réalisé par ces soins. Pour le service SELF Parking aucun état des lieux n’est effectué.<br>
                    6.3 En tant que prestataire GVA Park n\'assume une responsabilité qu\'en cas de faute grave et démontrée, que cela concerne des dommages matériels, corporels ou moraux causées au véhicule ou au client, ou bien en cas de restitution hors délais. Toute réclamation doit être notifiée à GVA Park avant que le véhicule ne quitte son point de restitution. Sous la responsabilité de GVA Park, le véhicule est assuré contre la collision, le vol, les inondations, le feu, mais pas contre la grêle. GVA Park n’est pas responsable des vols d\'objets situés à l\'intérieur des véhicules qui lui sont confiés (tels qu\'appareil photos, téléphone portable, autoradios, bijoux et autres objets de valeur…) toutes dommage sur des jantes, pneus ou des éventuelles griffes sur la carrosserie ne seraient pas couverts - GVA Park ne saurait être tenu responsable de ce qui échappe à son contrôle raisonnable.<br>
                    6.4 GVA Park est habilité à restituer le véhicule à la personne en possession du courrier électronique de prise en charge du véhicule. Pour garantir la sécurité du véhicule, GVA Park ne peut le restituer que sur la présentation d\'un moyen d\'identification adéquat. GVA Park n\'est pas responsable en cas de perte ou de vol du courrier électronique de prise en charge, notamment si celui-ci est présenté par un tiers qui l\'utilise de manière frauduleuse pour obtenir la remise du véhicule.<br>
                    6.5 En cas de perte des clés du véhicule de la part de GVA Park nous engageons à mettre à disposition du client un véhicule pour qu\'il puisse rentrer. Nous nous chargerons de récupérer chez le client la clé de rechange en temps voulu pour pouvoir procéder à la restitution du véhicule. GVA Park prendra en charge les frais de confection d\'une clé de remplacement.<br>',
    'tc_title_7' => '7. Remise des clés du véhicule',
    'tc_description_7' => 'Le client s\'engage contractuellement à remettre les clés de son véhicule lors de sa remise aux collaborateurs de GVA Park',
    'tc_title_8' => '8. Autre',
    'tc_description_8' => '8.1. Les données personnelles fournies par le client à www.gvapark.ch , notamment lors de la réservation, sont utilisées exclusivement pour assurer la mise en œuvre des services contractuels, et ne peuvent en aucun cas être révélées à un quelconque tiers.<br>
                    8.2. Tout changement des Termes et Conditions doit être notifié par forme écrite. Tout changement injuste ou inégal est proscrit.<br>
                    8.3. Les relations contractuelles entre le client et www.gvapark.ch sont soumises exclusivement au droit suisse. Le for judiciaire exclusif est Genève.',

];
