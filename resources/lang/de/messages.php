
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Website Localization Messages - DE
    |--------------------------------------------------------------------------
    |
    | The following language lines are used for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    // Events //
    'events_button_payment' => 'BEZAHLEN',
    'events_list_h1' => 'KOMMENDE VERANSTALTUNGEN',
    'events_see_more' => 'mehr sehen',

    // Cancel Mail //
    'cancel_mail_subject_1' => 'Bestätigung der Stornierung Buchungs-ID : ',
    'cancel_mail_subject_2' => ' von GVA Park',
    'cancel_mail_hello' => 'Hallo,',
    'cancel_mail_body' => 'Wir bestätigen die Stornierung Ihrer Buchung und deren volle Rückerstattung.',
    'cancel_mail_footer1' => 'Mit freundlichen Grüßen',
    'cancel_mail_footer2' => 'GVA Park Team',
    // Cancel Mail //

    // TYPE OF RESERVATIONS //
    'navet_title' => 'Self Parking',
    'navet_description' => 'Sie stellen Ihr Fahrzeug auf unserem Parkplatz ab.'."<br />".'Rue du Pré-de-la-Fontaine 19',
    'navet_tooltip' => 'Sie stellen Ihr Fahrzeug auf unserem Parkplatz an der folgenden Adresse ab: rue du Pré-de-la-Fontaine 19, 1242 Satigny. Ein Shuttle bringt Sie ohne zusätzliche Kosten zum Flughafen.',
    
    'valet_title' => 'Privilege Parking',
    'valet_description' => 'Sie stellen Ihr Fahrzeug in unseren Büros, WTC 1 Route de l\'Aéroport 10.',
    'valet_tooltip' => 'Sie stellen Ihr Fahrzeug bei unserer Agentur am Flughafen ab, die sich im World Trade Center befindet. Ihr Fahrzeug wird dann von einem Fahrer übernommen.',
    
    'valet_title_disabled' => 'Der PRIVILEGE Service ist ausgebucht,',
    'valet_description_disabled' => 'bitte wählen Sie die Option SELF',
    'valet_tooltip_disabled' => 'Bitte wählen Sie SELF PARKING',

    'navet_video_header1' => 'Ab Lausanne',
    'navet_video_header2' => 'Ab Bardonnex',
    'validation_reservation_type_must' => 'Der PRIVILEGE Service ist ausgebucht, bitte wählen Sie die Option SELF',
    // TYPE OF RESERVATIONS //
    'service_section_title' => 'Unsere Parkservice oder Selbstbedienungslösungen',
    'service_section_valet_head' => '- Geben Sie Ihr Fahrzeug vor unserem Büro am Flughafen ab und reisen Sie mit Vertrauen',
    'service_section_valet_body' => 'Mit der Option Privilege geben Sie Ihr Fahrzeug bei unserer Agentur am Flughafen im World Trade Center 1 ab. Ihr Fahrzeug wird von einem Parkservice übernommen. Am Tag Ihrer Ankunft wartet Ihr Fahrzeug im gleichen Raum. Schneller und bequemer Service.',
    'service_section_navet_head' => '- Geben Sie Ihr Fahrzeug auf unserem überdachten Parkplatz ab und wir holen Sie mit unserem kostenlosen Shuttle zum Flughafen ab',
    'service_section_navet_body' => 'Mit der Option Self können Sie Ihr Fahrzeug auf unserem überdachten und bewachten Parkplatz unter folgender Adresse abstellen: Rue de la Pre-de-la-Fontaine 19, 1242 Satigny. Ein Shuttlebus bringt Sie ohne zusätzliche Kosten zum Flughafen. Die Fahrt dauert etwa 10 Minuten und der Shuttle fährt jede halbe Stunde',

    'table_head_days_name' => 'Tage',
    'table_days_name' => 'Tage',
    'table_day_name' => 'Tag',
    'table_our_prices_header' => 'Unsere Preise',
    'homepage_scroll_down' => 'Runterscrollen',
    'homepage_table_reservation' => 'Jetzt reservieren',
    'open_from_to' => 'Geöffnet von 04:00 bis 01:00 Uhr',
    'welcome' => 'Willkommen!',
    'only_logout' => 'Logout',
    'only_login' => 'Login',
    'only_admin' => 'Admin',
    'only_log_in' => 'Log In',
    'order_description_instructions' => 'Haben Sie bereits einmal bei uns reserviert, geben Sie bitte nachfolgend Ihre Angaben ein. Neuer Kunde? Geben Sie bitte Ihre Rechnungs- und Versands-Informationen ein.',
    'form_email' => ' Nutzername oder E-Mail-Adresse *',
    'form_password' => ' Passwort *',
    'form_password_reset' => 'Passwort vergessen?',
    'form_button_connection' => ' Einloggen ',
    'menu_home' => 'Homepage',
    'menu_our_service' => 'Unsere Dienste',
    'menu_reservation' => 'Reservierung',
    'menu_avis_clients' => 'Kundenbewertungen',
    'menu_videos' => 'Zugang',
    'menu_contact' => 'Kontakt',
    'reservation_date_arrival' => ' Abreisedatum ',
    'reservation_hour_arrival' => ' Übergabetermin ',
    'reservation_date_departure' => ' Rückreisedatum ',
    'reservation_hour_departure' => ' Rückreiseuhrzeit ',
    'reservation_main_button_submit' => 'Reservierung',
    'homepage_first_title' => 'Park & Fly',
    'homepage_follow_us' => 'Follow us',
    'homepage_copyright' => '© COPYRIGHT 2018 GVAPARK.CH. ALLE RECHTE VORBEHALTEN. WEBDESIGN',

    'index_page_first_title' => 'Warum uns wählen?',
    'index_page_security_title' => 'Sicher',
    'index_page_simplicity_title' => 'Einfach',
    'index_page_speed_title' => 'Schnell',
    'index_page_service_title' => 'Ein anerkannter Service',
    'index_page_second_title' => 'GVA Park vs andere',

    'index_page_table_header_one' => '1. Tag ',
    'index_page_table_header_two' => 'Zusatztag',
    'index_page_table_header_three' => ' Übernahme ',

    'clients_page_title' => 'Kundenbewertungen',
    'contact_page_telephone_label' => 'Tel:',

    'profile_page_facture_details' => ' Abrechnungsdetails ',
    'profile_page_last_name_label' => 'Vorname',
    'profile_page_first_name_label' => 'Nachname',
    'profile_page_company_name_label' => ' Name des Unternehmens ',
    'profile_page_date_of_birth_label' => 'Geburtsdatum',
    'date_of_birth_tooltip' => 'Erforderlich bei Zahlung per Rechung « swissbilling » für in der Schweiz wohnhafte Personen',
    'profile_page_change_password_label' => 'E-mail oder Passwort ändern?',
    'profile_page_email_label' => 'E-mail-Adresse',
    'profile_page_email_confirm_label' => 'E-mail-Adresse bestätigen',
    'profile_page_password_label' => 'Passwort',
    'profile_page_phone_label' => 'Kontaktnummer',
    'profile_page_adress_label' => 'Adresse',
    'profile_page_adress2_label' => 'Adresse 2',
    'profile_page_city_label' => 'Stadt',
    'profile_page_postcode_label' => 'Postleitzahl',
    'profile_page_car_information_label' => ' Zusätzliche Angabe ',
    'profile_page_description_label' => 'Notizen zur Bestellung',
    'profile_page_car_brand_label' => 'Automarke',
    'profile_page_car_model_label' => 'Automodel',
    'profile_page_car_registration_number_label' => ' Autokennzeichen',
    'profile_page_flight_number_label' => ' Flugnummer des Rückfluges',
    'profile_page_update_button' => 'Aktualisieren',

    'profile_page_table_name' => 'Name',
    'profile_page_table_reservation_at' => 'Abreisedatum:',
    'profile_page_table_arrival_at' => 'Übergabetermin:',
    'profile_page_table_departure_at' => 'Rückreisedatum:',
    'profile_page_table_days_to_stay' => 'Dauer:',
    'profile_page_table_status' => 'Status:',
    'profile_page_table_gallery' => 'Gallery:',
    'profile_page_table_tax' => 'Tax:',
    'profile_page_table_new_tax' => 'New Tax:',
    'profile_page_table_button_view_gallery' => 'view',
    'profile_page_table_button_annulation' => 'Annullierung',
    'profile_page_table_button_facture' => 'Rechnung',

    'reservation_page_first_title' => ' Reservierung ',
    'reservation_page_options_title' => 'Autowäsche',
    'reservation_page_options_intereur_title' => ' - Innenreinigung',
    'reservation_page_options_extereur_title' => ' - Außenreinigung',
    'reservation_page_options_extra_title' => ' - Innenreinigung & Außenreinigung',
    'reservation_page_options_none_title' => 'Nicht',

    'reservation_page_options_intereur_selected' => 'Innenreinigung',
    'reservation_page_options_extereur_selected' => 'Außenreinigung',
    'reservation_page_options_extra_selected' => 'Innenreinigung & Außenreinigung',

    // Reservation Additions Re-design labels
    'reservation_step' => 'Schritt',
    'reservation_step_next' => 'Folgende',
    'reservation_page_options_h3' => 'Wählen Sie zusätzliche Dienste aus',
    'reservation_page_options_info' => 'Autowäsche',
    'reservation_page_options_electric_title' => 'Aufladung des Elektroautos',
    'reservation_page_options_intereur_descr' => 'Staubsauger, Glas, Kunststoff, Karcher (mit Wisch-) und Reinigungsbürstenfelgen',
    'reservation_page_options_extereur_descr' => 'Staubsauger, Staub und Kercher (ohne Abwischen)',

    'gallery_title' => 'Gallery',

    'facture_website_title' => 'GVA Automotive Switzerland SARL',
    'facture_adress1' => 'Case postale 193',
    'facture_adress2' => '1215 Genève 15 Aéroport',
    // 'facture_adress3' => '1215 Genève',
    'facture_navet_adress1' => 'rue du Pré-de-la-Fontaine 19',
    'facture_navet_adress2' => '1242 Satigny',
    'facture_telephone' => '+41 (0)22 510 14 40',
    'facture_email' => 'E-mail: info@gvapark.ch',
    'facture_tva_number' => 'TVA No: CHE-301.929.928',
    'facture_label' => 'FACTURE',
    'facture_reservation_id_label' => 'Reservation ID :',
    'facture_transaction_id_label' => 'Datatrans ID : ',
    'facture_registration_number' => 'Buchungsnummer:',
    'facture_date_of_reservation' => 'Buchungsdatum:',
    'facture_payment_method' => 'Zahlungsarten: ',
    'facture_car_brand' => 'Automarke:',
    'facture_car_model' => ' Automodell: ',
    'facture_registration_number' => ' Autokennzeichen: ',
    'facture_flight_number' => ' Flugnummer des Rückfluges:',

    'facture_products_label' => 'Produkt',
    'facture_quantity_label' => ' Anzahl ',
    'facture_price_label' => ' Preis ',
    'facture_price_to_pay_label' => ' Übernahme ',
    'facture_days_label' => 'Tage',
    'facture_option_label' => 'Option',
    'facture_promo_code_off_label' => 'PromoCode OFF',
    'facture_total_price_label' => 'Zwischensumme',
    'facture_only_total' => 'Gesamtsumme ',
    'facture_only_tva' => 'TVA',

    'reservation_cart_title' => 'Warenkorb',
    'reservation_cart_price_label' => '« Übernahme », «Tage» und «Außenreinigung» sind dem Warenkorb hinzugefügtworden.',
    'reservation_cart_panier_button' => ' Warenkorb leeren ',
    'reservation_cart_panier_button2' => ' Warenkorb aktualisieren ',
    'reservation_cart_apply_promo_button' => ' Gutschein anwenden ',
    'reservation_cart_table_total_panier_label' => 'Gesamtsumme',
    'reservation_cart_table_sous_total_label' => 'Zwischensumme',
    'reservation_cart_table_tva_label' => 'TVA (+7.7%)',
    'reservation_cart_table_total_label' => 'Gesamtsumme',
    'reservation_cart_table_promo_code_label' => 'Gutscheincode',
    'reservation_cart_table_sous_total_with_promo_label' => 'Gesamtsumme mit Gutschein Rabatt',
    'reservation_cart_table_submit_button_label' => ' Zur Bestellung übergehen ',

    'reservation_checkout_first_label' => ' Haben Sie einen Gutscheincode?',
    'reservation_checkout_second_label' => ' Klicken Sie hier, um den Code einzugeben.',
    'reservation_checkout_apply_promo_label' => 'Gutscheincode eingeben',
    'reservation_checkout_info_label' => ' Haben Sie bereits einmal bei uns reserviert, geben Sie bitte nachfolgend Ihre Angaben ein.',
    'reservation_checkout_info2_label' => ' Neuer Kunde? Hier klicken.',
    'reservation_checkout_new_client_button_label' => 'Neuer Kunde',
    'reservation_checkout_facture_details_label' => ' Abrechnungsdetails ',
    'reservation_checkout_table_head_label' => ' Ihre Reservation ',
    'reservation_checkout_table_button_label' => 'Bestellen',

    'reservation_confirm_label' => 'Reservierung bestätigt.',
    'reservation_confirm_label_strong' => 'Ihre Reservierung wurde bestätigt.',
    'reservation_confirm_label_second' => 'Danke für Ihr Vertrauen',
    'reservation_confirm_label_last' => 'Wichtig: Sie erhalten in wenigen Augenblicken eine Buchungsbestätigung in Ihrem Postfach. Sollten Sie innerhalb von 10 Minuten keine E-Mail erhalten, sehen Sie bitte nach, ob die Mails von GVA Park vielleicht in „unerwünschte Mails“ oder „Spams“ gelandet sind.',
    'video_page_title' => 'Wo finden Sie uns?',
    'video_page_header' => 'Ab Avanchets',
    'video_page_header2' => 'Ab Bardonnex',
    'video_page_header3' => 'Ab Blandonnet',
    'video_page_header4' => 'Ab Lausanne',
    'client_login_button' => 'Anmelden',
    'reservation_main_button' => 'Reservieren',
    'promo_code_placeholder' => 'Ihr Gutscheincode',

    'validation_date_of_arrival_required' => 'Das Anreisedatum ist erforderlich.',
    'validation_date_of_departure_required' => 'Das Abreisedatum ist erforderlich.',
    'validation_hour_of_arrival_required' => 'Die Anreisezeit / Stunde ist verpflichtend. ',
    'validation_minute_of_arrival_required' => 'Die Anreisezeit / Minute ist verpflichtend. ',
    'validation_hour_of_departure_required' => 'Die Abreisezeit / Stunde ist verpflichtend.',
    'validation_minute_of_departure_required' => 'Die Abreisezeit / Minute ist verpflichtend.',
    'validation_date_of_arrival_format' => 'Das Anreisedatum ist inkorrekt.',
    'validation_date_of_departure_format' => 'Das Abreisedatum ist inkorrekt.',
    'validation_hour_of_arrival_format' => 'Die Anreisezeit / Stunde ist inkorrekt.',
    'validation_minute_of_arrival_format' => 'Die Anreisezeit / Minute ist inkorrekt.',
    'validation_hour_of_departure_format' => 'Die Abreisezeit / Stunde ist inkorrekt.',
    'validation_minute_of_departure_format' => 'Die Abreisezeit / Minute ist inkorrekt.',
    'validation_login_email_required' => 'Ihre E-Mail-Adresse ist erforderlich.',
    'validation_login_email_exist' => 'Es scheint, dass diese E-Mail-Adresse bei uns nicht registriert ist.',
    'validation_login_password_required' => 'Passwort erforderlich.',
    'validation_first_name_required' => 'Der Vorname ist verpflichtend.',
    'validation_first_name_max' => 'Der Vorname darf nicht länger als 40 Zeichen sein.',
    'validation_last_name_required' => 'Der Familienname ist verpflichtend.',
    'validation_email_confirm_required' => 'Bitte geben Sie Ihre Bestätigungsmail ein.',
    'validation_email_confirm_same' => 'Die E-Mail-Adresse stimmt nicht überein.',
    'validation_password_required' => 'Passwort erforderlich.',
    'validation_last_name_max' => 'Der Name darf nicht länger als 40 Zeichen sein.',
    'validation_company_name_max' => 'Der Firmenname darf nicht länger als 40 Zeichen sein.',
    'validation_email_required' => 'E-Mail-Adresse erforderlich',
    'validation_email_email' => 'E-Mail-Adresse ungültig.',
    'validation_phone_required' => 'Telefonnummer erforderlich. ',
    'validation_adress_required' => 'Adresse erforderlich. ',
    'validation_city_required' => 'Die Stadt ist verpflichtend.',
    'validation_postcode_required' => 'Die Postleitzahl ist verpflichtend.',
    'validation_car_brand_required' => 'Die Marke ist verpflichtend.',
    'validation_car_model_required' => 'Das Modell ist verpflichtend.',
    'validation_car_registration_number_required' => 'Die Nummer des Kennzeichens ist verpflichtend.',
    'validation_flight_number_required' => 'Flugnummer des Rückflugs ist verpflichtend.',

    'validation_promo_code_required' => 'Der Promotionscode ist verpflichtend.',
    'validation_promo_code_exists' => 'Dieser Promotionscode existiert nicht.',

    'validation_payment_declined' => 'Zahlung verweigert. Bitte versuchen Sie es erneut.',
    'validation_payment_canceled' => 'Bei der Zahlung ist ein Fehler aufgetreten. Bitte kontaktieren Sie uns für weitere Informationen.',
    'validation_payment_failed' => 'Zahlung fehlgeschlagen.',

    'validation_reservation_not_allowed_time' => 'Sie können 12 Stunden vor dem Anreisedatum keine Reservierung durchführen.',
    'validation_reservation_future_date' => 'Ihre Reservierung konnte nicht durchgeführt werden. Bitte überprüfen Sie Ihre Daten.',
    'validation_promo_code_applied' => 'Der Promotionscode wurde bereits verwendet.',
    'validation_promo_code_old' => 'Der Promotionscode ist ungültig.',
    'validation_auth_client_incorect_pass' => 'Passwort inkorrekt. Bitte versuchen Sie es erneut.',
    'validation_anulate_reservation' => 'Sie können die Reservierung 24 Stunden vor dem Anreisedatum nicht stornieren.',

    'reservation_mail_hello' => 'Hallo',
    'reservation_mail_header' => 'Bitte antworten Sie nicht auf diese automatische Email, da es sich um eine No-Reply-Adresse handelt.',
    'reservation_mail_subject_1' => 'Bestätigung Ihrer Buchung ID : ',
    'reservation_mail_subject_2' => ' auprès GVA Park',
    'reservation_mail_from' => 'From:',
    'reservation_mail_to' => 'To:',
    'reservation_mail_promo_code' => 'Code promotionnel OFF:',
    'reservation_mail_total' => 'Gesamtsumme:',

    'reservation_mail_row_1' => 'Einer unserer Mitarbeiter erwartet Sie in unseren Geschäftsräumen am World Trade Center 1 – Route de l‘Aéroport 10 / 1215 Genf, um Ihren Wagen in Empfang zu nehmen :',
    'reservation_mail_row_2' => 'Damit Sie uns leichter finden, haben wir einige Videos ins Netz gestellt:',
    'reservation_mail_row_3' => 'Über folgenden Link können Sie Ihre Reservierung jederzeit im Kundenbereich ändern oder stornieren:',

    'reservation_mail_row_1_navet' => 'L’un de nos agents vous accueillera dans notre situé a Rue du Pré-de-la-Fontaine 19 / 1242 Satigny, coordonnées GPS: 46°13\'22.1"N+6°03\'07.5"E . Dann bringt Sie unser Shuttle zu unserem Büro am Flughafen. Abfahrt ist alle 30 Minuten.',
    'reservation_mail_row_2_navet' => 'Damit Sie uns leichter finden, haben wir einige Videos ins Netz gestellt:',
    'reservation_mail_row_3_navet' => 'Über folgenden Link können Sie Ihre Reservierung jederzeit im Kundenbereich ändern oder stornieren :',

    'admin_mail_header' => 'Es gibt neue Reservierung in GvaPark.',
    'admin_mail_h1' => 'Hallo Admin.',

    'accept_mail_row1' => 'wir bestätigen Ihnen hiermit die Übernahme Ihres Fahrzeuges. Die Fotos finden Sie in Ihrem Kundenbereich auf unserer Website.',
    'accept_mail_row2' => 'Wir wünschen Ihnen einen angenehmen Flug.',
    'accept_mail_row3' => 'Das Team von GVA Park',

    'tc_title_main_1' => 'Ich akzeptiere die',
    'tc_title_main_2' => 'Allgemeinbedingungen',

    'tc_title_1' => '1. Geltungsbereich',
    'tc_description_1' => 'GVA Park ist eine eingetragene Marke der GVA Automotive Switzerland SARL, IDE Nr.: CHE-301.929.928. <br> Diese Verkaufsbedingungen gelten für die Annahme von Kundenfahrzeugen am Internationalen Flughafen Genf - World Trade Center nach Buchung auf www.gvapark.ch. Die vorübergehende Abgabe des Fahrzeugs erfolgt ausschließlich zum Parken für die vereinbarte Mietdauer, bis zur Rückgabe des Fahrzeugs vom Unternehmen an den Kunden.',

    'tc_title_2' => '2. Vertragsabschluss',
    'tc_description_2' => 'Das Senden einer Buchung über die Webseite www.gvapark.ch stellt ein verbindliches Angebot zum Abschluss eines Vertrages dar, der durch Sendung einer Bestätigungsmail wirksam wird. Die Buchung muss mindestens 12 Stunden vor der gewünschten Zeit vorgenommen werden. Nach Ablauf dieser Frist sind keine Buchungen mehr möglich.
                             einem Dritten vorgenommen wird, haftet dieser gemeinsam mit dem tatsächlichen Auftraggeber bezüglich aller vertraglichen Verpflichtungen. Jede Beschwerde bezüglich dieses Vertragsverhältnis kann nur vom Kunden selbst vorgebracht werden und kann nur nach wirksamer Zustimmung von www.gvapark.ch auf einen Dritten übertragen werden.',

    'tc_title_3' => '3. Leistungsumfang',
    'tc_description_3' => '3.1 Parkservice
                            3.1.1 Für jede Buchung erteilt www.gvapark.ch dem Kunden eine ausschließliche Parkgenehmigung. Das Fahrzeug wird in einem privaten Parkplatz bewacht, an einem von GVA Park gewählten Stellplatz und für die gesamte Vertragsdauer. Die Leistung umfasst den Transfer des Fahrzeugs durch GVA Park auf einem gesicherten Parkplatz sowie dessen Rückgabe.
                            Der Kunde stellt sicher, dass das Fahrzeug während der gesamten Vertragsdauer fahrbereit ist. Im Falle eines technischen Problems ist es Sache des Kunden, geeignete Maßnahmen auf eigene Kosten zu ergreifen. Wenn die Mitarbeiter des Valet-Parking Flüssigkeitsaustritt wahrnehmen, behält sich Valet-Parking das Recht vor, angemessene Instandsetzungen auf Kosten des Kunden durchführen zu lassen, um das gesamte Problem zu lösen. Der Kunde erkennt an, dass er die Kosten für jede eventuelle Instandsetzung Valet-Parking gegenüber begleichen muss.
                            3.1.2 Die Öffnungszeiten sind von www.gvapark.ch festgelegt und auf unserer Website angegeben. GVA Park behält sich das Recht vor, diese Zeitpläne einseitig zu ändern, sofern dieses auf angemessene Weise mitgeteilt wird. Bei der Buchung verpflichtet sich der Kunde, diese Öffnungszeiten zu respektieren und sein Fahrzeug zu den vereinbarten Zeiten abzugeben und wieder abzuholen.',

    'tc_title_4' => '4. Tarife und Zahlungsbedingungen',
    'tc_description_4' => '4.1 Die aktuellen Tarife sind auf der Homepage von www.gvapark.ch aufgelistet und werden außerdem bei der Buchungsanfrage angegeben und bestätigt. Die aktuellen Preise sind ohne Mehrwertsteuer.
                            4.2 Der Kunde ist verpflichtet, die gewählte Leistung auf www.gvapark.ch zu regeln, bevor er das Auto abgibt. Die Bezahlung erfolgt zum Buchungszeitpunkt per Kreditkarte. Eine Verlängerung oder Überschreitung der vertraglichen Parkdauer wird nach den aktuellen Tarifen auf unserer Website in Rechnung gestellt. Der Kunde erkennt in diesem Fall an, www.gvapark.ch die Kosten gemäß der neuen Leistungen zu schulden.
                            4.3 Nach der geltenden Gesetzgebung behält sich www.gvapark.ch ein Zurückbehaltungsrecht vor und kann bei Nichtzahlung oder einem Problem mit der bei der Buchung verwendeten Kreditkarte die Rückgabe des Fahrzeugs an den Kunden verweigern. Gegebenenfalls kann GVA Park eine Betreibung durch Pfändung des Fahrzeugs und den darin enthaltenen Gegenständen einleiten, in Übereinstimmung mit dem Bundesgesetz über Schuldbetreibung und Konkurs.',

    'tc_title_5' => '5 Stornierungen, Umbuchungen und nachträgliche Änderungen von gebuchten Leistungen',
    'tc_description_5' => 'www.gvapark.ch bietet die Möglichkeit, eine Buchung kostenlos bis zu 24 Stunden vor dem eigentlichen Beginn zu stornieren. Dieser Vorgang kann ausschließlich in Ihrem Online-Kundenbereich vorgenommen werden.
                            Es ist nicht möglich, die Buchung weniger als 24 Stunden vor dem vereinbarten Termin zu stornieren.
                            Der Kunde hat jedoch die Möglichkeit, ohne zusätzliche Kosten, Zeit und Datum der Übergabe oder der Rückgabe seines Fahrzeugs bis zu sechs Stunden vor der vereinbarten Uhrzeit zu ändern, durch Verwendung eines hierfür vorgesehenen Hyperlinks in der Bestätigungsmail.',

    'tc_title_6' => '6. Haftung und Haftungsausschluss',
    'tc_description_6' => '6.1 Als Dienstleister verpflichtet sich GVA Park, alle Anstrengungen zu unternehmen, um seinen Auftrag mit der gebotenen Sorgfalt zur Zufriedenheit des Kunden auszuführen.
                            6.2 Für den PRIVILEGE-Parkservice wird zum Zeitpunkt der Hinterlegung des Fahrzeugs eine Inventur durchgeführt, um den Status zu überprüfen. Jede mögliche Beanstandung in Bezug auf den Zustand des Fahrzeugs muss vom Kunden während der Rückgabe des Fahrzeugs, nach dem Zustand der Austrittsorte, die durch diese Sorgfalt verwirklicht werden, formuliert werden. Für den SELF-Parkservice wird kein Inventar erstellt.
                            6.3 Als Dienstleister übernimmt GVA Park nur in schweren und nachgewiesenen Fällen eine Haftung, was materielle, physische oder moralische Schäden am Fahrzeug oder am Kunden angeht, oder im Falle einer nicht fristgerechten Rückgabe. Alle Reklamationen müssen GVA Park mitgeteilt werden, bevor das Fahrzeug den Rückgabeort verlässt. Unter der Verantwortung von GVA Park ist das Fahrzeug gegen Kollision, Diebstahl, Vandalismus, Überschwemmungen, Feuer, nicht aber gegen Hagel versichert. GVA Park ist nicht verantwortlich für den Diebstahl von Gegenständen in den Fahrzeugen, die ihm anvertraut sind (wie Kameras, Handys, Autoradios, Schmuck und andere Wertsachen ...) Schäden an Felgen, Reifen oder irgendwelche Krallen auf der Karosserie würden nicht abgedeckt - GVA Park kann nicht für irgendetwas verantwortlich gemacht werden, das außerhalb seiner angemessenen Kontrolle liegt.
                            6.4 GVA Park ist berechtigt, das Fahrzeug an die Person zurückzugeben, die im Besitz der E-Mail über die Annahme des Fahrzeugs ist. Um die Sicherheit des Fahrzeugs zu gewährleisten, kann GVA Park das Fahrzeug nur bei Vorlage eines angemessenen Identifizierungsdokuments zurückgeben. GVA Park ist nicht verantwortlich für den Verlust oder Diebstahl der E-Mail über die Fahrzeugannahme, insbesondere, wenn diese von einem Dritten vorgebracht wird, der dies auf betrügerische Weise nutzt, um die Rückgabe des Fahrzeugs zu erwirken.
                            6.6 Bei Verlust des Autoschlüssels seitens GVA Park verpflichten wir uns, dem Kunden ein Fahrzeug zur Verfügung zu stellen, so dass dieser zurückkehren kann. Wir kümmern uns darum, vom Kunden zu gegebener Zeit den Ersatzschlüssel zu bekommen, um die Rückgabe des Fahrzeugs durchzuführen. GVA Park übernimmt in einem solchen Fall die Kosten für die Herstellung eines Ersatzschlüssels.',

    'tc_title_7' => '7. Übergabe der Fahrzeugschlüssel',
    'tc_description_7' => 'Der Kunde ist vertraglich verpflichtet, die Schlüssel des Fahrzeugs bei dessen Übergabe an die Mitarbeiter von GVA Park zu übergeben.',

    'tc_title_8' => '8. Sonstiges',
    'tc_description_8' => '8.1. Die persönlichen Daten, die der Kunde insbesondere bei der Buchung an www.gvapark.ch übermittelt hat, werden ausschließlich zur Durchführung der vertraglichen Leistungen verwendet und in keinem Fall an Dritte weitergegeben.
                            8.2. Jede Änderung der Allgemeinen Geschäftsbedingungen muss schriftlich mitgeteilt werden. Jede ungerechte oder ungleiche Änderung ist verboten.
                            8.3 Das Vertragsverhältnis zwischen dem Kunden und www.gvapark.ch unterliegen ausschließlich schweizerischem Recht. Ausschließlicher Gerichtsstand ist Genf.',

];

