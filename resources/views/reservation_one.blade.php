@section('page-title', 'Parking Aéroport de Genève Tarifs')
@section('page-description', "Réservez votre place de parking à l'aéroport de Genève au meilleur tarifs possible. Réservez en ligne sur notre site votre place de parking 7jours sur 7")
@extends('layouts.app')
@section('content')
<div class="container-fluid page-content">
    <div class="container">
        <div class="row text-center">
            <h2 data-aos="zoom-in">@lang('messages.reservation_page_first_title')</h2>
        </div>
        <div class="row reservation">
          @if (count($errors) > 0)
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
          @endif
            
        <div class="step_navigation">
            <ul class="steps">
                <li class="current"><a href="{{ route('reservation.one.get') }}">@lang('messages.reservation_step') 1</a></li>
                <li><a onclick="return false;">@lang('messages.reservation_step') 2</a></li>
                <li><a onclick="return false;">@lang('messages.reservation_step') 3</a></li>
                <li><a onclick="return false;">@lang('messages.reservation_step') 4</a></li>
                <li><a onclick="return false;">@lang('messages.reservation_step') 5</a></li>
            </ul>
        </div>

        <div class="reservationform_wrapper">
            {!! Form::open(['action' => 'ReservationController@postReservationCart', 'method' => 'POST', 'class'=>'reservationNew']) !!}
            {!! Form::hidden('check_type', 1) !!}
            {!! Form::hidden('reservation_type', null, ['id'=>'selectedTypeReservation']) !!}
                <div class="form_row">
                    <div class="input-inline">
                        <label>@lang('messages.reservation_date_arrival')</label>
                        <div class="form_control pickArrival">
                            {!! Form::input('text', 'date_of_arrival', session('selectedDates') ? session('selectedDates')['date_of_arrival'] : \Carbon\Carbon::now()->format('Y/m/j'), ['class' => 'form-control', 'id'=>'datepicker-depart']) !!}
                        </div><!-- /.form_control -->
                    </div><!-- /.input-inline -->
            
                    <div class="input-inline_time timeArrival">
                        <label class="label-center">@lang('messages.reservation_hour_arrival')</label>
                        <div class="form_control_time">
                            {!! Form::input('text', 'hour_of_arrival', session('selectedDates') ? session('selectedDates')['hour_of_arrival'] : 'HH:MM', ['class' => 'form-control form-control field field__icon', 'id'=>'timeArrival']) !!}
                        </div><!-- /.form_control_time -->
                    </div><!-- /.input-inline_time -->
                </div><!-- /.form_row -->

                <div class="form_row">
                    <div class="input-inline pickDeparture">
                        <label>@lang('messages.reservation_date_departure')</label>
                        <div class="form_control">
                           {!! Form::input('text', 'date_of_departure', session('selectedDates') ? session('selectedDates')['date_of_departure'] : \Carbon\Carbon::tomorrow()->format('Y/m/j'), ['class' => 'form-control ', 'id'=>'datepicker-retour']) !!}
                        </div><!-- /.form_control -->
                    </div><!-- /.input-inline -->
                
                    <div class="input-inline_time timeDeparture">
                        <label class="label-center">@lang('messages.reservation_hour_departure')</label>
                        <div class="form_control_time">
                            {!! Form::input('text', 'hour_of_departure', session('selectedDates') ? session('selectedDates')['hour_of_departure'] : 'HH:MM', ['class' => 'form-control field field__icon', 'id'=>'timeDeparture']) !!}
                        </div><!-- /.form_control -->
                    </div><!-- /.input-inline_time -->
                </div><!-- /.form_row -->
            <!-- </form> -->
        </div><!--/.reservationform_wrapper-->
        <p class="cards_title">Veuillez choisir une option:</p>
        <div class="shell">
            <div class="cards">

                <div class="vip card {{ (session('disable_valet') == '0') ? 'enabled' : 'disabled' }} {{ (isset(session('selectedDates')['reservation_type']) && session('selectedDates')['reservation_type'] == 'valet') ? 'active' : '' }}">
                    <div class="card__inner-wrapper">
                        <input id="vip-card" class="radio-custom" name="reservation_type" {{ session('disable_valet') == '0' ? 'checked=checked' : '' }} value="valet" type="radio">
                        <label for="vip-card" class="radio-custom-label">VIP</label>
                        <div class="card__inner">
                            <figure class="card__image">
                                <img src="{!! asset('image/vip-red.png') !!}" class="vip-red" alt="" />
                            </figure>

                            <div class="card__content">
                                @if (session('disable_valet') == '1')
                                    <h3 class="valet_title">@lang('messages.valet_title_disabled')</h3>
                                @else
                                    <h3 class="valet_title">@lang('messages.valet_title')</h3>
                                @endif

                                @if (session('disable_valet') == '1')
                                    <p class="valet_description">@lang('messages.valet_description_disabled')</p>
                                @else
                                    <p class="valet_description">@lang('messages.valet_description')</p>
                                @endif
                                
                                @if (session('disable_valet') == '1')
                                    <h2>CHF --</h2>
                                @else
                                    <h2>CHF {{ isset(session('priceList')['valet_total']) ? number_format(session('priceList')['valet_total'], 2) : '--' }}</h2>
                                @endif
                            </div>
                        </div>

                        <img src="{!! asset('image/red_tooltip.png') !!}" alt="" class="info"/> 
         
                        <div class="card__tooltip">
                            @if (session('disable_valet') == '1')
                                <p class="valet_tooltip">@lang('messages.valet_tooltip_disabled')</p>
                            @else
                                <p class="valet_tooltip">@lang('messages.valet_tooltip')</p>
                            @endif
                        </div>
                    </div>
                    <div class="card_button {{ (session('disable_valet') == '1') ? 'disabled' : '' }}">
                        <button type="button" class="choose_button {{ (session('disable_valet') == '0') ? 'choosed' : '' }}">{{ (session('disable_valet') == '0') ? 'Choisir' : 'Choisissez' }}</button>
                    </div>
                </div>

                <div class="normal card {{ session('disable_valet') == '1' ? 'active' : '' }} {{ (isset(session('selectedDates')['reservation_type']) && session('selectedDates')['reservation_type'] == 'navet') ? 'active' : '' }}">
                    <div class="card__inner-wrapper">
                        <input id="normal-card" class="radio-custom" {{ session('disable_valet') == '1' ? 'checked=checked' : '' }} {{ (isset(session('selectedDates')['reservation_type']) && session('selectedDates')['reservation_type'] == 'navet') ? 'checked=checked' : '' }} name="reservation_type" value="navet" type="radio">
                        <label for="normal-card" class="radio-custom-label">Normal</label>
                    
                        <div class="card__inner">
                            <figure class="card__image">
                                 <img src="{!! asset('image/normal-red.png') !!}" class="normal-red" alt="" />
                            </figure><!-- /.card__image -->
                        
                            <div class="card__content">
                                <h3>@lang('messages.navet_title')</h3>
                                <p>@lang('messages.navet_description')</p>
                                <h2>CHF {{ isset(session('priceList')['navet_total']) ? number_format(session('priceList')['navet_total'], 2) : '--' }}</h2>
                            </div><!-- /.card__content -->
                        </div><!-- /.card__inner -->

                        <img src="{!! asset('image/red_tooltip.png') !!}" alt="" class="info"/>
                    
                        <div class="card__tooltip">
                            <p>@lang('messages.navet_tooltip')</p>
                        </div><!-- /.card__tooltip -->

                        <div class="card_button">
                            <button type="button" class="choose_button {{ (session('disable_valet') == '1') ? 'choosed' : '' }}">{{ (session('disable_valet') == '1') ? 'Choisir' : 'Choisissez' }}</button>
                        </div>
                    </div><!-- /.card__inner-wrapper -->
                </div><!-- /.normal -->

            </div><!-- /.cards -->
        </div><!-- /.shell -->

        <div class="row button text-center">
            <div class="col-xs-6 col-md-offset-3">
             {!! Form::submit('Reservation') !!}
            </div>
        </div>
        {!! Form::close() !!}
        <input type="hidden" name="date_arrival_calc">
        <input type="hidden" name="date_departure_calc">
        <a href="{{ route('home') }}" class="return_button">Retour</a>
    </div>
</div>
<script type="text/javascript">
    $( document ).ready(function() {

        $normal = $('.normal');
        $vip = $('.vip');

        $normal.click( function() {
            $('#selectedTypeReservation').val('navet');
            $('#normal-card').attr('checked', true);
            $('#vip-card').attr('checked', false);
            $('.normal.card .choose_button').addClass('choosed').text('Choisir');
            $('.vip.card .choose_button').removeClass('choosed').text('Choisissez');
        });

        $vip.click(function() {
            if ($vip.hasClass('disabled')) {
                $('#selectedTypeReservation').val('navet');
                $('#normal-card').attr('checked', true);
                $('.normal.card .choose_button').addClass('choosed').text('Choisir');
                $('.vip.card .choose_button').removeClass('choosed').text('Choisissez');
                return false;
            } else {
                $('#selectedTypeReservation').val('valet');
                $('#vip-card').attr('checked', true);
                $('#normal-card').attr('checked', false);
                $('.normal.card .choose_button').removeClass('choosed').text('Choisissez');
                $('.vip.card .choose_button').addClass('choosed').text('Choisir');
            }
        });

        $( ".card" ).hover( function() {
          $(this).find('.card__tooltip').toggleClass('active')
        });

        $('input[name="date_arrival_calc"]').val($('#datepicker-depart').val());
        $('input[name="date_departure_calc"]').val($('#datepicker-retour').val());

    });
</script>
@endsection
