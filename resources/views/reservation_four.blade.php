@section('page-title', 'Parking Aéroport de Genève Tarifs')
@section('page-description', "Réservez votre place de parking à l'aéroport de Genève au meilleur tarifs possible. Réservez en ligne sur notre site votre place de parking 7jours sur 7")
@extends('layouts.app')
@section('content')
<div class="container-fluid page-content">
  <div class="container">
    <div class="row text-center">
      <h2 data-aos="zoom-in">@lang('messages.reservation_page_first_title')</h2>
    </div>
            
    <div class="row sortie-info">
      @if (count($errors) > 0)
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
      @endif
      <div class="step_navigation">
        <ul class="steps">
          <li class="current"><a href="{{ route('reservation.one.get') }}">@lang('messages.reservation_step') 1</a></li>
          <li class="current"><a href="{{ route('reservation.two.get') }}">@lang('messages.reservation_step') 2</a></li>
          <li class="current"><a href="{{ route('reservation.three.get') }}">@lang('messages.reservation_step') 3</a></li>
          <li class="current"><a onclick="return false;">@lang('messages.reservation_step') 4</a></li>
          <li><a onclick="return false;">@lang('messages.reservation_step') 5</a></li>
        </ul>
      </div>

      <div class="row contact-form aos-init aos-animate" data-aos="zoom-in" data-aos-delay="300">
          {!! Form::open(['action' => 'ReservationController@postReservationUser', 'method' => 'POST', 'id'=>'checkoutForm']) !!}
            <div class="col-sm-6">
            <h3 class="step_four_title">@lang('messages.reservation_checkout_facture_details_label')</h3>
            <div class="form-inline">
              <div class="form-group">
                <label for="last_name">@lang('messages.profile_page_last_name_label')<span class="red_stars">*</span></label>
                {!! Form::input('text', 'last_name', isset(session('clientData')['last_name']) ? session('clientData')['last_name'] : '', ['class' => 'form-control', 'id'=>'last_name']) !!}
              </div>
              <div class="form-group">
                <label for="first_name">@lang('messages.profile_page_first_name_label')<span class="red_stars">*</span></label>
                {!! Form::input('text', 'first_name', isset(session('clientData')['first_name']) ? session('clientData')['first_name'] : '', ['class' => 'form-control', 'id'=>'first_name']) !!}
              </div>
            </div>
            @if (session('success_auth_client') == '1')
              <div class="form-inline">
                <div class="form-group">
                  <label for="email">@lang('messages.profile_page_email_label')<span class="red_stars">*</span></label>
                  <input class="form-control" disabled=disabled id="email" name="email" type="text" value="{{ session('clientData')['email'] ? session('clientData')['email'] : ''}}">
                </div>
                <div class="form-group">
                  <label for="phone">@lang('messages.profile_page_phone_label')<span class="red_stars">*</span></label>
                  {!! Form::input('text', 'phone', isset(session('clientData')['phone']) ? session('clientData')['phone'] : '', ['class' => 'form-control', 'id'=>'phone']) !!}
                </div>
              </div>
            @else
              <div class="form-inline">
                <div class="form-group">
                  <label for="email">@lang('messages.profile_page_email_label')<span class="red_stars">*</span></label>
                  <input class="form-control" id="email" name="email" type="text" value="">
                </div>
                <div class="form-group">
                  <label for="phone">@lang('messages.profile_page_phone_label')<span class="red_stars">*</span></label>
                  {!! Form::input('text', 'phone', isset(session('clientData')['phone']) ? session('clientData')['phone'] : '', ['class' => 'form-control', 'id'=>'phone']) !!}
                </div>
              </div>
              <div class="form-inline">
                <div class="form-group">
                  <label for="email_confirm">@lang('messages.profile_page_email_confirm_label')<span class="red_stars">*</span></label>
                  {!! Form::input('text', 'email_confirm', isset(session('clientData')['email_confirm']) ? session('clientData')['email_confirm'] : '', ['class' => 'form-control', 'id'=>'email_confirm']) !!}
                </div>
                <div class="form-group">
                  <label for="number">@lang('messages.profile_page_password_label')<span class="red_stars">*</span></label>
                  {!! Form::input('password', 'password', isset(session('clientData')['old_password']) ? session('clientData')['old_password'] : '', ['class' => 'form-control', 'id'=>'password']) !!}
                </div>
              </div>
            @endif
            <div class="form-inline">
              <div class="form-group">
                <label for="company_name">@lang('messages.profile_page_company_name_label')</label>
                {!! Form::input('text', 'company_name', isset(session('clientData')['company_name']) ? session('clientData')['company_name'] : '', ['class' => 'form-control', 'id'=>'company_name']) !!}
              </div>
              <div class="form-group pickArrival">
                <label for="date_of_birth">@lang('messages.profile_page_date_of_birth_label') <img src="{!! asset('image/red_tooltip.png') !!}" style="background: none;display: inline-block;padding: 0;" title="{{ trans('messages.date_of_birth_tooltip') }}" class="info-birth"/> </label>
                {!! Form::input('text', 'date_of_birth', isset(session('clientData')['date_of_birth']) ? Carbon\Carbon::parse(session('clientData')['date_of_birth'])->format('Y-m-d') : '', ['class' => 'form-control', 'id'=>'datepicker-birth']) !!}
              </div>
            </div>
            <h3 class="step_four_title">Véhicule</h3>
            <div class="form-inline">
              <div class="form-group">
                <label for="car_brand">@lang('messages.profile_page_car_brand_label')<span class="red_stars">*</span></label>
                {!! Form::input('text', 'car_brand', isset(session('clientData')['car_brand']) ? session('clientData')['car_brand'] : '', ['class' => 'form-control', 'id'=>'car_brand', 'placeholder'=> trans('messages.profile_page_car_brand_label') ]) !!}
              </div>
              <div class="form-group">
                <label for="car_model">@lang('messages.profile_page_car_model_label')<span class="red_stars">*</span></label>
                {!! Form::input('text', 'car_model', isset(session('clientData')['car_model']) ? session('clientData')['car_model'] : '', ['class' => 'form-control', 'id'=>'car_model', 'placeholder'=> trans('messages.profile_page_car_model_label') ]) !!}
              </div>
            </div>
              <div class="form-group">
                <label for="car_registration_number">@lang('messages.profile_page_car_registration_number_label') <span class="red_stars">*</span></label>
                {!! Form::input('text', 'car_registration_number', isset(session('clientData')['car_registration_number']) ? session('clientData')['car_registration_number'] : '', ['class'=>'form-control', 'id'=>'car_registration_number', 'placeholder'=> trans('messages.profile_page_car_registration_number_label') ]) !!}
              </div>
          </div>
          <div class="col-sm-6 mt-45">
            <div class="form-group">
              <label for="adresse">@lang('messages.profile_page_adress_label')<span class="red_stars">*</span></label>
              {!! Form::input('text', 'adress', isset(session('clientData')['adress']) ? session('clientData')['adress'] : '', ['class' => 'form-control', 'id'=>'adress', 'placeholder'=> trans('messages.profile_page_adress_label') ]) !!}
            </div>
            <div class="form-group">
              <label for="city">@lang('messages.profile_page_city_label')<span class="red_stars">*</span></label>
              {!! Form::input('text', 'city', isset(session('clientData')['city']) ? session('clientData')['city'] : '', ['class' => 'form-control', 'id'=>'city', 'placeholder'=> trans('messages.profile_page_city_label') ]) !!}
            </div>
            <div class="form-group">
              <label for="postcode">Code postal <span class="red_stars">*</span></label>
              {!! Form::input('text', 'postcode', isset(session('clientData')['postcode']) ? session('clientData')['postcode'] : '', ['class' => 'form-control', 'id'=>'postcode', 'placeholder'=> 'Postcode / Zip' ]) !!}
            </div>
          </div>
          <div class="col-sm-6 {{ session('success_auth_client') == '1' ? 'mt-15-min' : 'mt-85' }}">
            <h3 class="step_four_title">Information complémentarie</h3>
            <div class="form-group">
              <label for="flight_number">Numéro de vol retour <span class="red_stars">*</span></label>
              {!! Form::input('text', 'flight_number', isset(session('clientData')['flight_number']) ? session('clientData')['flight_number'] : '', ['class' => 'form-control', 'id'=>'flight_number', 'placeholder'=> 'Numéro de vol retour' ]) !!}
            </div>
            <div class="form-group">
            <label for="description">@lang('messages.profile_page_description_label')</label>
              {!! Form::textarea('description', isset(session('clientData')['description']) ? session('clientData')['description'] : '', ['class' => 'form-control', 'id'=>'description', 'rows'=>'3', 'placeholder'=> trans('Commentaires concernant votre commande, ex : consignes de livraison.') ]) !!}
            </div>
          </div>
          <div class="col-xs-12 text-center">
            <input class="step_four button" type="submit" value="{{ trans('messages.reservation_step_next') }}">
          </div>
      </form>
      <div class="col-xs-12 text-center">
        <input type="button" class="step_five return_button" value="Retour">
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {

    $('.info-birth').tooltip();

    $('#checkoutForm').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'has-success',
            invalid: 'has-error'
        },
        fields: {
            last_name: {
                validators: {
                    notEmpty: {
                        message: 'The name is required'
                    },
                }
            },
            first_name: {
                validators: {
                    notEmpty: {
                        message: 'The name is required'
                    }
                }
            },
            email: {
                validators: {
                  emailAddress: {
                    message: 'The value is not a valid email address'
                  },
                  // identical: {
                  //     field: 'email_confirm',
                  //     // message: 'The email and its confirm are not the same'
                  // },
                  regexp: {
                    regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                    message: 'The value is not a valid email address'
                  }
                }
              }, 
            phone: {
                validators: {
                    notEmpty: {
                        message: 'The phone is required'
                    }
                }
            },
            email_confirm: {
                validators: {
                  // emailAddress: {
                  //   message: 'The value is not a valid email address'
                  // },
                  identical: {
                      field: 'email',
                      message: 'The email and its confirm are not the same'
                  },
                  // regexp: {
                  //   regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
                  //   message: 'The value is not a valid email address'
                  // }
                }
              }, 
            password: {
                validators: {
                    notEmpty: {
                        message: 'The password is required'
                    }
                }
            },
            adress:{
              validators: {
                    notEmpty: {
                        message: 'The address is required'
                    }
                }
            },
            city:{
              validators: {
                    notEmpty: {
                        message: 'The city is required'
                    }
                }
            },
            postcode:{
              validators: {
                    notEmpty: {
                        message: 'The postcode/Zip is required'
                    }
                }
            },
            car_brand:{
              validators: {
                    notEmpty: {
                        message: 'The car brand is required'
                    }
                }
            },
            car_model:{
              validators: {
                    notEmpty: {
                        message: 'The car model is required'
                    }
                }
            },
            car_registration_number:{
              validators: {
                    notEmpty: {
                        message: 'The car registration number is required'
                    }
                }
            },
            flight_number:{
              validators: {
                    notEmpty: {
                        message: 'The flight number is required'
                    }
                }
            }, 
        }
    });
});
</script>      
@endsection