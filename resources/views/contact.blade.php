@section('page-title', 'Parking Aéroport Cointrin')
@section('page-description', "Contactez-nous et Réservez votre place de parking à proximité de l’Aéroport International de Genève Cointrin aux meilleurs prix !")
@extends('layouts.app')
@section('plugin-js')
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCLuX2-o4cCkmw7um4uaWKhoguEFOOc2K0" type="text/javascript"></script>
  <!-- https://maps.googleapis.com/maps/api/js?key=AIzaSyCLuX2-o4cCkmw7um4uaWKhoguEFOOc2K0&callback=initMap -->
  <script type="text/javascript" src="{!! asset('js/maps.js') !!}"></script>
  <script type="text/javascript" src="{!! asset('js/maps2.js') !!}"></script>
@endsection
@section('content')

<div class="container-fluid page-content">
  <!-- <div id="googleMap"></div> -->
  <div id="googleMap2"></div>
  <div class="container">
    <div class="row text-center contact-info-open">
      <div class="col-md-4 col-md-offset-4">
        <p class="title_contact_section">@lang('messages.navet_title')</p>
        <p>@lang('messages.open_from_to')</p>
      </div>
    </div>
    <div class="row text-center contact-info">
      <div class="col-sm-4" data-aos="zoom-in" data-aos-delay="300">
        <div class="img-back">
          <img src="{!! asset('image/contact-icon-1.png') !!}" alt="">
          <p>
            @if(!empty($data))
              {{ $data->navet_address1 }}
            </br>
              {{ $data->navet_address2 }}
            @endif
          </p>
        </div>
      </div>
      <div class="col-sm-4" data-aos="zoom-in" data-aos-delay="400">
        <div class="img-back">
          <img src="{!! asset('image/contact-icon-2.png') !!}" alt="">
          <p><a href="tel:022 510 14 40">@lang('messages.contact_page_telephone_label')
              @if(!empty($data))
                {{ $data->navet_telephone }}
              @endif</a>
          </p>
        </div>
      </div>
      <div class="col-sm-4" data-aos="zoom-in" data-aos-delay="400">
        <div class="img-back">
          <img src="{!! asset('image/contact-icon-3.png') !!}" alt="">
          <p><a href="mailto:info@gvapark.ch">
              @if(!empty($data))
                {{ $data->navet_email }}
              @endif</a>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container-fluid page-content">
  <div id="googleMap"></div>
  <!-- <div id="googleMap2"></div> -->
  <div class="container">
    <div class="row text-center contact-info-open">
      <div class="col-md-4 col-md-offset-4">
        <p class="title_contact_section">@lang('messages.valet_title')</p>
        <p>@lang('messages.open_from_to')</p>
      </div>
    </div>
    <div class="row text-center contact-info">
      <div class="col-sm-4" data-aos="zoom-in" data-aos-delay="300">
        <div class="img-back">
          <img src="{!! asset('image/contact-icon-1.png') !!}" alt="">
          <p>
            @if(!empty($data))
              {{ $data->address1 }}
            </br>
              {{ $data->address2 }}
            @endif
          </p>
        </div>
      </div>
      <div class="col-sm-4" data-aos="zoom-in" data-aos-delay="400">
        <div class="img-back">
          <img src="{!! asset('image/contact-icon-2.png') !!}" alt="">
          <p><a href="tel:022 510 14 40">@lang('messages.contact_page_telephone_label')
              @if(!empty($data))
                {{ $data->telephone }}
              @endif</a>
          </p>
        </div>
      </div>
      <div class="col-sm-4" data-aos="zoom-in" data-aos-delay="400">
        <div class="img-back">
          <img src="{!! asset('image/contact-icon-3.png') !!}" alt="">
          <p><a href="mailto:info@gvapark.ch">
              @if(!empty($data))
                {{ $data->email }}
              @endif</a>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
