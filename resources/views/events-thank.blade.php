@extends('layouts.events')

@section('content')
    <main class="main">
        <section style="background-image: url({!! asset('events/css/images/bg.jpg') !!}); height: 82.6vh" class="section section_background">
            <div class="section__body">
                <div class="shell">
                    <div class="thankYou">
                        <h1 class="thankYou__title">@lang('messages.reservation_confirm_label')</h1><!-- /.thankYou__title -->
                        <p style="text-align:center">
                            <strong>@lang('messages.reservation_confirm_label_strong')<strong>
                        </p>
                        <p style="text-align:center">
                            @lang('messages.reservation_confirm_label_second')
                        </p>
                        <p style="text-align:center" class="last-p">
                            @lang('messages.reservation_confirm_label_last')
                        </p>
                        <a target="_blank" href="{{ route('generate.invoice.event', $reservation->id) }}" class="btn btn_bordered">Download invoice</a>
                    </div><!-- /.thankYou -->
                </div><!-- /.shell -->
            </div><!-- /.section__body -->
        </section><!-- /.section -->
    </main>
@endsection