@section('page-title', 'Parking Service à l’Aéroport International de Genève - GVApark.ch')
@section('page-description', 'GVApark vous propose le Parking Service à l’Aéroport International de Genève. Réserver 7 jours sur 7 votre place de parking au prix IMBATTABLE')
@extends('layouts.app')
@section('content')
<div class="container-fluid page-content">
    <div class="container">
      <div class="row text-center">
        <h2 data-aos="zoom-in"  data-aos-delay="300">
          @if(!empty($data))
            {{ $data->main_info_title }}
          @endif
        </h2>
      </div>
      <div class="row"  data-aos="zoom-in" data-aos-delay="300">
        <p class="subtitle">
          @if(!empty($data))
            {{ $data->main_info_description }}
          @endif
        </p>
      </div>
      <div class="row service" data-aos="zoom-in" data-aos-delay="400">
        <p class="option-label">1.</p>
        <h3>
          @if(!empty($data))
            {{ $data->online_booking_info_title }}
          @endif
        </h3>
        <div class="page-text">
          <p>
          @if(!empty($data))
            {{ $data->online_booking_info_description }}
          @endif
          </p>
        </div>
      </div>
      <div class="row service" data-aos="zoom-in" data-aos-delay="500">
        <p class="option-label">2.</p>
        <h3>
          @if(!empty($data))
            {{ $data->post_your_vehicle_info_title }}
          @endif
        </h3>
        <div class="page-text">
          <p>
          @if(!empty($data))
            {{ $data->post_your_vehicle_info_description }}
          @endif
          </p>
        </div>
      </div>
      <div class="row service" data-aos="zoom-in" data-aos-delay="600">
        <p class="option-label">3.</p>
        <h3>
          @if(!empty($data))
            {{ $data->travel_info_title }}
          @endif
        </h3>
        <div class="page-text">
          <p>
          @if(!empty($data))
            {{ $data->travel_info_description }}
          @endif
          </p>
        </div>
      </div>
      <div class="row service" data-aos="zoom-in" data-aos-delay="700">
        <p class="option-label">4.</p>
        <h3>
          @if(!empty($data))
            {{ $data->pickup_info_title }}
          @endif
        </h3>
        <div class="page-text">
          <p>
          @if(!empty($data))
            {{ $data->pickup_info_description }}
          @endif
          </p>
        </div>
      </div>
    </div>
  </div>
@endsection
