@section('page-title', 'Parking Aéroport de Genève Tarifs')
@section('page-description', "Réservez votre place de parking à l'aéroport de Genève au meilleur tarifs possible. Réservez en ligne sur notre site votre place de parking 7jours sur 7")
@extends('layouts.app')
@section('content')
<div class="container-fluid page-content">
    <div class="container">
        <div class="row text-center">
            <h2 data-aos="zoom-in">@lang('messages.reservation_page_first_title')</h2>
        </div>
        <div class="row reservation">
          @if (count($errors) > 0)
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
          @endif
            {!! Form::open(['action' => 'ReservationController@postReservationCart', 'method' => 'POST', 'class'=>'reservation-section-form']) !!}
            {!! Form::hidden('check_type', 1) !!}
            <div class="container pb-40">
                <div class="row">
                    <div class="col-md-8 col-sm-12 col-md-offset-2 mb-40">

                        <div class="form_row">
                            <div class="input-inline pickArrival">
                                <label>@lang('messages.reservation_date_arrival')</label>
                                <div class="form_control">
                                    {!! Form::input('text', 'date_of_arrival', session('selectedDates') ? session('selectedDates')['date_of_arrival'] : \Carbon\Carbon::now()->format('Y/m/j'), ['class' => 'form-control', 'id'=>'datepicker-reservation-1']) !!}
                                </div><!-- /.form_control -->
                            </div><!-- /.input-inline -->

                            <div class="input-inline_time timeArrival">
                                <label class="label-center">@lang('messages.reservation_hour_arrival')</label>
                                <div class="form_control_time">
                                    {!! Form::input('text', 'hour_of_arrival', session('selectedDates') ? session('selectedDates')['hour_of_arrival'] : 'HH:MM', ['class' => 'form-control form-control field field__icon', 'id'=>'timeArrival']) !!}
                                </div><!-- /.form_control_time -->
                            </div><!-- /.input-inline_time -->
                        </div><!-- /.form_row -->

                        <div class="form_row">
                            <div class="input-inline pickDeparture">
                                <label>@lang('messages.reservation_date_departure')</label>
                                <div class="form_control">
                                    {!! Form::input('text', 'date_of_departure', session('selectedDates') ? session('selectedDates')['date_of_departure'] : \Carbon\Carbon::tomorrow()->format('Y/m/j'), ['class' => 'form-control ', 'id'=>'datepicker-reservation-2']) !!}
                                </div><!-- /.form_control -->
                            </div><!-- /.input-inline -->

                            <div class="input-inline_time timeDeparture">
                                <label class="label-center">@lang('messages.reservation_hour_departure')</label>
                                <div class="form_control_time">
                                    {!! Form::input('text', 'hour_of_departure', session('selectedDates') ? session('selectedDates')['hour_of_departure'] : 'HH:MM', ['class' => 'form-control field field__icon', 'id'=>'timeDeparture']) !!}
                                </div><!-- /.form_control -->
                            </div><!-- /.input-inline_time -->
                        </div><!-- /.form_row -->

                    </div>

                    <div class="shell">
                        <div class="cards">
                            <div {{ (session('disable_valet') == '0') ? "style=display:block" : "style=display:none"}} class="vip card enabled {{ (session('disable_valet') == '0' && session('selectedDates')['reservation_type'] == 'valet') ? 'active' : '' }} {{ (isset(session('selectedDates')['reservation_type']) && session('selectedDates')['reservation_type'] == 'valet') ? 'active' : '' }}">
                                <div class="card__inner-wrapper">
                                    <input id="radio-2" class="radio-custom" name="reservation_type" {{ session('disable_valet') == '0' ? 'checked' : '' }} value="valet" type="radio">
                                    <label for="radio-2" class="radio-custom-label">VIP</label>

                                    <div class="card__inner">
                                        <figure class="card__image">
                                            <img src="{!! asset('image/vip-red.png') !!}" class="vip-red" alt="" />

                                            <img src="{!! asset('image/vip-white.png') !!}" class="vip-white" alt="" />
                                        </figure>

                                        <div class="card__content">
                                            <h3>@lang('messages.valet_title')</h3>

                                            <p>@lang('messages.valet_description')</p>

                                            <h2>CHF {{ isset(session('priceList')['valet_total']) ? number_format(session('priceList')['valet_total'], 2) : '--' }}</h2>
                                        </div>
                                    </div>

                                    <img src="{!! asset('image/info.png') !!}" alt="" class="info info-white" />
                                    <img src="{!! asset('image/info-black.png') !!}" alt="" class="info info-black" />

                                    <div class="card__tooltip">
                                        <p>@lang('messages.valet_tooltip')</p>
                                    </div>
                                </div>
                            </div>

                            <div {{ ( session('disable_valet') == '1' ) ? "style=display:block" : "style=display:none" }} class="vip card disabled">
                                <div class="card__inner-wrapper disabled">
                                    <input id="radio-2" class="radio-custom" disabled="disabled" name="reservation_type" value="valet" type="radio">
                                    <label for="radio-2" class="radio-custom-label">VIP</label>
                                    <div class="card__inner disabled">
                                        <figure class="card__image disabled">
                                            <img src="{!! asset('image/vip-red.png') !!}" class="vip-red" alt="" />

                                            <img src="{!! asset('image/vip-white.png') !!}" class="vip-white" alt="" />
                                        </figure>
                                        <div class="card__content disabled">
                                            <h3>@lang('messages.valet_title_disabled')</h3>

                                            <p>@lang('messages.valet_description_disabled')</p>
                                            @if (session('disable_valet') == '1')
                                            <h2>CHF --</h2>
                                            @else
                                            <h2>CHF {{ isset(session('priceList')['valet_total']) ? session('priceList')['valet_total'] : '--' }}</h2>
                                            @endif
                                        </div>
                                    </div>
                                    <img src="{!! asset('image/info.png') !!}" alt="" class="info info-white" />
                                    <img src="{!! asset('image/info-black.png') !!}" alt="" class="info info-black" />
                                    <div class="card__tooltip disabled">
                                        <p>@lang('messages.valet_tooltip_disabled')</p>
                                    </div>
                                </div>
                            </div>

                            <div class="normal card {{ session('disable_valet') == '1' ? 'active' : '' }} {{ (isset(session('selectedDates')['reservation_type']) && session('selectedDates')['reservation_type'] == 'navet') ? 'active' : '' }}">
                                <div class="card__inner-wrapper">
                                    <input id="radio-1" class="radio-custom" {{ session('disable_valet') == '1' ? 'checked' : '' }} {{ (isset(session('selectedDates')['reservation_type']) && session('selectedDates')['reservation_type'] == 'navet') ? 'checked' : '' }} name="reservation_type" value="navet" type="radio">
                                    <label for="radio-1" class="radio-custom-label">Normal</label>
                                    
                                    <div class="card__inner">
                                        <figure class="card__image">
                                            <img src="{!! asset('image/normal.png') !!}" class="normal-white" alt="" />
                                            <img src="{!! asset('image/normal-red.png') !!}" class="normal-red" alt="" />
                                        </figure><!-- /.card__image -->
                                        
                                        <div class="card__content">
                                            <h3>@lang('messages.navet_title')</h3>

                                            <p>@lang('messages.navet_description')</p>

                                            <h2>CHF {{ isset(session('priceList')['navet_total']) ? number_format(session('priceList')['navet_total'], 2) : '--' }}</h2>
                                        </div><!-- /.card__content -->
                                    </div><!-- /.card__inner -->

                                    <img src="{!! asset('image/info.png') !!}" alt="" class="info info-white" />
                                    <img src="{!! asset('image/info-black.png') !!}" alt="" class="info info-black" />

                                    <div class="card__tooltip">
                                        <p>@lang('messages.navet_tooltip')</p>
                                    </div><!-- /.card__tooltip -->
                                </div><!-- /.card__inner-wrapper -->
                            </div><!-- /.normal -->
                        </div><!-- /.cards -->
                    </div><!-- /.shell -->
                    @if (isset($data['show_reservation_options']) && $data['show_reservation_options'] == 1)
                    <div class="col-md-4 col-sm-5 col-xs-12 col-md-offset-4">
                        <div class="row additional">
                            <p>@lang('messages.reservation_page_options_title')</p>
                            <img src="{!! asset('image/reservation-icon-1.jpg') !!}" alt="">
                            <div class="form-check">
                                <label class="form-check-label">
                                    {!! Form::radio('options', 'null', ((session('selectedDates')['options'] != 'null') && (session('selectedDates')['options'] != '') ) ? false : true, ['id' => 'radio-1-4', 'class' => 'form-check-input regular-radio']) !!}
                                    <label for="radio-1-4"></label>@lang('messages.reservation_page_options_none_title')
                                </label>
                                <label class="form-check-label">
                                    {!! Form::radio('options', 'intereur', session('selectedDates')['options'] == 'intereur' ? true : false, ['id' => 'radio-1-1', 'class' => 'form-check-input regular-radio']) !!}
                                    <label for="radio-1-1"></label>CHF {{$data['intereur_price']}} @lang('messages.reservation_page_options_intereur_title')
                                </label>
                                <label class="form-check-label">
                                    {!! Form::radio('options', 'extereur', session('selectedDates')['options'] == 'extereur' ? true : false, ['id' => 'radio-1-2', 'class' => 'form-check-input regular-radio']) !!}
                                    <label for="radio-1-2"></label>CHF {{$data['extereur_price']}} @lang('messages.reservation_page_options_extereur_title')
                                </label>
                                <label class="form-check-label">
                                    {!! Form::radio('options', 'extra', session('selectedDates')['options'] == 'extra' ? true : false, ['id' => 'radio-1-3', 'class' => 'form-check-input regular-radio']) !!}
                                    <label for="radio-1-3"></label>CHF {{$data['extra_price']}} @lang('messages.reservation_page_options_extra_title')
                                </label>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <div class="row button text-center">
                <div class="col-xs-12">
                    {!! Form::submit('Reservation') !!}
                </div>
            </div>
            {!! Form::close() !!}
            <input type="hidden" name="date_arrival_calc">
            <input type="hidden" name="date_departure_calc">
        </div>
    </div>
</div>
<script type="text/javascript">
    $( document ).ready(function() {

        $normal = $('.normal');
        $vip = $('.vip');

        $normal.click(function(){
            $(this).addClass('active');
            $(this).find('#radio-1').prop('checked',true);
            $('#radio-2').prop('checked',false);
            $vip.removeClass('active');
        });

        $vip.click(function(){
            if (!$(this).hasClass('disabled')) {
                $(this).addClass('active');
            }
            $(this).find('#radio-2').prop('checked',true);
            $('#radio-1').prop('checked',false);
            $normal.removeClass('active');
        });

        $( ".card" ).hover(function() {
          $(this).find('.card__tooltip').toggleClass('active')
        });

        $('.vip.disabled').click(function(){
            return false;
        });

        $('input[name="date_arrival_calc"]').val($('#datepicker-reservation-1').val());
        $('input[name="date_departure_calc"]').val($('#datepicker-reservation-2').val());

    });
</script>
@endsection
