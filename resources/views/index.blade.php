@section('page-title', 'Valet Parking Aéroport Genève, Parking Aéroport pas cher-GVApark')
@section('page-description', 'Le moins cher parking à l’Aéroport International de Genève. Voyagez en toute sérénité grâce à notre service de Valet Parking. Réserver 7jours/7')
@extends('layouts.app')
@section('content')
  <div class="container-fluid page-content">
    <div class="container">
      <!--
      <div class="promo text-center">
         <h4>VENTE FLASH</h4>
        <p>GVA Park -10% sur votre prochain place de parking avec le promo code <span>ILOVEGVAPARK</span></p>
      </div>
      -->
      <div class="row text-center">
        <h2 data-aos="zoom-in"  data-aos-delay="300">
          @if(!empty($data))
            {{ $data->title }}
          @endif
        </h2>
      </div>
      <div class="row page-text">
        <p>
          @if(!empty($data))
            {{ $data->description }}
          @endif
        </p>
      </div>
    </div>
    <div class="container-fluid ">
      <div class="row text-center">
        <h2 data-aos="zoom-in">@lang('messages.index_page_first_title')</h2>
      </div>
      <div class="container-fluid services">
        <div class="container">
          <div class="row text-center">
            <div class="col-sm-3" data-aos="zoom-in"  data-aos-delay="300">
              <img src="image/services-icon-1.png" alt="">
              <h4>@lang('messages.index_page_security_title')</h4>
              @if(!empty($data))
                <p>{{ $data->security_info }}</p>
              @endif
            </div>
            <div class="col-sm-3" data-aos="zoom-in"  data-aos-delay="400">
              <img src="image/services-icon-2.png" alt="">
              <h4>@lang('messages.index_page_simplicity_title')</h4>
              @if(!empty($data))
                <p>{{ $data->simplicity_info }}</p>
              @endif
            </div>
            <div class="col-sm-3" data-aos="zoom-in"  data-aos-delay="500">
              <img src="image/services-icon-3.png" alt="">
              <h4>@lang('messages.index_page_speed_title')</h4>
              @if(!empty($data))
                <p>{{ $data->speed_info }}</p>
              @endif
            </div>
            <div class="col-sm-3" data-aos="zoom-in"  data-aos-delay="600">
              <img src="image/services-icon-4.png" alt="">
              <h4>@lang('messages.index_page_service_title')</h4>
              @if(!empty($data))
                <p>{{ $data->service_info }}</p>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row text-center">
        <h2 data-aos="zoom-in">@lang('messages.index_page_second_title')</h2>
      </div>
      <div class="row page-table" data-aos="fade-up"     data-aos-anchor-placement="top-bottom">
        <table>
          <tr class="header-table">
            <th></th>
            <th>@lang('messages.index_page_table_header_one')</th>
            <th>@lang('messages.index_page_table_header_two')</th>
            <th>@lang('messages.index_page_table_header_three')</th>
          </tr>
          <!-- <tr>
            <th>GVA PARK PRIVILEGE</th>
            <th data-th="1ER Jour"> CHF {{ number_format($system->vip_daily_price, 2) }}</th>
            <th data-th="JOUR SUP."> CHF {{ number_format($system->vip_daily_price, 2) }}</th>
            <th data-th="PRISE EN CHARGE"> CHF {{ number_format($system->vip_initial_price, 2) }}</th>
          </tr>
          <tr>
            <th>GVA PARK SELF</th>
            <th data-th="1ER Jour"> CHF {{ number_format($system->daily_price, 2) }}</th>
            <th data-th="JOUR SUP."> CHF {{ number_format($system->daily_price, 2) }}</th>
            <th data-th="PRISE EN CHARGE"> CHF {{ number_format($system->initial_price, 2) }}</th>
          </tr> -->
          @if (count($concurents) > 0)
          @foreach ($concurents as $concurent)
          @if ($concurent->is_main == 1)
          <tr>
            <th> {{ $concurent->name }} </th>
            <th  data-th="1ER Jour"> {{ $concurent->daily_price }} </th>
            <th data-th="JOUR SUP."> {{ $concurent->daily_price_sup }} </th>
            <th data-th="PRISE EN CHARGE"> {{ $concurent->initial_price }} </th>
          </tr>
          @else
          <tr>
            <td> {{ $concurent->name }} </td>
            <td  data-th="1ER Jour"> {{ $concurent->daily_price }} </td>
            <td data-th="JOUR SUP."> {{ $concurent->daily_price_sup }} </td>
            <td data-th="PRISE EN CHARGE"> {{ $concurent->initial_price }} </td>
          </tr>
          @endif
          @endforeach
          @endif
        </table>
      </div>
    </div>
    <div class="container-fluid option">
        <div class="container">
          <div class="row text-center">
            @if (!empty($faqData))
            @foreach ($faqData as $k => $row)
            <div class="col-sm-4" data-aos="zoom-in">
              <p class="option-label">{!! $k !!}.</p>
              <h4>{!! $groups[app()->getLocale()][$k] !!}</h4>
               <div class="question">
              @foreach ($row as $faq)
                 <p data-toggle="collapse" data-target="#option{!! $faq['group_id'].'-'.$faq['id'] !!}" href="#option{!! $faq['group_id'].'-'.$faq['id'] !!}" class="collapsed">{!! $faq['question'] !!}</p>
                 <div id="option{!! $faq['group_id'].'-'.$faq['id'] !!}" class="answer collapse">
                   {!! $faq['answer'] !!}
                 </div>
               @endforeach
               </div>
            </div>
            @endforeach
            @endif
          </div>
        </div>
    </div>
  </div>
@endsection
