﻿<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}">

    <title>GVA Park Events</title>

    <link rel="shortcut icon" type="image/x-icon" href="{!! asset('events/css/images/favicon.ico') !!}" />

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,600" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.3.2/flatpickr.min.css" integrity="sha256-oGv0CSOLhZn4PcwlARoJZUxz2JyBG9K2Foduh6/47vY=" crossorigin="anonymous" />

    <!-- <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script> -->

    <!-- <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet"> -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" href="{!! asset('events/css/style.css') !!}" />
    <link rel="stylesheet" href="{!! asset('events/css/custom.css') !!}" />

    <script src="{!! asset('events/vendors/jquery/jquery-3.2.1.min.js') !!}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.3.2/flatpickr.min.js" integrity="sha256-oGDWBftOALdimipQLWh2ZUDwX1ytj0gX9Umhm5631Jk=" crossorigin="anonymous"></script>
    <script src="{!! asset('events/vendors/app.js') !!}"></script>

    <script src="https://pay.datatrans.com/upp/payment/js/datatrans-1.0.2.js"></script>
</head>
<body>
    <div class="wrapper">

        <header class="header">
            <div class="shell">
                <a href="{{ route('home') }}" class="logo">
                    <img src="{!! asset('events/css/images/logo.png') !!}" alt="" />
                </a>
                <nav class="menu">
                    <ul class="active">
                        <li>
                            <a href="{{ route('home') }}">@lang('messages.menu_home')</a>
                        </li>
                        <li>
                            <a href="{{ route('parking-service') }}">@lang('messages.menu_our_service')</a>
                        </li>
                        <li>
                            <a href="{{ route('reservation.one.get') }}">@lang('messages.menu_reservation')</a>
                        </li>
                        <li>
                            <a href="{{ route('avis-clients') }}">@lang('messages.menu_avis_clients')</a>
                        </li>
                        <li>
                            <a href="{{ route('video') }}">@lang('messages.menu_videos')</a>
                        </li>
                        <li>
                            <a href="{{ route('contact') }}">@lang('messages.menu_contact')</a>
                        </li>
                        <li>
                            <a href="{{ route('events.index') }}">Events</a>
                        </li>
                    </ul>
                    <div class="nav-lang">
                        <select onChange="window.location.href=this.value" class="">
                            @foreach (config('app.locales') as $lang => $language)
                                <option data-display-text="{{$lang}}" value="{{ route('lang.switch', [$lang]) }}" {{ app()->getLocale() == $lang ? 'selected' : '' }}>{{ $language }}</option>
                            @endforeach
                        </select>
                    </div><!-- /.nav-lang -->
                    <a class="toggle-nav"><i class=" fa fa-bars fa-2x " aria-hidden="true "></i></a>
                </nav><!--/.menu-->
                
            </div><!-- /.shell -->
        </header><!-- /.header -->

        @yield('content')

        <footer class="footer">
            <div class="shell">
                <div class="copyrights">
                    <p>Copyright © 2018 GVA Park. All rights reserved.</p>
                </div><!-- /.copyrights -->

                <div class="credits">
                    <a href="#" target="_blank">Created by <img src="{!! asset('events/css/images/pw.png') !!}" alt="" /></a>
                </div><!-- /.credits -->
            </div><!-- /.shell -->
        </footer><!-- /.footer -->
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBrJskn8I0ctCSMnWou_42TUno3ggY9STI&callback=initMap"></script>
        <script>
        $('.toggle-nav').click(function(e) {
            $(this).toggleClass('active');
            $('nav.menu ul').slideToggle();
            // $('.select-lang').slideToggle().css('display', 'block');
            e.preventDefault();
        });
        </script>
    </div><!-- /.wrapper -->
</body>
</html>