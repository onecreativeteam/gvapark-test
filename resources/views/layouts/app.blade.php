﻿<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
  <title>@yield('page-title')</title>
  <meta name="description" content="@yield('page-description')"/>
  <link rel="shortcut icon" type="image/png" href="{!! asset('image/favoticon.png') !!}"/>

  @yield('plugin-css')

  <link rel="stylesheet" href="{!! asset('lib/bootstrap/css/bootstrap.min.css') !!}">
  <link rel="stylesheet" href="{!! asset('lib/jquery-ui/jquery-ui.css') !!}">
  <link rel="stylesheet" href="{!! asset('lib/bootstrap-select/css/bootstrap-select.min.css') !!}">
  <link rel="stylesheet" href="{!! asset('lib/aos-master/aos.css') !!} ">
  <link rel="stylesheet" href="{!! asset('css/style.css') !!}">
  <link rel="stylesheet" href="//jonthornton.github.io/jquery-timepicker/jquery.timepicker.css">

  <script type="text/javascript" src="{!! asset('js/jquery-2.2.4.min.js') !!}"></script>
  <script src="{!! asset('lib/bootstrap/js/bootstrap.min.js') !!}"></script>
  <script src="{!! asset('lib/bootstrap-select/js/bootstrap-select.min.js') !!}"></script>
  <script src="{!! asset('lib/jquery-ui/jquery-ui.js') !!}"></script>
  <script src="{!! asset('lib/aos-master/aos.js') !!}"></script>
  
  @yield('plugin-js')

  <script type="text/javascript" src="{!! asset('js/custom.js') !!}"></script>

  @yield('data-layer')

  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({
    'gtm.start':  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-P9MRJXX');</script>
  <!-- End Google Tag Manager -->

  <!--
  <script src="https://pay.datatrans.com/upp/payment/js/datatrans-1.0.2.js"></script>
  -->
  <script src="https://pay.sandbox.datatrans.com/upp/payment/js/datatrans-1.0.2.js"></script>
  <script src="{!! asset('lib/formValidation/formValidator.js') !!}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.8.1/jquery.timepicker.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.1.4/flatpickr.js" integrity="sha256-uhIjVpGyImjyjj0lQtNVOZbE8nl1uXiRjhl1TOTGIJI=" crossorigin="anonymous"></script>
  
  <!-- Hotjar Tracking Code for www.gvapark.ch -->
  <script>
  (function(h,o,t,j,a,r){
  h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
  h._hjSettings={hjid:747129,hjsv:6};
  a=o.getElementsByTagName('head')[0];
  r=o.createElement('script');r.async=1;
  r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
  a.appendChild(r);
  })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
  </script>
  <!-- Hotjar Tracking Code for www.gvapark.ch -->
</head>
<body class="
@if( \Route::current()->getName() == 'home' )
        home-page
@elseif( \Route::current()->getName() == 'avis-clients' )
      testimonials-page
@elseif( (\Route::current()->getName() == 'reservation.one.get') || (\Route::current()->getName() == 'video') )
        reservation-page
@elseif( \Route::current()->getName() == 'contact' )
        contact-page
@elseif( \Route::current()->getName() == 'parking-service' )
        notre-service-page
@elseif( (\Route::current()->getName() == 'reservation.one.get') || (\Route::current()->getName() == 'reservation.cart.post') )
        panier-page
@elseif( (\Route::current()->getName() == 'reservation.checkout.get') || (\Route::current()->getName() == 'reservation.checkout.post') )
        sortie-page
@elseif( \Route::current()->getName() == 'video' )
        videos-page
@else
      steps-page
@endif" style="width: 100% !important;">
<noscript>
  <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P9MRJXX" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<div class="container-fluid">
  <div class="row header">
    <div class="container-fluid menu">
      <div class="row">
        <div class="col-md-6 col-md-offset-6 col-xs-8 col-xs-offset-4 text-right">
           <a href="{{ route('reservation.one.get') }}" class="shop-car"></a>
          @if (session('success_auth_client') == 1)
            <a href="{{ route('reservation.show.client.profile', [session('clientData')['id']])}}" class="button-login name-acc"><span></span>{{ session('clientData')['first_name'] }} {{ session('clientData')['last_name'] }}</a>
            <a href="{{ route('reservation.client.logout') }}" class="button-login">@lang('messages.only_logout')</a>
          @else
            <a class="button-login" onClick="showClientLoginModal()"  id="initClientAuthModal">@lang('messages.only_login')</a>
          @endif
        </div>
        <div class="modal fade" id="clientCheckoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">@lang('messages.only_log_in')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="answer">
                      @lang('messages.order_description_instructions')
                    </div>
                    {!! Form::open(['action' => 'ReservationController@authClient', 'method' => 'POST']) !!}
                      <div class="form-group">
                        <label for="login_email"> @lang('messages.form_email')</label>
                        <input type="email" class="form-control" name="login_email" id="login_email">
                      </div>
                      <div class="form-group">
                       <label for="login_password"> @lang('messages.form_password')</label>
                       <input type="password" class="form-control" name="login_password" id="login_password">
                      </div>
                      <a class="btn btn-link" href="{{ route('password.reset') }}"> @lang('messages.form_password_reset')</a>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                {!! Form::submit(trans('messages.client_login_button'), array('class'=>'button')) !!}
                {!! Form::close() !!}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3 text-right col-sm-2">
          <a href="{{ route('home') }}">
            <img class="logo" src="{!! asset('image/logo.png') !!}" alt="">
            <img class="logo-mobile" src="{!! asset('image/logo-m.png') !!}" alt="">
          </a>
        </div>
        <div class="col-xl-8 col-xl-offset-1 navigation col-md-9 ">
          <nav class="navbar">
            <div class="container-fluid">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
              </div>
              <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                  <li class="{{ Request::is('/')? 'active': '' }}"><a href="{{ route('home') }}">@lang('messages.menu_home')</a></li>
                  <li class="{{ Request::is('parking-service')? 'active': '' }}"><a href="{{ route('parking-service') }}">@lang('messages.menu_our_service')</a></li>
                  <li class="{{ Request::is('reservation')? 'active': '' }}"><a href="{{ route('reservation.one.get') }}">@lang('messages.menu_reservation')</a></li>
                  <li class="{{ Request::is('avis-clients')? 'active': '' }}"><a href="{{ route('avis-clients') }}">@lang('messages.menu_avis_clients')</a></li>
                  <li class="{{ Request::is('videos')? 'active': '' }}"><a href="{{ route('video') }}">@lang('messages.menu_videos')</a></li>
                  <li class="{{ Request::is('contact')? 'active': '' }}"><a href="{{ route('contact') }}">@lang('messages.menu_contact')</a></li>
                  <li class="{{ Request::is('events.index')? 'active': '' }}"><a href="{{ route('events.index') }}">Events</a></li>
                  <li>
                    <div class="dropdown">
                      <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">{{ app()->getLocale() }}
                      <span class="caret"></span></button>
                      <ul class="dropdown-menu">
                        @foreach (config('app.locales') as $lang => $language)
                            @if ($lang != app()->getLocale())
                                <li>
                                    <a style="color:black" href="{{ route('lang.switch', [$lang]) }}">
                                        {{ $language }}
                                    </a>
                                </li>
                            @endif
                        @endforeach
                      </ul>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        </div>
      </div>
    </div>
  </div>
  @if (\Route::current()->getName() != 'reservation.one.get')
  <div class="row after-header">
    <div class="container text-center">
      <img src="{!! asset('image/car-icon.png') !!}" alt="" data-aos="zoom-in">
      <h3 data-aos="zoom-in"  data-aos-delay="100">@lang('messages.homepage_first_title')</h3>
      <img src="{!! asset('image/fly-icon.png') !!}" alt=""  data-aos="zoom-in">
    </div>
  </div>
  @endif
  <div class="modal fade" id="clientTermsAndConditionsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">@lang('messages.tc_title_main_2')</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">

              <div class="answer">
                <h4 style="font-family:'Open Sans';font-size:24px;color:#bf1e2d;text-align:center;">@lang('messages.tc_title_1')</h4>
                <p>@lang('messages.tc_description_1')</p>
              </div>

              <div class="answer">
                <h4 style="font-family:'Open Sans';font-size:24px;color:#bf1e2d;text-align:center;">@lang('messages.tc_title_2')</h4>
                <p>@lang('messages.tc_description_2')</p>
              </div>

              <div class="answer">
                <h4 style="font-family:'Open Sans';font-size:24px;color:#bf1e2d;text-align:center;">@lang('messages.tc_title_3')</h4>
                <p>
                  @lang('messages.tc_description_3')
                </p>
              </div>

              <div class="answer">
                <h4 style="font-family:'Open Sans';font-size:24px;color:#bf1e2d;text-align:center;">@lang('messages.tc_title_4')</h4>
                <p>
                  @lang('messages.tc_description_4')
                </p>
              </div>

              <div class="answer">
                <h4 style="font-family:'Open Sans';font-size:24px;color:#bf1e2d;text-align:center;">@lang('messages.tc_title_5')</h4>
                <p>
                  @lang('messages.tc_description_5')
                </p>
              </div>

              <div class="answer">
                <h4 style="font-family:'Open Sans';font-size:24px;color:#bf1e2d;text-align:center;">@lang('messages.tc_title_6')</h4>
                <p>
                  @lang('messages.tc_description_6')
                </p>
              </div>

              <div class="answer">
                <h4 style="font-family:'Open Sans';font-size:24px;color:#bf1e2d;text-align:center;">@lang('messages.tc_title_7')</h4>
                <p>
                  @lang('messages.tc_description_7')
                </p>
              </div>

              <div class="answer">
                <h4 style="font-family:'Open Sans';font-size:24px;color:#bf1e2d;text-align:center;">@lang('messages.tc_title_8')</h4>
                <p>
                  @lang('messages.tc_description_8')
                </p>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@yield('content')
<footer>
  <div class="container">
    <div class="col-sm-8">
      <div class="social text-center">
        <a href="https://www.facebook.com/gvapark" target="_blank">
          <span>@lang('messages.homepage_follow_us')</span>
          <span></span></a>
      </div>
      <a class="perspective_logo" target="_blank" href="https://perspectiveweb.eu">@lang('messages.homepage_copyright')</a>
      <div class="text-center" style="margin-bottom:20px">
        <meta class="netreviewsWidget" id="netreviewsWidgetNum10350" data-jsurl="//cl.avis-verifies.com/fr/cache/1/b/a/1ba5dcf9-461f-17c4-d903-78e3a32c285a/widget4/widget03-10350_script.js"/><script src="//cl.avis-verifies.com/fr/widget4/widget03.min.js"></script>
      </div>
    </div>
    <div class="col-sm-4">
      <img style="margin-left: 30%;" src="{!! asset('image/footer_4.png') !!}" alt="" />
      <p style="text-align:right"><a style="color:#fff;" class="" onClick="showTermsAndConditionsModal()" id="initTermsAndConditionsModal">@lang('messages.tc_title_main_2')</a></p>
    </div>
  </div>
</footer>
<script type="text/javascript">
    
    $('#datepicker-depart').datepicker({
      dateFormat: "yy/m/d",
      onSelect: function(dateText, inst) {
          var date = $(this).datepicker('getDate');
          var    day  = date.getDate();  
          var   month = date.getMonth() + 1;       
          var    year =  date.getFullYear();
          
          $('input[name="date_arrival_calc"]').val(year+'/'+month+'/'+day);
          setCalculatedPrices();
      }
    });

    $('#datepicker-retour').datepicker({
      dateFormat: "yy/m/d",
      onSelect: function(dateText, inst) {
          var date = $(this).datepicker('getDate');
          var    day  = date.getDate();  
          var   month = date.getMonth() + 1;       
          var    year =  date.getFullYear();
          
          $('input[name="date_departure_calc"]').val(year+'/'+month+'/'+day);
          setCalculatedPrices();
      }
    });

    function setCalculatedPrices() {

      var r_hour_arrival = '';
      var r_hour_departure = '';

      setTimeout(function() {
        r_hour_arrival = $('#timeArrival').val();
        r_hour_departure = $('#timeDeparture').val();
      }, 100);

      if ($("body").hasClass("reservation-page")) {
        setTimeout(function() {
          var data = {};

          data.date_of_arrival = $("input[name='date_arrival_calc']").val();
          data.hour_of_arrival = r_hour_arrival.trim();
          data.date_of_departure = $("input[name='date_departure_calc']").val();
          data.hour_of_departure = r_hour_departure.trim();

          $.ajaxSetup( { headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() } } );
          $.ajax({
              method: "POST",
              url: "{{ route('get.calculated.prices') }}",
              dataType: "json",
              data: data,
              success: function (response) {
                if ($('.cards').length > 0) {

                  if (response.disable_valet == 0) {

                    $('p.valet_description').text("{!! trans('messages.valet_description') !!}");
                    $('h3.valet_title').text("{!! trans('messages.valet_title') !!}");
                    $('p.valet_tooltip').text("{!! trans('messages.valet_tooltip') !!}");
                    
                    $('#selectedTypeReservation').val('valet');

                    $('#vip-card').attr('checked', true);
                    $('.vip.card .choose_button').addClass('choosed').text('Choisir');

                    $('#normal-card').attr('checked', false);
                    $('.normal.card .choose_button').removeClass('choosed').text('Choisissez');

                    $('.vip.card').removeClass('disabled');
                    $('.vip.card .card_button').removeClass('disabled');
                    $('.vip.card .card__content h2').html('CHF '+response.prices.valet_total);

                  } else {

                    $('p.valet_description').text("{!! trans('messages.valet_description_disabled') !!}");
                    $('h3.valet_title').text("{!! trans('messages.valet_title_disabled') !!}");
                    $('p.valet_tooltip').text("{!! trans('messages.valet_tooltip_disabled') !!}");

                    $('#selectedTypeReservation').val('navet');

                    $('.vip.card').addClass('disabled');
                    $('.vip.card .card_button').addClass('disabled');

                    $('#vip-card').attr('checked', false);
                    $('#normal-card').attr('checked', true);

                    $('.normal.card .choose_button').addClass('choosed').text('Choisir');
                    $('.vip.card .choose_button').removeClass('choosed').text('Choisissez');

                    $('.vip.card.disabled .card__content h2').html('CHF --');

                  }

                  $('.normal.card .card__content h2').html('CHF '+response.prices.navet_total);

                }
              }
          });
        }, 200);
      }
    }
    </script>
</body>
</html>