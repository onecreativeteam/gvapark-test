<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}">
    <title>GvaPark Admin Panel</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="{!! asset('css/bootstrap.min.css') !!}">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="{!! asset('css/AdminLTE.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/_all-skins.min.css') !!}">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{!! asset('css/fullcalendar.css') !!}">
    <link rel="stylesheet" href="{!! asset('lib/lightbox/css/lightbox.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/fullcalendar.print.css') !!}" media="print" >

    <script type="text/javascript">
         public_path = "{{ URL::to('/') }}";
    </script>

    <!-- JavaScripts -->
    <script src="{!! asset('js/moment.min.js') !!}"></script>
    {{-- <script src="{!! asset('js/jquery-3.1.0.min.js') !!}"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
    <script src="{!! asset('js/bootstrap.min.js') !!}"></script>

    <script src="{!! asset('lib/bootstrap-select/js/bootstrap-select.min.js') !!}"></script>
    <script src="{!! asset('lib/jquery-ui/jquery-ui.js') !!}"></script>
    <script src="{!! asset('js/app.min.js') !!}"></script>
    <script src="{!! asset('js/custom_admin.js') !!}"></script>
    <script src="{!! asset('js/fullcalendar.js') !!}"></script>

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
        .container-calendar{
            width:100%;
            max-width:1170px;
        }

        .skin-blue .main-header .navbar .sidebar-toggle:hover, .skin-blue .main-header .logo:hover, .skin-blue .main-header .logo{ background-color: #981a26; } .skin-blue .main-header .navbar { background-color: #bf1e2d; } .skin-blue .sidebar-menu>li:hover>a, .skin-blue .sidebar-menu>li.active>a{ border-color:#bf1e2d; }
    </style>
 <body class="skin-blue sidebar-mini">

    <div class="wrapper">

        <header class="main-header">
            <a href="{!! url('/') !!}" class="logo">
                <span class="logo-mini"><b>GvaPark</b></span>
                <span class="logo-lg">GvaPark</span>
            </a>
            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
            </nav>


        </header>

        @include('layouts.parts.nav_admin')
        <div class="content-wrapper">
          <section class="content">
            {{-- <div class="row">

                <div class="col-xs-12"> --}}
                    @yield('content')
                {{-- </div>
            </div> --}}
          </section>
        </div>

    <footer class="main-footer">
        <div class="pull-right hidden-xs">  GvaPark Administration Panel</div>
        <strong>Copyright &copy; {{ date('Y') }} <b>GvaPark</b>.</strong> All rights reserved.
    </footer>
</div>
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
@stack('scripts')
<script src="{!! asset('lib/lightbox/js/lightbox.js') !!}"></script>
</body>
</html>
