<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section style="height: auto;" class="sidebar">
        <ul class="sidebar-menu">
            @if(Auth::user()->role == 'admin')
                <li class="">
                    <a href="/{{ app()->getLocale() }}/admin">
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="">
                    <a href="/{{ app()->getLocale() }}/admin/users">
                        <span>Users</span>
                    </a>
                </li>

                @if(count($admin_options))
                    @foreach($admin_options as $option)
                        @if(isset($option['sub_nav']))
                        <li class="treeview">
                            <a href="/{{ app()->getLocale() }}/admin">
                                <span>{{$option['title']}}</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-down pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                @foreach($option['sub_nav'] as $sub_nav_item)
                                    @if (isset($sub_nav_item['wildcart']))
                                    <li><a href="{{ route($sub_nav_item['url'], $sub_nav_item['wildcart']) }}"><i class="fa fa-circle-o"></i>{{$sub_nav_item['title']}}</a></li>
                                    @else
                                    <li><a href="{{ route($sub_nav_item['url']) }}"><i class="fa fa-circle-o"></i>{{$sub_nav_item['title']}}</a></li>
                                    @endif
                                @endforeach
                            </ul>
                        </li>
                        @else
                            @if (!isset($option['wildcart']))
                            <li class=""><a href="{{ route($option['url']) }}">{{$option['title']}}</a></li>
                            @else
                            <li class=""><a href="{{ route($option['url'], $option['wildcart']) }}">{{$option['title']}}</a></li>
                            @endif
                        @endif
                    @endforeach
                @endif
            @endif
            @if(Auth::user()->role == 'receptionist')
                <li class="">
                    <a href="/{{ app()->getLocale() }}/admin">
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="">
                    <a href="{{ route('admin.calendar.show', ['type' => 'valet']) }}">
                        <span>Privillege Calendar</span>
                    </a>
                </li>
                <li class="">
                    <a href="{{ route('admin.calendar.show', ['type' => 'navet']) }}">
                        <span>Self-Parking Calendar</span>
                    </a>
                </li>
                <li class="">
                    <a href="{{ route('admin.reservation.list') }}">
                        <span>Reservation List</span>
                    </a>
                </li>
                <li class="">
                    <a href="{{ route('logout') }}">
                        <span>Logout</span>
                    </a>
                </li>
            @endif
            @if(Auth::user()->role == 'worker')
                <li class="">
                    <a href="{{ route('admin.calendar.show', ['type' => 'valet']) }}">
                        <span>Privillege Calendar</span>
                    </a>
                </li>
                <li class="">
                    <a href="{{ route('admin.calendar.show', ['type' => 'navet']) }}">
                        <span>Self-Parking Calendar</span>
                    </a>
                </li>
                <li class="">
                    <a href="{{ route('logout') }}">
                        <span>Logout</span>
                    </a>
                </li>
            @endif
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>