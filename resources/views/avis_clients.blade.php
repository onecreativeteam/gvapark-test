@extends('layouts.app')
@section('plugin-css')
  <link id="iphorm-jqueryui-theme" rel="stylesheet" href="{!! asset('lib/jquery-bar-rating/themes/fontawesome-stars.css') !!}" type="text/css">
  <link rel="stylesheet" href="{!! asset('lib/font-awesome/css/font-awesome.min.css') !!}">
@endsection
@section('plugin-js')
    <script src="lib/jquery-bar-rating/jquery.barrating.min.js"></script>
@endsection
@section('content')
  <div class="container-fluid page-content">
    <div class="container">
      <div class="row text-center">
        <h2 data-aos="zoom-in">@lang('messages.clients_page_title')</h2>
      </div>
      <div class="row testimonials text-center">
        <iframe id='AV_widget_iframe' frameBorder="0" width="100%" height="100%" src="//cl.avis-verifies.com/fr/cache/1/b/a/1ba5dcf9-461f-17c4-d903-78e3a32c285a/widget4/1ba5dcf9-461f-17c4-d903-78e3a32c285ahorizontal_index.html"></iframe>
      </div>
      @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
    </div>
  </div>
  <script type="text/javascript">
     $(function() {
        $('#stars').barrating({
          theme: 'fontawesome-stars'
        });
     });
  </script>
@endsection