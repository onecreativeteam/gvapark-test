<h1>{{ trans('messages.admin_mail_h1') }}</h1>
<div>{{ $data['header'] }}</div>
<div>
	<h2>Event Reservation Details:</h2>
	<p>{{ trans('messages.reservation_mail_from') }} {{ $data['reservation']['date_of_arrival'] }}</p>

	</br>

	<p>{{ trans('messages.reservation_mail_total') }} {{ $data['reservation']['totalWithVat'] }}</p>

</div>
