<h1>{{ trans('messages.reservation_mail_hello') }},</h1>
<div>{{ $data['header'] }}</div>
<div>
	<p>{{ trans('messages.reservation_mail_from') }} {{ $data['reservation']['date_of_arrival'] }}</p>

	</br>

	<p>{{ trans('messages.reservation_mail_total') }} {{ $data['reservation']['totalWithVat'] }}</p>

	<p>info@gvapark.ch</p>
</div>
