<h1>{{ trans('messages.admin_mail_h1') }}</h1>
<div>{{ $data['header'] }}</div>
<div>
	<h2>Details:</h2>
	<p>{{ trans('messages.reservation_mail_from') }} {{ $data['reservationData']['date_of_arrival'] }}</p>
	<p>{{ trans('messages.reservation_mail_to') }} {{ $data['reservationData']['date_of_departure'] }}</p>

	</br>

	@if (!empty($data['priceListData']['optionsTax']) && isset($data['priceListData']['optionsTax']))
		<p> + {{ trans('messages.reservation_page_options_'.$data['priceListData']['selectedOption'].'_selected') }}</p>
	@endif

	</br>

	@if (isset($data['priceListData']['is_aplied_promo']))
		<p>Promo-code OFF: {{ $data['priceListData']['promo_value_percent'] }}%</p>
		<p>{{ trans('messages.reservation_mail_total') }} {{ $data['reservationData']['price_to_charge'] }}</p>
	@else
		<p>{{ trans('messages.reservation_mail_total') }} {{ $data['reservationData']['price_to_charge'] }}</p>
	@endif
</div>
