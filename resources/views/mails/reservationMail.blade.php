<h1>{{ trans('messages.reservation_mail_hello') }},</h1>
<div>{{ $data['header'] }}</div>
<div>
	<p>{{ trans('messages.reservation_mail_from') }} {{ $data['reservationData']['date_of_arrival'] }}</p>
	<p>{{ trans('messages.reservation_mail_to') }} {{ $data['reservationData']['date_of_departure'] }}</p>
	
	</br>

	@if (!empty($data['priceListData']['optionsTax']) && isset($data['priceListData']['optionsTax']))
		<p> + {{ trans('messages.reservation_page_options_'.$data['priceListData']['selectedOption'].'_selected') }}</p>
	@endif

	</br>

	@if (isset($data['priceListData']['is_aplied_promo']))
		<p>{{ trans('messages.reservation_mail_promo_code') }} {{ $data['priceListData']['promo_value_percent'] }}%</p>
		<p>{{ trans('messages.reservation_mail_total') }} {{ $data['reservationData']['price_to_charge'] }}</p>
	@else
		<p>{{ trans('messages.reservation_mail_total') }} {{ $data['reservationData']['price_to_charge'] }}</p>
	@endif

	<p>info@gvapark.ch</p>
	@if ($data['reservationData']['reservation_type'] == 'valet')
	<p>{{ trans('messages.reservation_mail_row_1') }} <a href="{{ route('contact') }}">{{ route('contact') }}</a></p>
	<p>{{ trans('messages.reservation_mail_row_2') }} <a href="{{ route('video') }}">{{ route('video') }}</a></p>
	<p>{{ trans('messages.reservation_mail_row_3') }} <a href="{{ route('home') }}">{{ route('home') }}</a></p>
	@else
	<p>{{ trans('messages.reservation_mail_row_1_navet') }} <a href="{{ route('contact') }}">{{ route('contact') }}</a></p>
	<p>{{ trans('messages.reservation_mail_row_2_navet') }} <a href="{{ route('video') }}">{{ route('video') }}</a></p>
	<p>{{ trans('messages.reservation_mail_row_3_navet') }} <a href="{{ route('home') }}">{{ route('home') }}</a></p>
	@endif
</div>
