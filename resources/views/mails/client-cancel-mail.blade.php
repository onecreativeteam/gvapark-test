<h2>{{ trans('messages.cancel_mail_hello') }}</h2>
<div>
	<p>{{ trans('messages.cancel_mail_body') }}</p>
	<br>
	<p>{{ trans('messages.cancel_mail_footer1') }}</p>
	<p>{{ trans('messages.cancel_mail_footer2') }}</p>
</div>
