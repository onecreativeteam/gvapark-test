<h1>Hello.</h1>
<div>
	<p>Reservation #{{ $data['reservationData']['id'] }} of {{ $data['reservationData']['client_first_name'] }} {{ $data['reservationData']['client_last_name'] }}, arriving on {{ $data['reservationData']['date_of_arrival'] }} was cancelled by the client.</p>
</div>
