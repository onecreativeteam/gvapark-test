@section('page-title', 'Parking Aéroport de Genève Tarifs')
@section('page-description', "Réservez votre place de parking à l'aéroport de Genève au meilleur tarifs possible. Réservez en ligne sur notre site votre place de parking 7jours sur 7")
@extends('layouts.app')
@section('content')
<div class="container-fluid page-content">
  <div class="container">
    <div class="row text-center">
      <h2 style="margin-top: 20px;margin-bottom: 10px;" data-aos="zoom-in">@lang('messages.reservation_page_first_title')</h2>
    </div>

    <div class="step_navigation">
      <ul class="steps">
        <li class="current"><a href="{{ route('reservation.one.get') }}">@lang('messages.reservation_step') 1</a></li>
          <li class="current"><a href="{{ route('reservation.two.get') }}">@lang('messages.reservation_step') 2</a></li>
          <li class="current"><a href="{{ route('reservation.three.get') }}">@lang('messages.reservation_step') 3</a></li>
          <li class="current"><a href="{{ route('reservation.four.get') }}">@lang('messages.reservation_step') 4</a></li>
          <li><a onclick="return false;">@lang('messages.reservation_step') 5</a></li>
      </ul>
    </div>

    <div class="alert alert-danger">
    @if (count($errors) > 0)
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
    @endif
    </div>
    <div class="row prices aos-init aos-animate" data-aos="zoom-in" data-aos-delay="400">
      <div class="col-xs-12">
        <h4>Votre commande</h4>
        <table>
          <tbody>
            <tr>
             <th>Produits</th>
             <th>Total&nbsp;</th>
            </tr>
            <tr>
              <td>Jours × {{ session('priceList')['daysToCharge'] }}</td>
              <td>{{ session('priceList')['dayTax'] }} CHF</td>
            </tr>
            @if(!empty(session('priceList')['selectedOption']) && isset(session('priceList')['optionsTax']))
            <tr>
              <td>@lang('messages.reservation_page_options_'.session('priceList')['selectedOption'].'_selected') x 1</td>
              <td>{!! number_format(@session('priceList')['optionsTax'], 2) !!}CHF</td>
            </tr>
            @endif
            @if(isset(session('priceList')['has_electric']) && session('priceList')['has_electric'] == 1)
            <tr>
              <td>@lang('messages.reservation_page_options_electric_title') x 1</td>
              <td>{!! number_format(@session('priceList')['electricTax'], 2) !!}CHF</td>
            </tr>
            @endif
            <tr>
              <td>TVA</td>
              <td>{{ number_format(session('priceList')['totalVAT'], 2) }} CHF</td>
            </tr>
            @if (isset(session('priceList')['is_aplied_promo']) && session('priceList')['is_aplied_promo'] === 1)
            <tr>
                <td>@lang('messages.reservation_cart_table_promo_code_label')</td>
                <td> - {!! number_format(session('priceList')['promo_value_percent'], 2) !!}%</td>
            </tr>
            @endif
            <tr>
              <td>Total&nbsp;</td>
              <td>{{ number_format(session('priceList')['absolute_total'], 2) }} CHF</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="row">
      <div style="margin:10px 0;" class="col-xs-12 text-center">
        <button type="button" onclick="postAjaxReservation(event)" class="step_five button">Commander</button>
      </div>
    </div>
    <div style="margin-botom:10px" class="row">
      <div class="col-xs-12 text-center">
        <p>J’accepte les <a style="color:#337ab7;" class="" onclick="showTermsAndConditionsModal()" id="initTermsAndConditionsModal">Conditions Générales</a></p>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function postAjaxReservation(event) {
    event.preventDefault();
    $.ajaxSetup( { headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() } } );
    $.ajax({
        method: "GET",
        url: "{{ route('reservation.data.get') }}",
        success: function (response) {
            loadPayment();
        },
        error: function (response) {
            var errors = '<ul>';
            $.each(error.responseJSON, function (key, val) {
                errors += '<li style="padding:5px">'+val+'</li>';
            });
            errors += '</ul>';

            $('.alert-danger').html(errors);

            $('html, body').animate({
                scrollTop: $(".errors-danger").offset().top
            }, 500);
        }
    });
}

function loadPayment() {
    Datatrans.startPayment({
        'form': '#paymentForm',
        'opened': function() {

        },
        'loaded': function() {
            $('body').css("top", "0px");
        },
        'closed': function() {
            window.location.replace("{{ route('reservation.five.get') }}");
        },
        'error': function() {
            window.location.replace("{{ route('reservation.five.get') }}");
        },
    });
}
</script>
<script>
window.dataLayer = window.dataLayer || [];
dataLayer.push({'event': 'formSubmissionSuccess', 'formId': 'paymentForm'});
</script>
<div class="row" style="display:none">
    <div class="col-xs-12">
        <form id="paymentForm"
            data-merchant-id="{{ $params['MERCHANT'] }}"
            data-amount="{{ $params['AMOUNT'] }}"
            data-currency="{{ $params['CURRENCY'] }}"
            data-refno="{{ $params['REFNO'] }}"
            data-success-url="{{ route('postfinance.status') }}"
            data-error-url="{{ route('postfinance.status') }}"
            data-cancel-url="{{ route('postfinance.status') }}"
            data-sign="{{ $params['SIGN'] }}"
            data-use-split-mode="false"
            data-upp-customer-details="yes"
            data-upp-customer-id="{{ $params['customer-id'] }}"
            data-upp-customer-name="{{ $params['customer-name'] }}"
            data-upp-customer-first-name="{{ $params['customer-first-name'] }}"
            data-upp-customer-last-name="{{ $params['customer-last-name'] }}"
            data-upp-customer-street="{{ $params['customer-street'] }}"
            data-upp-customer-street2="{{ $params['customer-street2'] }}"
            data-upp-customer-city="{{ $params['customer-city'] }}"
            data-upp-customer-country="CHE"
            data-upp-customer-zip-code="{{ $params['customer-zip-code'] }}"
            data-upp-customer-birth-date="{{ $params['customer-birth-date'] }}"
            data-upp-customer-language="{{ $params['customer-language'] }}"
            data-upp-customer-phone="{{ $params['customer-phone'] }}"
            data-upp-customer-email="{{ $params['customer-email'] }}"
            data-upp-article_1_-id="001"
            data-upp-article_1_-description="parking"
            data-upp-article_1_-name="parking place"
            data-upp-article_1_-price-gross="{{ $params['AMOUNT'] }}"
            data-upp-article_1_-quantity="1"

            data-upp-article_1_-tax="7.7"
            data-upp-article_1_-tax-amount="{{ $params['VAT'] }}"
            data-upp-article_1_-type="fees"
            
            data-upp-shipping-details="yes"
            data-upp-shipping-first-name="{{ $params['shipping-first-name'] }}"
            data-upp-shipping-last-name="{{ $params['shipping-last-name'] }}"
            data-upp-shipping-street="{{ $params['shipping-street'] }}"
            data-upp-shipping-street2="{{ $params['shipping-street2'] }}"
            data-upp-shipping-zip-code="{{ $params['shipping-zip-code'] }}"
            data-upp-shipping-city="{{ $params['shipping-city'] }}"
            data-upp-shipping-country="CHE">
        </form>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('a').click(function(){
        $('html, body').animate({
            scrollTop: $( $(this).attr('href') ).offset().top
        }, 500);
        return false;
    });
});
</script>
@endsection