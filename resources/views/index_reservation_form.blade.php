@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="content">
        	<div class="row">
                <div class="col-md-12">
		            <div class="panel panel-default">
		                <div class="panel-heading">
		                	Reservation Step One.
		                </div>
		            </div>
		        </div>
            </div>
            <div class="row">
                @include('forms.reservation_form')
            </div>
        </div>
    </div>
</div>
@endsection
