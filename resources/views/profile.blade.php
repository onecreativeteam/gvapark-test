@section('page-title', 'Profile')
@extends('layouts.app')
@section('content')
<div class="container-fluid page-content">
	<div class="container">
		<div class="row profile-form" data-aos="zoom-in" data-aos-delay="300" >
			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			{!! Form::open(['action' => array('ReservationController@updateClientProfile', $clientData['id']), 'method' => 'POST']) !!}
			<div class="col-sm-6"  >
				<h3>@lang('messages.profile_page_facture_details')</h3>
				<div class="form-group">
					<label for="last_name">@lang('messages.profile_page_last_name_label')</label>
					{!! Form::input('text', 'last_name', isset($clientData['last_name']) ? $clientData['last_name'] : '', ['class' => 'form-control', 'id'=>'last_name']) !!}
				</div>
				<div class="form-group">
					<label for="first_name">@lang('messages.profile_page_first_name_label')</label>
					{!! Form::input('text', 'first_name', isset($clientData['first_name']) ? $clientData['first_name'] : '', ['class' => 'form-control', 'id'=>'first_name']) !!}
				</div>
				<div class="form-group">
					<label for="company_name">@lang('messages.profile_page_company_name_label')</label>
					{!! Form::input('text', 'company_name', isset($clientData['company_name']) ? $clientData['company_name'] : '', ['class' => 'form-control', 'id'=>'company_name']) !!}
				</div>
				<div class="form-group">
					<label for="change_password">@lang('messages.profile_page_change_password_label')</label>
					{!! Form::checkbox('change_password', 1, 0, ['class' => 'field changePasswordProfile']) !!}
				</div>
				<div class="hasCheckedChangePassword" style="display:none">
					<div class="form-group">
						<label for="email">@lang('messages.profile_page_email_label')</label>
						{!! Form::input('text', 'email', isset($clientData['email']) ? $clientData['email'] : '', ['class' => 'form-control profile-credentials', 'id'=>'email', 'disabled'=>'disabled']) !!}
					</div>
					<div class="form-group">
						<label for="email_confirm">@lang('messages.profile_page_email_confirm_label')</label>
						{!! Form::input('text', 'email_confirm', isset($clientData['email_confirm']) ? $clientData['email_confirm'] : '', ['class' => 'form-control profile-credentials', 'id'=>'email_confirm', 'disabled'=>'disabled']) !!}
					</div>
					<div class="form-group">
						<label for="number">@lang('messages.profile_page_password_label')</label>
						{!! Form::input('password', 'password', isset($clientData['old_password']) ? $clientData['old_password'] : '', ['class' => 'form-control profile-credentials', 'id'=>'password', 'disabled'=>'disabled']) !!}
					</div>
				</div>
				<div class="form-group">
					<label for="phone">@lang('messages.profile_page_phone_label')</label>
					{!! Form::input('text', 'phone', isset($clientData['phone']) ? $clientData['phone'] : '', ['class' => 'form-control', 'id'=>'phone']) !!}
				</div>
				<div class="form-group">
					<label for="adresse">@lang('messages.profile_page_adress_label')</label>
					{!! Form::input('text', 'adress', isset($clientData['adress']) ? $clientData['adress'] : '', ['class' => 'form-control', 'id'=>'adress', 'placeholder'=>'Adresse']) !!}
				</div>
				<div class="form-group">
					<label for="city">@lang('messages.profile_page_adress2_label')</label>
					{!! Form::input('text', 'adress2', isset($clientData['adress2']) ? $clientData['adress2'] : '', ['class' => 'form-control', 'id'=>'adress2', 'placeholder'=>'Appartement, bureau, etc. (optionnel)']) !!}
				</div>
				<div class="form-group">
					<label for="city">@lang('messages.profile_page_city_label')</label>
					{!! Form::input('text', 'city', isset($clientData['city']) ? $clientData['city'] : '', ['class' => 'form-control', 'id'=>'city', 'placeholder'=>'Ville']) !!}
				</div>
				<div class="form-group">
					<label for="postcode">@lang('messages.profile_page_postcode_label')</label>
					{!! Form::input('text', 'postcode', isset($clientData['postcode']) ? $clientData['postcode'] : '', ['class' => 'form-control', 'id'=>'postcode', 'placeholder'=>'Postcode / Zip']) !!}
				</div>
			</div>
			<div class="col-sm-6" >
				<h3>@lang('messages.profile_page_car_information_label')</h3>
				<div class="form-group">
					<label for="description">@lang('messages.profile_page_description_label')</label>
					{!! Form::textarea('description', isset($clientData['description']) ? $clientData['description'] : '', ['class' => 'form-control', 'id'=>'description', 'rows'=>'3', 'placeholder'=>'Commentaires concernant votre commande, ex : consignes de livraison.']) !!}
				</div>
				<div class="form-group">
					<label for="car_brand">@lang('messages.profile_page_car_brand_label')</label>
					{!! Form::input('text', 'car_brand', isset($clientData['car_brand']) ? $clientData['car_brand'] : '', ['class' => 'form-control', 'id'=>'car_brand', 'placeholder'=>'Marque']) !!}
				</div>
				<div class="form-group">
					<label for="car_model">@lang('messages.profile_page_car_model_label')</label>
					{!! Form::input('text', 'car_model', isset($clientData['car_model']) ? $clientData['car_model'] : '', ['class' => 'form-control', 'id'=>'car_model', 'placeholder'=>'Modèle']) !!}
				</div>
				<div class="form-group">
					<label for="car_registration_number">@lang('messages.profile_page_car_registration_number_label')</label>
					{!! Form::input('text', 'car_registration_number', isset($clientData['car_registration_number']) ? $clientData['car_registration_number'] : '', ['class'=>'form-control', 'id'=>'car_registration_number', 'placeholder'=>'Plaque de immatriculation']) !!}
				</div>
				<div class="form-group">
					<label for="flight_number">@lang('messages.profile_page_flight_number_label')</label>
					{!! Form::input('text', 'flight_number', isset($clientData['flight_number']) ? $clientData['flight_number'] : '', ['class' => 'form-control', 'id'=>'flight_number', 'placeholder'=>'Numéro de vol retour']) !!}
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<button type="submit" class="btn btn-success profileBtn">@lang('messages.profile_page_update_button')</button>
				</div>
			</div>
			{!! Form::close() !!}
		</div>
		@if(!empty($reservations))
		<div class="row profile-reservations" data-aos="zoom-in" data-aos-delay="300" >
			<div class="col-sm-12">
				<table class="table table-hover" id="reservations-list-table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>@lang('messages.profile_page_table_name')</th>
                            <th>@lang('messages.profile_page_table_reservation_at') </th>
                            <th>@lang('messages.profile_page_table_arrival_at') </th>
                            <th>@lang('messages.profile_page_table_departure_at') </th>
                            <th>@lang('messages.profile_page_table_days_to_stay') </th>
                            <th>@lang('messages.profile_page_table_status') </th>
                            <th>@lang('messages.profile_page_table_gallery') </th>
                            <th>@lang('messages.profile_page_table_tax') </th>
                            <th>@lang('messages.profile_page_table_new_tax')</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach( $reservations as $k => $reservation)
                        <tr>
                            <td width="5%">
                                <p>{!! $reservation->id !!}</p>
                            </td>
                            <td width="10%">
                                <p>{!! $reservation->client_first_name .' '. $reservation->client_last_name !!}</p>
                            </td>
                            <td width="15%" >
                                <p>{!! $reservation->date_of_reservation !!}</p>
                            </td>
                            <td width="15%">
                                <p>{!! $reservation->date_of_arrival !!}</p>
                            </td>
                            <td width="15%" >
                                <p>{!! $reservation->date_of_departure !!}</p>
                            </td>
                            <td width="10%" >
                                <p>{!! $reservation->days_to_stay !!}</p>
                            </td>
                            <td width="5%" >
                                <p>{!! $reservation->payment_status !!}</p>
                            </td>
                            <td width="5%" >
                                @if ($reservation->has_gallery == 1)
                                <p>
                                    <a href="{{ route('reservations.gallery', [$reservation->id]) }}" target="_blank" class="btn btn-info btn-xs">@lang('messages.profile_page_table_button_view_gallery')</a>
                                </p>
                                @else
                                <p>none</p>
                                @endif
                            </td>
                            <td width="5%">
                                <p>{!! $reservation->price_to_charge !!}</p>
                            </td>
                            <td width="5%">
                                <p>{!! $reservation->changed_price_to_charge !!}</p>
                            </td>
                            <td width="10%">
                            	@if ($reservation->payment_status == 'annulated')
                            		Anulated
                            	@elseif ($reservation->allowAnnulation == 1 && !is_null($reservation->payment) && $reservation->is_canceled == 0)
                            		<a class="btn btn-danger" href="{!! route('web.transaction.cancel', [$reservation->id]) !!}">Cancel</a>
                                <!-- else -->
                                	<!-- <a href="{!! route('reservation.anulate.client.profile', [$reservation->id]) !!}" class="btn btn-primary btn-block btn-xs">@lang('messages.profile_page_table_button_annulation')</a> -->
                                @endif
                                @if ($reservation->payment_status == 'completed')
                                	<a href="{!! route('generate.invoice', [$reservation->id]) !!}" class="btn btn-warning btn-block btn-xs">@lang('messages.profile_page_table_button_facture')</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                	</tbody>
                </table>
			</div>
		</div>
		@endif
	</div>
</div>
@endsection
