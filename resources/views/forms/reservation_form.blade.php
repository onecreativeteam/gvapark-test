{!! Form::open(['action' => 'ReservationController@makeReservation', 'method' => 'POST']) !!}
    <div class="col-md-12">
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('email', 'Email:') !!}
                {!! Form::input('text', 'email', null, []) !!}
                <div class="error" style="color:red">{{ $errors->first('email') }}</div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-2">
            <div class="form-group">
                {!! Form::label('date_of_arrival', 'Date of arrival:') !!}
                {!! Form::input('text', 'date_of_arrival', null, ['class' => 'onlyDatepicker']) !!}
                <div class="error" style="color:red">{{ $errors->first('date_of_arrival') }}</div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                {!! Form::label('time_of_arrival', 'Time of arrival:') !!}
                {!! Form::input('text', 'time_of_arrival', null, ['class' => 'onlyTimepicker']) !!}
                <div class="error" style="color:red">{{ $errors->first('time_of_arrival') }}</div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-2">
            <div class="form-group">
                {!! Form::label('date_of_departure', 'Date of departure:') !!}
                {!! Form::input('text', 'date_of_departure', null, ['class' => 'onlyDatepicker']) !!}
                <div class="error" style="color:red">{{ $errors->first('date_of_departure') }}</div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                {!! Form::label('time_of_departure', 'Time of arrival:') !!}
                {!! Form::input('text', 'time_of_departure', null, ['class' => 'onlyTimepicker']) !!}
                <div class="error" style="color:red">{{ $errors->first('time_of_departure') }}</div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::submit('Send') !!}
        </div>
    </div>
{!! Form::close() !!}

<script type="text/javascript">
    $('.onlyDatepicker').datetimepicker({
        timepicker: false,
        format: 'Y/m/d',
        formatDate: 'Y/m/d',
    });

    $('.onlyTimepicker').datetimepicker({
        datepicker:false,
        format: 'H:i',
        allowTimes:[
            '12:00',
            '13:00',
            '14:00', 
            '15:00',
            '16:00',
            '17:00',
            '18:00',
            '19:00',
            '20:00',
        ],
    });
</script>