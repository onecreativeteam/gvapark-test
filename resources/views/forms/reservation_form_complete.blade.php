{!! Form::open(['action' => 'ReservationController@makeReservationCartPost', 'method' => 'POST']) !!}
    <div class="col-md-12">
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('email', 'Email:') !!}
                {!! Form::input('text', 'email', $data['email'], []) !!}
                <div class="error" style="color:red">{{ $errors->first('email') }}</div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-2">
            <div class="form-group">
                {!! Form::label('date_of_arrival', 'Date of arrival:') !!}
                {!! Form::input('text', 'date_of_arrival', $data['date_of_arrival'], ['class' => 'onlyDatepicker']) !!}
                <div class="error" style="color:red">{{ $errors->first('date_of_arrival') }}</div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                {!! Form::label('time_of_arrival', 'Time of arrival:') !!}
                {!! Form::input('text', 'time_of_arrival', $data['time_of_arrival'], ['class' => 'onlyTimepicker']) !!}
                <div class="error" style="color:red">{{ $errors->first('time_of_arrival') }}</div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-2">
            <div class="form-group">
                {!! Form::label('date_of_departure', 'Date of departure:') !!}
                {!! Form::input('text', 'date_of_departure', $data['date_of_departure'], ['class' => 'onlyDatepicker']) !!}
                <div class="error" style="color:red">{{ $errors->first('date_of_departure') }}</div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                {!! Form::label('time_of_departure', 'Time of arrival:') !!}
                {!! Form::input('text', 'time_of_departure', $data['time_of_departure'], ['class' => 'onlyTimepicker']) !!}
                <div class="error" style="color:red">{{ $errors->first('time_of_departure') }}</div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        {!! Form::label('radios', 'Washing options:') !!}
        <div class="form-group">
            {!! Form::label('radio1', 'CHF 30 - Nettoyage extérieur') !!}
            {!! Form::radio('options', 'intereur', false, ['id' => 'radio1']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('radio2', 'CHF 30 - Nettoyage intérieur') !!}
            {!! Form::radio('options', 'extereur', false, ['id' => 'radio2']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('radio3', 'CHF 50 - Nettoyage extérieur+intérieur') !!}
            {!! Form::radio('options', 'intereur_and_extereur', false, ['id' => 'radio3']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::submit('Send') !!}
        </div>
    </div>
{!! Form::close() !!}