<?php
echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<urlset
      xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">

  <url>
    <loc>https://www.gvapark.ch/</loc>
  </url>
  <url>
    <loc>https://www.gvapark.ch/parking-service</loc>
  </url>
  <url>
    <loc>https://www.gvapark.ch/reservation</loc>
  </url>
  <url>
    <loc>https://www.gvapark.ch/avis-clients</loc>
  </url>
  <url>
    <loc>https://www.gvapark.ch/videos</loc>
  </url>
  <url>
    <loc>https://www.gvapark.ch/contact</loc>
  </url>
</urlset>