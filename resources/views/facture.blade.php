<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Facture</title>
</head>
  <body class="facture" style="margin-top:-40px;">
    <div class="container-fluid" style="width:90%;margin: auto;">
      <div class="row" style="position:relative;">
        <div class="col-xs-6 text-left" style="width:50%;display: inline-block;text-align: left;">
          <img src="image/facture/logo.jpg" alt="" style="width: 30%;">
        </div>
        <div class="col-xs-6"  style="width:50%;display: inline-block;">
          <ul class="clearfix" style="list-style: none;position: absolute; right: 0;margin-left:30px;">
            <li><h3 style="font-size: 16px;margin: 5px 0;font-weight: bold;">@lang('messages.facture_website_title')</h3></li>
            <li><p style="margin: 0;font-size: 12px;">@lang('messages.facture_adress1')</p></li>
            <li><p style="margin: 0;font-size: 12px;">@lang('messages.facture_adress2')</p></li>
            <!-- <li><p style="margin: 0;font-size: 12px;">@lang('messages.facture_adress3')</p></li> -->
            <li><p style="margin: 0;font-size: 12px;">@lang('messages.facture_telephone')</p></li>
            <li><p style="margin: 0;font-size: 12px;">@lang('messages.facture_email')</p></li>
            <li><p style="margin: 0;font-size: 12px;">@lang('messages.facture_tva_number')</p></li>
          </ul>
        </div>
      </div>
      <div class="row" style="position:relative;">
        <div class="col-xs-12">
          <h2 style="font-size: 18px;font-weight: bold;margin: 20px 0;">@lang('messages.facture_label')</h2>
        </div>
      </div>
      <div class="row"  style="position:relative;margin-top:-60px;">
        <div class="col-xs-6" style="width:50%;display: inline-block;">
          <ul style=" list-style: none;padding: 0;">
            <li><p style="margin: 0;font-size: 12px;">{{ $clientName }}</p></li>
            <li><p style="margin: 0;font-size: 12px;">{{ $adress1 }}</p></li>
            <li><p style="margin: 0;font-size: 12px;">{{ $adress2 }}</p></li>
            <li><p style="margin: 0;font-size: 12px;">{{ $city }}</p></li>
            <li><p style="margin: 0;font-size: 12px;">{{ $postcode }}</p></li>
            <li><p style="margin: 0;font-size: 12px;">{{ $email }}</p></li>
            <li><p style="margin: 0;font-size: 12px;">{{ $phone }}</p></li>
            <li><p style="margin: 0;font-size: 12px;">Reservation Type: {{ $reservation_type }}</p></li>
            <li><p style="margin: 0;font-size: 12px;">@lang('messages.facture_reservation_id_label') {{ $reservation_id }}</p></li>
            @if (isset($system_payment_id))
            <li><p style="margin: 0;font-size: 12px;">@lang('messages.facture_transaction_id_label') {{ $system_payment_id }}</p></li>
            @endif
          </ul>
        </div>
        <div class="col-xs-6" style="width:40%;display: inline-block;position:absolute;right:0;">
          <table>
            <tr>
              <td style="margin: 0;font-size: 12px;padding-right: 30px;">@lang('messages.facture_registration_number')</td>
              <td  style=" margin: 0;font-size: 12px;"> {{ $car_registration_number }} </td>
            </tr>
            <tr>
              <td  style="margin: 0;font-size: 12px;padding-right: 30px;">@lang('messages.facture_date_of_reservation') </td>
              <td  style="margin: 0;font-size: 12px;"> {{ $date_of_reservation }} </td>
            </tr>
            <tr>
              <td  style="margin: 0;font-size: 12px;padding-right: 30px;">@lang('messages.facture_payment_method') </td>
              <td  style="margin: 0;font-size: 12px;"> {{ $payment_method }} </td>
            </tr>
            <tr>
              <td  style="margin: 0;font-size: 12px;padding-right: 30px;">@lang('messages.facture_car_brand') </td>
              <td  style="margin: 0;font-size: 12px;"> {{ $car_brand }} </td>
            </tr>
            <tr>
              <td  style="margin: 0;font-size: 12px;padding-right: 30px;">@lang('messages.facture_car_model')</td>
              <td  style="margin: 0;font-size: 12px;"> {{ $car_model }} </td>
            </tr>
            <tr>
              <td  style="margin: 0;font-size: 12px;padding-right: 30px;">@lang('messages.facture_registration_number')</td>
              <td  style="margin: 0;font-size: 12px;"> {{ $car_registration_number }} </td>
            </tr>
            <tr>
              <td  style="margin: 0;font-size: 12px;padding-right: 30px;">@lang('messages.facture_flight_number')</td>
              <td  style="margin: 0;font-size: 12px;"> {{ $flight_number }} </td>
            </tr>
          </table>
        </div>
      </div>
      <div class="row"  style="position:relative;">
        <div class="col-xs-12">
          <table style="width: 100%;margin-top:50px;">
            <tr>
              <th colspan="2" style="width: 60% !important;font-size: 16px; text-align: left;color: #fff;background: #000;padding: 10px 5px;">@lang('messages.facture_products_label')</th>
              <th style="width: 20% !important;  font-size: 16px;text-align: left;color: #fff;background: #000;padding: 10px 5px;">@lang('messages.facture_quantity_label')</th>
              <th style="width: 20% !important; font-size: 16px;text-align: left;color: #fff;background: #000;padding: 10px 5px;">@lang('messages.facture_price_label')</th>
            </tr>
            <!-- <tr>
              <td colspan="2" style="width: 60% !important;padding: 10px 5px;border-bottom: 2px solid #ccc; font-size: 12px;">@lang('messages.facture_price_to_pay_label')</br>
              </td>
              <td  style="width: 20% !important;padding: 10px 5px;border-bottom: 2px solid #ccc; font-size: 12px;">1</td>
              <td  style="width: 20% !important;padding: 10px 5px;border-bottom: 2px solid #ccc; font-size: 12px;">{{number_format($initial_tax,2)}} CHF</td>
            </tr> -->
            <tr>
              <td colspan="2"  style="width: 60% !important;padding: 10px 5px;border-bottom: 2px solid #ccc; font-size: 12px;">@lang('messages.facture_days_label')</br>
              </td>
              <td  style="width: 20% !important;padding: 10px 5px;border-bottom: 2px solid #ccc;font-size: 12px;"> {{ $days_to_stay }} </td>
              <td  style="width: 20% !important;padding: 10px 5px;border-bottom: 2px solid #ccc;font-size: 12px;"> {{ number_format($total_days_price,2) }} CHF </td>
            </tr>
            @if (isset($selectedOption))
              <tr>
              <td colspan="2"  style="width: 60% !important;padding: 10px 5px;border-bottom: 2px solid #ccc; font-size: 12px;">@lang('messages.reservation_page_options_'.$selectedOption.'_selected')</br>
              </td>
              <td  style="width: 20% !important;padding: 10px 5px;border-bottom: 2px solid #ccc;font-size: 12px;"> 1 </td>
              <td  style="width: 20% !important;padding: 10px 5px;border-bottom: 2px solid #ccc;font-size: 12px;"> {{number_format($optionTax,2)}} CHF </td>
              </tr>
            @endif
            @if (isset($has_electric) && $has_electric == 1)
              <tr>
              <td colspan="2"  style="width: 60% !important;padding: 10px 5px;border-bottom: 2px solid #ccc; font-size: 12px;">@lang('messages.reservation_page_options_electric_title')</br>
              </td>
              <td  style="width: 20% !important;padding: 10px 5px;border-bottom: 2px solid #ccc;font-size: 12px;"> 1 </td>
              <td  style="width: 20% !important;padding: 10px 5px;border-bottom: 2px solid #ccc;font-size: 12px;"> {{number_format($electricTax,2)}} CHF </td>
              </tr>
            @endif
            @if (isset($applied_promo) && $applied_promo == 1)
              <tr>
              <td colspan="2"  style="width: 60% !important;padding: 10px 5px;border-bottom: 2px solid #ccc; font-size: 12px;">@lang('messages.facture_promo_code_off_label')</br>
              </td>
              <td  style="width: 20% !important;padding: 10px 5px;border-bottom: 2px solid #ccc;font-size: 12px;"> 1 </td>
              <td  style="width: 20% !important;padding: 10px 5px;border-bottom: 2px solid #ccc;font-size: 12px;"> {{$promo_value_percent}}% </td>
              </tr>
            @endif
          </table>

          <table style="width: 40%; position:absolute;right:0;margin-top: 30px;" >
            <tr style="">
              <th style=" padding: 10px 5px; margin: 0; border-bottom: 2px solid #ccc;border-top: 2px solid #ccc;text-align: left;font-size: 16px;">@lang('messages.facture_total_price_label')</th>
              <td style="  padding: 10px 5px;margin: 0;border-bottom: 2px solid #ccc;border-top: 2px solid #ccc;  font-size: 12px;">{{number_format($total,2)}} CHF</td>
            </tr>
            <tr>
              <th style="  padding: 10px 5px;margin: 0;text-align: left;font-size: 16px;">TVA {{$tvaValue}}%</th>
              <td style="  padding: 10px 5px;margin: 0;font-size: 12px;"> {{number_format($vat,2)}} CHF</td>
            </tr>
            
            <tr style="">
              <th style="padding: 10px 5px;margin: 0;  border-bottom: 2px solid #000;border-top: 2px solid #000;text-align: left;font-size: 16px;">@lang('messages.facture_only_total')</th>
              <td style="padding: 10px 5px;margin: 0; border-bottom: 2px solid #000;border-top: 2px solid #000;font-size: 12px;"> {{ number_format($price_to_charge,2) }} CHF </td>
            </tr>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-7" style="margin-top:25px;">
          <table >
            <tr>
              <td style=" margin: 0;font-size: 12px;padding-right: 30px;">@lang('messages.reservation_date_arrival')</td>
              <td style=" margin: 0;font-size: 12px;  "> {{ $date_of_arrival }} </td>
            </tr>
            <tr>
              <td style="margin: 0;font-size: 12px; padding-right: 30px;">@lang('messages.reservation_hour_arrival')</td>
              <td style="margin: 0;font-size: 12px;  "> {{ $hour_of_arrival }} </td>
            </tr>
            <tr>
              <td style="margin: 0;font-size: 12px;padding-right: 30px;">@lang('messages.reservation_date_departure')</td>
              <td style="margin: 0;font-size: 12px;"> {{ $date_of_departure }} </td>
            </tr>
            <tr>
              <td style="margin: 0;font-size: 12px;padding-right: 30px;">@lang('messages.reservation_hour_departure')</td>
              <td style="margin: 0;font-size: 12px;"> {{ $hour_of_departure }} </td>
            </tr>
            <tr>
              <td style=" margin: 0;font-size: 12px;padding-right: 30px;">@lang('messages.facture_days_label')</td>
              <td style=" margin: 0;font-size: 12px;  "> {{ $days_to_stay }} </td>
            </tr>
          </table>
        </div>
        <div class="col-xs-5"></div>
      </div>
      <!--
      <div class="row" style="margin-top:40px;">
        <div class="col-xs-6 col-xs-offset-3" style="width: 50%;margin:auto;">
          <img src="image/facture/car.jpg" alt="" style="width: 80%;"/>
        </div>
      </div>
      -->
    </div>
  </body>
</html>
