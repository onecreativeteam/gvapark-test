@section('page-title', 'Parking Aéroport de Genève Tarifs')
@section('page-description', "Réservez votre place de parking à l'aéroport de Genève au meilleur tarifs possible. Réservez en ligne sur notre site votre place de parking 7jours sur 7")
@extends('layouts.app')
@section('content')
<div class="container-fluid page-content">
    <div class="container">
        <div class="row text-center">
            <h2 data-aos="zoom-in">@lang('messages.reservation_page_first_title')</h2>
        </div>
        <div class="row reservation">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
            </div>
            @endif
            <div class="step_navigation">
                <ul class="steps">
                    <li class="current"><a href="{{ route('reservation.one.get') }}">@lang('messages.reservation_step') 1</a></li>
                    <li class="current"><a href="{{ route('reservation.two.get') }}">@lang('messages.reservation_step') 2</a></li>
                    <li><a onclick="return false;">@lang('messages.reservation_step') 3</a></li>
                    <li><a onclick="return false;">@lang('messages.reservation_step') 4</a></li>
                    <li><a onclick="return false;">@lang('messages.reservation_step') 5</a></li>
                </ul>
            </div>
            {!! Form::open(['action' => 'ReservationController@postReservationOptions', 'method' => 'POST']) !!}
            {!! Form::hidden('reservation_type', session('selectedDates')['reservation_type']) !!}
            {!! Form::hidden('date_of_arrival', session('selectedDates')['date_of_arrival']) !!}
            {!! Form::hidden('hour_of_arrival', session('selectedDates')['hour_of_arrival']) !!}
            {!! Form::hidden('date_of_departure', session('selectedDates')['date_of_departure']) !!}
            {!! Form::hidden('hour_of_departure', session('selectedDates')['hour_of_departure']) !!}
            <div class="container pb-40">
                <div class="row">
                    @if (isset($data['show_reservation_options']) && $data['show_reservation_options'] == 1)
                    <h3 class="step_title mb-40">@lang('messages.reservation_page_options_h3'):</h3>
                    <div class="row center-row">
                        <div class="col-sm-5 col-sm-offset-1 mb-40">
                            <p>@lang('messages.reservation_page_options_info'):</p>
                            <label class="form-check-label none">
                                {!! Form::radio('options', 'null', ((session('selectedDates')['options'] != 'null') && (session('selectedDates')['options'] != '') ) ? false : true, ['id' => 'radio-1-4', 'class' => 'form-check-input regular-radio']) !!}
                                <label for="radio-1-4"></label>@lang('messages.reservation_page_options_none_title')
                            </label>
                            <div class="form-check">
                                <img src="{!! asset('image/carwash-icon.png') !!}" alt="">
                                <label class="form-check-label">
                                    {!! Form::radio('options', 'intereur', session('selectedDates')['options'] == 'intereur' ? true : false, ['id' => 'radio-1-1', 'class' => 'form-check-input regular-radio']) !!}
                                <label for="radio-1-1"></label>@lang('messages.reservation_page_options_intereur_title') - CHF {{ number_format($data['intereur_price'], 2) }}
                                <label for="radio-1-1"></label>@lang('messages.reservation_page_options_intereur_descr')
                                </label>
                            </div>
                            <div class="form-check">
                                <img src="{!! asset('image/carwash-icon-premium.jpg') !!}" alt="">      
                                <label class="form-check-label">
                                    {!! Form::radio('options', 'extereur', session('selectedDates')['options'] == 'extereur' ? true : false, ['id' => 'radio-1-2', 'class' => 'form-check-input regular-radio']) !!}
                                <label for="radio-1-2"></label>@lang('messages.reservation_page_options_extereur_title') - CHF {{ number_format($data['extereur_price'], 2) }}
                                <label for="radio-1-2"></label>@lang('messages.reservation_page_options_extereur_descr')
                                </label> 
                            </div>
                        </div>
                        <div class="col-sm-5 col-sm-offset-1 mb-40">
                            <p>@lang('messages.reservation_page_options_electric_title'):</p>
                            <label class="form-check-label none">
                                {!! Form::radio('has_electric', 'null', ( @session('selectedDates')['has_electric'] == 'null' || is_null(@session('selectedDates')['has_electric']) ) ? true : false, ['id' => 'radio-2-1', 'class' => 'form-check-input regular-radio']) !!}
                                <label for="radio-2-1"></label>@lang('messages.reservation_page_options_none_title')
                            </label>
                            <div class="form-check">
                                <img src="{!! asset('image/electric.png') !!}" alt="">
                                <label class="form-check-label">
                                    {!! Form::radio('has_electric', 1, (@session('selectedDates')['has_electric'] == '1') ? true : false, ['id' => 'radio-2-2', 'class' => 'form-check-input regular-radio']) !!}
                                    <label for="radio-2-2"></label>CHF {{ number_format($data['electric_charge_price'], 2) }}
                                </label>
                            </div>
                        </div> 
                    </div>
                    @endif
                    <div class="row button text-center">
                        <div class="col-xs-6 col-md-offset-3">
                            {!! Form::submit(trans('messages.reservation_step_next')) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <input type="submit" class="return_button" value="Retour">  
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
