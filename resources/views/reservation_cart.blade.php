@extends('layouts.app')
@section('content')
  <div class="container-fluid page-content">
    <div class="container" id="cartPage">
      <div class="row text-center">
        <h2 data-aos="zoom-in"  data-aos-delay="300">@lang('messages.reservation_cart_title')</h2>
      </div>
      {{-- <div class="row car">
        <div class="col-sm-8">
          <img src="{!! asset('image/Panier.png') !!}" alt="">
          <p>@lang('messages.reservation_cart_price_label')</p>
        </div>
        <div class="col-sm-4">
          <button type="submit" class="button">@lang('messages.reservation_cart_first_button') voir panier</button>
        </div>
      </div> --}}
      <div class="row prices"  data-aos="zoom-in" data-aos-delay="500">
        <div class="col-xs-12">
          <table>
            <tr>
              <th>@lang('messages.facture_products_label')</th>
              <th>@lang('messages.facture_price_label')</th>
              <th>@lang('messages.facture_quantity_label')</th>
              <th>@lang('messages.facture_only_total')</th>
            </tr>
            <!-- <tr>
              <td data-th="Produit">@lang('messages.facture_price_to_pay_label')</td>
              <td  data-th="Prix">{!! number_format($priceList['initialTax'], 2) !!}CHF</td>
              <td  data-th="Quantité">1</td>
              <td data-th="Total">{!! number_format($priceList['initialTax'], 2) !!}CHF</td>
            </tr> -->
            <tr>
              <td data-th="Produit">@lang('messages.facture_days_label')</td>
              <td data-th="Prix">{!! number_format($priceList['dayTax'], 2) !!}CHF</td>
              <td data-th="Quantité">{!! $priceList['daysToCharge'] !!}</td>
              <td data-th="Total">{!! number_format($priceList['totalDayTax'], 2) !!}CHF</td>
            </tr>
            @if (!empty($priceList['optionsTax']) && isset($priceList['optionsTax']))
            <tr class="optionsTax">
              <td data-th="Produit">@lang('messages.reservation_page_options_'.$priceList['selectedOption'].'_selected')</td>
              <td data-th="Prix">{!! number_format($priceList['optionsTax'], 2) !!}CHF</td>
              <td data-th="Quantité">1</td>
              <td data-th="Total">{!! number_format($priceList['optionsTax'], 2) !!}CHF <span class="cancel"></span></td>
            </tr>
            @endif
          </table>
        </div>
      </div>
      <div class="row prices total"  data-aos="zoom-in" data-aos-delay="500">
        <div class="col-xs-12">
          <table>
            <tr>
              <th>@lang('messages.reservation_cart_table_total_panier_label')</th>
              <th></th>
            </tr>
            <tr>
              <td>@lang('messages.reservation_cart_table_sous_total_label')</td>
              <td>{!! number_format($priceList['total'], 2) !!}CHF</td>
            </tr>
            <tr>
              <td>@lang('messages.reservation_cart_table_tva_label')</td>
              <td>{!! number_format($priceList['totalVAT'], 2) !!}CHF</td>
            </tr>
            <tr>
                <td>@lang('messages.reservation_cart_table_total_label')</td>
                <td>{!! number_format($priceList['totalPriceWithVAT'], 2) !!}CHF</td>
            </tr>
            @if (isset($priceList['is_aplied_promo']) && $priceList['is_aplied_promo'] === 1)
            <tr>
                <td>@lang('messages.reservation_cart_table_promo_code_label')</td>
                <td>{!! number_format($priceList['promo_value_percent'], 2) !!}%</td>
            </tr>
            <tr>
                <td>@lang('messages.reservation_cart_table_sous_total_with_promo_label')</td>
                <td>{!! number_format($priceList['totalPromoPriceWithVAT'], 2) !!}CHF</td>
            </tr>
            @endif
          </table>
        </div>
      </div>
      <div class="row submit">
        <a href="{{ route('reservation.checkout.get') }}" class="button">@lang('messages.reservation_cart_table_submit_button_label')</a>
      </div>
    </div>
  </div>
@endsection
