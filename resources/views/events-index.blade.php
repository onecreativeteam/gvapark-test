@extends('layouts.events')

@section('content')
   
<div class="main__events">
    <section class="section__list">
        <header class="list__head">
            <h1 class="list__title">@lang('messages.events_list_h1')</h1>
        </header><!-- /.list__head -->

        <div class="list__body">
            <div class="shell">
                @foreach ($events as $event)
                <div class="article__list">
                    <div class="list__img">
                        <figure class="event__img">
                            <img src="{!! asset($event->small_picture) !!}">
                        </figure><!-- /.event_img-->
                    </div><!-- /.list_img--> 
                    <div class="article__inside">
                        <header class="article__title">{{ $event->name }} <span> {{ Carbon\Carbon::parse($event->date_from)->format('d') }}-{{ Carbon\Carbon::parse($event->date_to)->format('d') }} {{ Carbon\Carbon::parse($event->date_from)->format('M') }} {{ Carbon\Carbon::parse($event->date_from)->format('Y') }}</span></header><!-- /.article__title--> 
                        <div class="article__content">
                            {!! $event->short_description !!}
                        </div><!-- /.article__content-->
                        <footer class="article__foot">
                            <a href="{{ route('events.inner', $event->slug) }}" class="button__red">@lang('messages.events_see_more')</a>
                        </footer><!-- /.article__footer-->
                    </div><!-- /.article__inside-->  
                </div><!-- /.article__list-->
                @endforeach
            </div><!-- /.shell -->
            
        </div><!-- /.body__list-->
        <!-- <footer class="list__footer">
            <a class="button_load">load more</a>
        </footer> -->
    </section>
</div>

@endsection