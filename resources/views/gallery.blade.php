@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="box-header with-border">
                <h3 class="box-title">@lang('messages.gallery_title')</h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box-body">
                <div class="panel-body">
                @if(!empty($data))
                <div class="row" style="margin-bottom:30px;">
                @foreach( $data as $k => $picture)
                    <div class="col-md-3">
                      <a href="/{!! $picture['picture'] !!}" data-lightbox="image-1" data-title="Gallery">
                        <img src="/{!! $picture['picture'] !!}" alt="{!! $picture['id'] !!}" style="width:100%;">
                      </a>
                    </div>
                @endforeach
                </div>
                @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
