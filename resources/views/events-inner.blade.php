
@extends('layouts.events')

@section('content')
    <main class="main">
        <section class="intro intro_arrow">
            <header class="intro__head">
                <!-- <h1>Events Parking <br /> <span>Geneva</span></h1> -->
                <h1>{!! $event->name !!}</h1>
            </header><!-- /.intro__head -->

            <div class="shell">
                <div style="background-image: url({!! asset($event->big_picture) !!})" class="callout"></div><!-- /.callout -->

                <div style="background-image: url({!! asset($event->big_picture) !!})" class="callout_mobile"></div><!-- /.callout -->

                <article class="article">
                    {!! $event->description !!}
                </article><!-- /.article -->

                <div class="intro__actions">
                    <a onClick="return false;" class="btn">@lang('messages.reservation_page_first_title')</a>
                </div><!-- /.intro__actions -->
            </div><!-- /.shell -->
        </section><!-- /.intro -->

        <section class="section_xl">
            <header class="section__head">
                <h3 class="section__title">@lang('messages.video_page_title')</h3><!-- /.section__title -->
            </header><!-- /.section__head -->

            <div class="section__body">
                <div class="shell">
                    <div class="cols">
                        <div class="col col_1of2">
                            <iframe width="100%" height="283px" src="https://www.youtube.com/embed/LtlVZqjdl_g?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div><!-- /.col col_1of2 -->   

                        <div class="col col_1of2">
                            <div class="youTube">
                                <iframe width="100%" height="283px" src="https://www.youtube.com/embed/KetZAHZnT4g?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </div><!-- /.youTube -->
                        </div><!-- /.col col_1of2 -->                   
                    </div><!-- /.cols -->

                    <div class="info">
                        <h5 class="info__title"> 
                            <i class="fa fa-mobile" aria-hidden="true"></i>+41 22 510 14 42
                        </h5><!-- /.info__phone -->

                        <h5 class="info__title">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>Rue du Pré-de-la-Fontaine 19 1242 Satigny
                        </h5><!-- /.subtitle -->
                    </div><!-- /.info -->
                </div><!-- /.shell -->
            </div><!-- /.section__body -->
        </section><!-- /.section -->

        <section class="section_map">
            <div id="map">
                <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2760.3376050691536!2d6.052006615583046!3d46.223630579117234!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x478c62e0496d4945%3A0x54f787e41400aea6!2sRue+du+Pr%C3%A9-de-la-Fontaine+11%2C+1217+Meyrin%2C+Switzerland!5e0!3m2!1sen!2sbg!4v1518376544973" frameborder="0" allowfullscreen></iframe> -->
            </div><!-- /#map -->
        </section><!-- /.section -->

        <section class="section section_solid">
            <header class="section__head">
                <h3 class="section__title section__title-alt">@lang('messages.reservation_page_first_title')</h3><!-- /.section__title -->
            </header><!-- /.section__head -->

            <div class="section__body">
                <div class="alert alert-danger">
                @if (count($errors) > 0)
                    <ul>
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                    </ul>
                @endif
                </div>
                <div class="shell">
                    <form class="form_center eventPaymentFrom">
                        {{ csrf_field() }}
                        {!! Form::hidden('event_start_date', Carbon\Carbon::parse(@$event->date_from)->format('Y/m/d'), ['id'=>'event_start_date']) !!}
                        {!! Form::hidden('event_end_date', Carbon\Carbon::parse(@$event->date_to)->format('Y/m/d'), ['id'=>'event_end_date']) !!}
                        <div class="form__row">
                            <div class="form__action">
                                <input type="text" name="first_name" id="first_name" value="{{ @$createdEventReservation ? $createdEventReservation->first_name : '' }}" class="field" placeholder="@lang('messages.profile_page_last_name_label')" />
                            </div><!-- /.form__action -->
                        </div><!-- /.form__row -->

                        <div class="form__row">
                            <div class="form__action">
                                <input type="text" name="last_name" id="last_name" value="{{ @$createdEventReservation ? $createdEventReservation->last_name : '' }}" class="field" placeholder="@lang('messages.profile_page_first_name_label')" />
                            </div><!-- /.form__action -->
                        </div><!-- /.form__row -->
                        
                        <div class="form__row">
                            <div class="form__action">
                                <input type="text" name="email" id="email" value="{{ @$createdEventReservation ? $createdEventReservation->email : '' }}" class="field" placeholder="E-mail" />
                            </div><!-- /.form__action -->
                        </div><!-- /.form__row -->
                        
                        <div class="form__row">
                            <div class="form__action">
                                <input type="text" name="car_number" id="car_number" value="{{ @$createdEventReservation ? $createdEventReservation->car_number : '' }}" class="field" placeholder="@lang('messages.profile_page_car_registration_number_label')" />
                            </div><!-- /.form__action -->
                        </div><!-- /.form__row -->

                        <div class="cols">
                            <div class="col col_70p">
                                <label for="" class="label_block">@lang('messages.profile_page_table_arrival_at')</label>

                                <div class="form__control">
                                    <input type="text" name="date_of_arrival" id="date_of_arrival" value="{{ @$createdEventReservation ? Carbon\Carbon::parse($createdEventReservation->date_of_arrival)->format('Y/m/d') : '' }}" class="field field_alt calendar" placeholder="{{ Carbon\Carbon::now()->format('Y/m/d') }}" />
                                    
                                    <div class="form__icon">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                    </div><!-- /.form__icon -->
                                </div><!-- /.form__control -->
                            </div><!-- /.col col_4of5 -->

                            <div class="col col_30p">
                                <label for="" class="label_block">@lang('messages.facture_price_label')</label>

                                <a onClick="return false;" class="btn btn_alt btn_block">CHF {{ number_format($event->price, 2) }}</a>
                            </div><!-- /.col col_1of5 -->
                        </div><!-- /.cols -->

                        <div class="form__actions">
                            <button type="button" onClick="postAjaxCheckout();" class="btn">@lang('messages.events_button_payment')</button>
                        </div><!-- /.form__actions -->
                    </form><!-- /.form -->
                </div><!-- /.shell -->
            </div><!-- /.section__body -->
        </section><!-- /.section_solid -->
    </main>

    <script type="text/javascript">

        function postAjaxCheckout() {

            var data                = {};

            data.first_name         = $('#first_name').val();
            data.last_name          = $('#last_name').val();
            data.email              = $('#email').val();
            data.car_number         = $('#car_number').val();
            data.date_of_arrival    = $('#date_of_arrival').val();
            data.event_start_date   = $('#event_start_date').val();
            data.event_end_date     = $('#event_end_date').val();

            $.ajaxSetup( { headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() } } );
            $.ajax({
                method: "POST",
                url: "{{ route('events.post.ajax', $event->id) }}",
                dataType: "json",
                data: data,
                success: function (response) {
                    // console.log(response);
                    loadPayment();
                },
                error: function (error) {
                    // console.log(error);

                    var errors = '<ul>';

                    $.each(error.responseJSON, function (key, val) {
                        errors += '<li style="padding:5px">'+val+'</li>';
                    });

                    errors += '</ul>';

                    $('.alert-danger').html(errors);

                    $('html, body').animate({
                        scrollTop: $(".alert-danger").offset().top
                    }, 500);

                }
            });
        }

        function loadPayment() {
            Datatrans.startPayment({
                'form': '#paymentForm',
                'opened': function() {

                },
                'loaded': function() {
                    $('body').css("top", "0px");
                },
                'closed': function() {
                    window.location.reload();
                },
                'error': function() {
                    window.location.reload();
                },
            });
        }

        window.dataLayer = window.dataLayer || [];
        dataLayer.push({'event': 'formSubmissionSuccess', 'formId': 'paymentForm'});

        $(document).ready( function() {

            if ($('.alert-danger ul').length > 0) {
                $('html, body').animate({
                    scrollTop: $(".alert-danger").offset().top
                }, 500);
            }
 
            // $('a').click( function() {
            //     $('html, body').animate({
            //         scrollTop: $( $(this).attr('href') ).offset().top
            //     }, 500);
            //     return false;
            // });

            $('.intro__actions a').click(function(){
                $('html, body').animate({
                    scrollTop: ($('.section_solid').offset().top)
                },500);
            })
        });

    </script>

    <div class="row" style="display:none">
        <div class="col-xs-12">
            <form id="paymentForm"
                data-merchant-id="{{ $params['MERCHANT'] }}"
                data-amount="{{ $params['AMOUNT'] }}"
                data-currency="{{ $params['CURRENCY'] }}"
                data-refno="{{ $params['REFNO'] }}"
                data-success-url="{{ route('postfinance.status') }}"
                data-error-url="{{ route('postfinance.status') }}"
                data-cancel-url="{{ route('postfinance.status') }}"
                data-sign="{{ $params['SIGN'] }}">
            </form>
        </div>
    </div>
@endsection