@extends('layouts.app')
@section('content')
  <div class="container-fluid page-content">
    <div class="container" id="cartPage">
      <div class="row text-center">
        <h2 data-aos="zoom-in"  data-aos-delay="300">Payment method</h2>
      </div>
      <div class="row prices"  data-aos="zoom-in" data-aos-delay="500">
        <div class="col-xs-12">
        {!! Form::open(['action' => 'ReservationController@postReservationCart', 'method' => 'POST']) !!}
            <div class="form-group">
                {!! Form::radio('payment_type', 'normal', false, array('id'=>'payment_type_normal')) !!}
                <label for="payment_type_normal">Standart Reservation / initial tax + days x perDayTax</label>
            </div>
            <div class="form-group">
                {!! Form::radio('payment_type', 'vip', false, array('id'=>'payment_type_vip')) !!}
                <label for="payment_type_vip">Premium Reservation / no initial tax + firstDayTax + daysLeftTax</label>
            </div>
            <div class="form-group">
                {!! Form::submit('Submit', array('class'=>'btn btn-danger btn-md')) !!}
            </div>
        {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
@endsection
