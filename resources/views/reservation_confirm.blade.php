@extends('layouts.app')
@section('data-layer')
<script>
window.dataLayer = window.dataLayer || [];
dataLayer.push({
  'paymentMethod':'DATATRANS',
  'ecommerce':{
    'purchase':{
      'actionField':{
        'id':'{{ $reservationData["payment_id"] ? $reservationData["payment_id"] : "1" }}', // Command ID
        'revenue':'{{ $reservationData["price_to_charge"] ? $reservationData["price_to_charge"] : "0.00" }}', // Total order amount
        'tax':'{{ $reservationData["vat"] ? $reservationData["vat"] : "0.00" }}', // Taxes amount
        'shipping':'0.00', // Shipping amount
        'coupon':'' // Promotional coupon at the ordering level
      },
      'products':[{
        'name':'Reservation', // Name of the 1st ordered product
        'id':'{{ $idData["id"] ? $idData["id"] : "0" }}', // ID of the 1st ordered product
        'price':'{{ $reservationData["price_to_charge"] ? $reservationData["price_to_charge"] : "0.00" }}', // Price of the 1st ordered product
        'brand':'', // Brand of the 1st ordered product
        'category':'reservation', // Category in english of the 1st ordered product
        'variant':'', // Variant of the 1st ordered product
        'quantity':1, // Quantity of the 1st ordered product
        'coupon':'' // Promotional coupon at the product level
       }]
    }
  }
});
</script>
@endsection
@section('content')
<div class="container-fluid page-content confirm">
    <div class="container">
        <div class="row text-center">
            <h2 data-aos="zoom-in">@lang('messages.reservation_confirm_label')</h2>
            <div class="row">
            	<div class="col-md-12">
	            	<p style="text-align:center">
                        <strong>@lang('messages.reservation_confirm_label_strong')<strong>
                    </p>
					<p style="text-align:center">
                        @lang('messages.reservation_confirm_label_second')
                    </p>
                    <p style="text-align:center" class="last-p">
                        @lang('messages.reservation_confirm_label_last')
                    </p>
            	</div>
            </div>
        </div>
    </div>
</div>
@endsection
