<script type="text/javascript" src="{!! asset('js/jquery-2.2.4.min.js') !!}"></script>
<!-- <script src="https://pay.sandbox.datatrans.com/upp/payment/js/datatrans-1.0.2.js"></script> -->
<script src="https://pay.datatrans.com/upp/payment/js/datatrans-1.0.2.js"></script>
<div class="row">
    <div class="col-xs-12">
      	<form id="paymentForm"
          data-merchant-id="{{ $params['MERCHANT'] }}"
          data-amount="{{ $params['AMOUNT'] }}"
          data-currency="{{ $params['CURRENCY'] }}"
          data-refno="{{ $params['REFNO'] }}"
          data-success-url="{{ route('postfinance.status') }}"
          data-error-url="{{ route('postfinance.status') }}"
          data-cancel-url="{{ route('postfinance.status') }}"
          data-sign="{{ $params['SIGN'] }}">
      	</form>
    </div>
</div>
<div id="loader">
	<img src='/image/datatrans_background.jpg'/>
</div>
<style>
#loader {
  /*position: fixed; */
/*  top: 0; 
  left: 0; */
  width: 100%; 
  height: 100%;
}
#loader img {
  /*position: absolute; 
  top: 0; 
  left: 0; 
  right: 0; 
  bottom: 0; 
  margin: auto;*/
  width:100%;
  height:100%;
}
</style>
<script type="text/javascript">
window.onload = function() {
	Datatrans.startPayment({
		'form': '#paymentForm',
		'opened': function() {
      
		},
		'loaded': function() {

		},
		'closed': function() {
      window.location.replace("{{ route('reservation.checkout.get') }}");
		},
		'error': function() {
      window.location.replace("{{ route('reservation.checkout.get') }}");
		},
	});
};
</script>
<script>
window.dataLayer = window.dataLayer || [];
dataLayer.push({'event': 'formSubmissionSuccess', 'formId': 'paymentForm'});
</script>
