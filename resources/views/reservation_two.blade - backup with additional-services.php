@section('page-title', 'Parking Aéroport de Genève Tarifs')
@section('page-description', "Réservez votre place de parking à l'aéroport de Genève au meilleur tarifs possible. Réservez en ligne sur notre site votre place de parking 7jours sur 7")
@extends('layouts.app')
@section('content')
<div class="container-fluid page-content">
    <div class="container">
        <div class="row text-center">
            <h2 data-aos="zoom-in">@lang('messages.reservation_page_first_title')</h2>
        </div>
        <div class="row reservation">
          @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
            </div>
          @endif
            <div class="step_navigation">
                <ul class="steps">
                    <li class="current"><a href="#">Étape 1</a></li>
                    <li><a href="#">Étape 2</a></li>
                    <li><a href="#">Étape 3</a></li>
                    <li><a href="#">Étape 4</a></li>
                    <li><a href="#">Étape 5</a></li>
                </ul>
            </div>
            {!! Form::open(['action' => 'ReservationController@postReservationCart', 'method' => 'POST']) !!}
            {!! Form::hidden('check_type', 1) !!}
            <div class="container pb-40">
                <div class="row">
                    
                    @if (isset($data['show_reservation_options']) && $data['show_reservation_options'] == 1)
                   <h3 class="step_title mb-40">Sélectionner des services supplémentaires:</h3>
                    <div class="row center-row">
                        <div class="col-sm-5 col-sm-offset-1 mb-40">
                    
                            <p>Lavage voiture:</p>
                            
                            <label class="form-check-label none">
                                {!! Form::radio('options', 'null', ((session('selectedDates')['options'] != 'null') && (session('selectedDates')['options'] != '') ) ? false : true, ['id' => 'radio-1-4', 'class' => 'form-check-input regular-radio']) !!}
                                <label for="radio-1-4"></label>@lang('messages.reservation_page_options_none_title')
                            </label>

                            <div class="form-check">
                                <img src="{!! asset('image/carwash-icon.png') !!}" alt="">
                                <label class="form-check-label">
                                    {!! Form::radio('options', 'intereur', session('selectedDates')['options'] == 'intereur' ? true : false, ['id' => 'radio-1-1', 'class' => 'form-check-input regular-radio']) !!}
                                <label for="radio-1-1"></label>Nettoyage basique (30min)  - CHF 60 
                                <label for="radio-1-1"></label>Aspirateur, poussière et karcher (sans essuyage)
                                </label>
                            </div>

                                
                            <div class="form-check">
                                <img src="{!! asset('image/carwash-icon-premium.jpg') !!}" alt="">      
                                <label class="form-check-label">
                                    {!! Form::radio('options', 'extereur', session('selectedDates')['options'] == 'extereur' ? true : false, ['id' => 'radio-1-2', 'class' => 'form-check-input regular-radio']) !!}
                                <label for="radio-1-2"></label>Nettoyage premium (60min) - CHF 150
                                <label for="radio-1-2"></label>Aspirateur, vitre, plastique, karcher (avec essuyage) et nettoyage jantes à la brosse
                                </label>
                                    
                            </div>
                        
                        </div>
                        
                        <div class="col-sm-5 col-sm-offset-1 mb-40">
                        
                            <p>Ravitaillement:</p>

                            <label class="form-check-label none">
                                    {!! Form::radio('has_electric', 'null', (session('selectedDates')['has_electric'] == 'null') ? true : false, ['id' => 'radio-2-1', 'class' => 'form-check-input regular-radio']) !!}
                                <label for="radio-2-1"></label>@lang('messages.reservation_page_options_none_title')
                                </label>
                            <div class="form-check">
                                <img src="{!! asset('image/electric.png') !!}" alt="">
                                <label class="form-check-label">
                                    {!! Form::radio('has_electric', 1, (session('selectedDates')['has_electric'] == '1') ? true : false, ['id' => 'radio-2-2', 'class' => 'form-check-input regular-radio']) !!}
                                <label for="radio-2-2"></label>CHF 5
                                </label>
                            </div>
                        
                        </div> 
                    </div>
                    @endif
                   
                    <div class="table">
                    <table>
                        <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">un service</th>
                            <th scope="col">prix</th>
                            <th scope="col">la description</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td data-label="" class="box"><label for="service1"><input type="checkbox" id ="service1"><span class="checkmark"></span></label></td> 
                                <td data-label="un service" class="bold-text pink"><label for="service1" class="box-lable">Option intérieur premium plus</label></td>
                                <td data-label="prix" class="bold-text">CHF 150</td>
                                <td data-label="la description">Nettoyage moquette et tapis à l’eau, traitement du cuire à la cire et/ou tissus, traitement des plastiques</td>
                            </tr>
                            <tr>
                                <td scope="row" class="box"><label for="service2"><input type="checkbox" id ="service2"><span class="checkmark"></span></label></td>
                                <td data-label="un service" class="bold-text pink"><label for="service2">Option odeur</label></td>
                                <td data-label="prix" class="bold-text">CHF 60</td>
                                <td data-label="la description">Traitement des odeurs, neutralisation et élimination.</td>
                            </tr>
                            <tr>
                                <td scope="row" class="box"><label for="service3"><label for="service3"><input type="checkbox" id ="service3"><span class="checkmark"></span></label></label></td>
                                <td data-label="un service" class="bold-text pink"><label for="service3">Option poils de chien</label></td>
                                <td data-label="prix" class="bold-text">CHF 60</td>
                                <td data-label="la description">Elimination complète. </td>
                            </tr>
                            <tr>
                                <td scope="row" class="box"><label for="service4"><input type="checkbox" id ="service4"><span class="checkmark"></span></label></td>
                                <td data-label="un service" class="bold-text pink"><label for="service4">Option extérieur premium plus</lable></td>
                                <td data-label="prix" class="bold-text">CHF 60</td>
                                <td data-label="la description">Nettoyage à la main au Savon-Cire de brillance, Séchage (main), entrée des portes et moteur, Jantes (main).</td>
                            </tr>
                            <tr>
                                <td scope="row" class="box"><label for="service5"><input type="checkbox" id ="service5"><span class="checkmark"></span></label></td>
                                <td data-label="un service" class="bold-text pink"><label for="service5">Option polissage</lable></td>
                                <td data-label="prix" class="italic-text">sur demande</td>
                                <td data-label="la description">Polish complet avec produits professionnels Meguiar’s.</td>
                            </tr>
                            <tr>
                                <td scope="row" class="box"><label for="service6"><input type="checkbox" id ="service6"/><span class="checkmark"></span></label></td>
                                <td data-label="un service" class="bold-text pink"><label for="service6">Option céramique</lable></td>
                                <td data-label="prix" class="italic-text">sur demande</td>
                                <td data-label="la description">Traitement avec produits professionnels Meguiar’s. (Polissage préalable obligatoire) </td>
                            </tr>
                            <tr>
                                <td scope="row" class="box"><label for="service7"><input type="checkbox" id ="service7"><span class="checkmark"></span></label></td>
                                <td data-label="un service" class="bold-text pink"><label for="service7">Option total contrôle</lable></td>
                                <td data-label="prix" class="bold-text">CHF 50 <span class="italic-text">+ matériel changé</td>
                                <td data-label="la description">Contrôle de toutes les ampoules, les niveaux d’huile, eau, moteur et lave glace. Pression des pneus et roue de secours. </td>
                            </tr>
                            <tr>
                                <td scope="row" class="box"><label for="service8"><input type="checkbox" id ="service8"><span class="checkmark"></span></label></td>
                                <td data-label="un service" class="bold-text pink"><label for="service8">Option Climatisation</lable></td>
                                <td data-label="prix" class="bold-text">CHF 150</td>
                                <td data-label="la description">Remplissage climatisation.</td>
                            </tr>
                            <tr>
                                <td scope="row" class="box"><label for="service9"><input type="checkbox" id ="service9"><span class="checkmark"></span></label></td>
                                <td data-label="un service" class="bold-text pink"><label for="service9">Option Climatisation (Cleaner)</lable></td>
                                <td data-label="prix" class="bold-text">CHF 170</td>
                                <td data-label="la description">Remplissage climatisation + cleaner hygiène conduite de clim. </td>
                            </tr>
                            <tr>
                                <td scope="row" class="box"><label for="service10"><input type="checkbox" id ="service10"><span class="checkmark"></span></label></td>
                                <td data-label="un service" class="bold-text pink"><label for="service10">Option vitres teintées</lable></td>
                                <td data-label="prix" class="italic-text">variable, devis obligatoire</td>
                                <td data-label="la description">Pose pro garantie à vie ! Colorie aux choix.</td>
                            </tr>
                            <tr>
                                <td scope="row" class="box"><label for="service11"><input type="checkbox" id ="service11"><span class="checkmark"></span></label></td>
                                <td data-label="un service" class="bold-text pink"><label for="service11">Option pare-brise</lable></td>
                                <td data-label="prix" class="italic-text">variable, devis obligatoire</td>
                                <td data-label="la description">Remplacement du pare-brise (franchise offerte). Facilité administrative avec toutes assurances.</td>
                            </tr>
                            <tr>
                                <td scope="row" class="box"><label for="service12"><input type="checkbox" id ="service12"><span class="checkmark"></span></label></td>
                                <td data-label="un service" class="bold-text pink"><label for="service12">Option service rapide (visite)</lable></td>
                                <td data-label="prix" class="italic-text">sur demande</td>
                                <td data-label="la description">Vidange,  changement filtre à huile et filtre, pollen, plaquettes freins disc, mécanique, pneus, radio branchement et multimédia.  <b>OFFERT  - (Option total contrôle).</b></td>
                            </tr>
                            <tr>
                                <td scope="row" class="box"><label for="service13"><input type="checkbox" id ="service13"><span class="checkmark"></span></label></td>
                                <td data-label="un service" class="bold-text pink"><label for="service13">Option carrosserie (Toutes marques)</lable></td>
                                <td data-label="prix" class="italic-text">devis préalable obligatoire</td>
                                <td data-label="la description">
                                    <ul>
                                        <li>Polissage complet</li>
                                        <li>Traitement céramique </li>
                                        <li>Retouche peinture </li>
                                        <li>Réparation, transformation, restauration</li>
                                        <li>Facilité administrative (auprès des assurances}</li>
                                        <li>Voiture de prêt</li>
                                    </ul>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div><!--/.table end -->
  
                    <div class="row button text-center">
                        <div class="col-xs-6 col-md-offset-3">
                            {!! Form::submit('Suivant') !!}
                        </div>
                    </div>

                    {!! Form::close() !!}
                    <input type="hidden" name="date_arrival_calc">
                    <input type="hidden" name="date_departure_calc">
                    
                    <input type="submit" class="return_button" value="Retour">  
                </div>
            </div>
        </div>
    </div>
</div>
   
            
<script type="text/javascript">
    $( document ).ready(function() {

        $normal = $('.normal');
        $vip = $('.vip');

        $normal.click(function(){
            $(this).addClass('active');
            $(this).find('#radio-1').prop('checked',true);
            $('#radio-2').prop('checked',false);
            $vip.removeClass('active');
        });

        $vip.click(function(){
            if (!$(this).hasClass('disabled')) {
                $(this).addClass('active');
            }
            $(this).find('#radio-2').prop('checked',true);
            $('#radio-1').prop('checked',false);
            $normal.removeClass('active');
        });

        $( ".card" ).hover(function() {
          $(this).find('.card__tooltip').toggleClass('active')
        });

        $('.vip.disabled').click(function(){
            return false;
        });

        $('input[name="date_arrival_calc"]').val($('#datepicker-reservation-1').val());
        $('input[name="date_departure_calc"]').val($('#datepicker-reservation-2').val());

    });
</script>
@endsection
