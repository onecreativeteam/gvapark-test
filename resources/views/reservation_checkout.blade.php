@extends('layouts.app')

@section('content')
  <div class="container-fluid page-content">
    <div class="row text-center">
      {{-- <h2 data-aos="zoom-in">Sortie</h2> --}}
    </div>

    <div class="row sortie-info">

      <div class="container">
        <div class="row">
          @if ($clientAuthSuccess == 0)
          <div class="col-sm-12">
              <div id="initClientAuthModal"></div>
          </div>
          @else
          <div class="col-sm-12">
              <span></span>
          </div>
          @endif
          <div class="col-sm-12 text-center"  data-aos="zoom-in" data-aos-delay="200">
            <img src="{!! asset('image/info.png') !!}" alt="">
            <div class="question">
               <p data-toggle="collapse" data-target="#option1-2" class="collapsed">@lang('messages.reservation_checkout_first_label') <span>@lang('messages.reservation_checkout_second_label')</span></p>
               <div id="option1-2" class=" collapse">
                   <div class="col-sm-6 col-sm-offset-3 text-center">
                    {!! Form::open(['action' => 'ReservationController@applyPromoCode', 'method' => 'POST']) !!}
                         <div class="form-group">
                          <input type="text" name="promo_code" class="form-control" id="code" placeholder="{{trans('messages.promo_code_placeholder')}}">
                        </div>
                       <button type="submit" class="button">@lang('messages.reservation_checkout_apply_promo_label')</button>
                    {!! Form::close() !!}
                   </div>
               </div>
             </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      @if ($clientAuthSuccess == 0)
    <div class="row">
        <div class="col-sm-6">
            <br>
            <div class="answer">
                @lang('messages.reservation_checkout_info_label')
            </div>
            {!! Form::open(['action' => 'ReservationController@authClient', 'method' => 'POST']) !!}
                <div class="form-group">
                    <label for="login_email">@lang('messages.form_email')</label>
                    <input type="email" class="form-control" name="login_email" id="login_email2">
                </div>
                <div class="form-group">
                    <label for="login_password">@lang('messages.form_password')</label>
                    <input type="password" class="form-control" name="login_password" id="login_password2">
                </div>
                <a class="btn btn-link" href="{{ route('password.reset') }}">
                    @lang('messages.form_password_reset')
                </a>
            {!! Form::submit(trans('messages.client_login_button'), array('class'=>'button')) !!}
            {!! Form::close() !!}
        </div>
        <div class="col-sm-6 text-center">
            <div class="go-to-register-wrapper">
                <div class="go-to-register-text">
                  @lang('messages.reservation_checkout_info2_label')
                </div>
                <a href="#go-to-register-link" class="go-to-register">@lang('messages.reservation_checkout_new_client_button_label')</a>
            </div>
        </div>
    </div>
      @endif
      <div id="go-to-register-link"></div>
      <div class="row errors-danger">
          <div class="alert alert-danger">
          @if (count($errors) > 0)
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          @endif
          </div>
      </div>
      <div class="row contact-form" data-aos="zoom-in" data-aos-delay="300" >
        {!! Form::open(['action' => 'ReservationController@postReservationCheckout', 'method' => 'POST']) !!}
          <div class="col-sm-6"  >
            <h3>@lang('messages.reservation_checkout_facture_details_label')</h3>
            <div class="form-inline">
              <div class="form-group">
                <label for="last_name">@lang('messages.profile_page_last_name_label')</label>
                {!! Form::input('text', 'last_name', isset($clientData['last_name']) ? $clientData['last_name'] : '', ['class' => 'form-control', 'id'=>'last_name']) !!}
              </div>
              <div class="form-group">
                <label for="first_name">@lang('messages.profile_page_first_name_label')</label>
                {!! Form::input('text', 'first_name', isset($clientData['first_name']) ? $clientData['first_name'] : '', ['class' => 'form-control', 'id'=>'first_name']) !!}
              </div>
            </div>
            <div class="form-group">
              <label for="company_name">@lang('messages.profile_page_company_name_label')</label>
              {!! Form::input('text', 'company_name', isset($clientData['company_name']) ? $clientData['company_name'] : '', ['class' => 'form-control', 'id'=>'company_name']) !!}
            </div>
            <div class="form-inline">
              <div class="form-group">
                <label for="email">@lang('messages.profile_page_email_label')</label>
                {!! Form::input('text', 'email', isset($clientData['email']) ? $clientData['email'] : '', ['class' => 'form-control', 'id'=>'email', ($clientAuthSuccess == '0') ? '' : 'disabled']) !!}
              </div>
              
              <div class="form-group">
                <label for="phone">@lang('messages.profile_page_phone_label')</label>
                {!! Form::input('text', 'phone', isset($clientData['phone']) ? $clientData['phone'] : '', ['class' => 'form-control', 'id'=>'phone']) !!}
              </div>
            </div>
            @if ($clientAuthSuccess == '0')
            <div class="form-inline">
              <div class="form-group">
                <label for="email_confirm">@lang('messages.profile_page_email_confirm_label')</label>
                {!! Form::input('text', 'email_confirm', isset($clientData['email_confirm']) ? $clientData['email_confirm'] : '', ['class' => 'form-control', 'id'=>'email_confirm']) !!}
              </div>
              <div class="form-group">
                <label for="number">@lang('messages.profile_page_password_label')</label>
                {!! Form::input('password', 'password', isset($clientData['old_password']) ? $clientData['old_password'] : '', ['class' => 'form-control', 'id'=>'password']) !!}
              </div>
            </div>
            @endif
            <div class="form-group">
              <label for="adresse">@lang('messages.profile_page_adress_label')</label>
              {!! Form::input('text', 'adress', isset($clientData['adress']) ? $clientData['adress'] : '', ['class' => 'form-control', 'id'=>'adress', 'placeholder'=> trans('messages.profile_page_adress_label') ]) !!}
            </div>
            <div class="form-group">
              <label for="city">@lang('messages.profile_page_adress2_label')</label>
              {!! Form::input('text', 'adress2', isset($clientData['adress2']) ? $clientData['adress2'] : '', ['class' => 'form-control', 'id'=>'adress2', 'placeholder'=> trans('messages.profile_page_adress2_label') ]) !!}
            </div>
            <div class="form-group">
              <label for="city">@lang('messages.profile_page_city_label')</label>
              {!! Form::input('text', 'city', isset($clientData['city']) ? $clientData['city'] : '', ['class' => 'form-control', 'id'=>'city', 'placeholder'=> trans('messages.profile_page_city_label') ]) !!}
            </div>
            <div class="form-group">
              <label for="postcode"></label>
              {!! Form::input('text', 'postcode', isset($clientData['postcode']) ? $clientData['postcode'] : '', ['class' => 'form-control', 'id'=>'postcode', 'placeholder'=> 'Postcode / Zip' ]) !!}
            </div>
          </div>
          <div class="col-sm-6" >
            <h3>@lang('messages.profile_page_car_information_label')</h3>
            <div class="form-group">
              <label for="description">@lang('messages.profile_page_description_label')</label>
              {!! Form::textarea('description', isset($clientData['description']) ? $clientData['description'] : '', ['class' => 'form-control', 'id'=>'description', 'rows'=>'3', 'placeholder'=> trans('messages.profile_page_description_label') ]) !!}
            </div>
            <div class="form-inline">
              <div class="form-group">
                <label for="car_brand">@lang('messages.profile_page_car_brand_label')</label>
                {!! Form::input('text', 'car_brand', isset($clientData['car_brand']) ? $clientData['car_brand'] : '', ['class' => 'form-control', 'id'=>'car_brand', 'placeholder'=> trans('messages.profile_page_car_brand_label') ]) !!}
              </div>
              <div class="form-group">
                <label for="car_model">@lang('messages.profile_page_car_model_label')</label>
                {!! Form::input('text', 'car_model', isset($clientData['car_model']) ? $clientData['car_model'] : '', ['class' => 'form-control', 'id'=>'car_model', 'placeholder'=> trans('messages.profile_page_car_model_label') ]) !!}
              </div>
            </div>
            <div class="form-inline">
              <div class="form-group">
                <label for="car_registration_number">@lang('messages.profile_page_car_registration_number_label')</label>
                {!! Form::input('text', 'car_registration_number', isset($clientData['car_registration_number']) ? $clientData['car_registration_number'] : '', ['class'=>'form-control', 'id'=>'car_registration_number', 'placeholder'=> trans('messages.profile_page_car_registration_number_label') ]) !!}
              </div>
              <div class="form-group">
                <label for="flight_number">@lang('messages.profile_page_flight_number_label')</label>
                {!! Form::input('text', 'flight_number', isset($clientData['flight_number']) ? $clientData['flight_number'] : '', ['class' => 'form-control', 'id'=>'flight_number', 'placeholder'=> trans('messages.profile_page_flight_number_label') ]) !!}
              </div>
            </div>
          </div>
      </div>
      <div class="row prices"  data-aos="zoom-in" data-aos-delay="400">
        <div class="col-xs-12">
          <h4>@lang('messages.reservation_checkout_table_head_label')</h4>
          <table>
            <tr>
              <th>@lang('messages.facture_products_label')</th>
              <th>@lang('messages.facture_only_total')</th>
            </tr>
            <tr>
              <td>@lang('messages.facture_days_label') × {!! $priceList['daysToCharge'] !!} </td>
              <td>{!! number_format($priceList['totalDayTax'], 2) !!}CHF</td>
            </tr>
            @if(!empty($priceList['selectedOption']) && isset($priceList['optionsTax']))
            <tr>
              <td>@lang('messages.reservation_page_options_'.$priceList['selectedOption'].'_selected') x 1</td>
              <td>{!! number_format(@$priceList['optionsTax'], 2) !!}CHF</td>
            </tr>
            @endif
            <tr>
              <td></td>
              <td>{!! number_format($priceList['total'], 2) !!}CHF</td>
            </tr>
            <tr>
              <td>@lang('messages.facture_only_tva')</td>
              <td>{!! number_format($priceList['totalVAT'], 2) !!}CHF</td>
            </tr>
            <tr>
              <td>@lang('messages.facture_only_total')</td>
              <td>{!! number_format($priceList['totalPriceWithVAT'], 2) !!}CHF</td>
            </tr>
            @if (isset($priceList['is_aplied_promo']) && $priceList['is_aplied_promo'] === 1)
            <tr>
                <td>@lang('messages.reservation_cart_table_promo_code_label')</td>
                <td>{!! number_format($priceList['promo_value_percent'], 2) !!}%</td>
            </tr>
            <tr>
                <td>@lang('messages.reservation_cart_table_sous_total_with_promo_label')</td>
                <td>{!! number_format($priceList['totalPromoPriceWithVAT'], 2) !!}CHF</td>
            </tr>
            @endif
          </table>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <button type="button" onClick="postAjaxCheckout()" class="button">@lang('messages.reservation_checkout_table_button_label')</button>
        </div>
        <div class="col-xs-12">
          <p style="float:right">@lang('messages.tc_title_main_1') <a style="color:#337ab7;" class="" onClick="showTermsAndConditionsModal()"  id="initTermsAndConditionsModal">@lang('messages.tc_title_main_2')</a></p>
        </div>
      </div>
     {!! Form::close() !!}
    </div>
  </div>

  <script type="text/javascript">
  function postAjaxCheckout() {

    var data = {};

    data.first_name               = $('#first_name').val();
    data.last_name                = $('#last_name').val();
    data.company_name             = $('#company_name').val();
    data.phone                    = $('#phone').val();
    data.adress                   = $('#adress').val();
    data.adress2                  = $('#adress2').val();
    data.city                     = $('#city').val();
    data.postcode                 = $('#postcode').val();
    data.description              = $('#description').val();
    data.car_brand                = $('#car_brand').val();
    data.car_model                = $('#car_model').val();
    data.car_registration_number  = $('#car_registration_number').val();
    data.flight_number            = $('#flight_number').val();

    if ( ($('#email').length > 0) && ($('#email').prop('disabled') === false ) ) {
      data.email          = $('#email').val();
    }
    if ($('#email_confirm').length > 0) {
      data.email_confirm  = $('#email_confirm').val();
    }
    if ($('#password').length > 0) {
      data.password       = $('#password').val();
    }

    $.ajaxSetup( { headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() } } );
    $.ajax({
        method: "POST",
        url: "{{ route('reservation.checkout.ajax') }}",
        dataType: "json",
        data: data,
        success: function (response) {
          loadPayment();
        },
        error: function (error) {
          var errors = '<ul>';
          $.each(error.responseJSON, function (key, val) {
            errors += '<li style="padding:5px">'+val+'</li>';
          });
          errors += '</ul>';
          $('.alert-danger').html(errors);

          $('html, body').animate({
              scrollTop: $(".errors-danger").offset().top
          }, 500);
          
        }
    });
  }

  function loadPayment() {
    Datatrans.startPayment({
      'form': '#paymentForm',
      'opened': function() {

      },
      'loaded': function() {
        // $('#datatransPaymentFrame').parent('div').css("position", "inherit");
        $('body').css("top", "0px");
      },
      'closed': function() {
        window.location.replace("{{ route('reservation.checkout.get') }}");
      },
      'error': function() {
        window.location.replace("{{ route('reservation.checkout.get') }}");
      },
    });
  }
  </script>
  <script>
  window.dataLayer = window.dataLayer || [];
  dataLayer.push({'event': 'formSubmissionSuccess', 'formId': 'paymentForm'});
  </script>
  <div class="row" style="display:none">
    <div class="col-xs-12">
        <form id="paymentForm"
          data-merchant-id="{{ $params['MERCHANT'] }}"
          data-amount="{{ $params['AMOUNT'] }}"
          data-currency="{{ $params['CURRENCY'] }}"
          data-refno="{{ $params['REFNO'] }}"
          data-success-url="{{ route('postfinance.status') }}"
          data-error-url="{{ route('postfinance.status') }}"
          data-cancel-url="{{ route('postfinance.status') }}"
          data-sign="{{ $params['SIGN'] }}">
        </form>
    </div>
  </div>
  <script type="text/javascript">
    $(document).ready(function(){
        $('a').click(function(){
            $('html, body').animate({
                scrollTop: $( $(this).attr('href') ).offset().top
            }, 500);
            return false;
        });
    });
  </script>
@endsection
