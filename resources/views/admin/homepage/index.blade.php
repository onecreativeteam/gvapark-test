@extends('layouts.app_admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Homepage Administration.</div>
                <div class="panel-body">
                    <div class="box-body">
                        <div id="tabs">
                            <ul>
                            @foreach ($supportedLocales as $k => $locale)
                                <li><a href="#tabs-{{$locale}}">{{$locale}}</a></li>
                            @endforeach
                            </ul>
                            {!! Form::model($data, ['method'=>'POST', 'action'=>['Admin\HomepageController@store']]) !!}
                                @foreach ($supportedLocales as $lang => $locale)
                                    <div id="tabs-{{$locale}}">
                                        <div class="form-group">
                                            {!! Form::label('title', 'Title:') !!}
                                            {!! Form::input('text', "translations[{$lang}][title]", isset($data->translations[$locale]) ? @$data->translations[$locale]->title : @$data->title) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('description', 'Description:') !!}
                                            {!! Form::textarea("translations[{$lang}][description]", isset($data->translations[$locale]) ? @$data->translations[$locale]->description : @$data->description, ['style'=>'width: 100%;']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('security_info', 'Sécurité:') !!}
                                            {!! Form::textarea("translations[{$lang}][security_info]", isset($data->translations[$locale]) ? @$data->translations[$locale]->security_info : @$data->security_info, ['style'=>'width: 100%;']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('simplicity_info', 'Simplicité:') !!}
                                            {!! Form::textarea("translations[{$lang}][simplicity_info]", isset($data->translations[$locale]) ? @$data->translations[$locale]->simplicity_info : @$data->simplicity_info, ['style'=>'width: 100%;']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('speed_info', 'Rapidité:') !!}
                                            {!! Form::textarea("translations[{$lang}][speed_info]", isset($data->translations[$locale]) ? @$data->translations[$locale]->speed_info : @$data->speed_info, ['style'=>'width: 100%;']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('service_info', 'Un service reconnu:') !!}
                                            {!! Form::textarea("translations[{$lang}][service_info]", isset($data->translations[$locale]) ? @$data->translations[$locale]->service_info : @$data->service_info, ['style'=>'width: 100%;']) !!}
                                        </div>
                                    </div>
                                @endforeach
                                <div class="form-group">
                                    {!! Form::submit('Save', array('class'=>'btn btn-primary')) !!}
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
$(function() {
    $("#tabs").tabs();
});
</script>
@endpush