@extends('layouts.app_admin')

@section('content')
<div class="container container-calendar">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Edit User</div>
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            {!! Form::open( array( 'action' => array('AdminController@updateUser', $user->id), 'method' => 'POST' ) ) !!}
                                <div class="form-group">
                                    {!! Form::label('role', 'User Role:') !!}
                                    {!! Form::select('role', $rolesSelect, $user->role, ['class' => 'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('name', 'Name:') !!}
                                    {!! Form::input('text', 'name', @$user->name, array('required', 'class'=>'form-control', 'placeholder'=>'Name')) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('email', 'Email:') !!}
                                    {!! Form::input('email', 'email', @$user->email, array('disabled', 'class'=>'form-control', 'placeholder'=>'Email')) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('password', 'Password:') !!}
                                    {!! Form::input('password', 'password', null, array('class'=>'form-control', 'placeholder'=>'Password')) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('password_confirmation', 'Repeat Password:') !!}
                                    {!! Form::input('password', 'password_confirmation', null, array('class'=>'form-control', 'placeholder'=>'Confirm Password')) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::submit('Update') !!}
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

</script>
@endsection
