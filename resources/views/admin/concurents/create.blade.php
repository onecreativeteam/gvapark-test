@extends('layouts.app_admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Concurents Administration.</div>
                <div class="box-body">
                    <div id="tabs">
                        <ul>
                        @foreach ($supportedLocales as $k => $locale)
                            <li><a href="#tabs-{{$locale}}">{{$locale}}</a></li>
                        @endforeach
                        </ul>
                        {!! Form::open( array('action' => 'Admin\ConcurentsController@store', 'method' => 'POST') ) !!}
                            @foreach ($supportedLocales as $lang => $locale)
                                <div id="tabs-{{$locale}}">
                                    <div class="form-group">
                                        {!! Form::label('name', 'Name:') !!}
                                        {!! Form::input('text', "translations[{$lang}][name]", null, array('required', 'class'=>'form-control', 'placeholder'=>'Name')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('daily_price', 'Jour:') !!}
                                        {!! Form::input('text', "translations[{$lang}][daily_price]", null, array('required', 'class'=>'form-control', 'placeholder'=>'Jour')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('daily_price_sup', 'Jour SUP:') !!}
                                        {!! Form::input('text', "translations[{$lang}][daily_price_sup]", null, array('required', 'class'=>'form-control', 'placeholder'=>'Jour Sup.')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('initial_price', 'Jour:') !!}
                                        {!! Form::input('text', "translations[{$lang}][initial_price]", null, array('required', 'class'=>'form-control', 'placeholder'=>'PRISE EN CHARGE')) !!}
                                    </div>
                                </div>
                            @endforeach
                            <div class="form-group">
                                {!! Form::submit('Save', array('class'=>'btn btn-primary')) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
$(function() {
    $("#tabs").tabs();
});
</script>
@endpush