@extends('layouts.app_admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="box-header with-border">
                <h3 class="box-title">Concurents</h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box-body table-responsive no-padding">
                <div class="">
                    <div class="table-toolbar">
                        <a class="btn btn-warning" href="{{ route('admin.create.concurents') }}">Create <i class="fa fa-plus"></i></a>
                    </div>
                    <br>
                    <table class="table table-hover" id="concurents-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Jour</th>
                                <th>JOUR SUP.</th>
                                <th>PRISE EN CHARGE</th>
                                <th>Operations</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach( $concurents as $k => $concurent)
                            <tr>
                                <td>{!! $concurent->id !!}</td>
                                <td>
                                    <p>{!! $concurent->name !!}</p>
                                </td>
                                <td>
                                    <p>{!! $concurent->daily_price !!}</p>
                                </td>
                                <td>
                                    <p>{!! $concurent->daily_price_sup !!}</p>
                                </td>
                                <td>
                                    <p>{!! $concurent->initial_price !!}</p>
                                </td>
                                <td>
                                    <a href="{{ route('admin.edit.concurents', [$concurent->id]) }}" class="btn btn-primary btn-block btn-xs"><i class="glyphicon glyphicon-edit"></i>&nbsp;&nbsp; Edit</a>
                                    @if ($concurent->is_main == 0)
                                    <form class="pull-left" method="POST" action="{{ route('admin.destroy.concurents', [$concurent->id]) }}" accept-charset="UTF-8">
                                        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                        <input name="_method" type="hidden" value="DELETE">
                                        <button class="btn btn-sm red" type="submit"><i class="fa fa-trash-o"></i> Delete</button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
$(function() {

});
</script>
@endpush
