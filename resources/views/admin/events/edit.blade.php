@extends('layouts.app_admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Events Administration.</div>
                <div class="box-body">
                    <div id="tabs">
                        <ul>
                        @foreach ($supportedLocales as $k => $locale)
                            <li><a href="#tabs-{{$locale}}">{{$locale}}</a></li>
                        @endforeach
                        </ul>
                        {!! Form::open( array('action' => array('Admin\EventsController@update', $event->id), 'method' => 'PUT', 'files' => 'true', 'style'=>'padding:10px') ) !!}
                            @foreach ($supportedLocales as $lang => $locale)
                                <div id="tabs-{{$locale}}">
                                    <div class="form-group">
                                        {!! Form::label('name', 'Name:') !!}
                                        {!! Form::input('text', "translations[{$lang}][name]", isset($event->translations[$locale]) ? $event->translations[$locale]->name : $event->name, array('required', 'class'=>'form-control', 'placeholder'=>'Name')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('description', 'Description:') !!}
                                        {!! Form::textarea("translations[{$lang}][description]", isset($event->translations[$locale]) ? $event->translations[$locale]->description : $event->description, array('required', 'class'=>'form-control', 'placeholder'=>'Description')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('short_description', 'Short Description:') !!}
                                        {!! Form::textarea("translations[{$lang}][short_description]", isset($event->translations[$locale]) ? $event->translations[$locale]->short_description : $event->short_description, array('required', 'class'=>'form-control', 'placeholder'=>'Short Description')) !!}
                                    </div>
                                </div>
                            @endforeach
                            <div class="form-group">
                                {!! Form::label('date_from', 'start date:') !!}
                                {!! Form::input('text', 'date_from', Carbon\Carbon::parse(@$event->date_from)->format('Y/m/d'), array('required', 'class'=>'form-control datepicker-promo', 'placeholder'=>'start date')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('date_to', 'end date:') !!}
                                {!! Form::input('text', 'date_to', Carbon\Carbon::parse(@$event->date_to)->format('Y/m/d'), array('required', 'class'=>'form-control datepicker-promo', 'placeholder'=>'end date')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('price', 'Price:') !!}
                                {!! Form::input('text', 'price', @$event->price, array('required', 'class'=>'form-control', 'placeholder'=>'Price')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('max_reservation_count', 'Maximum Reservation for Event:') !!}
                                {!! Form::input('text', 'max_reservation_count', @$event->max_reservation_count, array('required', 'class'=>'form-control', 'placeholder'=>'20')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('is_active', 'Active ?:') !!}
                                {!! Form::checkbox('is_active', 1, @$event->is_active) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('big_picture', 'big_picture:') !!}
                                {!! Form::file('big_picture', ['class'=>'form-control']) !!}
                                <img style="width:150px;height:150px;margin-top:10px" src="/{{@$event->big_picture}}"/>
                            </div>
                            <div class="form-group">
                                {!! Form::label('small_picture', 'small_picture:') !!}
                                {!! Form::file('small_picture', ['class'=>'form-control']) !!}
                                <img style="width:150px;height:150px;margin-top:10px" src="/{{@$event->small_picture}}"/>
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Save', array('class'=>'btn btn-primary')) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
$(function() {
    $("#tabs").tabs();
});
</script>
@endpush