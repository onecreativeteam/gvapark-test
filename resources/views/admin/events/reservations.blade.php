@extends('layouts.app_admin')

@section('content')
<div class="">
    <div class="">
        <div class="col-md-12">
            <div class="box-header with-border">
                <h3 class="box-title">Reservations List</h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box-body responsive table-responsive">
                <table class="table table-bordered table-responsive no-padding" id="event-reservations-table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Reservation at:</th>
                            <th>Arrival at:</th>
                            <th>Status:</th>
                            <th>Tax:</th>
                            <th>Event:</th>
                            <th>Cancel?</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
$(function() {
    $('#event-reservations-table').DataTable({
        processing: true,
        serverSide: true,
        paging: true,
        pageLength: 15,
        ajax: "{!! route('admin.datatables.event.reservations') !!}",
        order: [[0, 'desc']],
        columns: [
            { data: 'id', name: 'id', searchable: true },
            { data: 'first_name', name: 'first_name', searchable: true },
            { data: 'last_name', name: 'last_name', searchable: true },
            { data: 'date_of_reservation', name: 'date_of_reservation', searchable: true },
            { data: 'date_of_arrival', name: 'date_of_arrival', searchable: true },
            { data: 'payment_status', name: 'payment_status', searchable: true },
            { data: 'price', name: 'price', searchable: true },
            { data: 'event', name: 'event', searchable: false },
            { data: 'cancel', name: 'cancel', searchable: false },
        ]
    });
});
</script>
@endpush