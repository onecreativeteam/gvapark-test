@extends('layouts.app_admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="box-header with-border">
                <h3 class="box-title">Events</h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box-body table-responsive no-padding">
                <div class="">
                    <div class="table-toolbar">
                        <a class="btn btn-warning" href="{{ route('admin.create.events') }}">Create <i class="fa fa-plus"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box-body responsive table-responsive">
                <table class="table table-bordered table-responsive no-padding" id="events-table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Date From</th>
                            <th>Date To</th>
                            <th>Price</th>
                            <th>Is active</th>
                            <th>Operations:</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
$(function() {
    $('#events-table').DataTable({
        processing: true,
        serverSide: true,
        paging: true,
        pageLength: 15,
        ajax: "{!! route('admin.datatables.events') !!}",
        order: [[0, 'desc']],
        columns: [
            { data: 'id', name: 'id', searchable: true },
            { data: 'name', name: 'name', searchable: true },
            { data: 'description', name: 'description', searchable: false },
            { data: 'date_from', name: 'date_from', searchable: true },
            { data: 'date_to', name: 'date_to', searchable: true },
            { data: 'price', name: 'price', searchable: true },
            { data: 'is_active', name: 'is_active', searchable: false },
            { data: 'operations', name: 'operations', searchable: false },
        ]
    });
});
</script>
@endpush
