@extends('layouts.app_admin')

@section('content')
<div class="container container-calendar">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard.</div>
                <div class="panel-body">
                    <div class="form-group col-md-5">
                        {!! Form::label('year', 'Year:') !!}
                        {!! Form::select('year', @$yearsSelect ? $yearsSelect : [], $data['startYear'], ['class' => 'form-control yearSelect']) !!}
                    </div>
                    <div class="form-group col-md-5">
                        {!! Form::label('month', 'Month:') !!}
                        {!! Form::select('month', @$monthsSelect ? $monthsSelect : [], $data['startMonth'], ['class' => 'form-control monthSelect']) !!}
                    </div>
                    <div class="form-group col-md-2">
                        {!! Form::label('filter', 'Filter') !!}
                        {!! Form::submit('Go!', ['class' => 'form-control getReservationsProfitData']) !!}
                    </div>
                    <span id="total_profit">Profit: <strong>{{ $data['currentMonthProfit'][0]['total_profit'] }} CHF</strong></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">

      <div id="col_chart_html_tooltip">

      </div>

      <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
      <script type="text/javascript">

      $(document).ready( function() {

            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(drawChart);

      });

      </script>
    </div>
</div>
<script>
$(document).ready( function() {

    $('.getReservationsProfitData').click( function() {

        var year = $('.yearSelect option:selected').val();
        var month = $('.monthSelect option:selected').val();
        data = {};
        data.year = year;
        data.month = month;
        data._token = "{{ csrf_token() }}";

        $.ajaxSetup( { headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() } } );
        $.ajax({
            method: "POST",
            url: "{{route('admin.get.profits')}}",
            dataType: "json",
            data: data,
            success: function (response) {
                console.log(response);
                var array = [];

                drawChart(response.data);

                if (data.month == 0) {
                    $('#chart h2').html('Chart data array by MONTH:');
                    $.each(response.data, function(k, val) {
                        array += '<p> Month: '+val.month+' , Profit: '+val.profit+'</p>';
                    });
                } else {
                    $('#chart h2').html('Chart data array by DAY:');
                    $.each(response.data, function(k, val) {
                        array += '<p> Day: '+val.day+' , Profit: '+val.profit+'</p>';
                    });
                }

                $('.chartArrayHolder').html(array);
                $('span#total_profit').html("Profit: <strong>"+response.total[0].total_profit+" CHF</strong>");

            },
            error: function (jqXHR, exception) {
                //console.log(jqXHR);
            },
        });

    });

    $('.yearSelect').change( function() {

        var year = $(this).val();
        data = {};
        data.year = year;
        data._token = "{{ csrf_token() }}";

        $.ajaxSetup( { headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() } } );
        $.ajax({
            method: "POST",
            url: "{{ route('admin.get.profit.months') }}",
            dataType: "json",
            data: data,
            success: function (response) {

                var options = [];

                $.each(response, function(k, v) {
                    options += '<option value="'+k+'">'+v+'</option>';
                });

                $('.monthSelect').html(options);

            },
            error: function (jqXHR, exception) {
                //console.log(jqXHR);
            },
        });

    });

    $('.monthSelect').change( function() {
        $('.getReservationsProfitData').trigger('click');
    });


});

// var object = {}
// function setData(data) {
//     object.data = data;
// }

function drawChart(data) {

    if (data) {
        var fullData = data;
    } else {
        var fullData = {!! json_encode($data['profitChartArray']) !!};
    }

    console.log(fullData);

    var dataTable = new google.visualization.DataTable();

    if (fullData[0].day) {
        dataTable.addColumn('number', 'Year');
        dataTable.addColumn('number', 'Profit');
        dataTable.addColumn({type: 'string', role: 'tooltip'});
    } else {
        dataTable.addColumn('string', 'Year');
        dataTable.addColumn('number', 'Profit');
        dataTable.addColumn({type: 'string', role: 'tooltip'});
    }

    for (var i = 0; i < fullData.length; i++) {

        if (fullData[i].day) {
            dataTable.addRows([
                [fullData[i].day, Math.round(fullData[i].profit),"Your profit is "+fullData[i].profit],
            ]);
        }
        else {
            var monthEng = [
                "","January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
            ]
            dataTable.addRows([
                [monthEng[fullData[i].month], Math.round(fullData[i].profit),"Your profit is "+fullData[i].profit],
            ]);
        }
        // console.log(monthEng);
        if (fullData[i].day) {
          var title = 'Days of month';
          var options = {
            colors: ['#bf1e2d'],
            height: 500,
            tooltip: {isHtml: true},
            // legend: 'top',
            hAxis: {
                title: title,
              },
            vAxis: {
               title: 'Profit level'
             }
          };
        }
        else {
          var title = 'Months of year';
          var options = {
            colors: ['#bf1e2d'],
            height: 500,
            tooltip: {isHtml: true},
            // legend: 'none',
            hAxis: {
                title: title,
                slantedText:true,
                slantedTextAngle:90
              },
            vAxis: {
             title: 'Profit level'
           }
          };
        }
    }
    var chart = new google.visualization.ColumnChart(document.getElementById('col_chart_html_tooltip'));
    chart.draw(dataTable, options);

}
</script>
@endsection
