@extends('layouts.app_admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="box-header with-border">
                <h3 class="box-title">Faq</h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box-body table-responsive no-padding">
                <div class="">
                    <div class="table-toolbar">
                        <a class="btn btn-warning" href="{{ route('admin.create.faq') }}">Create <i class="fa fa-plus"></i></a>
                    </div>
                    <br>
                    <table class="table table-hover" id="faq-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Question</th>
                                <th>Answer</th>
                                <th>Operations</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach( $faqData as $k => $faq)
                            <tr>
                                <td width="5%">{!! $faq->id !!}</td>
                                <td width="40%">
                                    <p>{!! $faq->question !!}</p>
                                </td>
                                <td width="40%" >
                                    <p>{!! $faq->answer !!}</p>
                                </td>
                                <td width="15%">
                                    <a href="{{ route('admin.edit.faq', [$faq->id]) }}" class="btn btn-primary btn-block btn-xs"><i class="glyphicon glyphicon-edit"></i>&nbsp;&nbsp; Edit</a>
                                    <form class="pull-left" method="POST" action="{{ route('admin.destroy.faq', [$faq->id]) }}" accept-charset="UTF-8">
                                        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                        <input name="_method" type="hidden" value="DELETE">
                                        <button class="btn btn-sm red" type="submit"><i class="fa fa-trash-o"></i> DELETE</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">

</script>
@endpush
