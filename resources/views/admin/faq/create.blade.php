@extends('layouts.app_admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Faq Administration.</div>
                <div class="box-body">
                    <div id="tabs">
                        <ul>
                        @foreach ($supportedLocales as $k => $locale)
                            <li><a href="#tabs-{{$locale}}">{{$locale}}</a></li>
                        @endforeach
                        </ul>
                        {!! Form::open( array('action' => 'Admin\FaqController@store', 'method' => 'POST') ) !!}
                            @foreach ($supportedLocales as $lang => $locale)
                                <div id="tabs-{{$locale}}">
                                    <div class="form-group">
                                        {!! Form::label('question', 'Question:') !!}
                                        {!! Form::input('text', "translations[{$lang}][question]", null, array('required', 'class'=>'form-control', 'placeholder'=>'Question')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('answer', 'Answer:') !!}
                                        {!! Form::input('text', "translations[{$lang}][answer]", null, array('required', 'class'=>'form-control', 'placeholder'=>'Answer')) !!}
                                    </div>
                                </div>
                            @endforeach
                            <div class="form-group">
                                {!! Form::label('group_id', 'Group:') !!}
                                {!! Form::select('group_id', $groups['fr'], null, array('required', 'class'=>'form-control')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Save', array('class'=>'btn btn-primary')) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
$(function() {
    $("#tabs").tabs();
});
</script>
@endpush