@extends('layouts.app_admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">New reservation</div>
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="panel-body">
                    {!! Form::open( array('action' => array('Admin\ReservationController@postNewAdminReservation'), 'method' => 'POST') ) !!}
                        <div class="row">
                            <div class="col-md-12">
                                <h2 style="text-align:center"> Client data </h2>
                                <hr style="border-top: 1px solid black;">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('first_name', 'Client first name:') !!}
                                    {!! Form::input('text', 'first_name', null, array('required', 'class'=>'form-control', 'placeholder'=>'Client First Name')) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('last_name', 'Client last name:') !!}
                                    {!! Form::input('text', 'last_name', null, array('required', 'class'=>'form-control', 'placeholder'=>'Client Last Name')) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('company_name', 'Company name:') !!}
                                    {!! Form::input('text', 'company_name', null, array('required', 'class'=>'form-control', 'placeholder'=>'Company name')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('city', 'Client city:') !!}
                                    {!! Form::input('text', 'city', null, array('required', 'class'=>'form-control', 'placeholder'=>'Client city')) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('postcode', 'Client postcode:') !!}
                                    {!! Form::input('text', 'postcode', null, array('required', 'class'=>'form-control', 'placeholder'=>'Client postcode')) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('description', 'Company description:') !!}
                                    {!! Form::input('text', 'description', null, array('required', 'class'=>'form-control', 'placeholder'=>'Client description')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label('email', 'Client e-mail:') !!}
                                    {!! Form::input('text', 'email', null, array('required', 'class'=>'form-control', 'placeholder'=>'Client Email')) !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label('phone', 'Client phone:') !!}
                                    {!! Form::input('text', 'phone', null, array('required', 'class'=>'form-control', 'placeholder'=>'Client phone')) !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label('adress', 'Client address 1:') !!}
                                    {!! Form::input('text', 'adress', null, array('required', 'class'=>'form-control', 'placeholder'=>'Adress 1')) !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label('adress2', 'Client address 2:') !!}
                                    {!! Form::input('text', 'adress2', null, array('required', 'class'=>'form-control', 'placeholder'=>'Adress 2')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label('car_brand', 'Client car_brand:') !!}
                                    {!! Form::input('text', 'car_brand', null, array('required', 'class'=>'form-control', 'placeholder'=>'Client car_brand')) !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label('car_model', 'Client car_model:') !!}
                                    {!! Form::input('text', 'car_model', null, array('required', 'class'=>'form-control', 'placeholder'=>'Client car_model')) !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label('car_registration_number', 'car_registration_number:') !!}
                                    {!! Form::input('text', 'car_registration_number', null, array('required', 'class'=>'form-control', 'placeholder'=>'car_registration_number')) !!}
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    {!! Form::label('flight_number', 'flight_number:') !!}
                                    {!! Form::input('text', 'flight_number', null, array('required', 'class'=>'form-control', 'placeholder'=>'flight_number')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h2 style="text-align:center"> Reservation data </h2>
                                <hr style="border-top: 1px solid black;">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('date_of_arrival', 'Date arrival:') !!}
                                    {!! Form::input('text', 'date_of_arrival', null, ['class' => 'form-control adminDatepicker']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('hour_of_arrival', 'Hour of arrival:') !!}
                                    {!! Form::select('hour_of_arrival', Config::get('select_hours'), null, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('hour_of_arrival', 'Minute of arrival:') !!}
                                    {!! Form::select('minute_of_arrival', Config::get('select_minutes'), null, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('date_of_departure', 'Date departure:') !!}
                                    {!! Form::input('text', 'date_of_departure', null, ['class' => 'form-control adminDatepicker']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('hour_of_departure', 'Hour of departure:') !!}
                                    {!! Form::select('hour_of_departure', Config::get('select_hours'), null, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('minute_of_departure', 'Minute of departure:') !!}
                                    {!! Form::select('minute_of_departure', Config::get('select_minutes'), null, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    {!! Form::label('reservation_type', 'Reservation Type:') !!}
                                    {!! Form::select('reservation_type', array('valet'=>'Privillege Parking', 'navet'=>'Self-Parking'), null, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::submit('create') !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function() {
    
});
</script>

@endsection
