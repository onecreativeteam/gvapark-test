@extends('layouts.app_admin')

@section('content')
<div class="">
    <div class="">
        <div class="col-md-12">
            <div class="box-header with-border">
                <h3 class="box-title">Reservations List</h3>
            </div>
        </div>
        <div class="col-md-12">
            <div class="box-header with-border">
                <a href="{!! route('admin.new.reservation') !!}" class="btn btn-success btn-sm">New reservation</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box-body responsive table-responsive">
                <table class="table table-bordered table-responsive no-padding" id="reservations-table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Reservation at:</th>
                            <th>Arrival at:</th>
                            <th>Departure at:</th>
                            <th>Days to stay:</th>
                            <th>Status:</th>
                            <th>Type:</th>
                            <th>Tax:</th>
                            <th>New Tax:</th>
                            <th>Image:</th>
                            <th>Cancel?</th>
                            <th>Operations:</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
$(function() {
    $('#reservations-table').DataTable({
        processing: true,
        serverSide: true,
        paging: true,
        pageLength: 15,
        ajax: "{!! route('admin.datatables.reservations') !!}",
        order: [[0, 'desc']],
        columns: [
            { data: 'id', name: 'id', searchable: true },
            { data: 'client_first_name', name: 'client_first_name', searchable: true },
            { data: 'client_last_name', name: 'client_last_name', searchable: true },
            { data: 'date_of_reservation', name: 'date_of_reservation', searchable: true },
            { data: 'date_of_arrival', name: 'date_of_arrival', searchable: true },
            { data: 'date_of_departure', name: 'date_of_departure', searchable: true },
            { data: 'days_to_stay', name: 'days_to_stay', searchable: false },
            { data: 'payment_status', name: 'payment_status', searchable: true },
            { data: 'reservation_type', name: 'reservation_type', searchable: false },
            { data: 'price_to_charge', name: 'price_to_charge', searchable: true },
            { data: 'changed_price_to_charge', name: 'changed_price_to_charge', searchable: true },
            { data: 'image', name: 'image', searchable: false },
            { data: 'cancel', name: 'cancel', searchable: false },
            { data: 'operations', name: 'operations', searchable: false },
        ]
    });
});

$(document).ready(function(){
    jQuery('.cancelBtn').click(function(event){
        if(!confirm('Are You Sure?')){
            event.preventDefault();
        }
    })
});
</script>
@endpush