@extends('layouts.app_admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Edit reservation</div>
                @if (Auth::user()->role === 'admin')
                <div class="box-header with-border" style="float:right">
                    {!! Form::open( array('action' => array('Admin\ReservationController@deleteReservation', $data->id), 'method' => 'POST') ) !!}
                    {!! Form::submit('Delete', array('class'=>'btn btn-danger btn-md deleteReservationButton', 'onClick'=>'alert("Are u sure u want to delete?")')) !!}
                    {!! Form::close() !!}
                </div>
                @endif
                <div class="panel-body">
                    {!! Form::open( array('action' => array('Admin\ReservationController@updateReservation', $data->id), 'method' => 'PATCH') ) !!}
                        <div class="form-group">
                            {!! Form::label('client_first_name', 'Client First Name:') !!}
                            {!! Form::input('text', 'client_first_name', $data->client_first_name, array('required', 'class'=>'form-control', 'placeholder'=>'Client First Name')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('client_last_name', 'Client Last Name:') !!}
                            {!! Form::input('text', 'client_last_name', $data->client_last_name, array('required', 'class'=>'form-control', 'placeholder'=>'Client Last Name')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('email', 'Client Email:') !!}
                            {!! Form::input('text', 'email', $data->email, array('required', 'class'=>'form-control', 'placeholder'=>'Client Email')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('date_of_arrival', 'Date arrival:') !!}
                            {!! Form::input('text', 'date_of_arrival', isset($data->date_of_arrival) ? $data->date_of_arrival : '', ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('date_of_departure', 'Date departure:') !!}
                            {!! Form::input('text', 'date_of_departure', isset($data->date_of_departure) ? $data->date_of_departure : '', ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('reservation_type', 'Reservation Type:') !!}
                            {!! Form::select('reservation_type', array('valet'=>'Privillege Parking', 'navet'=>'Self-Parking'), @$data->reservation_type, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('payment_status', 'STATUS:') !!}
                            {!! Form::select('payment_status', $statusesSelect, $data->payment_status, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('options', 'Options:') !!}
                            {!! Form::select('options', $optionsSelect, $data->reservation_option, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('price_to_charge', 'Price:') !!}
                            {!! Form::input('text', 'price_to_charge_admin', $data->price_to_charge, array('required', 'class'=>'form-control', 'placeholder'=>'Price:')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('use_admin_price', 'Use admin price:') !!}
                            {!! Form::checkbox('use_admin_price', null, null, array('id'=>'adminPrice')) !!}
                        </div>
                        <div class="alert alert-warning" role="alert">
                            <h4><strong>NOTE:</strong></h4>
                            <p>Your price will be re-calculated after submitting form!</p>
                            <p>If u check "Use admin price", the price will be used instead.</p>
                        </div>
                        <hr/>
                        <div class="form-group">
                            {!! Form::label('car_brand', 'Client Car Brand:') !!}
                            {!! Form::input('text', 'car_brand', @$client->car_brand, array('required', 'class'=>'form-control', 'placeholder'=>'Client Car Brand')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('car_model', 'Client Car Model:') !!}
                            {!! Form::input('text', 'car_model', @$client->car_model, array('required', 'class'=>'form-control', 'placeholder'=>'Client Car Model')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('car_registration_number', 'Client Car Registraion Number:') !!}
                            {!! Form::input('text', 'car_registration_number', @$client->car_registration_number, array('required', 'class'=>'form-control', 'placeholder'=>'Client Car Registration Number')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('flight_number', 'Client Car Flight Number:') !!}
                            {!! Form::input('text', 'flight_number', @$client->flight_number, array('required', 'class'=>'form-control', 'placeholder'=>'Client Car Flight Number')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('update') !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
