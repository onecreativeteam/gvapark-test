@extends('layouts.app_admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Promo Codes Administration.</div>
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <div class="panel-body">
                    {!! Form::open( array('action' => 'Admin\DatePricesController@store', 'method' => 'POST') ) !!}
                        <div class="form-group">
                            {!! Form::label('date_from', 'From date:') !!}
                            {!! Form::input('text', 'date_from', null, array('required', 'class'=>'form-control datepicker-promo', 'placeholder'=>'')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('date_to', 'To date:') !!}
                            {!! Form::input('text', 'date_to', null, array('required', 'class'=>'form-control datepicker-promo', 'placeholder'=>'')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('price', 'Price:') !!}
                            {!! Form::input('text', 'price', null, array('required', 'class'=>'form-control', 'placeholder'=>'-.- CHF')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('type', 'Reservation Type:') !!}
                            {!! Form::select('type', array('valet'=>'Privillege Parking', 'navet'=>'Self-Parking'), null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Save') !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
