@extends('layouts.app_admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="box-header with-border">
                <h3 class="box-title">Date Prices</h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box-body table-responsive no-padding">
                <div class="panel-body">
                    <div class="table-toolbar">
                        <a class="btn btn-warning" href="{{ route('admin.create.date.prices') }}">Create <i class="fa fa-plus"></i></a>
                    </div>
                    <br>
                    <table class="table table-hover" id="date-prices-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>DateFrom</th>
                                <th>DateTo</th>
                                <th>Price</th>
                                <th>Type</th>
                                <th>Operations</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
$(function() {
    $('#date-prices-table').DataTable({
        processing: true,
        serverSide: true,
        paging: true,
        pageLength: 15,
        ajax: "{!! route('admin.datatables.date.prices') !!}",
        order: [[0, 'desc']],
        columns: [
            { data: 'id', name: 'id', searchable: true },
            { data: 'date_from', name: 'date_from', searchable: true },
            { data: 'date_to', name: 'date_to', searchable: true },
            { data: 'price', name: 'price', searchable: true },
            { data: 'type', name: 'type', searchable: true },
            { data: 'operations', name: 'operations', searchable: false },
        ]
    });
});
</script>
@endpush
