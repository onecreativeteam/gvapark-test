@extends('layouts.app_admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Reports Administration.</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                {!! Form::open( array( 'action' => 'Admin\ReportsController@reservationsProfitExcelExport', 'id' => 'reservationsProfitExcelExport' ) ) !!}
                                    <div class="form-group">
                                        {!! Form::label('start_date', 'start date:') !!}
                                        {!! Form::input('text', 'start_date', null, array('required', 'class'=>'form-control datepicker-promo', 'placeholder'=>'start date')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('end_date', 'end date:') !!}
                                        {!! Form::input('text', 'end_date', null, array('required', 'class'=>'form-control datepicker-promo', 'placeholder'=>'end date')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::submit('Download reservation profits excel.') !!}
                                    </div>
                                {!! Form::close() !!}
                            </div>

                            <div class="col-md-6">
                                {!! Form::open( array( 'action' => 'Admin\ReportsController@reservationsExcelExport', 'id' => 'reservationsExcelExport' ) ) !!}
                                    <div class="form-group" style="float:right">
                                        {!! Form::submit('Download reservation list excel.') !!}
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
