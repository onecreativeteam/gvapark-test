@extends('layouts.app_admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="box-header with-border">
                <h3 class="box-title">Reservation Additions</h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box-body table-responsive no-padding">
                <div class="">
                    <div class="table-toolbar">
                        <a class="btn btn-warning" href="{{ route('admin.create.additions') }}">Create <i class="fa fa-plus"></i></a>
                    </div>
                    <br>
                    <table class="table table-hover" id="additions-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Operations</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach( $additions as $k => $addition)
                            <tr>
                                <td>{!! $addition->id !!}</td>
                                <td>
                                    <p>{!! $addition->name !!}</p>
                                </td>
                                <td>
                                    <p>{!! $addition->price !!}</p>
                                </td>
                                <td>
                                    <a href="{{ route('admin.edit.additions', [$addition->id]) }}" class="btn btn-primary btn-block btn-xs"><i class="glyphicon glyphicon-edit"></i>&nbsp;&nbsp; Edit</a>
                                    <form class="pull-left" method="POST" action="{{ route('admin.destroy.additions', [$addition->id]) }}" accept-charset="UTF-8">
                                        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                        <input name="_method" type="hidden" value="DELETE">
                                        <button class="btn btn-sm red" type="submit"><i class="fa fa-trash-o"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
$(function() {

});
</script>
@endpush
