@extends('layouts.app_admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Additions Administration.</div>
                <div class="box-body">
                    <div id="tabs">
                        <ul>
                        @foreach ($supportedLocales as $k => $locale)
                            <li><a href="#tabs-{{$locale}}">{{$locale}}</a></li>
                        @endforeach
                        </ul>
                        {!! Form::open( array('action' => 'Admin\ReservationAdditionsController@store', 'method' => 'POST') ) !!}
                            @foreach ($supportedLocales as $lang => $locale)
                                <div id="tabs-{{$locale}}">
                                    <div class="form-group">
                                        {!! Form::label('name', 'Name:') !!}
                                        {!! Form::input('text', "translations[{$lang}][name]", null, array('required', 'class'=>'form-control', 'placeholder'=>'Name')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('visual_price', 'Visual Price:') !!}
                                        {!! Form::input('text', "translations[{$lang}][visual_price]", null, array('required', 'class'=>'form-control', 'placeholder'=>'Name')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('description', 'Description:') !!}
                                        {!! Form::textarea("translations[{$lang}][description]", null, array('required', 'class'=>'form-control', 'placeholder'=>'Description')) !!}
                                    </div>
                                </div>
                            @endforeach
                            <div class="form-group">
                                {!! Form::label('price', 'Price CHF') !!}
                                {!! Form::input('text', 'price', null, ['class'=>'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Save', array('class'=>'btn btn-primary')) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
$(function() {
    $("#tabs").tabs();
});
</script>
@endpush