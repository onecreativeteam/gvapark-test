﻿@extends('layouts.app_admin')

@section('content')
<div class="container container-calendar">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                        </ul>
                    </div>
                @endif
                <div class="panel-heading">Calendar.</div>
                <div class="panel-heading counter">Parking Free Spots for the day: <p class="counter"><strong>{{$nonOccupiedParking}}</strong></p></div>
                <div class="panel-heading counter2">Reservations in system for the day: <p class="counter"><strong>{{$countParking}}</strong></p></div>
                <div class="panel-body">
                    {!! Form::hidden('calendar_type', $type) !!}
                    <div id="calendar"></div>
                    {!! Form::open( array( 'action' => 'Admin\ReservationController@setArrived', 'id' => 'formArrived2', 'files' => true ) ) !!}
                        {!! Form::hidden('obj_id') !!}
                        {!! Form::hidden('file') !!}
                    {!! Form::close() !!}

                    {!! Form::open( array( 'action' => 'Admin\ReservationController@changeReservationDates', 'id' => 'formChangeReservationDates' ) ) !!}
                        {!! Form::hidden('cd_obj_id') !!}
                        {!! Form::hidden('obj_start_date') !!}
                        {!! Form::hidden('obj_end_date') !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
@if(!empty($data))
    calendar = <?php echo json_encode($data); ?>;
@endif

var imageFileCollection = [];

$(document).ready(function() {

    function parkPlaceDisplayName(reservation) {

        if (reservation.park_place !== null) {
            return reservation.park_place.name;
        }

        return '';
    }

    var pictureRoute = "{{ route('admin.reservations.gallery', ['replacePictureRoute']) }}";
    calendarDataNew = [];

    /* ============================================================================================= */
    // loop elements to add modal
    for (var i = 0; i < calendar.length; i++) {

        var _obj = {};

        if ((calendar[i].reservation_option != null) && (calendar[i].reservation_option != 'null') && (calendar[i].reservation_option != '')) {
            // if ((calendar[i].reservation_option != 'null') && (calendar[i].reservation_option != '')) {
                if (calendar[i].reservation_option == 'extra') {
                    var optionAdded = 'IE - ';
                    var optionAddDisplay = 'intérieure & extérieur';
                } else if (calendar[i].reservation_option == 'intereur') {
                    var optionAdded = 'P - ';
                    var optionAddDisplay = 'Premium';
                    // var optionAdded = 'I - ';
                    // var optionAddDisplay = calendar[i].reservation_option;
                } else if (calendar[i].reservation_option == 'extereur') {
                    var optionAdded = 'B - ';
                    var optionAddDisplay = 'Basic';
                    // var optionAdded = 'E - ';
                    // var optionAddDisplay = calendar[i].reservation_option;
                }
            // }
        } else {
            var optionAdded = 'N - ';
            var optionAddDisplay = 'None';
        }
        
        // 
        if (calendar[i].park_place != null) {
            var parkPlaceName = calendar[i].park_place.name;
        } else {
            var parkPlaceName = 'No';
        }
        // 

        if (calendar[i].has_electric == 1) {
            var electricOption = '+E';
        } else {
            var electricOption = '';
        }

        if (calendar[i].user != null) {
            var flightNumber = calendar[i].user.flight_number;
            var registrationNumber = calendar[i].user.car_registration_number;
        } else {
            var flightNumber = '';
            var registrationNumber = '';
        }

        if (calendar[i].is_cloned) {
            _obj.title = optionAdded+calendar[i].id+" : P - "+parkPlaceName+" : "+calendar[i].client_first_name+" "+calendar[i].client_last_name+" : "+calendar[i].date_of_departure+" : FL : "+flightNumber+" : REG : "+registrationNumber;
        } else {
            _obj.title = optionAdded+calendar[i].id+" : P - "+parkPlaceName+" : "+calendar[i].client_first_name+" "+calendar[i].client_last_name+" : "+calendar[i].date_of_arrival+" : REG : "+registrationNumber;
        }
        _obj.start = calendar[i].start_date;
        _obj.id = calendar[i].id;

        if (calendar[i].has_gallery == 1) {
            var picture = '<p style="float:right"><a href='+pictureRoute.replace("replacePictureRoute", calendar[i].id)+' target="_blank" class="btn btn-info btn-xs">view</a></p>';
        } else {
            var picture = "";
        }

        if (calendar[i].is_arrived == 1) {
            var arrivedByUser = "<p> <strong>User marked as arrived: </strong>" + calendar[i].arrivedByUserName +" at: "+ calendar[i].arrived_at + "</p>";
        } else {
            var arrivedByUser = "";
        }

        if (calendar[i].is_returned == 0) {
            var returnedForm = "<p style='float:right'><a href='/{{ app()->getLocale() }}/admin/calendar/return-car/"+calendar[i].id+"' class='btn btn-info btn-md'>RETURN</a></p>";
            var returnedByUser = "";
        } else {
            var returnedForm = "";
            var returnedByUser = "<p> <strong>User marked as returned: </strong>" + calendar[i].returnedByUserName +" at: "+ calendar[i].returned_at + "</p>";
        }

        if (calendar[i].is_arrived == 1) {
            var form_open = '{!! Form::open( array( "action" => "Admin\ReservationController@setArrived", "id" => "formArrived", "files" => true ) ) !!}';
            // {{--var form_submit = '{!! Form::submit("Update") !!}';--}}
            var form_submit = "<input type='hidden' name='reservation_id' id='reservation_id' value="+calendar[i].id+"><button class='btn set-arrived-button' type=button>Update</button>";
            var form_close = '{!! Form::close() !!}';
            var input = "<p> <strong>Picture: </strong><input name='pictures[]' onChange='setFiles(this, event)' id='pictures' type='file' multiple='true'></p>";
        }
        else if (calendar[i].is_arrived == 0 )  {
            var form_open = '{!! Form::open( array( "action" => "Admin\ReservationController@setArrived", "id" => "formArrived", "files" => true ) ) !!}';
            // {{--var form_submit = '{!! Form::submit("Arrived") !!}';--}}
            var form_submit = "<input type='hidden' name='reservation_id' id='reservation_id' value="+calendar[i].id+"><button class='btn set-arrived-button' type=button>Arrived</button>";
            var form_close = '{!! Form::close() !!}';
            var input = "<p> <strong>Picture: </strong><input name='pictures[]' onChange='setFiles(this, event)' id='pictures' type='file' multiple='true'></p>";
        }

        _obj.description = 'Reservation not found.';
        
        if (calendar[i].user != null) {
            _obj.description =
                picture +
                form_open +
                "<input name='obj_id' type='hidden' value="+calendar[i].id+">" +
                arrivedByUser +
                returnedByUser +
                "<p> <strong>Date of reservation: </strong>" + calendar[i].date_of_reservation + "</p>" +
                "<p> <strong>First Name: </strong>" + calendar[i].client_first_name + "</p>" +
                "<p> <strong>Last Name: </strong>" + calendar[i].client_last_name + "</p>" +
                "<p> <strong>Car manufacturer: </strong>" + calendar[i].user.car_brand + "</p>" +
                "<p> <strong>Car model: </strong>" + calendar[i].user.car_model + "</p>" +
                "<p> <strong>Car reg.number: </strong>" + calendar[i].user.car_registration_number + "</p>" +
                "<p> <strong>Car fl.number: </strong>" + calendar[i].user.flight_number + "</p>" +
                "<p> <strong>Date of arrival: </strong>" + calendar[i].date_of_arrival + "</p>" +
                "<p> <strong>Date of departure: </strong>" + calendar[i].date_of_departure + "</p>" +
                "<p> <strong>Days to stay: </strong>" + calendar[i].days_to_stay + "</p>" +
                "<p> <strong>E-mail: </strong>" + calendar[i].email + "</p>" +
                "<p> <strong>Added option: </strong>" + optionAddDisplay+electricOption + "</p>" +
                "<p> <strong>Price to charge: </strong>" + calendar[i].price_to_charge + "</p>" +
                "<p> <strong>Park place: </strong><input name='park_place' type='text' value='" + parkPlaceDisplayName(calendar[i]) + "'></p>" +
                input +
                form_submit +
                form_close +
                returnedForm +
                "@if (Auth::user()->role === 'admin')" +
                '{!! Form::open( array("action" => array("Admin\ReservationController@deleteReservation", "'+calendar[i].id+'"), "method" => "POST") ) !!}' +
                '<input type="hidden" name="id" value='+calendar[i].id+'>' +
                '{!! Form::submit("Delete", array("class"=>"btn btn-danger btn-md", "style"=>"margin:10px 0px")) !!}' +
                '{!! Form::close() !!}' +
                "@endif"
                ;
        }


        if( new Date(calendar[i].date_of_arrival).setHours(0,0,0,0) < new Date().setHours(0,0,0,0) ) {

            _obj.color = 'gray';

            if(calendar[i].is_arrived=="0") {
                _obj.color = 'gray';
            }

        }else{

            if(calendar[i].is_arrived=="1"){
                _obj.color = 'green';
            }else{
                _obj.color = '#3a87ad';
            }

        }

        if (calendar[i].is_cloned) {
          _obj.color = 'red';
        }

        if (calendar[i].is_returned == "1") {
          _obj.color = '#f4ad42';
        }

        calendarDataNew[i] = _obj;
    }
    // END loop elements to add modal
    /* ============================================================================================= */

    initCalendar();
});

function setFiles(el, event) {
    var files = event.target.files;

    $.each(files, function(i, file) {
        imageFileCollection.push(file);
    });
}

function getFiles() {
    return imageFileCollection;
}

function initCalendar() {

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        },
        defaultDate: new Date(),
        navLinks: true, // can click day/week names to navigate views
        eventLimit: true, // allow "more" link when too many events
        displayEventTime: false,
        events: calendarDataNew,
        editable: true,
        eventClick: function(event, jsEvent, view) {
            $('#modalTitle').empty();
            $('#modalBody').empty();
            $('#modalTitle').append(event.title);
            $('#modalBody').append(event.description);
            $('#fullCalModal').modal();
            //parkPlace
            $('.set-arrived-button').click(function () {
                let form = $(this).parent('form');
                ajaxUpdateReservation(form.serialize(), event);
            });
        },
        eventDrop: function(event, delta, revertFunc) {
            var endDateReservation;
            if(event.end==null){
                endDateReservation=event.start.format()
            }else{
                endDateReservation=event.end.format()
            }
            changeReservationDates(event.id, event.start.format(), endDateReservation);
        },
        dayClick: function(date, jsEvent, view) {
            ajaxGetParkingCount(date.format());
        }
    });

}

// function updateArrived(obj_id) {

//     $('#formArrived input[name="obj_id"]').val(obj_id);

//     var r = confirm("Are you sure u want to update this reservation?");

//     if (r == true) {
//         $('#formArrived').submit();
//     }
// }

function changeReservationDates(id, startDate, endDate) {
    $('#formChangeReservationDates input[name="cd_obj_id"]').val(id);
    $('#formChangeReservationDates input[name="obj_start_date"]').val(startDate);
    $('#formChangeReservationDates input[name="obj_end_date"]').val(endDate);

    var r = confirm("Are you sure you want to change reservation dates ?");

    if (r == true) {
        $('#formChangeReservationDates').submit();
    }
}

function ajaxGetParkingCount(date) {
    var date = date;
    var type = $('input[name="calendar_type"]').val();
    var data = {};
    data.date = date;
    data.type = type;
    $.ajaxSetup( { headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() } } );
    $.ajax({
        method: "POST",
        url: "{{ route('admin.get.parking.count') }}",
        dataType: "json",
        data: data,
        success: function (data) {
            $('.panel-heading.counter p.counter').html('<strong>'+data.free_spots+'</strong>');
            $('.panel-heading.counter2 p.counter').html('<strong>'+data.reservations+'</strong>');
            $('#avableParkingSlots .modal-body .free-spots').html('<p>Free Parking Slots</p><strong>'+data.free_spots+'</strong>');
            $('#avableParkingSlots .modal-body .reservations').html('<p>Reservations for the day</p><strong>'+data.reservations+'</strong>');
            $('#avableParkingSlots').modal();
        }
    });
}

function ajaxUpdateReservation(data, event) {

    var data = new FormData();

    jQuery.each(getFiles(), function(i, file) {
        data.append('file-'+i, file);
    });
    data.append('obj_id', $('#reservation_id').val());
    data.append('park_place', $('input[name="park_place"]').val());

    $.ajaxSetup( { headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() } } );
    $.ajax({
        url: "{{ route('admin.calendar.set.arrived') }}",
        type: 'POST',
        method: "POST",
        cache: false,
        processData: false,
        contentType: false,
        data: data,
        success: function (data) {
            swal("Done!", "You successfully updated this reservation!", "success");

            if( new Date(data.date_of_arrival).setHours(0,0,0,0) < new Date().setHours(0,0,0,0) ){
                event.color = 'gray';
                if(data.is_arrived == "0"){
                    event.color = 'gray';
                }
            }else{
                if(data.is_arrived == true){
                    event.color = 'green';
                }else{
                    event.color = '#3a87ad';
                }

            }
            if (data.is_cloned) {
                event.color = 'red';
            }

            if (data.is_returned == "1") {
                event.color = '#f4ad42';
            }

            $('#calendar').fullCalendar( 'refetchEvents');
        },
        error: function (data) {
            var validationMessages = data.responseJSON;
            var sweetAlertMessage = "";

            for (var key in validationMessages) {
                // skip loop if the property is from prototype
                if (! validationMessages.hasOwnProperty(key)) continue;

                var messages = validationMessages[key];
                for (var index in messages) {
                    // skip loop if the indexerty is from prototype
                    if(! messages.hasOwnProperty(index)) continue;

                    sweetAlertMessage += messages[index] + "\n";
                }
            }

            swal("Fail!", sweetAlertMessage, "error");
        }
    });
}

</script>
<div id="fullCalModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">close</span>
                </button>
                <h4 id="modalTitle" class="modal-title"></h4>
            </div>
            <div id="modalBody" class="modal-body">

            </div>

        </div>
    </div>
</div>
<div id="avableParkingSlots" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">close</span>
                </button>
                <h4 class="modal-title">Day Parking Information</h4>
            </div>
            <div class="modal-body text-center">
                <div class="free-spots"></div>
                <div class="reservations"></div>
            </div>

        </div>
    </div>
</div>
@endsection
