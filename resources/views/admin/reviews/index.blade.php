@extends('layouts.app_admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Reviews.</div>
                <div class="panel-body" style="overflow-x: auto;">
                    <table class="table table-bordered" id="faq-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Message</th>
                                <th>Operations</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach( $data as $k => $review)
                            <tr>
                                <td width="5%">{!! $review->id !!}</td>
                                <td width="15%">
                                    <p>{!! $review->name !!}</p>
                                </td>
                                <td width="60%" >
                                    <p>{!! $review->message !!}</p>
                                </td>
                                <td width="10%">
                                    <form class="pull-left" method="POST" action="reviews/{!! $review->id !!}" accept-charset="UTF-8">
                                        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                        <input name="_method" type="hidden" value="PATCH">
                                        <button class="btn btn-sm red {!! ($review->is_approved == 1) ? 'btn-success' : '' !!}" {!! ($review->is_approved == 1) ? 'disabled=disabled' : '' !!} type="submit"><i class="fa fa-trash-o"></i> Approve</button>
                                    </form>
                                </td>
                                <td width="10%">
                                    <form class="pull-left" method="POST" action="reviews/delete/{!! $review->id !!}" accept-charset="UTF-8">
                                        <input type="hidden" name="_token" id="csrf-token2" value="{{ Session::token() }}" />
                                        <input name="_method" type="hidden" value="POST">
                                        <button class="btn btn-sm red btn-warning" type="submit"><i class="fa fa-trash-o"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {

});
</script>
@endsection
