@extends('layouts.app_admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Parking Service Administration.</div>
                <div class="panel-body">
                    <div class="box-body">
                        <div id="tabs">
                            <ul>
                            @foreach ($supportedLocales as $k => $locale)
                                <li><a href="#tabs-{{$locale}}">{{$locale}}</a></li>
                            @endforeach
                            </ul>
                            {!! Form::model($data, ['method'=>'POST', 'action'=>['Admin\ParkingServiceController@store']]) !!}
                                @foreach ($supportedLocales as $lang => $locale)
                                    <div id="tabs-{{$locale}}">
                                        <div class="form-group">
                                            {!! Form::label('main_info_title', 'Main Info Title:') !!}
                                            {!! Form::input('text', "translations[{$lang}][main_info_title]", isset($data->translations[$locale]) ? @$data->translations[$locale]->main_info_title : @$data->main_info_title) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('main_info_description', 'Main Info Description:') !!}
                                            {!! Form::textarea("translations[{$lang}][main_info_description]", isset($data->translations[$locale]) ? @$data->translations[$locale]->main_info_description : @$data->main_info_description, ['style'=>'width: 100%;']) !!}
                                        </div>

                                        <div class="form-group">
                                            {!! Form::label('online_booking_info_title', 'Online Booking Title:') !!}
                                            {!! Form::input('text', "translations[{$lang}][online_booking_info_title]", isset($data->translations[$locale]) ? @$data->translations[$locale]->online_booking_info_title : @$data->online_booking_info_title) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('online_booking_info_description', 'Online Booking Description:') !!}
                                            {!! Form::textarea("translations[{$lang}][online_booking_info_description]", isset($data->translations[$locale]) ? @$data->translations[$locale]->online_booking_info_description : @$data->online_booking_info_description,  ['style'=>'width: 100%;']) !!}
                                        </div>

                                        <div class="form-group">
                                            {!! Form::label('post_your_vehicle_info_title', 'Your Vehicle Title:') !!}
                                            {!! Form::input('text', "translations[{$lang}][post_your_vehicle_info_title]", isset($data->translations[$locale]) ? @$data->translations[$locale]->post_your_vehicle_info_title : @$data->post_your_vehicle_info_title) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('post_your_vehicle_info_description', 'Your Vehicle Description:') !!}
                                            {!! Form::textarea("translations[{$lang}][post_your_vehicle_info_description]", isset($data->translations[$locale]) ? @$data->translations[$locale]->post_your_vehicle_info_description : @$data->post_your_vehicle_info_description,  ['style'=>'width: 100%;']) !!}
                                        </div>

                                        <div class="form-group">
                                            {!! Form::label('travel_info_title', 'Travel Info Title:') !!}
                                            {!! Form::input('text', "translations[{$lang}][travel_info_title]", isset($data->translations[$locale]) ? @$data->translations[$locale]->travel_info_title : @$data->travel_info_title) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('travel_info_description', 'Travel Info Description:') !!}
                                            {!! Form::textarea("translations[{$lang}][travel_info_description]", isset($data->translations[$locale]) ? @$data->translations[$locale]->travel_info_description : @$data->travel_info_description,  ['style'=>'width: 100%;']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('pickup_info_title', 'Pickup Info Title:') !!}
                                            {!! Form::input('text', "translations[{$lang}][pickup_info_title]", isset($data->translations[$locale]) ? @$data->translations[$locale]->pickup_info_title : @$data->pickup_info_title) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('pickup_info_description', 'Pickup Info Description:') !!}
                                            {!! Form::textarea("translations[{$lang}][pickup_info_description]", isset($data->translations[$locale]) ? @$data->translations[$locale]->pickup_info_description : @$data->pickup_info_description,  ['style'=>'width: 100%;']) !!}
                                        </div>
                                    </div>
                                @endforeach
                                <div class="form-group">
                                    {!! Form::submit('Save', array('class'=>'btn btn-primary')) !!}
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
$(function() {
    $("#tabs").tabs();
});
</script>
@endpush