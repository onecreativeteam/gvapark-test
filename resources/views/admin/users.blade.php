@extends('layouts.app_admin')

@section('content')
<div class="container container-calendar">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Users</div>
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if ($user->role == 'admin')
                <div class="panel-body">
                    <div class="row">
                        <div class="panel-heading"><h2>{!! strtoupper(Auth::user()->name) !!}, you can change your password here.</h2></div>
                        <div class="col-md-12">
                            {!! Form::open( array( 'action' => 'AdminController@updatePassword' ) ) !!}
                                <div class="form-group">
                                    {!! Form::label('password', 'Password:') !!}
                                    {!! Form::input('password', 'password', null, array('required', 'class'=>'form-control', 'placeholder'=>'Password')) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('password_confirmation', 'Repeat Password:') !!}
                                    {!! Form::input('password', 'password_confirmation', null, array('required', 'class'=>'form-control', 'placeholder'=>'Confirm Password')) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::submit('Change Password') !!}
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <hr/>
                @endif
                <div class="panel-body">
                    <div class="row">
                        <div class="panel-heading"><h2>{!! strtoupper(Auth::user()->name) !!}, Create new ofice-admin user.</h2></div>
                        <div class="col-md-12">
                        {!! Form::open( array( 'action' => 'AdminController@createUser', 'method' => 'POST' ) ) !!}

                        <div class="form-group">
                            {!! Form::label('role', 'User Role:') !!}
                            {!! Form::select('role', $rolesSelect, null, ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('name', 'Name:') !!}
                            {!! Form::input('text', 'name', @old('name'), array('required', 'class'=>'form-control', 'placeholder'=>'Name')) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('email', 'Email:') !!}
                            {!! Form::input('email', 'email', @old('email'), array('required', 'class'=>'form-control', 'placeholder'=>'Email')) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('password', 'Password:') !!}
                            {!! Form::input('password', 'password', null, array('required', 'class'=>'form-control', 'placeholder'=>'Password')) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('password_confirmation', 'Repeat Password:') !!}
                            {!! Form::input('password', 'password_confirmation', null, array('required', 'class'=>'form-control', 'placeholder'=>'Confirm Password')) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::submit('Create') !!}
                        </div>
                        {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <hr/>
                <!-- <div class="panel-body"> -->
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box-body table-responsive no-padding">
                        <div class="panel-body">
                            <table class="table table-hover" id="users-list-table">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Role</th>
                                        <th>Operations:</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(count(@$users))
                                @foreach( @$users as $k => $user)
                                    <tr>
                                        <td>
                                            <p>{!! $user->id !!}</p>
                                        </td>
                                        <td>
                                            <p>{!! $user->name !!}</p>
                                        </td>
                                        <td>
                                            <p>
                                                @if ($user->role == 'worker')
                                                    Voiturier
                                                @elseif ($user->role == 'receptionist')
                                                    Receptionist
                                                @else
                                                    Administrator
                                                @endif
                                            </p>
                                        </td>
                                        <td>
                                            <a class="btn btn-warning btn-block btn-xs" href="{{ route('admin.users.edit', $user->id) }}">Edit User</a>
                                            <a class="btn btn-danger btn-block btn-xs" href="{{ route('admin.users.delete', $user->id) }}">Delete User</a>
                                        </td>
                                    </tr>
                                @endforeach
                                @endif
                            </tbody>
                            </table>

                        </div>
                    </div>
                    </div>
                </div>
                <!-- </div> -->
            </div>
        </div>
    </div>
</div>
<script>

</script>
@endsection
