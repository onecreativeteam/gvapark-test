@extends('layouts.app_admin')

@section('content')
<style>
  .line-error {
      color: red;
      padding: 10px;
  }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Reservation Administration.</div>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                        </ul>
                    </div>
                @endif
                <div class="panel-body">
                    {!! Form::model($prices, ['method'=>'POST', 'action'=>['Admin\ReservationPricesController@store']]) !!}
                        {!! Form::hidden('type', $type) !!}
                        @for ($i = 1; $i <= $pricesCount; $i++)
                            <div class="form-group col-md-2">
                                @if ($i == 1)
                                    {!! Form::label($i.' Day :') !!}
                                @else
                                    {!! Form::label($i.' Days :') !!}
                                @endif
                                {!! Form::text("prices[{$i}][price]", @$prices[$i-1]->price, array('required', 'class'=>'form-control', 'placeholder'=>'Day '.$i)) !!}
                            </div>
                        @endfor
                        <div class="form-group col-md-12">
                            {!! Form::submit('Save', array('class'=>'btn btn-primary')) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
