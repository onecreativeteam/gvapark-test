@extends('layouts.app_admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Promo Codes Administration.</div>

                <div class="panel-body">
                    {!! Form::open( array('action' => array('Admin\PromoCodesController@update', $data->id), 'method' => 'PATCH') ) !!}
                        <div class="form-group">
                            {!! Form::label('code', 'Code:') !!}
                            {!! Form::input('text', 'code', $data->code, array('required', 'class'=>'form-control', 'placeholder'=>'Code')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('promotion_value', 'Promotion Value %:') !!}
                            {!! Form::input('text', 'promotion_value', $data->promotion_value, array('required', 'class'=>'form-control', 'placeholder'=>'10')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('start_date', 'Code start date:') !!}
                            {!! Form::input('text', 'start_date', $data->start_date, array('required', 'class'=>'form-control datepicker-promo', 'placeholder'=>'Code start date')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('end_date', 'Code end date:') !!}
                            {!! Form::input('text', 'end_date', $data->end_date, array('required', 'class'=>'form-control datepicker-promo', 'placeholder'=>'Code end date')) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Save') !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
