@extends('layouts.app_admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="box-header with-border">
                <h3 class="box-title">Promo Codes</h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box-body table-responsive no-padding">
                <div class="panel-body">
                    <div class="table-toolbar">
                        <a class="btn btn-warning" href="{{ route('admin.create.promo') }}">Create <i class="fa fa-plus"></i></a>
                    </div>
                    <br>
                    <table class="table table-hover" id="promo-codes-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Code</th>
                                <th>Promotion</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Operations</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach( $promoCodes as $k => $promoCode)
                            <tr>
                                <td width="5%">{!! $promoCode->id !!}</td>
                                <td width="20%">
                                    <p>{!! $promoCode->code !!}</p>
                                </td>
                                <td width="20%" >
                                    <p>{!! $promoCode->promotion_value !!}</p>
                                </td>
                                <td width="20%" >
                                    <p>{!! $promoCode->start_date !!}</p>
                                </td>
                                <td width="20%" >
                                    <p>{!! $promoCode->end_date !!}</p>
                                </td>
                                <td width="15%">
                                    <a href="{!! route('admin.edit.promo', [$promoCode->id]) !!}" class="btn btn-primary btn-block btn-xs"><i class="glyphicon glyphicon-edit"></i>&nbsp;&nbsp; Edit</a>
                                    <form class="pull-left" method="POST" action="{{ route('admin.destroy.promo', [$promoCode->id]) }}" accept-charset="UTF-8">
                                        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                                        <input name="_method" type="hidden" value="DELETE">
                                        <button class="btn btn-sm red" type="submit"><i class="fa fa-trash-o"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
$(function() {

});
</script>
@endpush
