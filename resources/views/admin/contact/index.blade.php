@extends('layouts.app_admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Contact Administration.</div>

                <div class="panel-body">
                    {!! Form::model($data, ['method'=>'POST', 'action'=>['Admin\ContactController@store']]) !!}
                        <div class="form-group">
                            {!! Form::label('address1', 'Address 1:') !!}
                            {!! Form::input('text', 'address1', @$data->address1) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('address2', 'Address 2:') !!}
                            {!! Form::input('text', 'address2', @$data->address2) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('telephone', 'Telephone:') !!}
                            {!! Form::input('text', 'telephone', @$data->telephone) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('email', 'Email:') !!}
                            {!! Form::input('text', 'email', @$data->email) !!}
                        </div>
                        <hr>
                        <div class="form-group">
                            {!! Form::label('navet_address1', 'Navet Address 1:') !!}
                            {!! Form::input('text', 'navet_address1', @$data->navet_address1) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('navet_address2', 'Navet Address 2:') !!}
                            {!! Form::input('text', 'navet_address2', @$data->navet_address2) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('navet_telephone', 'Navet Telephone:') !!}
                            {!! Form::input('text', 'navet_telephone', @$data->navet_telephone) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('navet_email', 'Navet Email:') !!}
                            {!! Form::input('text', 'navet_email', @$data->navet_email) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Save') !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
