@extends('layouts.app_admin')

@section('content')
<style>
  .line-error {
      color: red;
      padding: 10px;
  }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">System Options Administration.</div>

                <div class="panel-body">
                    {!! Form::model($data, ['method'=>'POST', 'action'=>['Admin\SystemOptionsController@store']]) !!}
                        <div class="form-group">
                            {!! Form::label('parking_spots_count', 'Parking Spots:') !!}
                            {!! Form::input('text', 'parking_spots_count', @$data->parking_spots_count) !!}
                            @if ($errors->has('parking_spots_count'))
                                <span class="line-error">{{ $errors->first('parking_spots_count') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            {!! Form::label('valet_max_count', 'Privillege Parking Max Count per hour:') !!}
                            {!! Form::input('text', 'valet_max_count', @$data->valet_max_count) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('initial_price', 'Initial Price (CHF):') !!}
                            {!! Form::input('text', 'initial_price', @$data->initial_price) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('daily_price', 'Per Day Price (CHF):') !!}
                            {!! Form::input('text', 'daily_price', @$data->daily_price) !!}
                        </div>
                        <hr/>
                        <div class="form-group">
                            {!! Form::label('vip_initial_price', 'VIP Initial Price (CHF):') !!}
                            {!! Form::input('text', 'vip_initial_price', @$data->vip_initial_price) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('vip_daily_price', 'VIP Per Day Price (CHF):') !!}
                            {!! Form::input('text', 'vip_daily_price', @$data->vip_daily_price) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('reservation_have_options', 'Allow reservation options:') !!}
                            {!! Form::checkbox('reservation_have_options', null, @$data->reservation_have_options) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Nettoyage basique:') !!}
                            {!! Form::text('extereur_price', @$data->extereur_price) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Nettoyage premium:') !!}
                            {!! Form::text('intereur_price', @$data->intereur_price) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Nettoyage premium & basique:') !!}
                            {!! Form::text('extra_price', @$data->extra_price) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Electric Charge price:') !!}
                            {!! Form::text('electric_charge_price', @$data->electric_charge_price) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Save') !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
