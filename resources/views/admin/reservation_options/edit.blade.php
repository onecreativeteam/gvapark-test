@extends('layouts.app_admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Options Administration.</div>
                <div class="box-body">
                    <div id="tabs">
                        <ul>
                        @foreach ($supportedLocales as $k => $locale)
                            <li><a href="#tabs-{{$locale}}">{{$locale}}</a></li>
                        @endforeach
                        </ul>
                        {!! Form::open( array('action' => array('Admin\ReservationOptionsController@update', $option->id), 'method' => 'PUT', 'files' => 'true') ) !!}
                            @foreach ($supportedLocales as $lang => $locale)
                                <div id="tabs-{{$locale}}">
                                    <div class="form-group">
                                        {!! Form::label('name', 'Name:') !!}
                                        {!! Form::input('text', "translations[{$lang}][name]", @$option->name, array('required', 'class'=>'form-control', 'placeholder'=>'Name')) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('description', 'Description:') !!}
                                        {!! Form::textarea("translations[{$lang}][description]", @$option->description, array('required', 'class'=>'form-control', 'placeholder'=>'Description')) !!}
                                    </div>
                                </div>
                            @endforeach
                            <div class="form-group">
                                {!! Form::label('price', 'Price CHF') !!}
                                {!! Form::input('text', 'price', @$option->price, ['class'=>'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('type', 'Type') !!}
                                {!! Form::select('type', $optionTypes, @$option->type, ['class'=>'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('Logo') !!}
                                {!! Form::file('logo') !!}
                                @if (file_exists($option->logo))
                                <img src="/{{ $option->logo }}">
                                @endif
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Save', array('class'=>'btn btn-primary')) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
$(function() {
    $("#tabs").tabs();
});
</script>
@endpush