@extends('layouts.eli')
@section('content')
<div class="container-fluid page-content videos">
    <div class="container">
      <div class="row text-center">
          <h2 data-aos="zoom-in">@lang('messages.video_page_title')</h2>
      </div>
      <div class="row text-center">
          <h3 data-aos="zoom-in">@lang('messages.navet_title') @lang('messages.menu_videos')</h3>
      </div>
      <div class="row">
        <div class="col-sm-4 col-sm-offset-2">
          <h3 class="text-center">@lang('messages.navet_video_header1')</h3>
          <div class="play-pic">
            <div class="play-arrow">
            </div>
            <img src="/video/self-bardonnex.jpg" alt=""  data-toggle="modal" data-target="#video1" />
          </div>
          <div class="modal fade" id="video1" role="dialog">
            <div class="modal-dialog">
              <video  width="100%" controls  >
                <source src="/video/self-bardonnex.mp4" type="video/mp4">
                {{-- <source src="mov_bbb.ogg" type="video/ogg"> --}}
                Your browser does not support HTML5 video.
              </video>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <h3 class="text-center">@lang('messages.navet_video_header2')</h3>
          <div class="play-pic">
            <div class="play-arrow">
            </div>
          <img src="/video/self-lausanne.jpg" alt=""   data-toggle="modal" data-target="#video2"  />
        </div>
          <div class="modal fade" id="video2" role="dialog">
            <div class="modal-dialog">
              <video  width="100%" controls  >
                <source src="/video/self-lausanne.mp4" type="video/mp4">
                {{-- <source src="mov_bbb.ogg" type="video/ogg"> --}}
                Your browser does not support HTML5 video.
              </video>
            </div>
          </div>
        </div>
      </div>
    </div>
    <br>
    <br>
    <div class="container">
      <div class="row text-center">
          <h3 data-aos="zoom-in">@lang('messages.valet_title') @lang('messages.menu_videos')</h3>
      </div>
      <div class="row">
        <div class="col-sm-4">
          <h3 class="text-center">@lang('messages.video_page_header')</h3>
          <div class="play-pic">
            <div class="play-arrow">
            </div>
            <img src="/video/1.jpg" alt=""  data-toggle="modal" data-target="#video1" />
          </div>
          <div class="modal fade" id="video1" role="dialog">
            <div class="modal-dialog">
              <video  width="100%" controls  >
                <source src="/video/Avalanchets.mp4" type="video/mp4">
                {{-- <source src="mov_bbb.ogg" type="video/ogg"> --}}
                Your browser does not support HTML5 video.
              </video>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <h3 class="text-center">@lang('messages.video_page_header2')</h3>
          <div class="play-pic">
            <div class="play-arrow">
            </div>
          <img src="/video/2.jpg" alt=""   data-toggle="modal" data-target="#video2"  />
        </div>
          <div class="modal fade" id="video2" role="dialog">
            <div class="modal-dialog">
              <video  width="100%" controls  >
                <source src="/video/Bardonnex.mp4" type="video/mp4">
                {{-- <source src="mov_bbb.ogg" type="video/ogg"> --}}
                Your browser does not support HTML5 video.
              </video>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <h3 class="text-center">@lang('messages.video_page_header3')</h3>
          <div class="play-pic">
            <div class="play-arrow">
            </div>
          <img src="/video/3.jpg" alt=""   data-toggle="modal" data-target="#video3"  />
        </div>
          <div class="modal fade" id="video3" role="dialog">
            <div class="modal-dialog">
              <video  width="100%" controls  >
                <source src="/video/Blandonet.mp4" type="video/mp4">
                {{-- <source src="mov_bbb.ogg" type="video/ogg"> --}}
                Your browser does not support HTML5 video.
              </video>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-4 col-sm-offset-4">
          <h3 class="text-center">@lang('messages.video_page_header4')</h3>
          <div class="play-pic">
            <div class="play-arrow">
            </div>
          <img src="/video/4.jpg" alt=""   data-toggle="modal" data-target="#video4"  />
        </div>
          <div class="modal fade" id="video4" role="dialog">
            <div class="modal-dialog">
              <video  width="100%" controls  >
                <source src="/video/Lausanne.mp4" type="video/mp4">
                {{-- <source src="mov_bbb.ogg" type="video/ogg"> --}}
                Your browser does not support HTML5 video.
              </video>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<script>
$('.modal').on('hidden.bs.modal', function () {
  var video = $(this).find("video");
  video[0].pause();
})
$( ".videos" ).find(".play-pic").on( "click", function() {
  var video = $(this).next(".modal").find("video");
  video[0].play();
});
</script>

@endsection
