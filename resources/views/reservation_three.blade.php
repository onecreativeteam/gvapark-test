@section('page-title', 'Parking Aéroport de Genève Tarifs')
@section('page-description', "Réservez votre place de parking à l'aéroport de Genève au meilleur tarifs possible. Réservez en ligne sur notre site votre place de parking 7jours sur 7")
@extends('layouts.app')
@section('content')
<div class="container-fluid page-content">
    <div class="container">
        <div class="row text-center">
            <h2 data-aos="zoom-in">@lang('messages.reservation_page_first_title')</h2>
        </div>
        <div class="row sortie-info">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="step_navigation">
                <ul class="steps">
                    <li class="current"><a href="{{ route('reservation.one.get') }}">@lang('messages.reservation_step') 1</a></li>
                    <li class="current"><a href="{{ route('reservation.two.get') }}">@lang('messages.reservation_step') 2</a></li>
                    <li class="current"><a href="{{ route('reservation.three.get') }}">@lang('messages.reservation_step') 3</a></li>
                    <li><a onclick="return false;">@lang('messages.reservation_step') 4</a></li>
                    <li><a onclick="return false;">@lang('messages.reservation_step') 5</a></li>
                </ul>
            </div>
            <div class="container">
                <div class="row text-center">
                    <div class="col-sm-12">
                        <div id="initClientAuthModal"></div>
                    </div>
                    <div class="col-sm-12">
                        <span></span>
                    </div>
                    <div class="question">
                        <p data-toggle="collapse" data-target="#option1-2" class="collapsed">@lang('messages.reservation_checkout_first_label') <span>@lang('messages.reservation_checkout_second_label')</span></p>
                        <div id="option1-2" class=" collapse">
                             <div class="col-sm-6 col-sm-offset-3 text-center">
                                {!! Form::open(['action' => 'ReservationController@applyPromoCode', 'method' => 'POST']) !!}
                                    <div class="form-group">
                                        <input type="text" name="promo_code" class="form-control" id="code" placeholder="Votre code promo">
                                    </div>
                                    <button type="submit" class="button">@lang('messages.reservation_checkout_apply_promo_label')</button>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
        <div class="row">
            <div class="col-sm-6">
            <br>
            <div class="answer">
                Déjà client ? <span style="color:#bf1e2d;font-weight:bold">Cliquez ici pour vous connecter</span>
            </div>
            {!! Form::open(['action' => 'ReservationController@authClient', 'method' => 'POST','id'=>'customerForm']) !!}
                <div class="form-group">
                    <label for="login_email">@lang('messages.form_email')</label>
                    <input type="email" class="form-control" name="login_email" id="login_email">
                </div>
                <div class="form-group">
                    <label for="login_password">@lang('messages.form_password')</label>
                    <input type="password" class="form-control" name="login_password" id="login_password">
                </div>
                <input class="step_three button" type="submit" value="Connexion">       
            {!! Form::close() !!}
            <a class="btn btn-link red-link" href="{{ route('password.reset') }}">Mot de passe perdu?</a>
        </div>
        <div class="col-sm-6 text-center">
            <div class="go-to-register-wrapper">
                <div class="go-to-register-text">
                    @lang('messages.reservation_checkout_info2_label')               
                </div>
                <a href="{{ route('reservation.four.get') }}" class="go-to-register">@lang('messages.reservation_checkout_new_client_button_label')</a>
            </div>
        </div>
    </div>
</div>

<script>

$(document).ready(function() {
    $('#customerForm').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'has-success',
            invalid: 'has-error'
        },
        fields: {
            login_email: {
                validators: {
                    notEmpty: {
                        message: 'The e-mail is required'
                    },
                }
            },
            login_password: {
                validators: {
                    notEmpty: {
                        message: 'The password is required'
                    }
                }
            }  
        }
    });
});
</script>    
          
@endsection
