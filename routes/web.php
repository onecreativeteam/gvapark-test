<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('/login', ['as' => 'login.post', 'uses' => 'Auth\LoginController@login']);
Route::get('/logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
Route::get('/register', ['as' => 'register', 'uses' => 'Auth\RegisterController@showRegistrationForm']);
Route::post('/register', ['as' => 'register.post', 'uses' => 'Auth\RegisterController@register']);
Route::get('/password/reset', ['as' => 'password.reset', 'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
Route::post('/password/email', ['as' => 'password.email', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);
Route::get('/password/reset/{token}', ['as' => 'password.reset.token', 'uses' => 'Auth\ResetPasswordController@showResetForm']);
Route::post('/password/reset', ['as' => 'password.reset.post', 'uses' => 'Auth\ResetPasswordController@reset']);

Route::get('/language/{language}', ['as' => 'lang.switch', 'uses' => 'LanguageController@index']);

Route::group([ 'middleware' => ['params'] ], function() {
	Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
});

Route::get('/videos', ['as' => 'video', 'uses' => 'HomeController@videoIndex']);
Route::get('contact', ['as' => 'contact', 'uses' => 'HomeController@contactIndex']);
Route::get('avis-clients', ['as' => 'avis-clients', 'uses' => 'HomeController@avisClientsIndex']);
Route::get('parking-service', ['as' => 'parking-service', 'uses' => 'HomeController@parkingServiceIndex']);

Route::get('/sitemap', ['as' => 'sitemap', 'uses' => 'HomeController@sitemap']);
Route::get('/facture', ['as' => 'facture', 'uses' => 'HomeController@showFacture']);
Route::get('/generate-invoice/{id?}', ['as' => 'generate.invoice', 'uses' => 'ReservationController@generateInvoice']);

/* Reservation Routes */
Route::get('/reservation', ['as' => 'reservation.one.get', 'uses' => 'ReservationController@getReservationOne']);
Route::get('/reservation-two', ['as' => 'reservation.two.get', 'uses' => 'ReservationController@getReservationTwo']);
Route::get('/reservation-three', ['as' => 'reservation.three.get', 'uses' => 'ReservationController@getReservationThree']);
Route::get('/reservation-four', ['as' => 'reservation.four.get', 'uses' => 'ReservationController@getReservationFour']);
Route::get('/reservation-five', ['as' => 'reservation.five.get', 'uses' => 'ReservationController@getReservationFive']);

Route::post('/reservation-cart', ['as' => 'reservation.cart.post', 'uses' => 'ReservationController@postReservationCart']);
Route::post('/reservation-options', ['as' => 'reservation.options.post', 'uses' => 'ReservationController@postReservationOptions']);
Route::post('/reservation-client', ['as' => 'reservation.client.post', 'uses' => 'ReservationController@postReservationUser']);
Route::get('/reservation-data', ['as' => 'reservation.data.get', 'uses' => 'ReservationController@initReservation']);

// Route::post('/reservation-checkout', ['as' => 'reservation.checkout.post', 'uses' => 'ReservationController@postReservationCheckout']);

Route::get('/reservation-confirm', ['as' => 'reservation.complete', 'uses' => 'ReservationController@getReservationConfirmationPage']);

Route::get('/transaction-cancel/{id}', ['as' => 'web.transaction.cancel', 'uses' => 'PostFinanceController@cancelTransaction']);

Route::post('/client_auth', ['as' => 'reservation.client.auth', 'uses' => 'ReservationController@authClient']);
Route::get('/logout-client', ['as' => 'reservation.client.logout', 'uses' => 'ReservationController@logoutClient']);
Route::get('/client/profile/{id}', ['as' => 'reservation.show.client.profile', 'uses' => 'ReservationController@showClientProfile']);
Route::post('/client/profile/update/{id}', ['as' => 'reservation.update.client.profile', 'uses' => 'ReservationController@updateClientProfile']);
Route::get('/reservations/{id}/anulate', ['as' => 'reservation.anulate.client.profile', 'uses' => 'ReservationController@anulateClientReservation']);
Route::get('/reservation/gallery/view/{id}', ['as' => 'reservations.gallery', 'uses' => 'ReservationController@viewClientGallery']);

Route::post('/apply-promo', ['as' => 'apply.promo', 'uses' => 'ReservationController@applyPromoCode']);

Route::get('/events', ['as' => 'events.index', 'uses' => 'EventsController@index']);
Route::get('/events/reservation-complete/thank-you', ['as' => 'events.confirm', 'uses' => 'EventsController@thankYou']);
Route::get('/events/{slug}', ['as' => 'events.inner', 'uses' => 'EventsController@inner']);
Route::post('/events/{id}', ['as' => 'events.post.ajax', 'uses' => 'EventsController@postAjax']);
Route::get('/events/generate-invoice/{id?}', ['as' => 'generate.invoice.event', 'uses' => 'EventsController@generateInvoice']);

// Route::post('/remove-reservation-option', 'ReservationController@updateReservationOption');

Route::post('/precalculate-prices', ['as' => 'get.calculated.prices', 'uses' => 'ReservationController@getPreCalculatedPrices']);

Route::post('/payment-status', ['as' => 'postfinance.status', 'uses' => 'PostFinanceController@statusPayment']);
Route::post('/events/status-payment', ['as' => 'events.status', 'uses' => 'EventsController@paymentStatus']);

/* ==================================== Ajax Routes ==================================== */





/* ==================================== Ajax Routes ==================================== */