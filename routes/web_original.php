<?php
// Route::get('test111', function () {
//      return view('reservation_confirm');
// });
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/* ==================================== Auth Routes ==================================== */
Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('login', ['as' => 'login.post', 'uses' => 'Auth\LoginController@login']);
Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
Route::get('register', ['as' => 'register', 'uses' => 'Auth\RegisterController@showRegistrationForm']);
Route::post('register', ['as' => 'register.post', 'uses' => 'Auth\RegisterController@register']);
Route::get('password/reset', ['as' => 'password.reset', 'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
Route::post('password/email', ['as' => 'password.email', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);
Route::get('password/reset/{token}', ['as' => 'password.reset.token', 'uses' => 'Auth\ResetPasswordController@showResetForm']);
Route::post('password/reset', ['as' => 'password.reset.post', 'uses' => 'Auth\ResetPasswordController@reset']);
/* ==================================== Auth Routes ==================================== */

// Route::get('client/password/reset', ['as' => 'client.password.reset', 'uses' => 'ClientAuth\ForgotPasswordController@showLinkRequestForm']);
// Route::post('client/password/email', ['as' => 'client.password.email', 'uses' => 'ClientAuth\ForgotPasswordController@sendResetLinkEmail']);
// Route::get('client/password/reset/{token}', ['as' => 'client.password.reset.token', 'uses' => 'ClientAuth\ResetPasswordController@showResetForm']);
// Route::post('client/password/reset', ['as' => 'client.password.reset.post', 'uses' => 'ClientAuth\ResetPasswordController@reset']);

/* ==================================== Web Routes ==================================== */

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::get('/sitemap', ['as' => 'sitemap', 'uses' => 'HomeController@sitemap']);
Route::get('/videos', ['as' => 'video', 'uses' => 'HomeController@videoIndex']);

Route::get('/facture', ['as' => 'facture', 'uses' => 'HomeController@showFacture']);
Route::get('/generate-invoice/{id?}', ['as' => 'generate.invoice', 'uses' => 'ReservationController@generateInvoice']);

/* Reservation Routes */
Route::get('/reservation', ['as' => 'reservation.get', 'uses' => 'ReservationController@getReservation']);
Route::post('/reservation', ['as' => 'reservation.post', 'uses' => 'ReservationController@postReservation']);

Route::get('/reservation-cart', ['as' => 'reservation.cart.get', 'uses' => 'ReservationController@getReservationCart']);
Route::post('/reservation-cart', ['as' => 'reservation.cart.post', 'uses' => 'ReservationController@postReservationCart']);

Route::get('/reservation-checkout', ['as' => 'reservation.checkout.get', 'uses' => 'ReservationController@getReservationCheckout']);
Route::post('/reservation-checkout', ['as' => 'reservation.checkout.post', 'uses' => 'ReservationController@postReservationCheckout']);

Route::get('/reservation-confirm', ['as' => 'reservation.complete', 'uses' => 'ReservationController@getReservationConfirmationPage']);

Route::post('/client_auth', ['as' => 'reservation.client.auth', 'uses' => 'ReservationController@authClient']);
Route::get('/logout-client', ['as' => 'reservation.client.logout', 'uses' => 'ReservationController@logoutClient']);
Route::get('/client/profile/{id}', ['as' => 'reservation.show.client.profile', 'uses' => 'ReservationController@showClientProfile']);
Route::post('/client/profile/update/{id}', ['as' => 'reservation.update.client.profile', 'uses' => 'ReservationController@updateClientProfile']);
Route::get('/reservations/{id}/anulate', ['as' => 'reservation.anulate.client.profile', 'uses' => 'ReservationController@anulateClientReservation']);
Route::get('/reservation/gallery/view/{id}', ['as' => 'reservations.gallery', 'uses' => 'ReservationController@viewClientGallery']);

Route::post('/apply-promo', ['as' => 'apply.promo', 'uses' => 'ReservationController@applyPromoCode']);

Route::get('parking-service', ['as' => 'parking-service', 'uses' => 'HomeController@parkingServiceIndex']);
Route::get('avis-clients', ['as' => 'avis-clients', 'uses' => 'HomeController@avisClientsIndex']);
Route::get('contact', ['as' => 'contact', 'uses' => 'HomeController@contactIndex']);

Route::post('/remove-reservation-option', 'ReservationController@updateReservationOption');
Route::post('/reviews', 'ReviewsController@store');

Route::get('/paypal-payment', ['as' => 'paypal.payment', 'uses' => 'PaypalController@postPayment']);
Route::get('/paypal-payment/status', ['as' => 'paypal.status', 'uses' => 'PaypalController@getPaymentStatus']);

Route::get('/postfinance-payment', ['as' => 'postfinance.payment', 'uses' => 'PostFinanceController@postPayment']);
Route::any('/postfinance-status', ['as' => 'postfinance.status', 'uses' => 'PostFinanceController@statusPayment']);

// Route::any('/postfinance-testing-test', ['as' => 'postfinance.test', 'uses' => 'PostFinanceController@testRedirect']);
// Route::any('/postfinance-testing', ['as' => 'postfinance.payment.test', 'uses' => 'PostFinanceController@testResult']);

/* ==================================== Web Routes ==================================== */


/* ==================================== Admin Routes ==================================== */
Route::group(['middleware' => ['auth', 'admin'], 'prefix' => 'admin'], function() {

	Route::get('/', ['as' => 'admin.home', 'uses' => 'AdminController@index']);

	/* Homepage Information Routes */
	Route::get('/homepage', ['as' => 'admin.homepage.show', 'uses' => 'Admin\HomepageInformationController@index']);
	Route::post('/homepage', ['as' => 'admin.homepage.store', 'uses' => 'Admin\HomepageInformationController@store']);

	/* Our Service Routes */
	Route::get('/parking-service', ['as' => 'admin.parking.service.show', 'uses' => 'Admin\ParkingServiceController@index']);
	Route::post('/parking-service', ['as' => 'admin.parking.service.store', 'uses' => 'Admin\ParkingServiceController@store']);

	/* Contact Routes */
	Route::get('/contact', ['as' => 'admin.contact.show', 'uses' => 'Admin\ContactController@index']);
	Route::post('/contact', ['as' => 'admin.contact.store', 'uses' => 'Admin\ContactController@store']);

	/* System Options Routes */
	Route::get('/system-options', ['as' => 'admin.system.show', 'uses' => 'Admin\SystemOptionsController@index']);
	Route::post('/system-options', ['as' => 'admin.system.store', 'uses' => 'Admin\SystemOptionsController@store']);

	/* FAQ Routes */
	Route::get('/faq', ['as' => 'admin.faq.index', 'uses' => 'Admin\FaqController@index']);
	Route::get('/faq/create', ['as' => 'admin.faq.create', 'uses' => 'Admin\FaqController@create']);
	Route::post('/faq', ['as' => 'admin.faq.store', 'uses' => 'Admin\FaqController@store']);
	Route::get('/faq/{id}/edit', ['as' => 'admin.faq.edit', 'uses' => 'Admin\FaqController@edit']);
	Route::patch('/faq/{id}', ['as' => 'admin.faq.update', 'uses' => 'Admin\FaqController@update']);
	Route::delete('/faq/{id}', ['as' => 'admin.faq.delete', 'uses' => 'Admin\FaqController@delete']);

	/* FAQ Routes */
	Route::get('/promo-codes', ['as' => 'admin.promo.index', 'uses' => 'Admin\PromoCodesController@index']);
	Route::get('/promo-codes/create', ['as' => 'admin.promo.create', 'uses' => 'Admin\PromoCodesController@create']);
	Route::post('/promo-codes', ['as' => 'admin.promo.store', 'uses' => 'Admin\PromoCodesController@store']);
	Route::get('/promo-codes/{id}/edit', ['as' => 'admin.promo.edit', 'uses' => 'Admin\PromoCodesController@edit']);
	Route::patch('/promo-codes/{id}', ['as' => 'admin.promo.update', 'uses' => 'Admin\PromoCodesController@update']);
	Route::delete('/promo-codes/{id}', ['as' => 'admin.promo.delete', 'uses' => 'Admin\PromoCodesController@delete']);

	/* Calendar Routes */
	Route::get('/calendar', ['as' => 'admin.calendar.show', 'uses' => 'Admin\HomepageInformationController@indexCalendar']);
	Route::post('/calendar', ['as' => 'admin.calendar.set.arrived', 'uses' => 'ReservationController@setArrived']);
	Route::post('/calendar-update-reservation', ['as' => 'admin.calendar.update.reservation', 'uses' => 'ReservationController@changeReservationDates']);

	/* Reviews Routes */
	Route::get('/reviews', ['as' => 'admin.reviews.show', 'uses' => 'ReviewsController@indexReviews']);
	Route::patch('/reviews/{id}', ['as' => 'admin.reviews.approve', 'uses' => 'ReviewsController@approveReviews']);
	Route::post('/reviews/delete/{id}', ['as' => 'admin.reviews.delete', 'uses' => 'ReviewsController@deleteReviews']);

	Route::get('/reservations/{id}/edit', ['as' => 'admin.reservations.edit', 'uses' => 'ReservationController@editReservation']);
	Route::patch('/reservations/{id}', ['as' => 'admin.reservations.update', 'uses' => 'ReservationController@updateReservation']);
	Route::post('/reservations/delete/{id}', ['as' => 'admin.reservations.delete', 'uses' => 'ReservationController@deleteReservation']);
	Route::get('/reservations-list', ['as' => 'admin.reservation.list', 'uses' => 'ReservationController@reservationsList']);
	Route::get('/new-reservation', ['as' => 'admin.new.reservation', 'uses' => 'ReservationController@newAdminReservation']);
	Route::post('/new-reservation', ['as' => 'admin.new.reservation.post', 'uses' => 'ReservationController@postNewAdminReservation']);
	Route::get('/reservation/gallery/view/{id}', ['as' => 'admin.reservations.gallery', 'uses' => 'ReservationController@viewGallery']);

	Route::get('/reports', ['as' => 'admin.reports', 'uses' => 'Admin\ReportsController@index']);
	Route::post('/reservations-reports', ['as' => 'admin.reports.reservations', 'uses' => 'Admin\ReportsController@reservationsExcelExport']);
	Route::post('/profit-reports', ['as' => 'admin.reports.profit', 'uses' => 'Admin\ReportsController@reservationsProfitExcelExport']);
		
	/* Autres Routes */
	Route::get('/concurents', ['as' => 'admin.concurents.index', 'uses' => 'Admin\ConcurentsController@index']);
	Route::get('/concurents/create', ['as' => 'admin.concurents.create', 'uses' => 'Admin\ConcurentsController@create']);
	Route::post('/concurents', ['as' => 'admin.concurents.store', 'uses' => 'Admin\ConcurentsController@store']);
	Route::get('/concurents/{id}/edit', ['as' => 'admin.concurents.edit', 'uses' => 'Admin\ConcurentsController@edit']);
	Route::patch('/concurents/{id}', ['as' => 'admin.concurents.update', 'uses' => 'Admin\ConcurentsController@update']);
	Route::delete('/concurents/{id}', ['as' => 'admin.concurents.delete', 'uses' => 'Admin\ConcurentsController@delete']);

	// Route::get('/nerds', ['as' => 'admin.homepage.nerds.index', 'uses' => 'Admin\NerdsController@indexNerds']);
	// Route::get('/nerds/create', ['as' => 'admin.homepage.nerds.create', 'uses' => 'Admin\NerdsController@showFormNerds']);
	// Route::post('/nerds', ['as' => 'admin.homepage.nerds.store', 'uses' => 'Admin\NerdsController@storeNerds']);
	// Route::get('/nerds/{id}/edit', ['as' => 'admin.homepage.nerds.edit', 'uses' => 'Admin\NerdsController@showEditFormNerds']);
	// Route::patch('/nerds/{id}', ['as' => 'admin.homepage.nerds.update', 'uses' => 'Admin\NerdsController@updateNerds']);
	// Route::delete('/nerds/{id}', ['as' => 'admin.homepage.nerds.delete', 'uses' => 'Admin\NerdsController@deleteNerds']);

});
/* ==================================== Admin Routes ==================================== */


/* ==================================== Ajax Routes ==================================== */

Route::get('ajax/faq-table', ['as' => 'admin.faq.table', 'uses' => 'Admin\AjaxController@FaqDataTable']);
Route::post('ajax/get-daily-parkspots', ['as' => 'admin.get.parking.count', 'uses' => 'Admin\AjaxController@getParkingOccupationCount']);
Route::post('ajax/get-profits-by-date', ['as' => 'admin.get.profits', 'uses' => 'Admin\AjaxController@getProfitByDate']);
Route::post('ajax/get-months-by-year', ['as' => 'admin.get.profit.months', 'uses' => 'Admin\AjaxController@getMonthsByYear']);


/* ==================================== Ajax Routes ==================================== */
