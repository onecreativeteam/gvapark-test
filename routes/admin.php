<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::group([ 'middleware' => ['auth', 'admin'] , 'prefix' => 'admin' ], function() {

	Route::get('/', ['as' => 'admin.home', 'uses' => 'AdminController@index']);

	Route::get('/update-park-places', ['as' => 'admin.update.park.places', 'uses' => 'Admin\ReservationController@updateParkPlaces']);
	Route::get('/remove-park-places', ['as' => 'admin.remove.park.places', 'uses' => 'Admin\ReservationController@removeParkPlacesByTime']);

	Route::get('/users', ['as' => 'admin.users', 'uses' => 'AdminController@users']);
	Route::post('/update-password', ['as' => 'admin.update.password', 'uses' => 'AdminController@updatePassword']);
	Route::post('/users/create', ['as' => 'admin.users.create', 'uses' => 'AdminController@createUser']);
	Route::get('/users/delete/{id}', ['as' => 'admin.users.delete', 'uses' => 'AdminController@deleteWorkerUser']);
	Route::get('/users/edit/{id}', ['as' => 'admin.users.edit', 'uses' => 'AdminController@editUser']);
	Route::post('/users/update/{id}', ['as' => 'admin.users.update', 'uses' => 'AdminController@updateUser']);

	Route::resource('/homepage', 'Admin\HomepageController',
	['names' => [
		'create' => 'admin.homepage.show',
		'store' => 'admin.homepage.store'
	],'except' => [
		'index',
	    'show',
	    'edit',
	    'update',
	    'destroy'
	]]);

	Route::resource('/parking-service', 'Admin\ParkingServiceController',
	['names' => [
		'create' => 'admin.parking.service.show',
		'store' => 'admin.parking.service.store'
	],'except' => [
		'index',
	    'show',
	    'edit',
	    'update',
	    'destroy'
	]]);

	Route::resource('/contact', 'Admin\ContactController',
	['names' => [
		'create' => 'admin.contact.show',
		'store' => 'admin.contact.store'
	],'except' => [
		'index',
	    'show',
	    'edit',
	    'update',
	    'destroy'
	]]);

	Route::resource('/system-options', 'Admin\SystemOptionsController',
	['names' => [
		'create' => 'admin.system.show',
		'store' => 'admin.system.store'
	],'except' => [
		'index',
	    'show',
	    'edit',
	    'update',
	    'destroy'
	]]);

	Route::get('/reservation-prices/{type}', ['as' => 'admin.prices.show', 'uses' => 'Admin\ReservationPricesController@index']);
	Route::resource('/reservation-prices', 'Admin\ReservationPricesController',
	['names' => [
		// 'create' => 'admin.prices.show',
		'store' => 'admin.prices.store'
	],'except' => [
		'index',
		'create',
	    'show',
	    'edit',
	    'update',
	    'destroy'
	]]);

	Route::resource('/faq', 'Admin\FaqController',
	['names' => [
		'index' => 'admin.list.faq',
		'create' => 'admin.create.faq',
		'store' => 'admin.store.faq',
		'edit' => 'admin.edit.faq',
		'update' => 'admin.update.faq',
		'destroy' => 'admin.destroy.faq',
	],'except' => [
	    'show',
	    // 'destroy'
	]]);

	Route::resource('/promo-codes', 'Admin\PromoCodesController',
	['names' => [
		'index' => 'admin.list.promo',
		'create' => 'admin.create.promo',
		'store' => 'admin.store.promo',
		'edit' => 'admin.edit.promo',
		'update' => 'admin.update.promo',
		'destroy' => 'admin.destroy.promo',
	],'except' => [
	    'show',
	    // 'destroy'
	]]);

	Route::resource('/concurents', 'Admin\ConcurentsController',
	['names' => [
		'index' => 'admin.list.concurents',
		'create' => 'admin.create.concurents',
		'store' => 'admin.store.concurents',
		'edit' => 'admin.edit.concurents',
		'update' => 'admin.update.concurents',
		'destroy' => 'admin.destroy.concurents',
	],'except' => [
	    'show',
	    // 'destroy'
	]]);

	Route::get('/event-reservations', ['as' => 'admin.list.event.reservation', 'uses' => 'Admin\EventsController@listReservations']);
	Route::resource('/events', 'Admin\EventsController',
	['names' => [
		'index' => 'admin.list.events',
		'create' => 'admin.create.events',
		'store' => 'admin.store.events',
		'edit' => 'admin.edit.events',
		'update' => 'admin.update.events',
		'destroy' => 'admin.destroy.events',
	],'except' => [
	    'show',
	    // 'destroy'
	]]);

	/* Calendar Custom Routes */
	Route::get('/calendar/{type}', ['as' => 'admin.calendar.show', 'uses' => 'Admin\HomepageController@indexCalendar']);
	Route::get('/calendar/return-car/{id}', ['as' => 'admin.calendar.set.returned', 'uses' => 'Admin\ReservationController@setReturned']);
	Route::post('/calendar', ['as' => 'admin.calendar.set.arrived', 'uses' => 'Admin\ReservationController@setArrived']);
	Route::post('/calendar-update-reservation', ['as' => 'admin.calendar.update.reservation', 'uses' => 'Admin\ReservationController@changeReservationDates']);
	
	/* Reservation Custom Routes */
	Route::get('/reservations/{id}/edit', ['as' => 'admin.reservations.edit', 'uses' => 'Admin\ReservationController@editReservation']);
	Route::patch('/reservations/{id}', ['as' => 'admin.reservations.update', 'uses' => 'Admin\ReservationController@updateReservation']);
	Route::post('/reservations/delete/{id}', ['as' => 'admin.reservations.delete', 'uses' => 'Admin\ReservationController@deleteReservation']);
	Route::post('/reservations/delete', ['as' => 'admin.reservations.caledar.delete', 'uses' => 'Admin\ReservationController@deleteCalendarReservation']);
	Route::get('/reservations-list', ['as' => 'admin.reservation.list', 'uses' => 'Admin\ReservationController@reservationsList']);
	Route::get('/new-reservation', ['as' => 'admin.new.reservation', 'uses' => 'Admin\ReservationController@newAdminReservation']);
	Route::post('/new-reservation', ['as' => 'admin.new.reservation.post', 'uses' => 'Admin\ReservationController@postNewAdminReservation']);
	Route::get('/reservation/gallery/view/{id}', ['as' => 'admin.reservations.gallery', 'uses' => 'Admin\ReservationController@viewGallery']);

	Route::get('/transaction-status/{id}', ['as' => 'transaction.status', 'uses' => 'PostFinanceController@statusTransaction']);
	Route::get('/transaction-cancel/{id}', ['as' => 'transaction.cancel', 'uses' => 'PostFinanceController@cancelTransaction']);
	Route::get('/event-transaction-cancel/{id}', ['as' => 'transaction.cancel.event', 'uses' => 'PostFinanceController@cancelEventTransaction']);
	
	/* Reviews Custom Routes */
	// Route::get('/reviews', ['as' => 'admin.reviews.show', 'uses' => 'ReviewsController@indexReviews']);
	// Route::patch('/reviews/{id}', ['as' => 'admin.reviews.approve', 'uses' => 'ReviewsController@approveReviews']);
	// Route::post('/reviews/delete/{id}', ['as' => 'admin.reviews.delete', 'uses' => 'ReviewsController@deleteReviews']);

	/* Reports Custom Routes */
	Route::get('/reports', ['as' => 'admin.reports', 'uses' => 'Admin\ReportsController@index']);
	Route::post('/reservations-reports', ['as' => 'admin.reports.reservations', 'uses' => 'Admin\ReportsController@reservationsExcelExport']);
	Route::post('/profit-reports', ['as' => 'admin.reports.profit', 'uses' => 'Admin\ReportsController@reservationsProfitExcelExport']);

	Route::get('/ajax/faq-table', ['as' => 'admin.faq.table', 'uses' => 'Admin\AjaxController@FaqDataTable']);
	Route::post('/ajax/get-daily-parkspots', ['as' => 'admin.get.parking.count', 'uses' => 'Admin\AjaxController@getParkingOccupationCount']);
	Route::post('/ajax/get-profits-by-date', ['as' => 'admin.get.profits', 'uses' => 'Admin\AjaxController@getProfitByDate']);
	Route::post('/ajax/get-months-by-year', ['as' => 'admin.get.profit.months', 'uses' => 'Admin\AjaxController@getMonthsByYear']);

	Route::get('/datatables/reservations', ['as' => 'admin.datatables.reservations', 'uses' => 'Admin\DataTablesController@reservationDataTable']);
	Route::get('/datatables/reservations-events', ['as' => 'admin.datatables.event.reservations', 'uses' => 'Admin\DataTablesController@eventReservationDataTable']);
	Route::get('/datatables/events', ['as' => 'admin.datatables.events', 'uses' => 'Admin\DataTablesController@eventsDataTable']);

});
