<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsGalleryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations_gallery', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reservations_id')->unsigned()->index()->nullable();
            $table->foreign('reservations_id')->references('id')->on('reservations')->onDelete('cascade');
            $table->string('picture')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations_gallery');
    }
}
