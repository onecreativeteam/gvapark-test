<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reservations', function (Blueprint $table) {
            $table->integer('park_place_id')->unsigned()->index()->nullable()->after('user_id');
            $table->foreign('park_place_id', 'fk_r_park_place')
                ->references('id')
                ->on('park_places')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservations', function (Blueprint $table) {
            $table->dropForeign('fk_r_park_place');
            $table->dropColumn('park_place_id');
        });
    }
}
