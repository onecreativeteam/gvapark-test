<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomepageTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homepage_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('homepage_id')->nullable()->unsigned();
            $table->foreign('homepage_id')->references('id')->on('homepage')->onDelete('cascade');
            $table->string('title', 255)->nullable();
            $table->text('description')->nullable();
            $table->string('security_info', 255)->nullable();
            $table->string('simplicity_info', 255)->nullable();
            $table->string('speed_info', 255)->nullable();
            $table->string('service_info', 255)->nullable();
            $table->string('slug')->nullable(false);
            $table->string('locale')->index();
            $table->unique(['homepage_id', 'locale']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('homepage_translations', function (Blueprint $table) {
            $table->dropForeign(['homepage_id']);
        });

        Schema::dropIfExists('homepage_translations');
    }
}
