<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parking_spots_count')->nullable(false);
            $table->double('initial_price', 10, 2)->nullable(false);
            $table->double('daily_price', 10, 2)->nullable(false);
            $table->boolean('reservation_have_options')->nullable(false);
            $table->double('extereur_price', 10, 2)->nullable(false);
            $table->double('intereur_price', 10, 2)->nullable(false);
            $table->double('extra_price', 10, 2)->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('system_options');
    }
}
