<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaqTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faq_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('faq_id')->nullable()->unsigned();
            $table->foreign('faq_id')->references('id')->on('faq')->onDelete('cascade');
            $table->string('question', 255)->nullable(false);
            $table->text('answer')->nullable(false);
            $table->string('slug')->nullable(false);
            $table->string('locale')->index();
            $table->unique(['faq_id', 'locale']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('faq_translations', function (Blueprint $table) {
            $table->dropForeign(['faq_id']);
        });

        Schema::dropIfExists('faq_translations');
    }
}
