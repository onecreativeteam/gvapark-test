<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConcurentsTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('concurents_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('concurents_id')->nullable()->unsigned();
            $table->foreign('concurents_id')->references('id')->on('concurents')->onDelete('cascade');
            $table->string('name')->nullable(false);
            $table->string('initial_price')->nullable();
            $table->string('daily_price')->nullable();
            $table->string('daily_price_sup')->nullable();
            $table->string('slug')->nullable(false);
            $table->string('locale')->index();
            $table->unique(['concurents_id', 'locale']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('concurents_translations', function (Blueprint $table) {
            $table->dropForeign(['concurents_id']);
        });

        Schema::dropIfExists('concurents_translations');
    }
}
