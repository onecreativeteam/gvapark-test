<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOurServiceTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('our_service_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('our_service_id')->nullable()->unsigned();
            $table->foreign('our_service_id')->references('id')->on('our_service')->onDelete('cascade');
            $table->string('main_info_title', 255)->nullable();
            $table->text('main_info_description')->nullable();
            $table->string('online_booking_info_title', 255)->nullable();
            $table->text('online_booking_info_description')->nullable();
            $table->string('post_your_vehicle_info_title', 255)->nullable();
            $table->text('post_your_vehicle_info_description')->nullable();
            $table->string('travel_info_title', 255)->nullable();
            $table->text('travel_info_description')->nullable();
            $table->string('pickup_info_title', 255)->nullable();
            $table->text('pickup_info_description')->nullable();
            $table->string('slug')->nullable(false);
            $table->string('locale')->index();
            $table->unique(['our_service_id', 'locale']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('our_service_translations', function (Blueprint $table) {
            $table->dropForeign(['our_service_id']);
        });

        Schema::dropIfExists('our_service_translations');
    }
}
