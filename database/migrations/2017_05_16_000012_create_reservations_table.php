<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reservation_options_id')->unsigned()->index()->nullable();
            $table->foreign('reservation_options_id', 'fk_r_options')->references('id')->on('reservation_options')->onDelete('set null');
            $table->string('reservation_option')->nullable();
            $table->integer('user_id')->unsigned()->index()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('promo_code_id')->unsigned()->index()->nullable();
            $table->foreign('promo_code_id')->references('id')->on('promo_codes');
            $table->string('client_first_name', 60)->nullable();
            $table->string('client_last_name', 60)->nullable();
            $table->char('email')->nullable(false);
            $table->dateTime('date_of_reservation')->nullable(false);
            $table->dateTime('date_of_arrival')->nullable(false);
            $table->dateTime('date_of_departure')->nullable(false);
            $table->integer('days_to_stay')->nullable(false);
            $table->double('price_to_charge', 10, 2)->nullable(false);
            $table->double('changed_price_to_charge', 10, 2)->nullable();
            $table->double('vat', 10, 2)->nullable(false);
            $table->string('payment_status')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('payment_id')->nullable();
            $table->string('system_payment_id')->nullable();
            $table->boolean('applied_promo')->nullable(false)->default(0);
            $table->boolean('is_arrived')->nullable(false)->default(0);
            $table->boolean('has_gallery')->nullable(false)->default(0);
            $table->dateTime('arrived_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservations', function (Blueprint $table) {
            $table->dropForeign(['reservation_options_id']);
            $table->dropForeign(['user_id']);
            $table->dropForeign(['promo_code_id']);
        });

        Schema::drop('reservations');
    }
}
