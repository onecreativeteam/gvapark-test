<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOurServiceInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('our_service_information', function (Blueprint $table) {
            $table->increments('id');
            $table->string('main_info_title', 255)->nullable();
            $table->text('main_info_description')->nullable();
            $table->string('online_booking_info_title', 255)->nullable();
            $table->text('online_booking_info_description')->nullable();
            $table->string('post_your_vehicle_info_title', 255)->nullable();
            $table->text('post_your_vehicle_info_description')->nullable();
            $table->string('travel_info_title', 255)->nullable();
            $table->text('travel_info_description')->nullable();
            $table->string('pickup_info_title', 255)->nullable();
            $table->text('pickup_info_description')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('our_service_information');
    }
}
