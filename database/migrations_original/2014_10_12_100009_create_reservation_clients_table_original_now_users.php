<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservation_clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 40)->nullable(false);
            $table->string('last_name', 40)->nullable(false);
            $table->string('company_name', 40)->nullable();
            $table->char('email', 60)->nullable(false);
            $table->string('phone', 30)->nullable(false);
            $table->string('adress', 30)->nullable(false);
            $table->string('adress2', 30)->nullable();
            $table->string('city', 30)->nullable(false);
            $table->string('postcode', 30)->nullable(false);
            $table->text('description')->nullable();
            $table->string('car_brand', 30)->nullable(false);
            $table->string('car_model', 30)->nullable(false);
            $table->string('car_registration_number', 30)->nullable(false);
            $table->string('flight_number', 30)->nullable(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reservation_clients');
    }
}
