<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::connection('mysql2')->create('some_table', function($table) {
        //     $table->increments('id'):
        // });

        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('reservation_options_id')->unsigned()->index()->nullable();
            $table->foreign('reservation_options_id')->references('id')->on('reservation_options')->onDelete('set null');
            $table->string('reservation_option', 60)->nullable();

            $table->integer('reservation_clients_id')->unsigned()->index()->nullable();
            $table->foreign('reservation_clients_id')->references('id')->on('reservation_clients');

            $table->integer('promo_code_id')->unsigned()->index()->nullable();
            $table->foreign('promo_code_id')->references('id')->on('promo_codes');

            $table->string('client_first_name', 60)->nullable();
            $table->string('client_last_name', 60)->nullable();

            $table->char('email', 60)->nullable(false);
            $table->dateTime('date_of_reservation')->nullable(false);
            $table->dateTime('date_of_arrival')->nullable(false);
            $table->dateTime('date_of_departure')->nullable(false);
            $table->integer('days_to_stay')->nullable(false);
            $table->double('price_to_charge', 10, 2)->nullable(false);
            $table->double('changed_price_to_charge', 10, 2)->nullable();
            $table->boolean('is_arrived');
            $table->string('picture', 255)->nullable();
            $table->boolean('applied_promo');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reservations', function (Blueprint $table) {
            $table->dropForeign(['reservation_options_id']);
            $table->dropForeign(['reservation_clients_id']);
            $table->dropForeign(['promo_code_id']);
        });

        Schema::drop('reservations');
    }
}
