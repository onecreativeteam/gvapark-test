<?php

return [
    '' => 'Choose',
    'intereur' => 'Nettoyage premium',
    'extereur' => 'Nettoyage basique',
    'extra' => 'Nettoyage premium & basique',
];
