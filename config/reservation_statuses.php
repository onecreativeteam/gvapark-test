<?php

return [
    '' => 'Choose',
    'completed' => 'Completed',
    'cancelled' => 'Cancelled',
    // 'exception' => 'Exception',
    'declined' => 'Declined',
    'pending' => 'Pending'
];
