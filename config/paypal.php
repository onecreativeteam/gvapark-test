<?php
return array(
    // set your paypal credential
    'client_id' => 'AYyy0MN93NlLsMXGREjxZwix5m996H-aYSjUl07lVk1CgTR6d1ynr5yXlXNfYoLYSQBq0md9z4XYPlY1',
    'secret' => 'EMhHuOj37JNk0Ef-Ky-31-RX87Fohygc-8Gbux0vniY0x8AVdGb54N5lq5_YZImzQwPsCs6wcMqn80zz',

    /**
     * SDK configuration 
     */
    'settings' => array(
        /**
         * Available option 'sandbox' or 'live'
         */
        'mode' => 'sandbox',

        /**
         * Specify the max request time in seconds
         */
        'http.ConnectionTimeOut' => 30,

        /**
         * Whether want to log to a file
         */
        'log.LogEnabled' => true,

        /**
         * Specify the file that want to write on
         */
        'log.FileName' => storage_path() . '/logs/paypal.log',

        /**
         * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the 'FINE' level and decreases as you
         * proceed towards ERROR
         */
        'log.LogLevel' => 'FINE'
    ),
);