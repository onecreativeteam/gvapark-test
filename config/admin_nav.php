<?php

return [
    [
        'title' => 'Homepage Information',
        'sub_nav' => [
            [
                'title' => 'Homepage',
                'url' => 'admin.homepage.show'
            ],
            [
                'title' => 'FAQ',
                'url' => 'admin.list.faq'
            ]
        ]
    ],
    [
        'title' => 'Parking Service',
        'url' => 'admin.parking.service.show'
    ],
    [
        'title' => 'Contact Information',
        'url' => 'admin.contact.show'
    ],
    // [
    //     'title' => 'Reviews',
    //     'url' => 'admin.reviews.show'
    // ],
    [
        'title' => 'Promo codes',
        'url' => 'admin.list.promo'
    ],
    [
        'title' => 'Reservations List',
        'url' => 'admin.reservation.list'
    ],
    [
        'title' => 'Privilege Calendar',
        'url' => 'admin.calendar.show',
        'wildcart' => ['type' => 'valet']
    ],
    [
        'title' => 'Self-parking Calendar',
        'url' => 'admin.calendar.show',
        'wildcart' => ['type' => 'navet']
    ],
    [
        'title' => 'System Options',
        'url' => 'admin.system.show'
    ],
    [
        'title' => 'PRICES',
        'sub_nav' => [
            [
                'title' => 'Privillege-parking Prices',
                'url' => 'admin.prices.show',
                'wildcart' => ['type' => 'valet']
            ],
            [
                'title' => 'Self-parking Prices',
                'url' => 'admin.prices.show',
                'wildcart' => ['type' => 'navet']
            ],
        ]
    ],
    
        [
        'title' => 'Reports',
        'url' => 'admin.reports'
    ],
    [
        'title' => 'Autres',
        'url' => 'admin.list.concurents'
    ],
    [
        'title' => 'Events',
        'sub_nav' => [
            [
                'title' => 'Events',
                'url' => 'admin.list.events'
            ],
            [
                'title' => 'Reservations',
                'url' => 'admin.list.event.reservation'
            ]
        ]
    ],
    [
        'title' => 'Logout',
        'url' => 'logout'
    ]
];
