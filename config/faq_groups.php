<?php

return [
	'fr' => [
	    '' => 'Choose',
	    '1' => 'Effectuer votre réservation',
	    '2' => 'Déposer votre véhicule',
	    '3' => 'Récupérer votre véhicule',
    ],
    'en' => [
	    '' => 'Choose',
	    '1' => 'Make your booking',
	    '2' => 'Dropping off your vehicle',
	    '3' => 'Collecting your vehicle',
    ],
    'de' => [
	    '' => 'Choose',
	    '1' => 'Reservieren',
	    '2' => 'Das Fahrzeug abstellen',
	    '3' => 'Den Wagen abholen',
    ],
];
