<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'paypal' => [
        'client_id' => 'AYyy0MN93NlLsMXGREjxZwix5m996H-aYSjUl07lVk1CgTR6d1ynr5yXlXNfYoLYSQBq0md9z4XYPlY1',
        'secret' => 'EJoRJvbHr2d_r1u2Ydx2eyXZrTCgjomYDo2KBPcCNN0rHWsmxZtavM4XTyqzHqGugZcqA-cMEvh7ex2q',
        'settings' => array(
            /**
             * Available option 'sandbox' or 'live'
             */
            'mode' => 'sandbox',
     
            /**
             * Specify the max request time in seconds
             */
            'http.ConnectionTimeOut' => 30,
     
            /**
             * Whether want to log to a file
             */
            'log.LogEnabled' => true,
     
            /**
             * Specify the file that want to write on
             */
            'log.FileName' => storage_path() . '/logs/paypal.log',
     
            /**
             * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
             *
             * Logging is most verbose in the 'FINE' level and decreases as you
             * proceed towards ERROR
             */
            'log.LogLevel' => 'FINE'
        ),
    ],

];
