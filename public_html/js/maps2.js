function initialize() {
    var myLatLng = {lat: 46.222806, lng: 6.052083};
    var map = new google.maps.Map(document.getElementById('googleMap2'), {
       zoom: 16,
       center: myLatLng,
       scrollwheel: false,
       draggable: true,
       mapTypeId: google.maps.MapTypeId.ROADMAP
     });

     var contentString ="Rue de la Pré-de-la-Fontaine 19 / 1242 Satigny"
     var infowindow = new google.maps.InfoWindow({
        content: contentString
      });
     var marker = new google.maps.Marker({
       position: myLatLng,
       map: map,
     });
     marker.addListener('click', function() {
       infowindow.open(map, marker);
     });
     if (screen.width < 768) {
       var map = new google.maps.Map(document.getElementById('googleMap2'), {
          zoom: 16,
          center: myLatLng,
          scrollwheel: false,
          draggable: true,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        var infowindow = new google.maps.InfoWindow({
           content: contentString
         });
        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
        });
        marker.addListener('click', function() {
          infowindow.open(map, marker);
        });
     }
}
google.maps.event.addDomListener(window, 'load', initialize);
