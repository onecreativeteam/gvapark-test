$( document ).ready(function() {

  $('#timeArrival').timepicker({
      'step': 15,
      'timeFormat': 'H:i',
      'disableTimeRanges': [
          [ "1am", "4am" ],
      ],
      'appendTo': $('.timeArrival')
  });

  $('#timeDeparture').timepicker({
      'step': 15,
      'timeFormat': 'H:i',
      'disableTimeRanges': [
          [ "1am", "4am" ],
      ],
      'appendTo': $('.timeDeparture')
  });

  $('.form_control_time').click(function () {
      if ($(this).parent('div').hasClass('timeArrival')) {
          $('#timeArrival').timepicker('show');
      } else {
          $('#timeDeparture').timepicker('show');
      }
  });

  $("#datepicker-birth").datepicker({
        dateFormat: "yy-mm-dd",
        changeMonth: true,
        changeYear: true
  });

  $('.pickArrival').click(function () {
    $("#datepicker-depart").datepicker({
        dateFormat: "yy/m/d"
    }).datepicker("show");
  });

  $('.pickDeparture').click(function () {
    $("#datepicker-retour").datepicker({
        dateFormat: "yy/m/d"
    }).datepicker("show");
  });

  $("input[name='hour_of_arrival'], input[name='hour_of_departure']").change(function(e){
    setCalculatedPrices();
  });

  $('.changePasswordProfile').change(function(){
    var checked = this.checked ? true : false;
    if (checked == true) {
      $('.hasCheckedChangePassword').show();
      $('input.profile-credentials').attr("disabled", false);
    } else {
      $('.hasCheckedChangePassword').hide();
      $('input.profile-credentials').attr("disabled", true);
    }
  });

  $( function() {

    $("#datepicker-depart.defaultDate").datepicker({
        dateFormat: "yy/m/d"
    }).datepicker("setDate", "0");

    $("#datepicker-retour.defaultDate").datepicker({
        dateFormat: "yy/m/d"
    }).datepicker("setDate", "1");

    $("#datepicker-depart").datepicker({
        dateFormat: "yy/m/d"
    });

    $("#datepicker-retour").datepicker({
        dateFormat: "yy/m/d"
    });

    $(".datepicker-promo").datepicker({
        dateFormat: "yy/m/d"
    });

  });

  $('.selectpicker').selectpicker({
    size: 20
  });

  AOS.init();

  $('.optionsTax span.cancel').click( function() {
      $.ajaxSetup( { headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() } } );
      $.ajax({
          method: "POST",
          url: "/remove-reservation-option",
          dataType: "json",
          data: 'remove=' + 1,
          success: function () {
            $('.optionsTax').remove();
            window.location = "/reservation-cart";
          }
      });
  });

});

function showClientLoginModal() {
  $('#clientCheckoutModal').modal('show');
}

function showTermsAndConditionsModal() {
  $('#clientTermsAndConditionsModal').modal('show');
}
