function initialize() {
    var myLatLng = {lat: 46.2289722, lng: 6.105551300000002};
    var map = new google.maps.Map(document.getElementById('googleMap'), {
       zoom: 16,
       center: myLatLng,
       scrollwheel: false,
       draggable: true,
       mapTypeId: google.maps.MapTypeId.ROADMAP
     });
     var contentString ="Route de l'Aeroport 10,</br> 1215 Meyrin, Switzerland</br><a href='https://goo.gl/QvDjYA'>Directions in Google Maps</a>"
     var infowindow = new google.maps.InfoWindow({
        content: contentString
      });
     var marker = new google.maps.Marker({
       position: myLatLng,
       map: map,
     });
     marker.addListener('click', function() {
       infowindow.open(map, marker);
     });
     if (screen.width < 768) {
       var map = new google.maps.Map(document.getElementById('googleMap'), {
          zoom: 16,
          center: myLatLng,
          scrollwheel: false,
          draggable: true,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        var infowindow = new google.maps.InfoWindow({
           content: contentString
         });
        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
        });
        marker.addListener('click', function() {
          infowindow.open(map, marker);
        });
     }
}
google.maps.event.addDomListener(window, 'load', initialize);
